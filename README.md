# coyote_bi

BI Console for Coyote

# [Run Server in Dev Mode]

$ cd biapp
$ export FLASK_APP=app.js
$ export FLASK_ENV=development
$ sudo flask run

# [Run Server in Dev Mode with IP access]

$ cd biapp
$ export FLASK_APP=app.js
$ export FLASK_ENV=development
$ sudo flask run --host=0.0.0.0


# [Run Server For Analysis Console]

$ cd biapp/slicer
$ sudo slicer serve slice.ini

#[Output]

Ex: 
$ Running on http://127.0.0.1:5000/
$ Restarting with reloader...

# [Run Server in PROD Mode with IP access]

$ waitress-serve --listen=*:8080 app:server

ps -fA | grep python
