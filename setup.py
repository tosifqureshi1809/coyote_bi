import io

from setuptools import find_packages, setup

setup(
    name='yourapplication',
    packages=['yourapplication'],
    include_package_data=True,
    install_requires=[
        'flask',
        'dash',
        'dash_renderer',
        'dash_core_components',
        'dash_html_components',
        'cubes',
        'sqlalchemy'
    ],
)
