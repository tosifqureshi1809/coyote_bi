import functools
from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from werkzeug.security import check_password_hash, generate_password_hash

from biapp.db import get_db
import json

bp = Blueprint('auth', __name__, url_prefix='/auth')

@bp.route('/register', methods=('GET', 'POST'))
def register():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        db = get_db()
        error = None

        if not username:
            error = 'Username is required.'
        elif not password:
            error = 'Password is required.'
        elif db.execute(
            'SELECT id FROM bi_user WHERE username = ?', (username,)
        ).fetchone() is not None:
            error = 'User {} is already registered.'.format(username)

        if error is None:
            db.execute(
                'INSERT INTO bi_user (username, password) VALUES (?, ?)',
                (username, generate_password_hash(password))
            )
            db.commit()
            return redirect(url_for('auth.login'))

        flash(error)

    return render_template('auth/register.html')

@bp.route('/login', methods=('GET', 'POST'))
def login():
    
    if request.method == 'POST':
        user_number = request.form['user_number']
       
        password = request.form['password']
        db = get_db()
        error = None
        user = db.execute(
            "SELECT * FROM bi_user WHERE user_number = '"+user_number.encode("ascii")+"'"
        ).fetchone()

        if user is None:
            error = 'Incorrect user number.'
        #elif not check_password_hash(user['password'], password):
        elif user['password'] <> password :
            error = 'Incorrect password.'

        if error is None:
            session.clear()
            session['user_data'] = {"user_id":user['id'],"user_outlet":user['outlet'],"user_zone":user['zone']}
            return redirect(url_for('report_2020'))

        flash(error)

    return render_template('auth/login.html')

# This method used for manage user login from coyote app
@bp.route('/applogin', methods=('GET', 'POST'))
def applogin():
    user_data = {}
    result_code = 0
    if request.method == 'POST':
        user_number = request.form['user_number']
       
        password = request.form['password']
        db = get_db()
        error = None
        user = db.execute(
            "SELECT * FROM bi_user WHERE user_number = '"+user_number.encode("ascii")+"'"
        ).fetchone()

        if user is None:
            error = 'Incorrect user number.'
        elif user['password'] <> password :
            error = 'Incorrect password.'

        if error is None:
            session.clear()
            session['user_data'] = {"user_id":user['id'],"user_outlet":user['outlet'],"user_zone":user['zone']}
            user_data = session['user_data']
            result_code = 1

    return json.dumps({'user_data':user_data,'result_code':result_code})

@bp.before_app_request
def load_logged_in_user():
    user_id = session.get('user_id')

    # if user_id is None:
    #    g.user = None
    #else:
    #    g.user = get_db().execute(
    #        'SELECT * FROM bi_user WHERE id = ?', (user_id,)
    #    ).fetchone()

@bp.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('index'))

def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('auth.login'))

        return view(**kwargs)

    return wrapped_view    