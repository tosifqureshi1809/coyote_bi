import dash
import dash_core_components as dcc
import dash_html_components as htm
import plotly.plotly as py
import plotly.graph_objs as go
import numpy as np
from flask import Flask, render_template, request, g

import cubes
import os.path
from configparser import ConfigParser
from sqlalchemy import create_engine
#import pymssql
import logging

logger = cubes.get_logger()
logger.setLevel(logging.DEBUG)

app = dash.Dash()

#
# pymssql
# engine = create_engine(r'mssql+pymssql://Coyote_live:G5rVJz8QwbvhzJ4ijdGR@10.10.10.4\MSSQL2012/Coyote_live')
#
database_server_name = r'10.10.10.4\MSSQL2012'
mssql_host = r'10.10.10.4\MSSQL2012'
mssql_port = ''
mssql_user = 'Coyote_live'
mssql_pwd = 'G5rVJz8QwbvhzJ4ijdGR'
mssql_db = 'Coyote_live'
mssql_driver = 'pymssql'

connection_string = r'mssql+{0}://{1}:{2}@{3}/{4}'.format(
        mssql_driver, mssql_user, mssql_pwd, mssql_host, mssql_db)

engine = create_engine(connection_string)
connection = engine.connect()

result = connection.execute("SELECT * from user_role")
for row in result:
    print('row = %r' % (row,))

#result = connection.execute("SELECT * from commodity")
#for row in result:
#    print('row = %r' % (row,))
#connection.close()

'''
# Direct
conn = pymssql.connect(
    host=r'10.10.10.4\MSSQL2012',
    user='Coyote_live',
    password='G5rVJz8QwbvhzJ4ijdGR',
    database='Coyote_live'
)
cursor = conn.cursor()
cursor.execute("SELECT * from commodity")
for row in cursor:
    print('row = %r' % (row,))
conn.close()
'''

if __name__ == '__main__':
    app.run_server(debug=True)