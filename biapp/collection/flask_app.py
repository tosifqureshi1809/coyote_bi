# Start :: waitress-serve --listen=*:8080 flask_app:server
import os.path
import dash
import dash_renderer
import dash_core_components as dcc
import dash_html_components as html
import plotly.plotly as py
import plotly.graph_objs as go
import cubes
import json
import sqlalchemy
import numpy as np
import time
from datetime import datetime as dt

from flask import Flask, render_template, request, g, redirect, url_for, request
from werkzeug.exceptions import abort
from dash.dependencies import Input, Output
from textwrap import dedent as d
from configparser import ConfigParser
from waitress import serve

server = Flask(__name__, instance_relative_config=False)
app = dash.Dash(__name__, server=server, url_base_pathname='/dummy') 
#server.config.from_object('config')
#server.config.from_pyfile('config.py')

# --- Graph Prepration STARTS ---
N = 100
random_x = np.linspace(0, 1, N)
random_y0 = np.random.randn(N)+5
random_y1 = np.random.randn(N)
random_y2 = np.random.randn(N)-5

# Create traces
trace10 = go.Scatter(
    x = random_x,
    y = random_y0,
    mode = 'lines',
    name = 'Solid',
	line = dict(
		dash = 'solid',
		width = 4
	)
)
trace11 = go.Scatter(
    x = random_x,
    y = random_y1,
    mode = 'lines+markers',
    name = 'lines+markers'
)
trace12 = go.Scatter(
    x = random_x,
    y = random_y2,
    mode = 'markers',
    name = 'markers'
)
data1 = [trace10, trace11, trace12]

N = 2
trace0 = go.Scatter(
	x = np.random.randn(N),
	y = np.random.randn(N)+2,
	name = 'Above',
	mode = 'markers',
	marker = dict(
		size = 10,
		color = 'rgba(152, 0, 0, .8)',
		line = dict(
			width = 2,
			color = 'rgb(0, 0, 0)'
		)
	)
)

trace1 = go.Scatter(
	x = np.random.randn(N),
	y = np.random.randn(N)-2,
	name = 'Below',
	mode = 'markers',
	marker = dict(
		size = 10,
		color = 'rgba(255, 182, 193, .9)',
		line = dict(
			width = 2,
		)
	)
)

data = [trace0, trace1]

layout = dict(title = 'Styled Scatter',
			  yaxis = dict(zeroline = True),
			  xaxis = dict(zeroline = True)
			 )

fig = dict(data=data, layout=layout)

data1 = [trace10, trace11, trace12];
layout1 = dict(title = 'Line Chart');
fig1 = dict(data=data1, layout=layout1);

styles = {
    'pre': {
        'border': 'thin lightgrey solid',
        'overflowX': 'scroll'
    }
}
# --- Graph Prepration ENDS ---



# --- Template Render STARTS ---
app.layout = html.Div(children=[
    html.H1(children='Coyote Dashboard'),
    html.Div(children='''
        BI Reporting Dashboard.
    '''),
	html.Div(dcc.Input(id='input-box', type='text')),
    html.Button('Submit', id='button'),
    html.Div(id='output-container-button',
             children='Enter a value and press submit'),

	dcc.Dropdown(
		options=[
			{'label': 'New York City', 'value': 'NYC'},
			{'label': 'Montreal', 'value': 'MTL'},
			{'label': 'San Francisco', 'value': 'SF'}
		],
		multi=True,
		value="MTL"
	),

	html.Label('Radio Items'),
    dcc.RadioItems(
        options=[
            {'label': 'New York City', 'value': 'NYC'},
            {'label': u'Montreal', 'value': 'MTL'},
            {'label': 'San Francisco', 'value': 'SF'}
        ],
        value='MTL'
    ),

	html.Label('Checkboxes'),
    dcc.Checklist(
        options=[
            {'label': 'New York City', 'value': 'NYC'},
            {'label': u'Montreal', 'value': 'MTL'},
            {'label': 'San Francisco', 'value': 'SF'}
        ],
        values=['MTL', 'SF']
    ),

	html.Label('Text Input'),
    dcc.Input(value='MTL', type='text'),

	dcc.DatePickerSingle(
		id='date-picker-single',
		date=dt(1997, 5, 10)
	),

    dcc.Graph(id='graph', figure=fig),

	dcc.Graph(id='graph1', figure=fig1),
	
	html.Div([
		dcc.Markdown(d("""
			**Hover Data**

			Mouse over values in the graph.
		""")),
		html.Pre(id='hover-data', style=styles['pre'])
	], className='three columns'),

	html.Div([
		dcc.Markdown(d("""
			**Click Data**

			Click on points in the graph.
		""")),
		html.Pre(id='click-data', style=styles['pre']),
	], className='three columns'),

	html.Div([
		dcc.Markdown(d("""
			**Selection Data**

			Choose the lasso or rectangle tool in the graph's menu
			bar and then select points in the graph.
		""")),
		html.Pre(id='selected-data', style=styles['pre']),
	], className='three columns'),

	html.Div([
		dcc.Markdown(d("""
			**Zoom and Relayout Data**

			Click and drag on the graph to zoom or click on the zoom
			buttons in the graph's menu bar.
			Clicking on legend items will also fire
			this event.
		""")),
		html.Pre(id='relayout-data', style=styles['pre']),
	], className='three columns'),

    html.H3('Loading State Example'),
    dcc.Dropdown(
        id='dropdown',
        options=[{'label': i, 'value': i} for i in ['a', 'b', 'c']]
    ),
    html.Div(id='output')
])
# --- Template Render ENDS ---

# --- Register Call Back STARTS ---
@app.callback(
    dash.dependencies.Output('output-container-button', 'children'),
    [dash.dependencies.Input('button', 'n_clicks')],
    [dash.dependencies.State('input-box', 'value')])
def update_output(n_clicks, value):
    return 'The input value was "{}" and the button has been clicked {} times'.format(
        value,
        n_clicks
    )

@app.callback(
    Output('hover-data', 'children'),
    [Input('graph', 'hoverData')])
def display_hover_data(hoverData):
    return json.dumps(hoverData, indent=2)


@app.callback(
    Output('click-data', 'children'),
    [Input('graph', 'clickData')])
def display_click_data(clickData):
    return json.dumps(clickData, indent=2)


@app.callback(
    Output('selected-data', 'children'),
    [Input('graph', 'selectedData')])
def display_selected_data(selectedData):
    return json.dumps(selectedData, indent=2)


@app.callback(
    Output('relayout-data', 'children'),
    [Input('graph', 'relayoutData')])
def display_selected_data(relayoutData):
    return json.dumps(relayoutData, indent=2)

@app.callback(
    Output('output', 'children'),
    [Input('dropdown', 'value')])
def update_value(value):
    time.sleep(2)
    return 'You have computed {}'.format(value)


# --- Register Call Back ENDS ---


# --- Server Routes STARTS ---
# Route for handling the login page logic
#@server.route('/login', methods=['GET', 'POST'])
#def login():
 #   error = None
  #  if request.method == 'POST':
   #     if request.form['username'] != 'admin' or request.form['password'] != 'admin':
    #        error = 'Invalid Credentials. Please try again.'
     #   else:
      #      return redirect(url_for('dashboard'))
    #return render_template('login.html', error=error)

@server.route('/')
def index():
    return render_template('login.html')	

@server.route('/dashboard')
def dashboard():
    return render_template('home.html')	

@server.route('/hello')
def hello():
    return 'hello world'

@server.route('/select', methods=['GET', 'POST'])
def select():
    return render_template('dashboards/select.html')

@server.route('/dash_one', methods=['GET', 'POST'])
def dash_one():
	return app.index()

@server.route('/cube', methods=['GET', 'POST'])
def cube():
	return render_template('cube/cube.html')

# --- Server Routes ENDS ---

if __name__ == '__main__':
    app.run_server(debug=True, port=8080)
    serve(server, host='0.0.0.0', port=8080)    