import click
from flask import current_app, g
from flask.cli import with_appcontext
from sqlalchemy import create_engine

def get_db():
    if 'db' not in g:
        connection_string = r'mssql+{0}://{1}:{2}@{3}/{4}'.format(
        current_app.config['MSSQL_DRIVER'], current_app.config['MSSQL_USER'], current_app.config['MSSQL_PWD'], current_app.config['MSSQL_HOST'], current_app.config['MSSQL_DB'])
        engine = create_engine(connection_string)
        g.db = engine.connect()
        
    return g.db

def close_db(e=None):
    db = g.pop('db', None)

    if db is not None:
        db.close()

def init_db():
    db = get_db()

    # with current_app.open_resource('schema.sql') as f:
    #    db.executescript(f.read().decode('utf8'))


@click.command('init-db')
@with_appcontext
def init_db_command():
    """Clear the existing data and create new tables."""
    init_db()
    click.echo('Initialized the database.')

def init_app(app):
    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_command)    

def get_livedb():
    if 'live_db' not in g:
        connection_string = r'mssql+{0}://{1}:{2}@{3}/{4}'.format(
        current_app.config['MSSQL_DRIVER'], current_app.config['LIVE_MSSQL_USER'], current_app.config['LIVE_MSSQL_PWD'], current_app.config['LIVE_MSSQL_HOST'], current_app.config['LIVE_MSSQL_DB'])
        engine = create_engine(connection_string)
        g.live_db = engine.connect()
        
    return g.live_db

def get_testdb():
    if 'live_db' not in g:
        connection_string = r'mssql+{0}://{1}:{2}@{3}/{4}'.format(
        current_app.config['MSSQL_DRIVER'], current_app.config['TEST_MSSQL_USER'], current_app.config['TEST_MSSQL_PWD'], current_app.config['TEST_MSSQL_HOST'], current_app.config['TEST_MSSQL_DB'])
        engine = create_engine(connection_string)
        g.live_db = engine.connect()
        
    return g.live_db 