import os
#import dash
from flask import Flask

# create and configure the app
server = Flask(__name__, instance_relative_config=True)

server.config.from_object('config')
server.config.from_pyfile('config.py', silent=True)

# ensure the instance folder exists
try:
    os.makedirs(server.instance_path)
except OSError:
    pass


#from . import db
#from biapp.db import db
#db.init_app(server)
from biapp.auth import bp, login_required
from biapp.analysis import cb
server.register_blueprint(bp)
server.register_blueprint(cb)

# utilities we import from Werkzeug and Jinja2 that are unused
# in the module but are exported as public interface.
from werkzeug.exceptions import abort
from werkzeug.utils import redirect
from jinja2 import Markup, escape