from flask import Flask, Response, g, redirect, url_for
from flask_caching import Cache
import dash
import dash_renderer
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import js2py
import plotly
import plotly.plotly as py
import plotly.graph_objs as go
import numpy as np
import pandas as pd
import random
import webbrowser
import execjs
import grasia_dash_components as gdc
from base64 import urlsafe_b64encode
from socketclusterclient import Socketcluster
import logging

# first of all import the socket library 
#import socket
# -- Important Assets STARTS --
from biapp.db import get_db
from biapp.app import app, server
from biapp.data.scatterchart import scatterchart
# -- Important Assets ENDS --		 

cache = Cache(server, config={'CACHE_TYPE': 'simple'})
timeout = 20

# --- Graph Prepration STARTS ---
app = dash.Dash(__name__,static_folder='static', sharing=True,server=server, url_base_pathname='/20_20_Report')
app.config.suppress_callback_exceptions = True
app.scripts.config.serve_locally=True
app.css.config.serve_locally=True

server = Flask(__name__, instance_relative_config=True)
# --- Initialize redis server ---
import redis
redis_conn = redis.Redis('127.0.0.1')

def generate_layout_20_20_Report():

    res = scatterchart()
    res = res.pg1_scatterchart_20_20_Report()
    max_x = 0
    max_y = 0
    res_x = []
    res_y = []
    res_text = []
    res_customdata = []
    res_size = []
    if res[0] != None and len(res[0]) > 0:
        max_x =  int(round(np.amax(res[0])))
        res_x = res[0]
    if res[1] != None and len(res[1]) > 0:
        max_y = int(round(np.amax(res[1])))
        res_y = res[1]
    if res[2] != None and len(res[2]) > 0:
        res_text = res[2]
    if res[3] != None and len(res[3]) > 0:
        res_customdata = res[3]
    if res[4] != None and len(res[4]) > 0:
        res_size = res[4]
    #set x and y axis range interval
    x_interval = 10
    y_interval = 10 
    if max_x > 200 :
        x_interval = np.divide(max_x,10)  
    if max_y > 300 :
        y_interval = np.divide(max_y,10)    
    # Get analysis level data; If exist
    #analysis_index_label = '20-20 Report'
    analysis_index_label = ''
    drilldown_analysis_data = redis_conn.hgetall("drilldown_analysis_data")
    if drilldown_analysis_data.get("label") != None and drilldown_analysis_data.get("label") != '':
        analysis_index_label = drilldown_analysis_data.get("label")          
    # Create traces
    trace10 = go.Scatter(
        x = res_x[1:20],
        y = res_y[1:20],
        text = res_text,
        textposition='center center',
        customdata = res_customdata,
        mode = 'markers+text',
        hoverinfo='none',
        textfont={
                "color": "#000000",
                "size": 8
            },
        name = '20-20 Report :: Commodity',
        marker = dict(
            size = res_size[1:20],
            #size = 20,
            #color = ["#"+''.join([random.choice('0123456789ABCDEF') for j in range(6)])
            # for i in range(20)],
            color = list(np.random.choice(range(256), size=20)),
            line = dict(
                width = 2,
                color = 'rgb(0, 0, 0)'
            )
        )
    )
    
    data = [trace10]
    layout = go.Layout(
        title =  analysis_index_label,
        xaxis=dict(
            range = [-max_x,max_x],
            autorange=False,
            showgrid=True,
            zeroline=True,
            showline=False,
            showticklabels=True,
            autotick=False,
            ticks='',
            tick0=0,
            #dtick=np.divide(max_x,10),
            dtick=x_interval,
            ticklen=10,
            tickwidth=8,
            tickcolor='#000'
        ),
        yaxis=dict(
            range = [-max_y,max_y],
            autorange=False,
            showgrid=True,
            zeroline=True,
            showline=False,
            showticklabels=True,
            autotick=False,
            ticks='',
            tick0=0,
            dtick=y_interval,
            #dtick=np.power(2,2),
            #dtick=np.multiply.accumulate( np.ones( 10000 )*2).astype(int),
            ticklen=10,
            tickwidth=8,
            tickcolor='#000'
        ),
        margin= {'l': 20, 'b': 40, 't': 30, 'r': 5}

    )
    fig = go.Figure(data=data, layout=layout)

    return html.Div(children=[
    
        html.Link(href='/static/style.css', rel='stylesheet'),
        #html.Button(
        #    'Click Me',
        #    id='exec_js',
        #),
        #html.Div(
        #    id='msg_for_parent',
        #    title='initial value'
            
        #),
        #gdc.Import(src="http://127.0.0.1:5000/static/js/test.js"),
        #html.Script(src='http://127.0.0.1:5000/static/js/test.js'),
        #dcc.Dropdown(
          #  id='dropdown',
          #  options=[{'label': i, 'value': i} for i in ['a', 'b', 'c']]
        #),
        #dcc.Input(id='msg_for_parent', value='initial value', type='text'),
        dcc.Graph(
            style = {'height': '100vh'},
            id = '20_20_Report',
            figure = fig
        )
    ])

def update_layout_20_20_Report(clickData):
    
    # --- TODO :: Put case based modle call --
    
    res = scatterchart()
    res = res.pg1_scatterchart_20_20_Report()
    # --- Return Updated Figure Object ---
    return {
        'data': [go.Scatter(
            x = res[0],
            y = res[1],
            text = res[2],
            customdata = res[3],
            mode = 'markers',
            name = '20-20 Report :: Comodity',
            marker = dict(
                size = 20,
                color = 'rgba(152, 0, 0, .8)',
                line = dict(
                        width = 2,
                        color = 'rgb(0, 0, 0)'
                )
            )
        )],
        'layout': go.Layout(
            title =  clickData['points'][0]['text'].encode("ascii") if clickData['points'][0]['text'].encode("ascii") !="" else " 20-20 Report",
            xaxis={
                'title': 'Differce',
                'type': 'linear',
                'autorange' : True
            },
            yaxis={
                'title': 'Time',
                'type': 'linear',
                'autorange' : True
            },
            margin={'l': 20, 'b': 40, 't': 30, 'r': 5},
            height=900,
            hovermode='closest'
        )
    }


# --- Graph Prepration ENDS ---
app.layout = generate_layout_20_20_Report

# --------------------------------------------------- 
# [Input('20_20_Report', 'clickData')]) 
#        ID of Graph,   Event:: clickData  
#-------------------------------------------
# Output('20_20_Report', 'figure'),
#        ID of Graph,  Update:: figure Object   
# --------------------------------------------------

@app.callback(
    Output('20_20_Report', 'figure'),
    [Input('20_20_Report', 'clickData')])
def display_click_data(clickData):
   
    # Get drilldown level filter data; If exist
    analysis_level = '1'
    department_id  = ''
    drilldown_analysis_data = redis_conn.hgetall("drilldown_analysis_data")
    if drilldown_analysis_data.get("level") != None and drilldown_analysis_data.get("level") != '':
        analysis_level = drilldown_analysis_data.get("level")
        analysis_index_id = drilldown_analysis_data.get("id")     
    
    if analysis_level != '3' :
        if analysis_level == '1' :
            analysis_level = '2'
        elif analysis_level == '2' :
            department_id = analysis_index_id
            analysis_level = '3'
        print('==222analysis_level==')
        print(analysis_level)
        return_value = 0
        if analysis_level != '1' :
            return_value = 1
        
        # Set analysis level data in redis json obj
        # redis_conn.hmset("drilldown_analysis_data", {"level":analysis_level,"id":clickData['points'][0]['customdata'],"department_id":department_id,"label":clickData['points'][0]['text'].encode("ascii")})
        
        # -- Socket.io --
        socket = Socketcluster.socket("ws://localhost:8000/socketcluster/")
        socket.connect()
        socket.onchannel('sample', channelmessage)
        socket.emitack("chat", "Hi", ack)

    return_value = ''
    if clickData is not None:
        return_value = '1'     
    return return_value

def ack(eventname, error, object):
    print "Got ack data " + object + " and error " + error + " and eventname is " + eventname       
   
def suback(channel, error, object):
    if error is '':
        logging.info("Subscribed successfully to channel " + channel)

def channelmessage(key, object):
    print "Got data " + object + " from key " + key
# -- Socket.io --

#@app.callback(
#    Output('20_20_Report', 'figure'),
#    [Input('20_20_Report', 'clickData')])

def display_click_data_(clickData):
    #driver = webdriver.Firefox()
    #driver.Navigate().GoToUrl("http://example.com");
    #driver.get('http://127.0.0.1:5000/report_2020')
    #while True:    
    #    driver.navigate().to(driver.getCurrentUrl());
    #driver.quit()
    # Create a socket object 
    #s = socket.socket() 

    # Define the port on which you want to connect 
    #port = 8000                
    
    # connect to the server on local computer 
    #s.connect(('192.168.0.100', port)) 

    # receive data from the server 
    #print s.recv(1024) 
    # close the connection 
    #s.close()  
    #if clickData['points'][0]['customdata'] != None :

        # Get drilldown level filter data; If exist
        #analysis_level = '1'
        #department_id  = ''
        #drilldown_analysis_data = redis_conn.hgetall("drilldown_analysis_data")
        #if drilldown_analysis_data.get("level") != None and drilldown_analysis_data.get("level") != '':
        #    analysis_level = drilldown_analysis_data.get("level")
        #    analysis_index_id = drilldown_analysis_data.get("id")     
        
        #if analysis_level != '3' :
        #    if analysis_level == '1' :
        #        analysis_level = '2'
        #    elif analysis_level == '2' :
        #        department_id = analysis_index_id
        #        analysis_level = '3'
        #    print('==222analysis_level==')
        #    print(analysis_level)    
            # Set analysis level data in redis json obj
        #    redis_conn.hmset("drilldown_analysis_data", {"level":analysis_level,"id":clickData['points'][0]['customdata'],"department_id":department_id,"label":clickData['points'][0]['text'].encode("ascii")})
        #    a_website = "http://127.0.0.1:5000/report_2020"
            #a_website = "http://192.168.0.100:5000/report_2020"
        #    webbrowser.open_new(a_website)
    return update_layout_20_20_Report(clickData)

#@app.callback(
 #   Output('dropdown', 'value'),
  #  [Input('dropdown', 'value')])
#def update_value(value):
 #   g.dropdown = format(value)
  #  return format(value)


