import dash
from flask_caching import Cache
import dash_renderer
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly
import plotly.plotly as py
import plotly.graph_objs as go
import numpy as np
import pandas as pd

# -- Important Assets STARTS --
from biapp.db import get_db
from biapp.app import app, server
from biapp.data.charts import charts	             

cache = Cache(server, config={'CACHE_TYPE': 'simple'})
timeout = 20
# -- Important Assets ENDS --

# --- Graph Prepration STARTS ---
def generate_layout_TYS_vs_LYS():
    # res = pg_scatterchart_TYS_vs_LYS()
    res = charts()
    res = res.pg_scatterchart_TYS_vs_LYS()

    # Create traces
    trace1 = go.Scatter(
        x = res[0],
        y = res[1],
        mode = 'lines+markers',
        name = 'This Year Sales',
    )

    trace2 = go.Scatter(
        x = res[0],
        y = res[2],
        mode = 'lines+markers',
        name = 'Last Year Sales',
    )

    trace3 = go.Scatter(
        x = res[0],
        y = res[3],
        mode = 'lines+markers',
        name = 'This Year Comparison Sales',
    )
    
    data = [trace1, trace2, trace3]
    layout = go.Layout(
        title = 'TYS vs LYS',
        xaxis=dict(
            range = res[0],
            autorange=True
        )
    )
    fig = go.Figure(data=data, layout=layout)


    return html.Div(children=[
    
        html.Link(href='/static/style.css', rel='stylesheet'),

        dcc.Graph(style={'height': '100vh'}, id='TYS_vs_LYS', figure=fig)
        
    ])

def generate_layout_TYGP_vs_LYGP():
    # res = pg_scatterchart_TYGP_vs_LYGP()
    res = charts()
    res = res.pg_scatterchart_TYGP_vs_LYGP()

    # Create traces
    trace1 = go.Scatter(
        x = res[0],
        y = res[1],
        mode = 'lines+markers',
        name = 'This Year GP%',
    )

    trace2 = go.Scatter(
        x = res[0],
        y = res[2],
        mode = 'lines+markers',
        name = 'Last Year GP%',
    )

    trace3 = go.Scatter(
        x = res[0],
        y = res[3],
        mode = 'lines+markers',
        name = 'This Year Comparison GP%',
    )
    
    data = [trace1, trace2, trace3]
    layout = go.Layout(
        title = 'TYGP vs LYGP',
        xaxis=dict(
            range = res[0],
            autorange=True
        )
    )
    fig = go.Figure(data=data, layout=layout)

    #expensive_data = pg_scatterchart_TYGP_vs_LYGP()
    return html.Div(children=[
    
        html.Link(href='/static/style.css', rel='stylesheet'),

        dcc.Graph(style={'height': '100vh'}, id='TYGP_vs_LYGP', figure=fig)
        
    ])

def generate_layout_THAVGB_vs_LYAVGB():
    # res = pg_scatterchart_THAVGB_vs_LYAVGB()
    res = charts()
    res = res.pg_scatterchart_THAVGB_vs_LYAVGB()

    # Create traces
    trace1 = go.Scatter(
        x = res[0],
        y = res[1],
        mode = 'lines+markers',
        name = 'This Year AvgBasket',
    )

    trace2 = go.Scatter(
        x = res[0],
        y = res[2],
        mode = 'lines+markers',
        name = 'Last Year AvgBasket',
    )

    trace3 = go.Scatter(
        x = res[0],
        y = res[3],
        mode = 'lines+markers',
        name = 'This Year Comparison AvgBasket',
    )
    
    data = [trace1, trace2, trace3]
    layout = go.Layout(
        title = 'TYAVGB vs LYAVGB',
        xaxis=dict(
            range = res[0],
            autorange=True
        )
    )
    fig = go.Figure(data=data, layout=layout)

    return html.Div(children=[
    
        html.Link(href='/static/style.css', rel='stylesheet'),

        dcc.Graph(style={'height': '100vh'}, id='TYAVGB_vs_LYAVGB', figure=fig)

    ])

def generate_layout_TYC_vs_LYC():
    # res = pg_scatterchart_TYC_vs_LYC()
    res = charts()
    res = res.pg_scatterchart_TYC_vs_LYC()

    # Create traces
    trace1 = go.Scatter(
        x = res[0],
        y = res[1],
        mode = 'lines+markers',
        name = 'This Year Customers',
    )

    trace2 = go.Scatter(
        x = res[0],
        y = res[2],
        mode = 'lines+markers',
        name = 'Last Year Customers',
    )

    trace3 = go.Scatter(
        x = res[0],
        y = res[3],
        mode = 'lines+markers',
        name = 'This Year Comparison Customers',
    )
    
    data = [trace1, trace2, trace3]
    layout = go.Layout(
        title = 'TYC vs LYC',
        xaxis=dict(
            range = res[0],
            autorange=True
        )
    )
    fig = go.Figure(data=data, layout=layout)

    return html.Div(children=[
    
        html.Link(href='/static/style.css', rel='stylesheet'),

        dcc.Graph(style={'height': '100vh'}, id='TYC_vs_LYC', figure=fig)
        
    ])
# --- Graph Prepration ENDS ---


# --- Routh 1 STARTS :: '/TYS_vs_LYS' for 1 Graph  ---
#app = dash.Dash(__name__,static_folder='static', sharing=True, server=server, url_base_pathname='/TYS_vs_LYS')
#app.config.suppress_callback_exceptions = True
#app.scripts.config.serve_locally=True
#app.css.config.serve_locally=True
# --- Layout for Graph 1 Prepration  ---
# app.layout = generate_layout_TYS_vs_LYS
# --- Routh 1 STARTS :: '/TYS_vs_LYS' for 1 Graph  ---


# --- Routh 2 STARTS :: '/TYGP_vs_LYGP' for 2 Graph  ---
#app = dash.Dash(__name__,static_folder='static', sharing=True, server=server, url_base_pathname='/TYGP_vs_LYGP')
#app.config.suppress_callback_exceptions = True
#app.scripts.config.serve_locally=True

# --- Layout for Graph 2 Prepration ---
#app.layout = generate_layout_TYGP_vs_LYGP
# --- Routh 2 ENDS :: '/TYGP_vs_LYGP' for 2 Graph  ---

# --- Routh 3 STARTS :: '/THAVGB_vs_LYAVGB' for 3 Graph  ---
#app = dash.Dash(__name__,static_folder='static', sharing=True, server=server, url_base_pathname='/THAVGB_vs_LYAVGB')
#app.config.suppress_callback_exceptions = True
#app.scripts.config.serve_locally=True

# --- Layout for Graph 3 Prepration ---
#app.layout = generate_layout_THAVGB_vs_LYAVGB
# --- Routh 3 ENDS :: '/THAVGB_vs_LYAVGB' for 3 Graph  ---

# --- Routh 4 STARTS :: '/TYC_vs_LYC' for 4 Graph  ---
#app = dash.Dash(__name__,static_folder='static', sharing=True, server=server, url_base_pathname='/TYC_vs_LYC')
#app.config.suppress_callback_exceptions = True
#app.scripts.config.serve_locally=True
# --- Layout for Graph 4 Prepration ---
#app.layout = generate_layout_TYC_vs_LYC
# --- Routh 4 ENDS :: '/TYC_vs_LYC' for 4 Graph  ---