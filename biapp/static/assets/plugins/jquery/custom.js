/* ===================================================
 * Custom JS for new added functions in site
 * =================================================== */
 
$(document).ready(function(){
	// Manage secondary filter availability
    $('#environment').on('change', function (e) {
		var value = this.value;
		$(".loader").fadeIn("slow");
		  $.ajax({
			type: 'GET',
			url: 'change_environment',
			data : {
			  env_value:value
			},
			contentType: "application/json",
			success:function(data){
			  window.location.href = window.location.href;
			},
			error: function(data) { // if error occured
			  alert("Error occured.please try again");
			  $(".loader").fadeOut("slow");
			}
		  });
    });
});
