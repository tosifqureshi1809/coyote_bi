from flask import Flask, Response, g, request,session
from flask_caching import Cache
from flask_sqlalchemy import SQLAlchemy
import sqlalchemy
import pandas as pd
import numpy as np
# -- Important Assets STARTS --
from biapp.db import get_db
from biapp.app import server
from datetime import datetime, timedelta
# -- Important Assets ENDS --
cache = Cache(server, config={'CACHE_TYPE': 'simple'})
timeout = 20
# --- Initialize redis server ---
#import redis
#redis_conn = redis.Redis('127.0.0.1')

class charts():
    title = ""
    db = []
    TYS_result = []
    LYS_result = []
    TYCS_result = []
    g_stores_count = 0
    
    @cache.memoize(timeout=timeout)
    def __init__(self):
        with server.app_context():        
            if 'db' not in g:
                self.db = get_db()
                g.db = self.db
    
    @cache.memoize(timeout=timeout)
    def pg_scatterchart_TYS_vs_LYS(self):
        # -- For Graph X-Axis, Y-Axis, Custome Data for current Years. --
        temp_Sale_TYS_VS_LYS_X = []
        temp_Sale_TYS_VS_LYS_X2 = []
        temp_Sale_TYS_VS_LYS_Y = []
        
        # -- For Graph X-Axis, Y-Axis, Custome Data for current Years. --
        graph_x1 = []
        graph_x2 = []
        graph_y1 = []
        graph_y2 = []
        graph_y3 = []
        graph_text = []
        graph_customdata = []
        TY_weeks_count = 0
        LY_weeks_count = 0
        TY_week_start_date = ""
        LY_last_weekend_date = ""
        LY_first_weekend = ""
        LY_second_week_start_date = ""
        # Get drilldown level filter data; If exist
        analysis_level = '1'
        analysis_index_id = ''
        analysis_department_id = ''
        if request.args.get('level') is not None:
            analysis_level = request.args.get('level')
        if request.args.get('id') is not None:    
            analysis_index_id = request.args.get('id')
        if request.args.get('department_id') is not None:    
            analysis_department_id = request.args.get('department_id')  
            
        # Get filter data; If exist
        #filter_data = redis_conn.hgetall("filter_data")
        # Get session filter data
        if 'filter_data' in session:
            filter_data = session['filter_data']
        # Set filter params
        p_outlet_id = ''
        p_zone_id = ''
        s_outlet_id = ''
        s_zone_id = ''
        start_date = ''
        end_date = ''
        comparison_period = 1
        if filter_data.get("p_outlet_id") != None and filter_data.get("p_outlet_id") != '':
            p_outlet_id = filter_data.get("p_outlet_id")
        
        if filter_data.get("p_zone_id") != None and filter_data.get("p_zone_id") != '':
            p_zone_id = filter_data.get("p_zone_id")
       
        if filter_data.get("s_outlet_id") != None and filter_data.get("s_outlet_id") != '':
            s_outlet_id = filter_data.get("s_outlet_id")
        
        if filter_data.get("s_zone_id") != None and filter_data.get("s_zone_id") != '':
            s_zone_id = filter_data.get("s_zone_id")
        
        if filter_data.get("start_date") != None and filter_data.get("start_date") != '' and filter_data.get("end_date") != None and filter_data.get("end_date") != '':
            start_date = filter_data.get("start_date")
            end_date = filter_data.get("end_date")

        if filter_data.get("comparison_period") != None and filter_data.get("comparison_period") != '':
            comparison_period = int(filter_data.get("comparison_period"))
        
        # return if date range empty
        if start_date == '' or end_date == '' :
            res = [graph_x1, graph_y1, graph_y2, graph_y3, graph_x2]
            return res

        # -- Set dates
        convt_startdate = datetime.strptime(start_date, '%Y-%m-%d').date()
        current_year = int(convt_startdate.strftime('%Y'))
        current_month = convt_startdate.strftime('%m')
        current_day = convt_startdate.strftime('%d')

        convt_enddate = datetime.strptime(end_date, '%Y-%m-%d').date()
        current_endmonth = convt_enddate.strftime('%m')
        current_endday = convt_enddate.strftime('%d')
        endday = convt_enddate.strftime('%a')
        
        prev_year = current_year-comparison_period
        year_days = self.is_leap_year(prev_year)-1
        
        convt_prev_startdate = datetime.strptime(str(prev_year)+'-'+current_month+'-'+current_day, '%Y-%m-%d').date()
        convt_prev_enddate = datetime.strptime(str(prev_year)+'-'+current_endmonth+'-'+current_endday, '%Y-%m-%d').date()
        
        convt_startdate.strftime('%Y-%m-%d')
        
        #LY_start_date = convt_startdate - timedelta(days=year_days)
        LY_start_date = convt_prev_startdate.strftime('%Y-%m-%d')
        
        convt_enddate = datetime.strptime(end_date, '%Y-%m-%d').date()
        convt_enddate.strftime('%Y-%m-%d')
        LY_endday = convt_enddate.strftime('%a')

        #LY_end_date = convt_enddate - timedelta(days=year_days)
        LY_end_date = convt_prev_enddate.strftime('%Y-%m-%d')
    
        # -- For this year sales. --
        if(p_outlet_id != '' or p_zone_id != '') :
            # Set table, colomn name based on analysis level
            tbl_alais = 'btoa'
            tbl_prefix = 'btoa.btoa'
            tbl_name = 'bi_trx_outlet_aggregates'
            tbl_customer_col = 'btoa.btoa_customers'
            if analysis_level == '2' :
                tbl_alais = 'btda'
                tbl_prefix = 'btda.btda'
                tbl_name = 'bi_trx_department_aggregates'
                tbl_customer_col = 'btda.btda_customers_count'
            elif analysis_level == '3' :
                tbl_alais = 'btca'
                tbl_prefix = 'btca.btca'
                tbl_name = 'bi_trx_commodity_aggregates'
                tbl_customer_col = 'btca.btca_customers_count'
            # Prepare sql for fetching this year sales records
            df_sale_TYS_VS_LYS_query = "SELECT SUM("+tbl_prefix+"_amt) as total_amount,SUM("+tbl_prefix+"_cost) as total_cost, SUM("+tbl_customer_col+") as total_customers,SUM("+tbl_prefix+"_gp_percent) as gp_percent, DATEADD(DAY, 8 - DATEPART(WEEKDAY, btt.trxt_date), CAST(btt.trxt_date AS DATE)) as  week_end_date,trxt_year as year"
            if p_zone_id != '' :
                df_sale_TYS_VS_LYS_query += ",(SELECT count(zout_id) FROM zone_outlet zo WHERE zout_zone_id = '"+p_zone_id+"') as total_stores"
            df_sale_TYS_VS_LYS_query += " FROM "+tbl_name+" "+tbl_alais+" JOIN bi_trx_time btt ON btt.trxt_id = "+tbl_prefix+"_time_id WHERE "  
            if p_outlet_id != '':
                df_sale_TYS_VS_LYS_query += ""+tbl_prefix+"_outlet_id  = '"+p_outlet_id+"' AND "
            elif p_zone_id != '' :
                df_sale_TYS_VS_LYS_query += ""+tbl_prefix+"_outlet_id  IN ( select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+p_zone_id+"' )  AND "
            if analysis_level == '2' :
                df_sale_TYS_VS_LYS_query += " "+tbl_prefix+"_department_id  = '"+analysis_index_id+"' AND "
            elif analysis_level == '3' :
                df_sale_TYS_VS_LYS_query += " "+tbl_prefix+"_department_id  = '"+analysis_department_id+"' AND "+tbl_prefix+"_commodity_id  = '"+analysis_index_id+"' AND "
            df_sale_TYS_VS_LYS_query += " ( btt.trxt_date BETWEEN '"+start_date+"' AND '"+end_date+"' ) GROUP BY DATEADD(DAY, 8 - DATEPART(WEEKDAY, btt.trxt_date), CAST(btt.trxt_date AS DATE)),btt.trxt_year  "
            if endday == 'Sun' :
                df_sale_TYS_VS_LYS_query += " having DATEADD(DAY, 8 - DATEPART(WEEKDAY, btt.trxt_date), CAST(btt.trxt_date AS DATE)) <= '"+end_date+"' "
            df_sale_TYS_VS_LYS_query += " ORDER BY week_end_date"    
            print(df_sale_TYS_VS_LYS_query)
            df_sale_TYS_VS_LYS = pd.read_sql_query(df_sale_TYS_VS_LYS_query,con=self.db)
            # Get => Y-Axis!!
            df_sale_TYS_VS_LYS=df_sale_TYS_VS_LYS.groupby(['year'])
            # Set this year sale result globally
            global TYS_result
            TYS_result = df_sale_TYS_VS_LYS
            for k, v in df_sale_TYS_VS_LYS:
                temp_group_split=v.to_dict(orient="records")
                week_number = 1
                TY_weeks_count = len(temp_group_split)
                for v in temp_group_split:
                    TY_week_date = v['week_end_date']
                    if(week_number == 1):
                        #datetime_object = datetime.strptime(v['week_end_date'], '%Y-%m-%d')
                        # -- Set dates
                        convt_date = datetime.strptime(TY_week_date, '%Y-%m-%d').date()
                        #prev_date = convt_date - timedelta(days=1)
                        current_year = int(convt_date.strftime('%Y'))
                        prev_year = current_year-comparison_period
                        year_days = self.is_leap_year(prev_year)
                        #convt_date.strftime('%Y-%m-%d')
                        #LY_first_weekend = convt_date - timedelta(days=year_days)
                        #LY_first_weekend = LY_first_weekend.strftime('%Y-%m-%d')
                        
                        LY_first_weekend = datetime.strptime(str(prev_year)+'-'+convt_date.strftime('%m')+'-'+convt_date.strftime('%d'), '%Y-%m-%d').date()

                        #LY_second_week_start_date = datetime.strptime(LY_first_weekend, '%Y-%m-%d').date()
                        LY_second_week_start_date = LY_first_weekend + timedelta(days=1)
                        #LY_second_week_start_date = LY_second_week_start_date.strftime('%Y-%m-%d')

                        LY_second_week_start_date = datetime.strptime(str(prev_year)+'-'+LY_second_week_start_date.strftime('%m')+'-'+LY_second_week_start_date.strftime('%d'), '%Y-%m-%d').date()

                        LY_first_weekend = LY_first_weekend.strftime('%Y-%m-%d')
                        LY_second_week_start_date = LY_second_week_start_date.strftime('%Y-%m-%d')
                        #prev_date = convt_date - timedelta(days=1)
                    elif(TY_weeks_count == week_number):
                        
                        if(end_date != TY_week_date):
                            TY_week_date = end_date

                        convt_date = datetime.strptime(TY_week_date, '%Y-%m-%d').date()
                        current_year = int(convt_date.strftime('%Y'))
                        prev_year = current_year-comparison_period
                        year_days = self.is_leap_year(prev_year)
                        #LY_last_weekend_date = convt_date - timedelta(days=year_days)
                        #LY_last_weekend_date = LY_last_weekend_date.strftime('%Y-%m-%d')

                        LY_last_weekend_date = datetime.strptime(str(prev_year)+'-'+convt_date.strftime('%m')+'-'+convt_date.strftime('%d'), '%Y-%m-%d').date()
                        LY_last_weekend_date = LY_last_weekend_date.strftime('%Y-%m-%d')

                    # Set total stores count var 
                    total_stores = 1  
                    if p_zone_id != '' :
                        total_stores = v['total_stores']
                
                    temp_Sale_TYS_VS_LYS_X.append({ 'total_amount': (np.divide(v['total_amount'],total_stores)),"total_cost":(np.divide(v['total_cost'],total_stores)), "total_customers":(np.divide(v['total_customers'],total_stores)),"avg_basket":(np.divide(np.divide(v['total_amount'],v['total_customers']),total_stores)), "gp_percent":(np.divide(v['gp_percent'],total_stores)), "week_end_date":TY_week_date, "year":v['year']})        
                    # -- X for 2018 --
                    graph_x1.append(TY_week_date.encode("ascii"))                 
                    graph_y1.append((v['total_amount']/total_stores))
                    week_number = week_number+1

            # -- For last year sales. --
            df_sale_TYS_VS_LYS_query = "SELECT SUM("+tbl_prefix+"_amt) as total_amount,SUM("+tbl_prefix+"_cost) as total_cost,SUM("+tbl_customer_col+") as total_customers,SUM("+tbl_prefix+"_gp_percent) as gp_percent,trxt_year as year  "
            if p_zone_id != '' :
                df_sale_TYS_VS_LYS_query += ",(SELECT count(zout_id) FROM zone_outlet zo WHERE zout_zone_id = '"+p_zone_id+"') as total_stores"
            df_sale_TYS_VS_LYS_query += " FROM "+tbl_name+" "+tbl_alais+" JOIN bi_trx_time btt ON btt.trxt_id = "+tbl_prefix+"_time_id WHERE " 
            if p_outlet_id != '':
                df_sale_TYS_VS_LYS_query += ""+tbl_prefix+"_outlet_id  = '"+p_outlet_id+"' AND "
            elif p_zone_id != '' :
                df_sale_TYS_VS_LYS_query += ""+tbl_prefix+"_outlet_id  IN ( select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+p_zone_id+"' )  AND "
            if analysis_level == '2' :
                df_sale_TYS_VS_LYS_query += " "+tbl_prefix+"_department_id  = '"+analysis_index_id+"' AND "
            elif analysis_level == '3' :
                df_sale_TYS_VS_LYS_query += " "+tbl_prefix+"_department_id  = '"+analysis_department_id+"' AND "+tbl_prefix+"_commodity_id  = '"+analysis_index_id+"' AND "    
            df_sale_TYS_VS_LYS_query += " (btt.trxt_date BETWEEN '"+LY_start_date+"' AND '"+LY_first_weekend+"' ) GROUP BY btt.trxt_year"
            
            df_sale_TYS_VS_LYS = pd.read_sql_query(df_sale_TYS_VS_LYS_query,con=self.db)
            df_sale_TYS_VS_LYS=df_sale_TYS_VS_LYS.groupby(['year'])
            for k, v in df_sale_TYS_VS_LYS:
                temp_group_split=v.to_dict(orient="records")
                for v in temp_group_split:
                    LY_weeks_count = 1
                    year = v['year']
                    # Set total stores count var 
                    total_stores = 1  
                    if p_zone_id != '' :
                        total_stores = v['total_stores']
                    temp_Sale_TYS_VS_LYS_X2.append({ 'total_amount': (v['total_amount']/total_stores),"total_cost":(v['total_cost']/total_stores), "total_customers":(v['total_customers']/total_stores),"avg_basket":((v['total_amount']/v['total_customers'])/total_stores), "gp_percent":(v['gp_percent']/total_stores), "week_end_date":'"+LY_first_weekend+"', "year":year})
                    graph_y2.append((v['total_amount']/total_stores))
                    graph_x2.append(LY_first_weekend.encode("ascii")) 
           
            if p_outlet_id != '':
                df_sale_TYS_VS_LYS_query = "SELECT "+tbl_prefix+"_amt as total_amount,"+tbl_prefix+"_cost as total_cost, "+tbl_customer_col+" as total_customers,"+tbl_prefix+"_gp_percent as gp_percent, CAST(btt.trxt_date AS DATE) as ty_date,trxt_year as year FROM "+tbl_name+" "+tbl_alais+" JOIN bi_trx_time btt ON btt.trxt_id = "+tbl_prefix+"_time_id WHERE "+tbl_prefix+"_outlet_id  = '"+p_outlet_id+"' AND "
                if analysis_level == '2' :
                    df_sale_TYS_VS_LYS_query += " "+tbl_prefix+"_department_id  = '"+analysis_index_id+"' AND "
                elif analysis_level == '3' :
                    df_sale_TYS_VS_LYS_query += " "+tbl_prefix+"_department_id  = '"+analysis_department_id+"' AND "+tbl_prefix+"_commodity_id  = '"+analysis_index_id+"' AND "
                df_sale_TYS_VS_LYS_query += " ( btt.trxt_date BETWEEN '"+LY_second_week_start_date+"' AND '"+LY_end_date+"' )  ORDER BY btt.trxt_date ASC"    
            elif p_zone_id != '' :
                df_sale_TYS_VS_LYS_query = "SELECT SUM("+tbl_prefix+"_amt) as total_amount,SUM("+tbl_prefix+"_cost) as total_cost, SUM("+tbl_customer_col+") as total_customers,SUM("+tbl_prefix+"_gp_percent) as gp_percent, CAST(btt.trxt_date AS DATE) as ty_date,trxt_year as year,(SELECT count(zout_id) FROM zone_outlet zo WHERE zout_zone_id = '"+p_zone_id+"') as total_stores FROM "+tbl_name+" "+tbl_alais+" JOIN bi_trx_time btt ON btt.trxt_id = "+tbl_prefix+"_time_id WHERE "+tbl_prefix+"_outlet_id  IN ( select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+p_zone_id+"' )  AND "
                if analysis_level == '2' :
                    df_sale_TYS_VS_LYS_query += " "+tbl_prefix+"_department_id  = '"+analysis_index_id+"' AND "
                elif analysis_level == '3' :
                    df_sale_TYS_VS_LYS_query += " "+tbl_prefix+"_department_id  = '"+analysis_department_id+"' AND "+tbl_prefix+"_commodity_id  = '"+analysis_index_id+"' AND "
                df_sale_TYS_VS_LYS_query += " ( btt.trxt_date BETWEEN '"+LY_second_week_start_date+"' AND '"+LY_end_date+"' ) GROUP BY CAST(btt.trxt_date AS DATE),trxt_year ORDER BY ty_date ASC"     
            
            df_sale_TYS_VS_LYS = pd.read_sql_query(df_sale_TYS_VS_LYS_query,con=self.db)
            # Get => X-Axis!!
            df_sale_TYS_VS_LYS=df_sale_TYS_VS_LYS.groupby(['year'])
            
            for k, v in df_sale_TYS_VS_LYS:
                temp_group_split=v.to_dict(orient="records")
                weekday = 1
                last_weekend_date = ''
                for v in temp_group_split:
                    if weekday == 1 :
                        total_amount = 0
                        total_cost = 0
                        avg_basket = 0
                        total_customers = 0
                        gp_percent = 0
                        year = ''
                    # Add total sales params    
                    if p_zone_id != '' :  
                        total_amount = total_amount+(np.divide(v['total_amount'],v['total_stores']))
                        total_cost   = total_cost+(np.divide(v['total_cost'],v['total_stores']))
                        avg_basket   = avg_basket+((np.divide(v['total_amount'],v['total_customers']))/total_stores)
                        total_customers = total_customers+(np.divide(v['total_customers'],v['total_stores']))
                        gp_percent   = gp_percent+(np.divide(v['gp_percent'],v['total_stores']))
                    else:
                        total_amount = total_amount+v['total_amount']
                        total_cost   = total_cost+v['total_cost']
                        avg_basket   = avg_basket+(np.divide(v['total_amount'],v['total_customers']))
                        total_customers = total_customers+v['total_customers']
                        gp_percent   = gp_percent+v['gp_percent']
                    year = v['year']

                    if weekday == 7:
                        # Set weekend date
                        weekend_date = datetime.strptime(v['ty_date'], '%Y-%m-%d').date()
                        #weekend_date = weekend_date + timedelta(days=1)
                        weekend_date = weekend_date.strftime('%Y-%m-%d')
                        LY_weeks_count = LY_weeks_count+1
                        last_weekend_date = weekend_date
                        #weekend_date = v['ty_date'].encode("ascii")
                        temp_Sale_TYS_VS_LYS_X2.append({ 'total_amount': total_amount,"total_cost":total_cost,"avg_basket":avg_basket,  "total_customers":total_customers, "gp_percent":gp_percent, "week_end_date":weekend_date, "year":year})
                        graph_y2.append(total_amount)
                        graph_x2.append(weekend_date.encode("ascii"))
                        weekday = 1
                        
                    else:
                        weekday = weekday+1
                        LY_lastweek_json = { 'total_amount': total_amount,"total_cost":total_cost,"avg_basket":avg_basket, "total_customers":total_customers, "gp_percent":gp_percent, "week_end_date":LY_last_weekend_date, "year":year}
                        LY_lastweek_amount = total_amount

                if((TY_weeks_count-1) == LY_weeks_count) :
                    LY_weeks_count = LY_weeks_count+1

                if((TY_weeks_count == LY_weeks_count) and (LY_last_weekend_date <> last_weekend_date)):
                    temp_Sale_TYS_VS_LYS_X2.append(LY_lastweek_json)
                    graph_y2.append(LY_lastweek_amount)
                    graph_x2.append(LY_last_weekend_date.encode("ascii"))

            # -- Get zone outlet count --
            stores_count = 1
            if(s_zone_id != '' ): 
                Zone_outlet_res = pd.read_sql_query("SELECT count(zout_id) as stores_count from zone_outlet where zout_zone_id = '"+s_zone_id+"'",con=self.db)
                Zone_outlet_res=Zone_outlet_res.groupby(['stores_count'])
                for k, v in Zone_outlet_res:
                    temp_zone_split=v.to_dict(orient="records")
                    for v in temp_zone_split:
                        stores_count = v['stores_count']
                        # Set store count globally
                        global g_stores_count
                        g_stores_count = stores_count
        
        # -- For this year's secondary sales
        if(s_outlet_id != '' or s_zone_id != '') :
            df_sale_sec_TYS_VS_LYS_query = "SELECT SUM("+tbl_prefix+"_amt)  as total_amount,SUM("+tbl_prefix+"_cost) as total_cost,SUM("+tbl_customer_col+") as total_customers,SUM("+tbl_prefix+"_gp_percent) as gp_percent,DATEADD(DAY, 8 - DATEPART(WEEKDAY, btt.trxt_date), CAST(btt.trxt_date AS DATE)) as week_end_date,trxt_year as year FROM "+tbl_name+" "+tbl_alais+" JOIN bi_trx_time btt ON btt.trxt_id = "+tbl_prefix+"_time_id WHERE "
            if s_zone_id != '' :
                df_sale_sec_TYS_VS_LYS_query += " "+tbl_prefix+"_outlet_id  IN ( select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+s_zone_id+"' ) AND "
            elif s_outlet_id != '' :
                df_sale_sec_TYS_VS_LYS_query += " "+tbl_prefix+"_outlet_id = '"+s_outlet_id+"' AND "
            if analysis_level == '2' :
                df_sale_sec_TYS_VS_LYS_query += " "+tbl_prefix+"_department_id  = '"+analysis_index_id+"' AND "
            elif analysis_level == '3' :
                df_sale_sec_TYS_VS_LYS_query += " "+tbl_prefix+"_department_id  = '"+analysis_department_id+"' AND "+tbl_prefix+"_commodity_id  = '"+analysis_index_id+"' AND "
            df_sale_sec_TYS_VS_LYS_query += " (btt.trxt_date BETWEEN '"+start_date+"' AND '"+end_date+"') GROUP BY DATEADD(DAY, 8 - DATEPART(WEEKDAY, btt.trxt_date), CAST(btt.trxt_date AS DATE)),btt.trxt_year "
            if endday == 'Sun' :
                df_sale_sec_TYS_VS_LYS_query += " having DATEADD(DAY, 8 - DATEPART(WEEKDAY, btt.trxt_date), CAST(btt.trxt_date AS DATE)) <= '"+end_date+"' "
            df_sale_sec_TYS_VS_LYS_query += " ORDER BY week_end_date" 
           
            df_sale_sec_TYS_VS_LYS = pd.read_sql_query(df_sale_sec_TYS_VS_LYS_query,con=self.db)
            # Get => Y-Axis!!
            df_sale_sec_TYS_VS_LYS=df_sale_sec_TYS_VS_LYS.groupby(['year'])
            # Set this year comparison sale result globally
            global TYCS_result
            TYCS_result = df_sale_sec_TYS_VS_LYS
            # print(list(df_sale_TYS_VS_LYS))
            for k, v in df_sale_sec_TYS_VS_LYS:
                temp_group_split=v.to_dict(orient="records")

                for v in temp_group_split:
                    # print(v['year'])
                    temp_Sale_TYS_VS_LYS_Y.append({ 'total_amount': (np.divide(v['total_amount'],stores_count)),"total_cost":(np.divide(v['total_cost'],stores_count)), "total_customers":(np.divide(v['total_customers'],stores_count)),"avg_basket":(np.divide(v['total_amount'],v['total_customers'])/stores_count), "gp_percent":(np.divide(v['gp_percent'],stores_count)), "week_end_date":v['week_end_date'], "year":v['year']})     
                    # -- Y for 2018 --
                    graph_y3.append(v['total_amount']/stores_count)

        # Set weekend date; if week days diffrence less then a week
        if len(graph_x1) == 1 :
            date_format = "%Y-%m-%d"
            a = datetime.strptime(start_date, date_format)
            b = datetime.strptime(end_date, date_format)
            delta = b - a
            day_diffrence = delta.days
            if day_diffrence <= 6 :
                # Set weekend date as end date
                graph_x1[0] = end_date

        graph_dictionary = []
        # Set marker name as combination of name and axis values
        for i in range(0,len(graph_x1)):
            # Handle exeption  
            try:
                pty_value = float(graph_y1[i])
            except IndexError:
                pty_value = 0
            try:
                ply_value = float(graph_y2[i])
            except IndexError:
                ply_value = 0
            try:
                sty_value = float(graph_y3[i])
            except IndexError:
                sty_value = 0
            # Prepare dictionary 
            myDict = {}
            myDict["weeks"] = graph_x1[i]
            myDict["pty_value"] = pty_value
            myDict["ply_value"] = ply_value
            myDict["sty_value"] = sty_value
            graph_dictionary.append(myDict)
       
        res = [graph_x1, graph_y1, graph_y2, graph_y3, graph_x2,graph_dictionary]

        return res

    # Check if the int given year is a leap year
    # return true if leap year or false otherwise
    @cache.memoize(timeout=timeout)
    def is_leap_year(self,year):
        leap_year_days = 366
        non_leap_year_days = 365
        if (year % 4) == 0:
            if (year % 100) == 0:
                if (year % 400) == 0:
                    return leap_year_days
                else:
                    return non_leap_year_days
            else:
                return leap_year_days
        else:
            return non_leap_year_days

    @cache.memoize(timeout=timeout)
    def pg_scatterchart_TYGP_vs_LYGP(self):
        
        # -- For Graph X-Axis, Y-Axis, Custome Data for current Years. --
        temp_Sale_TYGP_vs_LYGP_X = []
        temp_Sale_TYGP_vs_LYGP_Y = []
        temp_Sale_TYGP_VS_LYGP_X2 = []
        
        # -- For Graph X-Axis, Y-Axis, Custome Data for current Years. --
        graph_x1 = []
        graph_x2 = []
        graph_y1 = []
        graph_y2 = []
        graph_y3 = []
        graph_text = []
        graph_customdata = []
        TY_weeks_count = 0
        LY_weeks_count = 0
        TY_week_start_date = ""
        LY_last_weekend_date = ""
        LY_first_weekend = ""
        LY_second_week_start_date = ""
         # Get drilldown level filter data; If exist
        analysis_level = '1'
        analysis_index_id = ''
        analysis_department_id = ''
        if request.args.get('level') is not None:
            analysis_level = request.args.get('level')
        if request.args.get('id') is not None:    
            analysis_index_id = request.args.get('id')
        if request.args.get('department_id') is not None:    
            analysis_department_id = request.args.get('department_id')
        # Get filter data; If exist
        #filter_data = redis_conn.hgetall("filter_data")
        # Get session filter data
        if 'filter_data' in session:
            filter_data = session['filter_data']
        # Set filter params
        p_outlet_id = ''
        p_zone_id = ''
        s_outlet_id = ''
        s_zone_id = ''
        start_date = ''
        end_date = ''
        comparison_period = 1
        if filter_data.get("p_outlet_id") != None and filter_data.get("p_outlet_id") != '':
            p_outlet_id = filter_data.get("p_outlet_id")
        
        if filter_data.get("p_zone_id") != None and filter_data.get("p_zone_id") != '':
            p_zone_id = filter_data.get("p_zone_id")
       
        if filter_data.get("s_outlet_id") != None and filter_data.get("s_outlet_id") != '':
            s_outlet_id = filter_data.get("s_outlet_id")
        
        if filter_data.get("s_zone_id") != None and filter_data.get("s_zone_id") != '':
            s_zone_id = filter_data.get("s_zone_id")
        
        if filter_data.get("start_date") != None and filter_data.get("start_date") != '' and filter_data.get("end_date") != None:
            start_date = filter_data.get("start_date")
            end_date = filter_data.get("end_date")

        if filter_data.get("comparison_period") != None and filter_data.get("comparison_period") != '':
            comparison_period = int(filter_data.get("comparison_period"))

        # return if date range empty
        if start_date == '' or end_date == '' :
            res = [graph_x1, graph_y1, graph_y2, graph_y3, graph_x2]
            return res

       # -- Set dates
        convt_startdate = datetime.strptime(start_date, '%Y-%m-%d').date()
        current_year = int(convt_startdate.strftime('%Y'))
        current_month = convt_startdate.strftime('%m')
        current_day = convt_startdate.strftime('%d')
        convt_enddate = datetime.strptime(end_date, '%Y-%m-%d').date()
        current_endmonth = convt_enddate.strftime('%m')
        current_endday = convt_enddate.strftime('%d')
        prev_year = current_year-comparison_period
        year_days = self.is_leap_year(prev_year)-1
        convt_prev_startdate = datetime.strptime(str(prev_year)+'-'+current_month+'-'+current_day, '%Y-%m-%d').date()
        convt_prev_enddate = datetime.strptime(str(prev_year)+'-'+current_endmonth+'-'+current_endday, '%Y-%m-%d').date()
        convt_startdate.strftime('%Y-%m-%d')
        LY_start_date = convt_prev_startdate.strftime('%Y-%m-%d')
        convt_enddate = datetime.strptime(end_date, '%Y-%m-%d').date()
        convt_enddate.strftime('%Y-%m-%d') 
        LY_end_date = convt_prev_enddate.strftime('%Y-%m-%d')

        # Get this year sale result
        if(p_outlet_id != '' or p_zone_id != '') :
            # Set table, colomn name based on analysis level 
            tbl_alais = 'btoa'
            tbl_prefix = 'btoa.btoa'
            tbl_name = 'bi_trx_outlet_aggregates'
            tbl_customer_col = 'btoa.btoa_customers'
            if analysis_level == '2' :
                tbl_alais = 'btda'
                tbl_prefix = 'btda.btda'
                tbl_name = 'bi_trx_department_aggregates'
                tbl_customer_col = 'btda.btda_customers_count'
            elif analysis_level == '3' :
                tbl_alais = 'btca'
                tbl_prefix = 'btca.btca'
                tbl_name = 'bi_trx_commodity_aggregates'
                tbl_customer_col = 'btca.btca_customers_count'

            if not TYS_result:
                # -- For X, Y of Y-Axis for this Year. --
                df_sale_TYGP_vs_LYGP_query = "SELECT SUM("+tbl_prefix+"_amt) as total_amount,SUM("+tbl_prefix+"_cost) as total_cost, SUM("+tbl_customer_col+") as total_customers,SUM("+tbl_prefix+"_gp_percent) as gp_percent, DATEADD(DAY, 8 - DATEPART(WEEKDAY, btt.trxt_date), CAST(btt.trxt_date AS DATE)) as  week_end_date,trxt_year as year"
                if p_zone_id != '' :
                    df_sale_TYGP_vs_LYGP_query += ",(SELECT count(zout_id) FROM zone_outlet zo WHERE zout_zone_id = '"+p_zone_id+"') as total_stores"
                df_sale_TYGP_vs_LYGP_query += " FROM "+tbl_name+" "+tbl_alais+" JOIN bi_trx_time btt ON btt.trxt_id = "+tbl_prefix+"_time_id WHERE "  
                if p_outlet_id != '':
                    df_sale_TYGP_vs_LYGP_query += ""+tbl_prefix+"_outlet_id  = '"+p_outlet_id+"' AND "
                elif p_zone_id != '' :
                    df_sale_TYGP_vs_LYGP_query += ""+tbl_prefix+"_outlet_id  IN ( select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+p_zone_id+"' )  AND "
                if analysis_level == '2' :
                    df_sale_TYGP_vs_LYGP_query += " "+tbl_prefix+"_department_id  = '"+analysis_index_id+"' AND "
                elif analysis_level == '3' :
                    df_sale_TYGP_vs_LYGP_query += " "+tbl_prefix+"_department_id  = '"+analysis_department_id+"' AND "+tbl_prefix+"_commodity_id  = '"+analysis_index_id+"' AND "
                df_sale_TYGP_vs_LYGP_query += " ( btt.trxt_date BETWEEN '"+start_date+"' AND '"+end_date+"' ) GROUP BY DATEADD(DAY, 8 - DATEPART(WEEKDAY, btt.trxt_date), CAST(btt.trxt_date AS DATE)),btt.trxt_year ORDER BY week_end_date"

                df_sale_TYGP_vs_LYGP = pd.read_sql_query(df_sale_TYGP_vs_LYGP_query,con=self.db)
                # Get => Y-Axis!!
                df_sale_TYGP_vs_LYGP=df_sale_TYGP_vs_LYGP.groupby(['year']) 
            else:
                df_sale_TYGP_vs_LYGP = TYS_result

            # print(list(df_sale_TYS_VS_LYS)) 
            for k, v in df_sale_TYGP_vs_LYGP:
                temp_group_split=v.to_dict(orient="records")
                week_number = 1
                TY_weeks_count = len(temp_group_split)
                for v in temp_group_split:
                    TY_week_date = v['week_end_date']
                    if(week_number == 1):
                        # -- Set dates
                        convt_date = datetime.strptime(TY_week_date, '%Y-%m-%d').date()
                        current_year = int(convt_date.strftime('%Y'))
                        prev_year = current_year-comparison_period
                        year_days = self.is_leap_year(prev_year)
                        LY_first_weekend = datetime.strptime(str(prev_year)+'-'+convt_date.strftime('%m')+'-'+convt_date.strftime('%d'), '%Y-%m-%d').date()
                        LY_second_week_start_date = LY_first_weekend + timedelta(days=1)
                        LY_second_week_start_date = datetime.strptime(str(prev_year)+'-'+LY_second_week_start_date.strftime('%m')+'-'+LY_second_week_start_date.strftime('%d'), '%Y-%m-%d').date()
                        LY_first_weekend = LY_first_weekend.strftime('%Y-%m-%d')
                        LY_second_week_start_date = LY_second_week_start_date.strftime('%Y-%m-%d')
                        
                    elif(TY_weeks_count == week_number):
                        
                        if(end_date != TY_week_date):
                            TY_week_date = end_date

                        convt_date = datetime.strptime(TY_week_date, '%Y-%m-%d').date()
                        current_year = int(convt_date.strftime('%Y'))
                        prev_year = current_year-comparison_period
                        year_days = self.is_leap_year(prev_year)
                        LY_last_weekend_date = datetime.strptime(str(prev_year)+'-'+convt_date.strftime('%m')+'-'+convt_date.strftime('%d'), '%Y-%m-%d').date()
                        LY_last_weekend_date = LY_last_weekend_date.strftime('%Y-%m-%d')
                    # Set total stores count var 
                    total_stores = 1  
                    if p_zone_id != '' :
                        total_stores = v['total_stores']
                
                    temp_Sale_TYGP_vs_LYGP_X.append({ 'total_amount': (v['total_amount']/total_stores),"total_cost":(v['total_cost']/total_stores), "total_customers":(v['total_customers']/total_stores), "gp_percent":(v['gp_percent']/total_stores), "week_end_date":TY_week_date, "year":v['year']})       
                    # -- X for primary year --
                    graph_x1.append(TY_week_date.encode("ascii"))                    
                    graph_y1.append(np.divide((np.divide((v['total_amount'] - v['total_cost']) , v['total_amount'])*100),total_stores))
                    
                    week_number = week_number+1

            # -- For last year sales. --
            df_sale_TYGP_vs_LYGP_query = "SELECT SUM("+tbl_prefix+"_amt) as total_amount,SUM("+tbl_prefix+"_cost) as total_cost,SUM("+tbl_customer_col+") as total_customers,SUM("+tbl_prefix+"_gp_percent) as gp_percent,trxt_year as year  "
            if p_zone_id != '' :
                df_sale_TYGP_vs_LYGP_query += ",(SELECT count(zout_id) FROM zone_outlet zo WHERE zout_zone_id = '"+p_zone_id+"') as total_stores"
            df_sale_TYGP_vs_LYGP_query += " FROM "+tbl_name+" "+tbl_alais+" JOIN bi_trx_time btt ON btt.trxt_id = "+tbl_prefix+"_time_id WHERE " 
            if p_outlet_id != '':
                df_sale_TYGP_vs_LYGP_query += ""+tbl_prefix+"_outlet_id  = '"+p_outlet_id+"' AND "
            elif p_zone_id != '' :
                df_sale_TYGP_vs_LYGP_query += ""+tbl_prefix+"_outlet_id  IN ( select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+p_zone_id+"' )  AND "
            if analysis_level == '2' :
                df_sale_TYGP_vs_LYGP_query += " "+tbl_prefix+"_department_id  = '"+analysis_index_id+"' AND "
            elif analysis_level == '3' :
                df_sale_TYGP_vs_LYGP_query += " "+tbl_prefix+"_department_id  = '"+analysis_department_id+"' AND "+tbl_prefix+"_commodity_id  = '"+analysis_index_id+"' AND "    
            df_sale_TYGP_vs_LYGP_query += " (btt.trxt_date BETWEEN '"+LY_start_date+"' AND '"+LY_first_weekend+"' ) GROUP BY btt.trxt_year"

            df_sale_TYGP_vs_LYGP = pd.read_sql_query(df_sale_TYGP_vs_LYGP_query,con=self.db)
            df_sale_TYGP_vs_LYGP=df_sale_TYGP_vs_LYGP.groupby(['year'])
            for k, v in df_sale_TYGP_vs_LYGP:
                temp_group_split=v.to_dict(orient="records")
                for v in temp_group_split:
                    LY_weeks_count = 1
                    year = v['year']
                    # Set total stores count var 
                    total_stores = 1  
                    if p_zone_id != '' :
                        total_stores = v['total_stores']
                    temp_Sale_TYGP_VS_LYGP_X2.append({ 'total_amount': (v['total_amount']/total_stores),"total_cost":(v['total_cost']/total_stores), "total_customers":(v['total_customers']/total_stores), "gp_percent":(v['gp_percent']/total_stores), "week_end_date":'"+LY_first_weekend+"', "year":year})
                    graph_y2.append(np.divide((np.divide((v['total_amount'] - v['total_cost']) , v['total_amount'])*100),total_stores))
                    graph_x2.append(LY_first_weekend.encode("ascii")) 

            if p_outlet_id != '':
                df_sale_TYGP_vs_LYGP_query = "SELECT "+tbl_prefix+"_amt as total_amount,"+tbl_prefix+"_cost as total_cost, "+tbl_customer_col+" as total_customers,"+tbl_prefix+"_gp_percent as gp_percent, CAST(btt.trxt_date AS DATE) as ty_date,trxt_year as year FROM "+tbl_name+" "+tbl_alais+" JOIN bi_trx_time btt ON btt.trxt_id = "+tbl_prefix+"_time_id WHERE "+tbl_prefix+"_outlet_id  = '"+p_outlet_id+"' AND "
                if analysis_level == '2' :
                    df_sale_TYGP_vs_LYGP_query += " "+tbl_prefix+"_department_id  = '"+analysis_index_id+"' AND "
                elif analysis_level == '3' :
                    df_sale_TYGP_vs_LYGP_query += " "+tbl_prefix+"_department_id  = '"+analysis_department_id+"' AND "+tbl_prefix+"_commodity_id  = '"+analysis_index_id+"' AND "
                df_sale_TYGP_vs_LYGP_query += " ( btt.trxt_date BETWEEN '"+LY_second_week_start_date+"' AND '"+LY_end_date+"' )  ORDER BY btt.trxt_date ASC"

            elif p_zone_id != '' :
                df_sale_TYGP_vs_LYGP_query = "SELECT SUM("+tbl_prefix+"_amt) as total_amount,SUM("+tbl_prefix+"_cost) as total_cost, SUM("+tbl_customer_col+") as total_customers,SUM("+tbl_prefix+"_gp_percent) as gp_percent, CAST(btt.trxt_date AS DATE) as ty_date,trxt_year as year,(SELECT count(zout_id) FROM zone_outlet zo WHERE zout_zone_id = '"+p_zone_id+"') as total_stores FROM "+tbl_name+" "+tbl_alais+" JOIN bi_trx_time btt ON btt.trxt_id = "+tbl_prefix+"_time_id WHERE "+tbl_prefix+"_outlet_id  IN ( select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+p_zone_id+"' )  AND "
                if analysis_level == '2' :
                    df_sale_TYGP_vs_LYGP_query += " "+tbl_prefix+"_department_id  = '"+analysis_index_id+"' AND "
                elif analysis_level == '3' :
                    df_sale_TYGP_vs_LYGP_query += " "+tbl_prefix+"_department_id  = '"+analysis_department_id+"' AND "+tbl_prefix+"_commodity_id  = '"+analysis_index_id+"' AND "
                df_sale_TYGP_vs_LYGP_query += " ( btt.trxt_date BETWEEN '"+LY_second_week_start_date+"' AND '"+LY_end_date+"' ) GROUP BY CAST(btt.trxt_date AS DATE),trxt_year ORDER BY ty_date ASC"

            df_sale_TYGP_vs_LYGP = pd.read_sql_query(df_sale_TYGP_vs_LYGP_query,con=self.db)
            # Get => X-Axis!!
            df_sale_TYGP_vs_LYGP=df_sale_TYGP_vs_LYGP.groupby(['year'])
            
            for k, v in df_sale_TYGP_vs_LYGP:
                temp_group_split=v.to_dict(orient="records")
                weekday = 1
                last_weekend_date = ''
                for v in temp_group_split:
                    if weekday == 1 :
                        total_amount = 0
                        total_cost = 0
                        avg_basket = 0
                        total_customers = 0
                        gp_percent = 0
                        year = ''
                    # Add total sales params    
                    if p_zone_id != '' :
                        total_amount = total_amount+(v['total_amount']/v['total_stores'])
                        total_cost   = total_cost+(v['total_cost']/v['total_stores'])
                        total_customers = total_customers+(v['total_customers']/v['total_stores'])
                        gp_percent   = gp_percent+(v['gp_percent']/v['total_stores'])
                    else:
                        total_amount = total_amount+v['total_amount']
                        total_cost   = total_cost+v['total_cost']
                        total_customers = total_customers+v['total_customers']
                        gp_percent   = gp_percent+v['gp_percent']
                    year = v['year']

                    if weekday == 7:
                        # Set weekend date
                        weekend_date = datetime.strptime(v['ty_date'], '%Y-%m-%d').date()
                        #weekend_date = weekend_date + timedelta(days=1)
                        weekend_date = weekend_date.strftime('%Y-%m-%d')
                        LY_weeks_count = LY_weeks_count+1
                        last_weekend_date = weekend_date
                        #weekend_date = v['ty_date'].encode("ascii")
                        temp_Sale_TYGP_VS_LYGP_X2.append({ 'total_amount': total_amount,"total_cost":total_cost, "total_customers":total_customers, "gp_percent":gp_percent, "week_end_date":weekend_date, "year":year})
                        
                        graph_y2.append(np.divide((np.divide((total_amount - total_cost) , total_amount)*100),total_stores))
                        graph_x2.append(weekend_date.encode("ascii"))
                        weekday = 1
                        
                    else:
                        weekday = weekday+1
                        LY_lastweek_json = { 'total_amount': total_amount,"total_cost":total_cost, "total_customers":total_customers, "gp_percent":gp_percent, "week_end_date":LY_last_weekend_date, "year":year}
                        #LY_lastweek_amount = gp_percent
                        LY_lastweek_amount = np.divide((np.divide((total_amount - total_cost) , total_amount)*100),total_stores)

                if((TY_weeks_count-1) == LY_weeks_count) :
                    LY_weeks_count = LY_weeks_count+1

                if((TY_weeks_count == LY_weeks_count) and (LY_last_weekend_date <> last_weekend_date)):
                    temp_Sale_TYGP_VS_LYGP_X2.append(LY_lastweek_json)
                    graph_y2.append(LY_lastweek_amount)
                    graph_x2.append(LY_last_weekend_date.encode("ascii"))

            # -- Get zone outlet count --
            stores_count = 1
            if(s_zone_id != '' ):
                if(g_stores_count <> 0):
                    stores_count = g_stores_count
                else: 
                    Zone_outlet_res = pd.read_sql_query("SELECT count(zout_id) as stores_count from zone_outlet where zout_zone_id = '"+s_zone_id+"'",con=self.db)
                    Zone_outlet_res=Zone_outlet_res.groupby(['stores_count'])
                    for k, v in Zone_outlet_res:
                        temp_zone_split=v.to_dict(orient="records")
                        for v in temp_zone_split:
                            stores_count = v['stores_count']

        # -- For this year's secondary sales
        if(s_outlet_id != '' or s_zone_id != '') :
            if not TYCS_result:
                df_sale_sec_TYGP_VS_LYGP_query = "SELECT SUM("+tbl_prefix+"_amt)  as total_amount,SUM("+tbl_prefix+"_cost) as total_cost,SUM("+tbl_customer_col+") as total_customers,SUM("+tbl_prefix+"_gp_percent) as gp_percent,DATEADD(DAY, 8 - DATEPART(WEEKDAY, btt.trxt_date), CAST(btt.trxt_date AS DATE)) as week_end_date,trxt_year as year FROM "+tbl_name+" "+tbl_alais+" JOIN bi_trx_time btt ON btt.trxt_id = "+tbl_prefix+"_time_id WHERE "
                if s_zone_id != '' :
                    df_sale_sec_TYGP_VS_LYGP_query += " "+tbl_prefix+"_outlet_id  IN ( select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+s_zone_id+"' ) AND "
                elif s_outlet_id != '' :
                    df_sale_sec_TYGP_VS_LYGP_query += " "+tbl_prefix+"_outlet_id = '"+s_outlet_id+"' AND "
                if analysis_level == '2' :
                    df_sale_sec_TYGP_VS_LYGP_query += " "+tbl_prefix+"_department_id  = '"+analysis_index_id+"' AND "
                elif analysis_level == '3' :
                    df_sale_sec_TYGP_VS_LYGP_query += " "+tbl_prefix+"_department_id  = '"+analysis_department_id+"' AND "+tbl_prefix+"_commodity_id  = '"+analysis_index_id+"' AND "
                df_sale_sec_TYGP_VS_LYGP_query += " (btt.trxt_date BETWEEN '"+start_date+"' AND '"+end_date+"') GROUP BY DATEADD(DAY, 8 - DATEPART(WEEKDAY, btt.trxt_date), CAST(btt.trxt_date AS DATE)),btt.trxt_year ORDER BY week_end_date"

                df_sale_sec_TYGP_VS_LYGP = pd.read_sql_query(df_sale_sec_TYGP_VS_LYGP_query,con=self.db)
                # Get => Y-Axis!!
                df_sale_sec_TYGP_VS_LYGP=df_sale_sec_TYGP_VS_LYGP.groupby(['year'])
            else:
                df_sale_sec_TYGP_VS_LYGP = TYCS_result
            # print(list(df_sale_TYS_VS_LYS)) 

            for k, v in df_sale_sec_TYGP_VS_LYGP:
                temp_group_split=v.to_dict(orient="records")

                for v in temp_group_split:
                    temp_Sale_TYGP_vs_LYGP_Y.append({ 'total_amount': (v['total_amount']/stores_count),"total_cost":(v['total_cost']/stores_count), "total_customers":(v['total_customers']/stores_count), "gp_percent":(v['gp_percent']/stores_count), "week_end_date":v['week_end_date'], "year":v['year']})        
                    # -- Y for 2018 --
                    graph_y3.append(np.divide((np.divide((v['total_amount'] - v['total_cost']) , v['total_amount'])*100),total_stores))  

        # Set weekend date; if week days diffrence less then a week
        if len(graph_x1) == 1 :
            date_format = "%Y-%m-%d"
            a = datetime.strptime(start_date, date_format)
            b = datetime.strptime(end_date, date_format)
            delta = b - a
            day_diffrence = delta.days
            if day_diffrence <= 6 :
                # Set weekend date as end date
                graph_x1[0] = end_date

        graph_dictionary = []
        # Set marker name as combination of name and axis values
        for i in range(0,len(graph_x1)):
            # Prepare dictionary 
            myDict = {}
            # Handle exeption  
            try:
                pty_value = float(graph_y1[i])
            except IndexError:
                pty_value = 0
            try:
                ply_value = float(graph_y2[i])
            except IndexError:
                ply_value = 0
            try:
                sty_value = float(graph_y3[i])
            except IndexError:
                sty_value = 0
            myDict["weeks"] = graph_x1[i]
            myDict["pty_value"] = pty_value
            myDict["ply_value"] = ply_value
            myDict["sty_value"] = sty_value
            graph_dictionary.append(myDict)
        res = [graph_x1, graph_y1, graph_y2, graph_y3, graph_x2, graph_dictionary]
        return res

    @cache.memoize(timeout=timeout)
    def pg_scatterchart_THAVGB_vs_LYAVGB(self):
       
        # -- For Graph X-Axis, Y-Axis, Custome Data for current Years. --
        temp_Sale_THAVGB_vs_LYAVGB_X = []
        temp_Sale_THAVGB_vs_LYAVGB_X2 = []
        temp_Sale_THAVGB_vs_LYAVGB_Y = []
        
        # -- For Graph X-Axis, Y-Axis, Custome Data for current Years. --
        graph_x1 = []
        graph_x2 = []
        graph_y1 = []
        graph_y2 = []
        graph_y3 = []
        graph_text = []
        graph_customdata = []
        TY_weeks_count = 0
        LY_weeks_count = 0
        TY_week_start_date = ""
        LY_last_weekend_date = ""
        LY_first_weekend = ""
        LY_second_week_start_date = ""
        # Get drilldown level filter data; If exist
        analysis_level = '1'
        analysis_index_id = ''
        analysis_department_id = ''
        if request.args.get('level') is not None:
            analysis_level = request.args.get('level')
        if request.args.get('id') is not None:    
            analysis_index_id = request.args.get('id')
        if request.args.get('department_id') is not None:    
            analysis_department_id = request.args.get('department_id')
        #filter_data = redis_conn.hgetall("filter_data")
        # Get session filter data
        if 'filter_data' in session:
            filter_data = session['filter_data']
        # Set filter params
        p_outlet_id = ''
        p_zone_id = ''
        s_outlet_id = ''
        s_zone_id = ''
        start_date = ''
        end_date = ''
        comparison_period = 1
        if filter_data.get("p_outlet_id") != None and filter_data.get("p_outlet_id") != '':
            p_outlet_id = filter_data.get("p_outlet_id")
        
        if filter_data.get("p_zone_id") != None and filter_data.get("p_zone_id") != '':
            p_zone_id = filter_data.get("p_zone_id")
       
        if filter_data.get("s_outlet_id") != None and filter_data.get("s_outlet_id") != '':
            s_outlet_id = filter_data.get("s_outlet_id")
        
        if filter_data.get("s_zone_id") != None and filter_data.get("s_zone_id") != '':
            s_zone_id = filter_data.get("s_zone_id")
        
        if filter_data.get("start_date") != None and filter_data.get("start_date") != '' and filter_data.get("end_date") != None:
            start_date = filter_data.get("start_date")
            end_date = filter_data.get("end_date")

        if filter_data.get("comparison_period") != None and filter_data.get("comparison_period") != '':
            comparison_period = int(filter_data.get("comparison_period"))

        # return if date range empty
        if start_date == '' or end_date == '' :
            res = [graph_x1, graph_y1, graph_y2, graph_y3, graph_x2]
            return res

        # -- Set dates
        convt_startdate = datetime.strptime(start_date, '%Y-%m-%d').date()
        current_year = int(convt_startdate.strftime('%Y'))
        current_month = convt_startdate.strftime('%m')
        current_day = convt_startdate.strftime('%d')
        convt_enddate = datetime.strptime(end_date, '%Y-%m-%d').date()
        current_endmonth = convt_enddate.strftime('%m')
        current_endday = convt_enddate.strftime('%d')
        prev_year = current_year-comparison_period
        year_days = self.is_leap_year(prev_year)-1
        convt_prev_startdate = datetime.strptime(str(prev_year)+'-'+current_month+'-'+current_day, '%Y-%m-%d').date()
        convt_prev_enddate = datetime.strptime(str(prev_year)+'-'+current_endmonth+'-'+current_endday, '%Y-%m-%d').date()
        convt_startdate.strftime('%Y-%m-%d')
        LY_start_date = convt_prev_startdate.strftime('%Y-%m-%d')
        convt_enddate = datetime.strptime(end_date, '%Y-%m-%d').date()
        convt_enddate.strftime('%Y-%m-%d')
        LY_end_date = convt_prev_enddate.strftime('%Y-%m-%d')
        # -- For this year sales. --
        if(p_outlet_id != '' or p_zone_id != '') :
            # Set table, colomn name based on analysis level
            tbl_alais = 'btoa'
            tbl_prefix = 'btoa.btoa'
            tbl_name = 'bi_trx_outlet_aggregates'
            tbl_customer_col = 'btoa.btoa_customers'
            if analysis_level == '2' :
                tbl_alais = 'btda'
                tbl_prefix = 'btda.btda'
                tbl_name = 'bi_trx_department_aggregates'
                tbl_customer_col = 'btda.btda_customers_count'
            elif analysis_level == '3' :
                tbl_alais = 'btca'
                tbl_prefix = 'btca.btca'
                tbl_name = 'bi_trx_commodity_aggregates'
                tbl_customer_col = 'btca.btca_customers_count'
            if not TYS_result:
                df_sale_THAVGB_vs_LYAVGB_query = "SELECT SUM("+tbl_prefix+"_amt) as total_amount,SUM("+tbl_prefix+"_cost) as total_cost, SUM("+tbl_customer_col+") as total_customers,SUM("+tbl_prefix+"_gp_percent) as gp_percent, DATEADD(DAY, 8 - DATEPART(WEEKDAY, btt.trxt_date), CAST(btt.trxt_date AS DATE)) as  week_end_date,trxt_year as year"
                if p_zone_id != '' :
                    df_sale_THAVGB_vs_LYAVGB_query += ",(SELECT count(zout_id) FROM zone_outlet zo WHERE zout_zone_id = '"+p_zone_id+"') as total_stores"
                df_sale_THAVGB_vs_LYAVGB_query += " FROM "+tbl_name+" "+tbl_alais+" JOIN bi_trx_time btt ON btt.trxt_id = "+tbl_prefix+"_time_id WHERE "  
                if p_outlet_id != '':
                    df_sale_THAVGB_vs_LYAVGB_query += ""+tbl_prefix+"_outlet_id  = '"+p_outlet_id+"' AND "
                elif p_zone_id != '' :
                    df_sale_THAVGB_vs_LYAVGB_query += ""+tbl_prefix+"_outlet_id  IN ( select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+p_zone_id+"' )  AND "
                if analysis_level == '2' :
                    df_sale_THAVGB_vs_LYAVGB_query += " "+tbl_prefix+"_department_id  = '"+analysis_index_id+"' AND "
                elif analysis_level == '3' :
                    df_sale_THAVGB_vs_LYAVGB_query += " "+tbl_prefix+"_department_id  = '"+analysis_department_id+"' AND "+tbl_prefix+"_commodity_id  = '"+analysis_index_id+"' AND "
                df_sale_THAVGB_vs_LYAVGB_query += " ( btt.trxt_date BETWEEN '"+start_date+"' AND '"+end_date+"' ) GROUP BY DATEADD(DAY, 8 - DATEPART(WEEKDAY, btt.trxt_date), CAST(btt.trxt_date AS DATE)),btt.trxt_year ORDER BY week_end_date"

                df_sale_THAVGB_vs_LYAVGB = pd.read_sql_query(df_sale_THAVGB_vs_LYAVGB_query,con=self.db)
                # Get => Y-Axis!!
                df_sale_THAVGB_vs_LYAVGB=df_sale_THAVGB_vs_LYAVGB.groupby(['year'])
            else:
                df_sale_THAVGB_vs_LYAVGB  = TYS_result

            for k, v in df_sale_THAVGB_vs_LYAVGB:
                temp_group_split=v.to_dict(orient="records")
                week_number = 1
                TY_weeks_count = len(temp_group_split)
                for v in temp_group_split:
                    TY_week_date = v['week_end_date']
                    if(week_number == 1):
                        # -- Set dates
                        convt_date = datetime.strptime(TY_week_date, '%Y-%m-%d').date()
                        current_year = int(convt_date.strftime('%Y'))
                        prev_year = current_year-comparison_period
                        year_days = self.is_leap_year(prev_year)
                        LY_first_weekend = datetime.strptime(str(prev_year)+'-'+convt_date.strftime('%m')+'-'+convt_date.strftime('%d'), '%Y-%m-%d').date()
                        LY_second_week_start_date = LY_first_weekend + timedelta(days=1)
                        LY_second_week_start_date = datetime.strptime(str(prev_year)+'-'+LY_second_week_start_date.strftime('%m')+'-'+LY_second_week_start_date.strftime('%d'), '%Y-%m-%d').date()
                        LY_first_weekend = LY_first_weekend.strftime('%Y-%m-%d')
                        LY_second_week_start_date = LY_second_week_start_date.strftime('%Y-%m-%d')
                       
                    elif(TY_weeks_count == week_number):
                        
                        if(end_date != TY_week_date):
                            TY_week_date = end_date

                        convt_date = datetime.strptime(TY_week_date, '%Y-%m-%d').date()
                        current_year = int(convt_date.strftime('%Y'))
                        prev_year = current_year-comparison_period
                        year_days = self.is_leap_year(prev_year)
                        #LY_last_weekend_date = convt_date - timedelta(days=year_days)
                        #LY_last_weekend_date = LY_last_weekend_date.strftime('%Y-%m-%d')

                        LY_last_weekend_date = datetime.strptime(str(prev_year)+'-'+convt_date.strftime('%m')+'-'+convt_date.strftime('%d'), '%Y-%m-%d').date()
                        LY_last_weekend_date = LY_last_weekend_date.strftime('%Y-%m-%d')
                    # Set total stores count var 
                    total_stores = 1  
                    if p_zone_id != '' :
                        total_stores = v['total_stores']
                
                    temp_Sale_THAVGB_vs_LYAVGB_X.append({ 'total_amount': (np.divide(v['total_amount'],total_stores)),"total_cost":(np.divide(v['total_cost'],total_stores)), "avg_basket":(np.divide(v['total_amount'],v['total_customers'])/total_stores), "total_customers":(np.divide(v['total_customers'],total_stores)), "gp_percent":(np.divide(v['gp_percent'],total_stores)), "week_end_date":TY_week_date, "year":v['year']})        
                    # -- X for 2018 --
                    graph_x1.append(TY_week_date.encode("ascii"))                    
                    graph_y1.append(np.divide(v['total_amount'],v['total_customers'])/total_stores)
                    week_number = week_number+1

            # -- For last year sales. --
            df_sale_THAVGB_vs_LYAVGB_query = "SELECT SUM("+tbl_prefix+"_amt) as total_amount,SUM("+tbl_prefix+"_cost) as total_cost,SUM("+tbl_customer_col+") as total_customers,SUM("+tbl_prefix+"_gp_percent) as gp_percent,trxt_year as year  "
            if p_zone_id != '' :
                df_sale_THAVGB_vs_LYAVGB_query += ",(SELECT count(zout_id) FROM zone_outlet zo WHERE zout_zone_id = '"+p_zone_id+"') as total_stores"
            df_sale_THAVGB_vs_LYAVGB_query += " FROM "+tbl_name+" "+tbl_alais+" JOIN bi_trx_time btt ON btt.trxt_id = "+tbl_prefix+"_time_id WHERE " 
            if p_outlet_id != '':
                df_sale_THAVGB_vs_LYAVGB_query += ""+tbl_prefix+"_outlet_id  = '"+p_outlet_id+"' AND "
            elif p_zone_id != '' :
                df_sale_THAVGB_vs_LYAVGB_query += ""+tbl_prefix+"_outlet_id  IN ( select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+p_zone_id+"' )  AND "
            if analysis_level == '2' :
                df_sale_THAVGB_vs_LYAVGB_query += " "+tbl_prefix+"_department_id  = '"+analysis_index_id+"' AND "
            elif analysis_level == '3' :
                df_sale_THAVGB_vs_LYAVGB_query += " "+tbl_prefix+"_department_id  = '"+analysis_department_id+"' AND "+tbl_prefix+"_commodity_id  = '"+analysis_index_id+"' AND "    
            df_sale_THAVGB_vs_LYAVGB_query += " (btt.trxt_date BETWEEN '"+LY_start_date+"' AND '"+LY_first_weekend+"' ) GROUP BY btt.trxt_year"

            df_sale_THAVGB_vs_LYAVGB = pd.read_sql_query(df_sale_THAVGB_vs_LYAVGB_query,con=self.db)
            df_sale_THAVGB_vs_LYAVGB=df_sale_THAVGB_vs_LYAVGB.groupby(['year'])
            for k, v in df_sale_THAVGB_vs_LYAVGB:
                temp_group_split=v.to_dict(orient="records")
                for v in temp_group_split:
                    LY_weeks_count = 1
                    year = v['year']
                    # Set total stores count var 
                    total_stores = 1  
                    if p_zone_id != '' :
                        total_stores = v['total_stores']
                    temp_Sale_THAVGB_vs_LYAVGB_X2.append({ 'total_amount': (v['total_amount']/total_stores),"total_cost":(v['total_cost']/total_stores), "avg_basket":((v['total_amount']/v['total_customers'])/total_stores), "total_customers":(v['total_customers']/total_stores), "gp_percent":(v['gp_percent']/total_stores), "week_end_date":'"+LY_first_weekend+"', "year":year})
                    graph_y2.append((v['total_amount']/v['total_customers'])/total_stores)
                    graph_x2.append(LY_first_weekend.encode("ascii")) 

            if p_outlet_id != '':
                df_sale_THAVGB_vs_LYAVGB_query = "SELECT "+tbl_prefix+"_amt as total_amount,"+tbl_prefix+"_cost as total_cost, "+tbl_customer_col+" as total_customers,"+tbl_prefix+"_gp_percent as gp_percent, CAST(btt.trxt_date AS DATE) as ty_date,trxt_year as year FROM "+tbl_name+" "+tbl_alais+" JOIN bi_trx_time btt ON btt.trxt_id = "+tbl_prefix+"_time_id WHERE "+tbl_prefix+"_outlet_id  = '"+p_outlet_id+"' AND "
                if analysis_level == '2' :
                    df_sale_THAVGB_vs_LYAVGB_query += " "+tbl_prefix+"_department_id  = '"+analysis_index_id+"' AND "
                elif analysis_level == '3' :
                    df_sale_THAVGB_vs_LYAVGB_query += " "+tbl_prefix+"_department_id  = '"+analysis_department_id+"' AND "+tbl_prefix+"_commodity_id  = '"+analysis_index_id+"' AND "
                df_sale_THAVGB_vs_LYAVGB_query += " ( btt.trxt_date BETWEEN '"+LY_second_week_start_date+"' AND '"+LY_end_date+"' )  ORDER BY btt.trxt_date ASC"

            elif p_zone_id != '' :
                df_sale_THAVGB_vs_LYAVGB_query = "SELECT SUM("+tbl_prefix+"_amt) as total_amount,SUM("+tbl_prefix+"_cost) as total_cost, SUM("+tbl_customer_col+") as total_customers,SUM("+tbl_prefix+"_gp_percent) as gp_percent, CAST(btt.trxt_date AS DATE) as ty_date,trxt_year as year,(SELECT count(zout_id) FROM zone_outlet zo WHERE zout_zone_id = '"+p_zone_id+"') as total_stores FROM "+tbl_name+" "+tbl_alais+" JOIN bi_trx_time btt ON btt.trxt_id = "+tbl_prefix+"_time_id WHERE "+tbl_prefix+"_outlet_id  IN ( select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+p_zone_id+"' )  AND "
                if analysis_level == '2' :
                    df_sale_THAVGB_vs_LYAVGB_query += " "+tbl_prefix+"_department_id  = '"+analysis_index_id+"' AND "
                elif analysis_level == '3' :
                    df_sale_THAVGB_vs_LYAVGB_query += " "+tbl_prefix+"_department_id  = '"+analysis_department_id+"' AND "+tbl_prefix+"_commodity_id  = '"+analysis_index_id+"' AND "
                df_sale_THAVGB_vs_LYAVGB_query += " ( btt.trxt_date BETWEEN '"+LY_second_week_start_date+"' AND '"+LY_end_date+"' ) GROUP BY CAST(btt.trxt_date AS DATE),trxt_year ORDER BY ty_date ASC"

            df_sale_THAVGB_vs_LYAVGB = pd.read_sql_query(df_sale_THAVGB_vs_LYAVGB_query,con=self.db)
            # Get => X-Axis!!
            df_sale_THAVGB_vs_LYAVGB=df_sale_THAVGB_vs_LYAVGB.groupby(['year'])
            
            for k, v in df_sale_THAVGB_vs_LYAVGB:
                temp_group_split=v.to_dict(orient="records")
                weekday = 1
                last_weekend_date = ''
                for v in temp_group_split:
                    if weekday == 1 :
                        total_amount = 0
                        total_cost = 0
                        avg_basket = 0
                        total_customers = 0
                        gp_percent = 0
                        year = ''
                    # Add total sales params
                    if p_zone_id != '' : 
                        total_amount = total_amount+(np.divide(v['total_amount'],v['total_stores']))
                        total_cost   = total_cost+(np.divide(v['total_cost'],v['total_stores']))
                        avg_basket   = avg_basket+((np.divide(v['total_amount'],v['total_customers']))/v['total_stores'])
                        total_customers = total_customers+(np.divide(v['total_customers'],v['total_stores']))
                        gp_percent   = gp_percent+(np.divide(v['gp_percent'],v['total_stores']))
                    else:
                        total_amount = total_amount+v['total_amount']
                        total_cost   = total_cost+v['total_cost']
                        avg_basket   = avg_basket+(np.divide(v['total_amount'],v['total_customers']))
                        total_customers = total_customers+v['total_customers']
                        gp_percent   = gp_percent+v['gp_percent']
                    year = v['year']
                        
                    if weekday == 7:
                        # Set weekend date
                        weekend_date = datetime.strptime(v['ty_date'], '%Y-%m-%d').date()
                        #weekend_date = weekend_date + timedelta(days=1)
                        weekend_date = weekend_date.strftime('%Y-%m-%d')
                        LY_weeks_count = LY_weeks_count+1
                        last_weekend_date = weekend_date
                        #weekend_date = v['ty_date'].encode("ascii")
                        temp_Sale_THAVGB_vs_LYAVGB_X2.append({ 'total_amount': total_amount,"total_cost":total_cost, "avg_basket":avg_basket, "total_customers":total_customers, "gp_percent":gp_percent, "week_end_date":weekend_date, "year":year})
                        graph_y2.append(avg_basket)
                        graph_x2.append(weekend_date.encode("ascii"))
                        weekday = 1
                        
                    else:
                        weekday = weekday+1
                        LY_lastweek_json = { 'total_amount': total_amount,"total_cost":total_cost, "avg_basket":avg_basket, "total_customers":total_customers, "gp_percent":gp_percent, "week_end_date":LY_last_weekend_date, "year":year}
                        LY_lastweek_amount = avg_basket

                if((TY_weeks_count-1) == LY_weeks_count) :
                    LY_weeks_count = LY_weeks_count+1

                if((TY_weeks_count == LY_weeks_count) and (LY_last_weekend_date <> last_weekend_date)):
                    temp_Sale_THAVGB_vs_LYAVGB_X2.append(LY_lastweek_json)
                    graph_y2.append(LY_lastweek_amount)
                    graph_x2.append(LY_last_weekend_date.encode("ascii"))

            # -- Get zone outlet count --
            stores_count = 1
            if(s_zone_id != '' ): 
                if(g_stores_count <> 0):
                    stores_count = g_stores_count
                else: 
                    Zone_outlet_res = pd.read_sql_query("SELECT count(zout_id) as stores_count from zone_outlet where zout_zone_id = '"+s_zone_id+"'",con=self.db)
                    Zone_outlet_res=Zone_outlet_res.groupby(['stores_count'])
                    for k, v in Zone_outlet_res:
                        temp_zone_split=v.to_dict(orient="records")
                        for v in temp_zone_split:
                            stores_count = v['stores_count']

        # -- For this year's secondary sales
        if(s_outlet_id != '' or s_zone_id != '') :
            if not TYCS_result:
                df_sale_THAVGB_vs_LYAVGB_query = "SELECT SUM("+tbl_prefix+"_amt)  as total_amount,SUM("+tbl_prefix+"_cost) as total_cost,SUM("+tbl_customer_col+") as total_customers,SUM("+tbl_prefix+"_gp_percent) as gp_percent,DATEADD(DAY, 8 - DATEPART(WEEKDAY, btt.trxt_date), CAST(btt.trxt_date AS DATE)) as week_end_date,trxt_year as year FROM "+tbl_name+" "+tbl_alais+" JOIN bi_trx_time btt ON btt.trxt_id = "+tbl_prefix+"_time_id WHERE "
                if s_zone_id != '' :
                    df_sale_THAVGB_vs_LYAVGB_query += " "+tbl_prefix+"_outlet_id  IN ( select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+s_zone_id+"' ) AND "
                elif s_outlet_id != '' :
                    df_sale_THAVGB_vs_LYAVGB_query += " "+tbl_prefix+"_outlet_id = '"+s_outlet_id+"' AND "
                if analysis_level == '2' :
                    df_sale_THAVGB_vs_LYAVGB_query += " "+tbl_prefix+"_department_id  = '"+analysis_index_id+"' AND "
                elif analysis_level == '3' :
                    df_sale_THAVGB_vs_LYAVGB_query += " "+tbl_prefix+"_department_id  = '"+analysis_department_id+"' AND "+tbl_prefix+"_commodity_id  = '"+analysis_index_id+"' AND "
                df_sale_THAVGB_vs_LYAVGB_query += " (btt.trxt_date BETWEEN '"+start_date+"' AND '"+end_date+"') GROUP BY DATEADD(DAY, 8 - DATEPART(WEEKDAY, btt.trxt_date), CAST(btt.trxt_date AS DATE)),btt.trxt_year ORDER BY week_end_date"

                df_sale_THAVGB_vs_LYAVGB = pd.read_sql_query(df_sale_THAVGB_vs_LYAVGB_query,con=self.db)
                # Get => Y-Axis!!
                df_sale_THAVGB_vs_LYAVGB=df_sale_THAVGB_vs_LYAVGB.groupby(['year'])
            else:
                df_sale_THAVGB_vs_LYAVGB = TYCS_result
            # print(list(df_sale_TYS_VS_LYS)) 

            for k, v in df_sale_THAVGB_vs_LYAVGB:
                temp_group_split=v.to_dict(orient="records")

                for v in temp_group_split:
                    # print(v['year'])
                    temp_Sale_THAVGB_vs_LYAVGB_Y.append({ 'total_amount': (np.divide(v['total_amount'],stores_count)),"total_cost":(np.divide(v['total_cost'],stores_count)), "avg_basket":(np.divide(v['total_amount'],v['total_customers'])/stores_count), "total_customers":(np.divide(v['total_customers'],stores_count)), "gp_percent":(np.divide(v['gp_percent'],stores_count)), "week_end_date":v['week_end_date'], "year":v['year']})     
                    # -- Y for 2018 --
                    graph_y3.append(np.divide(v['total_amount'],v['total_customers'])/stores_count) 
       
        # Set weekend date; if week days diffrence less then a week
        if len(graph_x1) == 1 :
            date_format = "%Y-%m-%d"
            a = datetime.strptime(start_date, date_format)
            b = datetime.strptime(end_date, date_format)
            delta = b - a
            day_diffrence = delta.days
            if day_diffrence <= 6 :
                # Set weekend date as end date
                graph_x1[0] = end_date

        graph_dictionary = []
        # Set marker name as combination of name and axis values
        for i in range(0,len(graph_x1)):
            # Prepare dictionary 
            myDict = {}
            # Handle exeption  
            try:
                pty_value = float(graph_y1[i])
            except IndexError:
                pty_value = 0
            try:
                ply_value = float(graph_y2[i])
            except IndexError:
                ply_value = 0
            try:
                sty_value = float(graph_y3[i])
            except IndexError:
                sty_value = 0
            myDict["weeks"] = graph_x1[i]
            myDict["pty_value"] = pty_value
            myDict["ply_value"] = ply_value
            myDict["sty_value"] = sty_value
            graph_dictionary.append(myDict)

        res = [graph_x1, graph_y1, graph_y2, graph_y3, graph_x2, graph_dictionary]
        return res

    @cache.memoize(timeout=timeout)
    def pg_scatterchart_TYC_vs_LYC(self):
        
        # -- For Graph X-Axis, Y-Axis, Custome Data for current Years. --
        temp_Sale_TYC_vs_LYC_X = []
        temp_Sale_TYC_vs_LYC_X2 = []
        temp_Sale_TYC_vs_LYC_Y = []
        
        # -- For Graph X-Axis, Y-Axis, Custome Data for current Years. --
        graph_x1 = []
        graph_x2 = []
        graph_y1 = []
        graph_y2 = []
        graph_y3 = []
        graph_text = []
        graph_customdata = []
        TY_weeks_count = 0
        LY_weeks_count = 0
        TY_week_start_date = ""
        LY_last_weekend_date = ""
        LY_first_weekend = ""
        LY_second_week_start_date = ""
        # Get drilldown level filter data; If exist
        analysis_level = '1'
        analysis_index_id = ''
        analysis_department_id = ''
        if request.args.get('level') is not None:
            analysis_level = request.args.get('level')
        if request.args.get('id') is not None:    
            analysis_index_id = request.args.get('id')
        if request.args.get('department_id') is not None:    
            analysis_department_id = request.args.get('department_id')
        #filter_data = redis_conn.hgetall("filter_data")
        # Get session filter data
        if 'filter_data' in session:
            filter_data = session['filter_data']
        # Set filter params
        p_outlet_id = ''
        p_zone_id = ''
        s_outlet_id = ''
        s_zone_id = ''
        start_date = ''
        end_date = ''
        comparison_period = 1
        if filter_data.get("p_outlet_id") != None and filter_data.get("p_outlet_id") != '':
            p_outlet_id = filter_data.get("p_outlet_id")
        
        if filter_data.get("p_zone_id") != None and filter_data.get("p_zone_id") != '':
            p_zone_id = filter_data.get("p_zone_id")
       
        if filter_data.get("s_outlet_id") != None and filter_data.get("s_outlet_id") != '':
            s_outlet_id = filter_data.get("s_outlet_id")
        
        if filter_data.get("s_zone_id") != None and filter_data.get("s_zone_id") != '':
            s_zone_id = filter_data.get("s_zone_id")
        
        if filter_data.get("start_date") != None and filter_data.get("start_date") != '' and filter_data.get("end_date") != None:
            start_date = filter_data.get("start_date")
            end_date = filter_data.get("end_date")

        if filter_data.get("comparison_period") != None and filter_data.get("comparison_period") != '':
            comparison_period = int(filter_data.get("comparison_period"))

        # return if date range empty
        if start_date == '' or end_date == '' :
            res = [graph_x1, graph_y1, graph_y2, graph_y3, graph_x2]
            return res

        # -- Set dates
        convt_startdate = datetime.strptime(start_date, '%Y-%m-%d').date()
        current_year = int(convt_startdate.strftime('%Y'))
        current_month = convt_startdate.strftime('%m')
        current_day = convt_startdate.strftime('%d')
        convt_enddate = datetime.strptime(end_date, '%Y-%m-%d').date()
        current_endmonth = convt_enddate.strftime('%m')
        current_endday = convt_enddate.strftime('%d')
        prev_year = current_year-comparison_period
        year_days = self.is_leap_year(prev_year)-1
        convt_prev_startdate = datetime.strptime(str(prev_year)+'-'+current_month+'-'+current_day, '%Y-%m-%d').date()
        convt_prev_enddate = datetime.strptime(str(prev_year)+'-'+current_endmonth+'-'+current_endday, '%Y-%m-%d').date()
        convt_startdate.strftime('%Y-%m-%d')
        LY_start_date = convt_prev_startdate.strftime('%Y-%m-%d')
        convt_enddate = datetime.strptime(end_date, '%Y-%m-%d').date()
        convt_enddate.strftime('%Y-%m-%d')
        LY_end_date = convt_prev_enddate.strftime('%Y-%m-%d')

        # -- For this year sales. --
        if(p_outlet_id != '' or p_zone_id != '') :
            # Set table, colomn name based on analysis level
            tbl_alais = 'btoa'
            tbl_prefix = 'btoa.btoa'
            tbl_name = 'bi_trx_outlet_aggregates'
            tbl_customer_col = 'btoa.btoa_customers'
            if analysis_level == '2' :
                tbl_alais = 'btda'
                tbl_prefix = 'btda.btda'
                tbl_name = 'bi_trx_department_aggregates'
                tbl_customer_col = 'btda.btda_customers_count'
            elif analysis_level == '3' :
                tbl_alais = 'btca'
                tbl_prefix = 'btca.btca'
                tbl_name = 'bi_trx_commodity_aggregates'
                tbl_customer_col = 'btca.btca_customers_count'

            if not TYS_result:
                df_sale_TYC_VS_LYC_query = "SELECT SUM("+tbl_prefix+"_amt) as total_amount,SUM("+tbl_prefix+"_cost) as total_cost, SUM("+tbl_customer_col+") as total_customers,SUM("+tbl_prefix+"_gp_percent) as gp_percent, DATEADD(DAY, 8 - DATEPART(WEEKDAY, btt.trxt_date), CAST(btt.trxt_date AS DATE)) as  week_end_date,trxt_year as year"
                if p_zone_id != '' :
                    df_sale_TYC_VS_LYC_query += ",(SELECT count(zout_id) FROM zone_outlet zo WHERE zout_zone_id = '"+p_zone_id+"') as total_stores"
                df_sale_TYC_VS_LYC_query += " FROM "+tbl_name+" "+tbl_alais+" JOIN bi_trx_time btt ON btt.trxt_id = "+tbl_prefix+"_time_id WHERE "  
                if p_outlet_id != '':
                    df_sale_TYC_VS_LYC_query += ""+tbl_prefix+"_outlet_id  = '"+p_outlet_id+"' AND "
                elif p_zone_id != '' :
                    df_sale_TYC_VS_LYC_query += ""+tbl_prefix+"_outlet_id  IN ( select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+p_zone_id+"' )  AND "
                if analysis_level == '2' :
                    df_sale_TYC_VS_LYC_query += " "+tbl_prefix+"_department_id  = '"+analysis_index_id+"' AND "
                elif analysis_level == '3' :
                    df_sale_TYC_VS_LYC_query += " "+tbl_prefix+"_department_id  = '"+analysis_department_id+"' AND "+tbl_prefix+"_commodity_id  = '"+analysis_index_id+"' AND "
                df_sale_TYC_VS_LYC_query += " ( btt.trxt_date BETWEEN '"+start_date+"' AND '"+end_date+"' ) GROUP BY DATEADD(DAY, 8 - DATEPART(WEEKDAY, btt.trxt_date), CAST(btt.trxt_date AS DATE)),btt.trxt_year ORDER BY week_end_date"    

                df_sale_TYC_VS_LYC = pd.read_sql_query(df_sale_TYC_VS_LYC_query,con=self.db)
                # Get => Y-Axis!!
                df_sale_TYC_VS_LYC=df_sale_TYC_VS_LYC.groupby(['year'])
            else:
                df_sale_TYC_VS_LYC = TYS_result    

            for k, v in df_sale_TYC_VS_LYC:
                temp_group_split=v.to_dict(orient="records")
                week_number = 1
                TY_weeks_count = len(temp_group_split)
                for v in temp_group_split:
                    TY_week_date = v['week_end_date']
                    if(week_number == 1):
                        # -- Set dates
                        convt_date = datetime.strptime(TY_week_date, '%Y-%m-%d').date()
                        current_year = int(convt_date.strftime('%Y'))
                        prev_year = current_year-comparison_period
                        year_days = self.is_leap_year(prev_year) 
                        LY_first_weekend = datetime.strptime(str(prev_year)+'-'+convt_date.strftime('%m')+'-'+convt_date.strftime('%d'), '%Y-%m-%d').date()
                        LY_second_week_start_date = LY_first_weekend + timedelta(days=1)
                        LY_second_week_start_date = datetime.strptime(str(prev_year)+'-'+LY_second_week_start_date.strftime('%m')+'-'+LY_second_week_start_date.strftime('%d'), '%Y-%m-%d').date()
                        LY_first_weekend = LY_first_weekend.strftime('%Y-%m-%d')
                        LY_second_week_start_date = LY_second_week_start_date.strftime('%Y-%m-%d')
                       
                    elif(TY_weeks_count == week_number):
                        
                        if(end_date != TY_week_date):
                            TY_week_date = end_date
                        # prepare dates
                        convt_date = datetime.strptime(TY_week_date, '%Y-%m-%d').date()
                        current_year = int(convt_date.strftime('%Y'))
                        prev_year = current_year-comparison_period
                        year_days = self.is_leap_year(prev_year)
                        LY_last_weekend_date = datetime.strptime(str(prev_year)+'-'+convt_date.strftime('%m')+'-'+convt_date.strftime('%d'), '%Y-%m-%d').date()
                        LY_last_weekend_date = LY_last_weekend_date.strftime('%Y-%m-%d')
                    # Set total stores count var 
                    total_stores = 1  
                    if p_zone_id != '' :
                        total_stores = v['total_stores']
                
                    temp_Sale_TYC_vs_LYC_X.append({ 'total_amount': (v['total_amount']/total_stores),"total_cost":(v['total_cost']/total_stores), "total_customers":(v['total_customers']/total_stores), "gp_percent":(v['gp_percent']/total_stores), "week_end_date":TY_week_date, "year":v['year']})         
                    # -- X for 2018 --
                    graph_x1.append(TY_week_date.encode("ascii"))                    
                    graph_y1.append(v['total_customers']/total_stores)
                    week_number = week_number+1

            # -- For last year sales. --
            df_sale_TYC_VS_LYC_query = "SELECT SUM("+tbl_prefix+"_amt) as total_amount,SUM("+tbl_prefix+"_cost) as total_cost,SUM("+tbl_customer_col+") as total_customers,SUM("+tbl_prefix+"_gp_percent) as gp_percent,trxt_year as year  "
            if p_zone_id != '' :
                df_sale_TYC_VS_LYC_query += ",(SELECT count(zout_id) FROM zone_outlet zo WHERE zout_zone_id = '"+p_zone_id+"') as total_stores"
            df_sale_TYC_VS_LYC_query += " FROM "+tbl_name+" "+tbl_alais+" JOIN bi_trx_time btt ON btt.trxt_id = "+tbl_prefix+"_time_id WHERE " 
            if p_outlet_id != '':
                df_sale_TYC_VS_LYC_query += ""+tbl_prefix+"_outlet_id  = '"+p_outlet_id+"' AND "
            elif p_zone_id != '' :
                df_sale_TYC_VS_LYC_query += ""+tbl_prefix+"_outlet_id  IN ( select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+p_zone_id+"' )  AND "
            if analysis_level == '2' :
                df_sale_TYC_VS_LYC_query += " "+tbl_prefix+"_department_id  = '"+analysis_index_id+"' AND "
            elif analysis_level == '3' :
                df_sale_TYC_VS_LYC_query += " "+tbl_prefix+"_department_id  = '"+analysis_department_id+"' AND "+tbl_prefix+"_commodity_id  = '"+analysis_index_id+"' AND "    
            df_sale_TYC_VS_LYC_query += " (btt.trxt_date BETWEEN '"+LY_start_date+"' AND '"+LY_first_weekend+"' ) GROUP BY btt.trxt_year"

            df_sale_TYC_VS_LYC = pd.read_sql_query(df_sale_TYC_VS_LYC_query,con=self.db)
            df_sale_TYC_VS_LYC=df_sale_TYC_VS_LYC.groupby(['year'])
            for k, v in df_sale_TYC_VS_LYC:
                temp_group_split=v.to_dict(orient="records")
                for v in temp_group_split:
                    LY_weeks_count = 1
                    year = v['year']
                    # Set total stores count var 
                    total_stores = 1  
                    if p_zone_id != '' :
                        total_stores = v['total_stores']
                    temp_Sale_TYC_vs_LYC_X2.append({ 'total_amount': (v['total_amount']/total_stores),"total_cost":(v['total_cost']/total_stores), "total_customers":(v['total_customers']/total_stores), "gp_percent":(v['gp_percent']/total_stores), "week_end_date":'"+LY_first_weekend+"', "year":year})
                    graph_y2.append(v['total_customers']/total_stores)
                    graph_x2.append(LY_first_weekend.encode("ascii")) 

            if p_outlet_id != '':
                df_sale_TYC_VS_LYC_query = "SELECT "+tbl_prefix+"_amt as total_amount,"+tbl_prefix+"_cost as total_cost, "+tbl_customer_col+" as total_customers,"+tbl_prefix+"_gp_percent as gp_percent, CAST(btt.trxt_date AS DATE) as ty_date,trxt_year as year FROM "+tbl_name+" "+tbl_alais+" JOIN bi_trx_time btt ON btt.trxt_id = "+tbl_prefix+"_time_id WHERE "+tbl_prefix+"_outlet_id  = '"+p_outlet_id+"' AND "
                if analysis_level == '2' :
                    df_sale_TYC_VS_LYC_query += " "+tbl_prefix+"_department_id  = '"+analysis_index_id+"' AND "
                elif analysis_level == '3' :
                    df_sale_TYC_VS_LYC_query += " "+tbl_prefix+"_department_id  = '"+analysis_department_id+"' AND "+tbl_prefix+"_commodity_id  = '"+analysis_index_id+"' AND "
                df_sale_TYC_VS_LYC_query += " ( btt.trxt_date BETWEEN '"+LY_second_week_start_date+"' AND '"+LY_end_date+"' )  ORDER BY btt.trxt_date ASC"

            elif p_zone_id != '' :
                df_sale_TYC_VS_LYC_query = "SELECT SUM("+tbl_prefix+"_amt) as total_amount,SUM("+tbl_prefix+"_cost) as total_cost, SUM("+tbl_customer_col+") as total_customers,SUM("+tbl_prefix+"_gp_percent) as gp_percent, CAST(btt.trxt_date AS DATE) as ty_date,trxt_year as year,(SELECT count(zout_id) FROM zone_outlet zo WHERE zout_zone_id = '"+p_zone_id+"') as total_stores FROM "+tbl_name+" "+tbl_alais+" JOIN bi_trx_time btt ON btt.trxt_id = "+tbl_prefix+"_time_id WHERE "+tbl_prefix+"_outlet_id  IN ( select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+p_zone_id+"' )  AND "
                if analysis_level == '2' :
                    df_sale_TYC_VS_LYC_query += " "+tbl_prefix+"_department_id  = '"+analysis_index_id+"' AND "
                elif analysis_level == '3' :
                    df_sale_TYC_VS_LYC_query += " "+tbl_prefix+"_department_id  = '"+analysis_department_id+"' AND "+tbl_prefix+"_commodity_id  = '"+analysis_index_id+"' AND "
                df_sale_TYC_VS_LYC_query += " ( btt.trxt_date BETWEEN '"+LY_second_week_start_date+"' AND '"+LY_end_date+"' ) GROUP BY CAST(btt.trxt_date AS DATE),trxt_year ORDER BY ty_date ASC" 

            df_sale_TYC_VS_LYC = pd.read_sql_query(df_sale_TYC_VS_LYC_query,con=self.db)
            # Get => X-Axis!!
            df_sale_TYC_VS_LYC=df_sale_TYC_VS_LYC.groupby(['year'])
            
            for k, v in df_sale_TYC_VS_LYC:
                temp_group_split=v.to_dict(orient="records")
                weekday = 1
                last_weekend_date = ''
                for v in temp_group_split:
                    if weekday == 1 :
                        total_amount = 0
                        total_cost = 0
                        avg_basket = 0
                        total_customers = 0
                        gp_percent = 0
                        year = ''
                    # Add total sales params    
                    if p_zone_id != '' :
                        total_amount = total_amount+(v['total_amount']/v['total_stores'])
                        total_cost   = total_cost+(v['total_cost']/v['total_stores'])
                        #avg_basket   = avg_basket+(v['avg_basket']/v['total_stores'])
                        total_customers = total_customers+(v['total_customers']/v['total_stores'])
                        gp_percent   = gp_percent+(v['gp_percent']/v['total_stores'])
                    else:
                        total_amount = total_amount+v['total_amount']
                        total_cost   = total_cost+v['total_cost']
                        #avg_basket   = avg_basket+v['avg_basket']
                        total_customers = total_customers+v['total_customers']
                        gp_percent   = gp_percent+v['gp_percent']
                    year = v['year']

                    if weekday == 7:
                        # Set weekend date
                        weekend_date = datetime.strptime(v['ty_date'], '%Y-%m-%d').date()
                        #weekend_date = weekend_date + timedelta(days=1)
                        weekend_date = weekend_date.strftime('%Y-%m-%d')
                        LY_weeks_count = LY_weeks_count+1
                        last_weekend_date = weekend_date
                        #weekend_date = v['ty_date'].encode("ascii")
                        temp_Sale_TYC_vs_LYC_X2.append({ 'total_amount': total_amount,"total_cost":total_cost, "avg_basket":avg_basket, "total_customers":total_customers, "gp_percent":gp_percent, "week_end_date":weekend_date, "year":year})
                        graph_y2.append(total_customers)
                        graph_x2.append(weekend_date.encode("ascii"))
                        weekday = 1
                        
                    else:
                        weekday = weekday+1
                        LY_lastweek_json = { 'total_amount': total_amount,"total_cost":total_cost, "avg_basket":avg_basket, "total_customers":total_customers, "gp_percent":gp_percent, "week_end_date":LY_last_weekend_date, "year":year}
                        LY_lastweek_amount = total_customers

                if((TY_weeks_count-1) == LY_weeks_count) :
                    LY_weeks_count = LY_weeks_count+1

                if((TY_weeks_count == LY_weeks_count) and (LY_last_weekend_date <> last_weekend_date)):
                    temp_Sale_TYC_vs_LYC_X2.append(LY_lastweek_json)
                    graph_y2.append(LY_lastweek_amount)
                    graph_x2.append(LY_last_weekend_date.encode("ascii"))

            # -- Get zone outlet count --
            stores_count = 1
            if(s_zone_id != '' ): 
                if(g_stores_count <> 0):
                    stores_count = g_stores_count
                else: 
                    Zone_outlet_res = pd.read_sql_query("SELECT count(zout_id) as stores_count from zone_outlet where zout_zone_id = '"+s_zone_id+"'",con=self.db)
                    Zone_outlet_res=Zone_outlet_res.groupby(['stores_count'])
                    for k, v in Zone_outlet_res:
                        temp_zone_split=v.to_dict(orient="records")
                        for v in temp_zone_split:
                            stores_count = v['stores_count']

        # -- For this year's secondary sales
        if(s_outlet_id != '' or s_zone_id != '') :
            if not TYCS_result:
                df_sale_sec_TYC_VS_LYC_query = "SELECT SUM("+tbl_prefix+"_amt)  as total_amount,SUM("+tbl_prefix+"_cost) as total_cost,SUM("+tbl_customer_col+") as total_customers,SUM("+tbl_prefix+"_gp_percent) as gp_percent,DATEADD(DAY, 8 - DATEPART(WEEKDAY, btt.trxt_date), CAST(btt.trxt_date AS DATE)) as week_end_date,trxt_year as year FROM "+tbl_name+" "+tbl_alais+" JOIN bi_trx_time btt ON btt.trxt_id = "+tbl_prefix+"_time_id WHERE "
                if s_zone_id != '' :
                    df_sale_sec_TYC_VS_LYC_query += " "+tbl_prefix+"_outlet_id  IN ( select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+s_zone_id+"' ) AND "
                elif s_outlet_id != '' :
                    df_sale_sec_TYC_VS_LYC_query += " "+tbl_prefix+"_outlet_id = '"+s_outlet_id+"' AND "
                if analysis_level == '2' :
                    df_sale_sec_TYC_VS_LYC_query += " "+tbl_prefix+"_department_id  = '"+analysis_index_id+"' AND "
                elif analysis_level == '3' :
                    df_sale_sec_TYC_VS_LYC_query += " "+tbl_prefix+"_department_id  = '"+analysis_department_id+"' AND "+tbl_prefix+"_commodity_id  = '"+analysis_index_id+"' AND "
                df_sale_sec_TYC_VS_LYC_query += " (btt.trxt_date BETWEEN '"+start_date+"' AND '"+end_date+"') GROUP BY DATEADD(DAY, 8 - DATEPART(WEEKDAY, btt.trxt_date), CAST(btt.trxt_date AS DATE)),btt.trxt_year ORDER BY week_end_date"

                df_sale_sec_TYC_VS_LYC = pd.read_sql_query(df_sale_sec_TYC_VS_LYC_query,con=self.db)
                # Get => Y-Axis!!
                df_sale_sec_TYC_VS_LYC=df_sale_sec_TYC_VS_LYC.groupby(['year'])
            else:
                df_sale_sec_TYC_VS_LYC = TYCS_result  
            # print(list(df_sale_TYS_VS_LYS)) 

            for k, v in df_sale_sec_TYC_VS_LYC:
                temp_group_split=v.to_dict(orient="records")

                for v in temp_group_split:
                    temp_Sale_TYC_vs_LYC_Y.append({ 'total_amount': (v['total_amount']/stores_count),"total_cost":(v['total_cost']/stores_count), "total_customers":(v['total_customers']/stores_count), "gp_percent":(v['gp_percent']/stores_count), "week_end_date":v['week_end_date'], "year":v['year']})        
                    # -- Y for 2018 --
                    graph_y3.append(v['total_customers']/stores_count)

        # Set weekend date; if week days diffrence less then a week
        if len(graph_x1) == 1 :
            date_format = "%Y-%m-%d"
            a = datetime.strptime(start_date, date_format)
            b = datetime.strptime(end_date, date_format)
            delta = b - a
            day_diffrence = delta.days
            if day_diffrence <= 6 :
                # Set weekend date as end date
                graph_x1[0] = end_date

        graph_dictionary = []
        # Set marker name as combination of name and axis values
        for i in range(0,len(graph_x1)):
            # Prepare dictionary 
            myDict = {}
            # Handle exeption  
            try:
                pty_value = float(graph_y1[i])
            except IndexError:
                pty_value = 0
            try:
                ply_value = float(graph_y2[i])
            except IndexError:
                ply_value = 0
            try:
                sty_value = float(graph_y3[i])
            except IndexError:
                sty_value = 0
            myDict["weeks"] = graph_x1[i]
            myDict["pty_value"] = pty_value
            myDict["ply_value"] = ply_value
            myDict["sty_value"] = sty_value
            graph_dictionary.append(myDict)

        res = [graph_x1, graph_y1, graph_y2, graph_y3, graph_x2, graph_dictionary]
        return res        

    def __repr__(self):
        return '<Book %r>' % (self.title) 
