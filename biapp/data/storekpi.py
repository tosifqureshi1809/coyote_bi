from flask import Flask, Response, g, request,session
from flask_caching import Cache
from flask_sqlalchemy import SQLAlchemy
import sqlalchemy
import pandas as pd
import numpy as np
import re
import base64
import json

# -- Important Assets STARTS --
from biapp.db import get_db
from biapp.app import server
from datetime import datetime, timedelta

app = Flask(__name__, instance_relative_config=True)
# -- Important Assets ENDS --
cache = Cache(server, config={'CACHE_TYPE': 'simple'})
timeout = 20

class storekpi():
   
    title = ""
    db = []

    @cache.memoize(timeout=timeout)
    def __init__(self):
        with server.app_context():        
            if 'db' not in g:
                self.db = get_db()
                g.db = self.db
    
    #@cache.memoize(timeout=timeout)
    def pg1_store_kpi_report(self):
        
        # -- For Graph X-Axis, Y-Axis for Outlets. --
        temp_wty_y = {}
        temp_wly_y = {}
        temp_w2y_y = {}
        graph_dictionary = []
        # Get session filter data if exist
        session_filters = self.get_session_filters()
        # Set filter parameters
        product_type = session_filters['product_type']
        f_department = session_filters['f_department']
        f_commodity = session_filters['f_commodity']
        f_group = session_filters['f_group']
        f_supplier = session_filters['f_supplier']
        f_manufacturer = session_filters['f_manufacturer']
        f_replicate_code = session_filters['f_replicate_code']
        f_promotion_code = session_filters['f_promotion_code']
        f_plu = session_filters['f_plu']
        location_type = session_filters['location_type']
        f_outlets = session_filters['f_outlets']
        f_am_zones = session_filters['f_am_zones']
        f_demo_zones = session_filters['f_demo_zones']
        f_region_zones = session_filters['f_region_zones']
        TY_start_date = session_filters['TY_start_date']
        TY_end_date = session_filters['TY_end_date']
        f_sort_type = session_filters['f_sort_type']
        product_condition = ''
        TY_total_sales_amount = 0
        TY_total_customers = 0
        LY_total_sales_amount = 0
        LY_total_customers = 0
        total_ty_cpc_val = 0
        total_ly_cpc_val = 0
        # Prepare department array in case no selection found
        if product_type == '1' and len(f_department) == 0 :
            f_department = []    
            res_sql = pd.read_sql_query("SELECT dept_id as id,dept as title,dept_id as code FROM department WHERE dept_is_default = 1 ORDER BY dept ASC",con=self.db)
            res_data=res_sql.groupby(['title'],sort=False)
            for k, v in res_data:
                temp_data_split=v.to_dict(orient="records")
                for v in temp_data_split:
                    f_department.append(v['id'])
        # Prepare location array in case no selection found
        if location_type == '1' and len(f_outlets) == 0 :
            f_outlets = []    
            loc_res_sql = pd.read_sql_query("SELECT outl_id as id ,outl_name as title,outl_id as code FROM bi_outlets WHERE outl_status = 'Active' and SubString(outl_name, 1, 2) not in ('ZZ','ZG','ZA','ZB','Z') ORDER BY outl_name ASC",con=self.db)
            loc_res_data=loc_res_sql.groupby(['title'])
            for k, v in loc_res_data:
                temp_data_split=v.to_dict(orient="records")
                for v in temp_data_split:
                    f_outlets.append(v['id'])

        # Set table, colomn name based on product type
        tbl_alais = 'btda'
        tbl_prefix = 'btda.btda'
        tbl_name = 'bi_trx_department_aggregates'
        tbl_customer_col = 'btda.btda_customers_count'
        tbl_product_type_child_field = 'btda.btda_department_id'
        tbl_product_type_child_val = f_department
        if product_type == '1' :  # Set data for department
            if len(f_department) > 0 :
                for i in range(0,len(f_department)):
                    product_condition += " "+tbl_prefix+"_department_id  = "+str(f_department[i])+" "
                    if i != (len(f_department)-1) :
                        product_condition += " OR "
        if product_type == '2' :  # Set data for commodity
            tbl_alais = 'btca'
            tbl_prefix = 'btca.btca'
            tbl_name = 'bi_trx_commodity_aggregates'
            tbl_customer_col = 'btca.btca_customers_count'
            tbl_product_type_child_field = 'btca.btca_commodity_id'
            tbl_product_type_child_val = f_commodity
            if len(f_commodity) > 0 :
                for i in range(0,len(f_commodity)):
                    product_condition += " "+tbl_prefix+"_commodity_id  = "+str(f_commodity[i])+" "
                    if i != (len(f_commodity)-1) :
                        product_condition += " OR "
        if product_type == '3' :  # Set data for group
            tbl_alais = 'btga'
            tbl_prefix = 'btga.btga'
            tbl_name = 'bi_trx_group_aggregates'
            tbl_customer_col = 'btga.btga_customers'
            tbl_product_type_child_field = 'btga.btga_group_id'
            tbl_product_type_child_val = f_group
            if len(f_group) > 0 :
                for i in range(0,len(f_group)):
                    product_condition += " "+tbl_prefix+"_group_id  = "+str(f_group[i])+" "
                    if i != (len(f_group)-1) :
                        product_condition += " OR "
        if product_type == '4' :  # Set data for product plu code
            tbl_alais = 'btpa'
            tbl_prefix = 'btpa.btpa'
            tbl_name = 'bi_trx_product_aggregates'
            tbl_customer_col = 'btpa.btpa_customers'
            tbl_product_type_child_field = 'btpa.btpa_product_id'
            tbl_product_type_child_val = f_plu 
            product_condition += " "+tbl_prefix+"_product_id  = "+f_plu+" "   
        if product_type == '5' and f_replicate_code != '' :  # Set data for replicate code
            tbl_alais = 'btra'
            tbl_prefix = 'btra.btra'
            tbl_name = 'bi_trx_replicate_code_aggregates'
            tbl_customer_col = 'btra.btra_customers'
            tbl_product_type_child_field = 'btra.btra_replicate_code'
            tbl_product_type_child_val = f_replicate_code    
            product_condition += " "+tbl_prefix+"_replicate_code  = "+f_replicate_code+" " 
        if product_type == '6' and f_promotion_code != '' :  # Set data for promotion
            tbl_alais = 'btpa'
            tbl_prefix = 'btpa.btpa'
            tbl_name = 'bi_trx_promo_aggregates'
            tbl_customer_col = 'btpa.btpa_customers'
            tbl_product_type_child_field = 'btpa.btpa_promo_code'
            tbl_product_type_child_val = f_promotion_code    
            product_condition += " "+tbl_prefix+"_promo_code  = "+f_promotion_code+" " 
        if product_type == '7' :  # Set data for supplier
            tbl_alais = 'btsa'
            tbl_prefix = 'btsa.btsa'
            tbl_name = 'bi_trx_supplier_aggregates'
            tbl_customer_col = 'btsa.btsa_customers'
            tbl_product_type_child_field = 'btsa.btsa_supplier_code'
            tbl_product_type_child_val = f_supplier
            if len(f_supplier) > 0 :
                for i in range(0,len(f_supplier)):
                    product_condition += " "+tbl_prefix+"_supplier_code  = '"+str(f_supplier[i])+"' "
                    if i != (len(f_supplier)-1) :
                        product_condition += " OR "
        if product_type == '8' :  # Set data for manufacturer
            tbl_alais = 'btma'
            tbl_prefix = 'btma.btma'
            tbl_name = 'bi_trx_manufacturer_aggregates'
            tbl_customer_col = 'btma.btma_customers'
            tbl_product_type_child_field = 'btma.btma_manufacturer_code'
            tbl_product_type_child_val = f_manufacturer
            if len(f_manufacturer) > 0 :
                for i in range(0,len(f_manufacturer)):
                    product_condition += " "+tbl_prefix+"_manufacturer_code  = '"+str(f_manufacturer[i])+"' "
                    if i != (len(f_manufacturer)-1) :
                        product_condition += " OR "       

        if (len(f_outlets) > 0 or len(f_am_zones) > 0 or len(f_demo_zones) > 0 or len(f_region_zones) > 0) and TY_start_date != '' and product_condition != '' : 
            # -- Set this year dates
            convt_startdate = datetime.strptime(TY_start_date, '%Y-%m-%d').date()
            current_year = int(convt_startdate.strftime('%Y'))
            current_month = convt_startdate.strftime('%m')
            current_day = convt_startdate.strftime('%d')

            convt_enddate = datetime.strptime(TY_end_date, '%Y-%m-%d').date()
            current_endmonth = convt_enddate.strftime('%m')
            current_endday = convt_enddate.strftime('%d')
            # -- Set last year dates
            prev_year = current_year-1
            convt_prev_startdate = datetime.strptime(str(prev_year)+'-'+current_month+'-'+current_day, '%Y-%m-%d').date()
            convt_prev_enddate = datetime.strptime(str(prev_year)+'-'+current_endmonth+'-'+current_endday, '%Y-%m-%d').date()

            LY_start_date = convt_prev_startdate.strftime('%Y-%m-%d')
            LY_end_date = convt_prev_enddate.strftime('%Y-%m-%d')
            # -- Set second last year dates
            prev_2lyear = current_year-2
            convt_2ly_startdate = datetime.strptime(str(prev_2lyear)+'-'+current_month+'-'+current_day, '%Y-%m-%d').date()
            convt_2ly_enddate = datetime.strptime(str(prev_2lyear)+'-'+current_endmonth+'-'+current_endday, '%Y-%m-%d').date()

            SLY_start_date = convt_2ly_startdate.strftime('%Y-%m-%d')
            SLY_end_date = convt_2ly_enddate.strftime('%Y-%m-%d')

            temp_wty_customers = {}
            temp_wly_customers = {}
            temp_w2y_customers = {}
            # Set outlet condition
            location_condition = ''
            if len(f_outlets) > 0 :
                for i in range(0,len(f_outlets)):
                    location_condition += " "+tbl_prefix+"_outlet_id  = "+str(f_outlets[i])+" "
                    if i != (len(f_outlets)-1) :
                        location_condition += " OR "

            elif len(f_am_zones) > 0 != '' :
                for i in range(0,len(f_am_zones)):
                    location_condition += ""+tbl_prefix+"_outlet_id  IN ( select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+str(f_am_zones[i])+"' ) "
                    if i != (len(f_am_zones)-1) :
                        location_condition += " OR "

            elif len(f_demo_zones) > 0 != '' :
                for i in range(0,len(f_demo_zones)):
                    location_condition += ""+tbl_prefix+"_outlet_id  IN ( select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+str(f_demo_zones[i])+"' ) "
                    if i != (len(f_demo_zones)-1) :
                        location_condition += " OR "

            elif len(f_region_zones) > 0 != '' :
                for i in range(0,len(f_region_zones)):
                    location_condition += ""+tbl_prefix+"_outlet_id  IN ( select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+str(f_region_zones[i])+"' ) "
                    if i != (len(f_region_zones)-1) :
                        location_condition += " OR "     
                
            # Prepare sql for fethcing outlet wise total customers count
            outl_customers_sql = "SELECT "+tbl_prefix+"_outlet_id as outlet_id,SUM("+tbl_customer_col+") as customers,btt.trxt_year as year FROM "+tbl_name+" "+tbl_alais+" JOIN bi_trx_time btt ON btt.trxt_id = "+tbl_prefix+"_time_id WHERE ("+location_condition+") AND ((btt.trxt_date BETWEEN '"+TY_start_date+"' AND '"+TY_end_date+"') OR (btt.trxt_date BETWEEN '"+LY_start_date+"' AND '"+LY_end_date+"') OR (btt.trxt_date BETWEEN '"+SLY_start_date+"' AND '"+SLY_end_date+"')) GROUP BY "+tbl_prefix+"_outlet_id,btt.trxt_year ORDER BY "+tbl_prefix+"_outlet_id"
            
            outl_customers_data = pd.read_sql_query(outl_customers_sql,con=self.db)
            outl_customers_data = outl_customers_data.groupby(['year'])
            for k, v in outl_customers_data:
                temp_group_split=v.to_dict(orient="records")
                for v in temp_group_split:
                    if (int(v['year']) == int(current_year)):
                        temp_wty_customers[v['outlet_id']]=v['customers']
                    elif (int(v['year']) == int(prev_year)):
                        temp_wly_customers[v['outlet_id']]=v['customers']
                    elif (int(v['year']) == int(prev_2lyear)):
                        temp_w2y_customers[v['outlet_id']]=v['customers']
            
            # Prepare sql for fetching this year sales records
            df_sale_query = "SELECT "+tbl_prefix+"_outlet_id as outlet_id,outl.outl_name,SUM("+tbl_prefix+"_amt) as total_amount,btt.trxt_year as year FROM "+tbl_name+" "+tbl_alais+" JOIN bi_trx_time btt ON btt.trxt_id = "+tbl_prefix+"_time_id JOIN bi_outlets outl ON outl.outl_id = "+tbl_prefix+"_outlet_id WHERE ("+product_condition+") AND ("+location_condition+") AND ( (btt.trxt_date BETWEEN '"+TY_start_date+"' AND '"+TY_end_date+"') OR (btt.trxt_date BETWEEN '"+LY_start_date+"' AND '"+LY_end_date+"') OR (btt.trxt_date BETWEEN '"+SLY_start_date+"' AND '"+SLY_end_date+"') ) GROUP BY "+tbl_prefix+"_outlet_id,outl.outl_name,btt.trxt_year ORDER BY "+tbl_prefix+"_outlet_id"
           
            df_sales_data = pd.read_sql_query(df_sale_query,con=self.db)
            df_sales_data = df_sales_data.groupby(['year'])
            for k, v in df_sales_data:
                temp_group_split=v.to_dict(orient="records")
                for v in temp_group_split:
                    outlet_id = v['outlet_id']
                    if (int(v['year']) == int(current_year)):
                        if outlet_id not in temp_wty_y:
                            temp_wty_y[outlet_id] = []
                        customers = temp_wty_customers[v['outlet_id']]
                        cpc_percent = np.divide(v['total_amount'],customers) 
                        temp_wty_y[outlet_id].append({'cpc_percent':cpc_percent,'outlet_id':v['outlet_id'],'outl_name':v['outl_name'],'customers':customers,'total_amount':v['total_amount']})
                        TY_total_sales_amount = TY_total_sales_amount+v['total_amount']
                        TY_total_customers = TY_total_customers+customers
                    elif (int(v['year']) == int(prev_year)):
                        if outlet_id not in temp_wly_y:
                            temp_wly_y[outlet_id] = []
                        customers = temp_wly_customers[v['outlet_id']]
                        cpc_percent = np.divide(v['total_amount'],customers)
                        temp_wly_y[outlet_id].append({'cpc_percent':cpc_percent,'outlet_id':v['outlet_id'],'outl_name':v['outl_name'],'customers':customers,'total_amount':v['total_amount']})
                        LY_total_sales_amount = LY_total_sales_amount+v['total_amount']
                        LY_total_customers = LY_total_customers+customers
                    elif (int(v['year']) == int(prev_2lyear)):
                        if outlet_id not in temp_w2y_y:
                            temp_w2y_y[outlet_id] = []
                        customers = temp_w2y_customers[v['outlet_id']]
                        cpc_percent = np.divide(v['total_amount'],customers)
                        temp_w2y_y[outlet_id].append({'cpc_percent':cpc_percent,'outlet_id':v['outlet_id'],'outl_name':v['outl_name'],'customers':customers,'total_amount':v['total_amount']})
            # Set this year and last cpc totals
            total_ty_cpc_val = np.multiply(np.divide(TY_total_sales_amount,TY_total_customers),100)
            total_ly_cpc_val = np.multiply(np.divide(LY_total_sales_amount,LY_total_customers),100)
            # Set marker name as combination of name and axis values
            #for i in range(0,len(temp_wty_y)):
            for v in temp_wty_y: 
                for i in temp_wty_y[v] :
                    # Handle exeption  
                    try:
                        wty_cpc_percent = np.multiply(i['cpc_percent'],100)
                    except IndexError:
                        wty_cpc_percent = 0
                    try:
                        if v in temp_wly_y :
                            wly_cpc_percent = np.multiply(temp_wly_y[v][0]['cpc_percent'],100)
                        else :
                            wly_cpc_percent = 0
                    except IndexError:
                        wly_cpc_percent = 0
                    try: 
                        if v in temp_w2y_y :
                            w2y_cpc_percent = np.multiply(temp_w2y_y[v][0]['cpc_percent'],100)
                        else :
                            w2y_cpc_percent = 0
                    except IndexError:
                        w2y_cpc_percent = 0
                    
                    # Prepare dictionary 
                    myDict = {}
                    myDict["outlets"] = i['outl_name'].encode("ascii")
                    myDict["wty_value"] = np.round(wty_cpc_percent, 2)
                    myDict["wly_value"] = np.round(wly_cpc_percent, 2)
                    myDict["w2y_value"] = np.round(w2y_cpc_percent, 2)
                    graph_dictionary.append(myDict)
            # Sort dictionary based on TY CPC    
            graph_dictionary = sorted(graph_dictionary, key=lambda dct: dct['wty_value'],reverse=f_sort_type)
        res = [graph_dictionary,temp_wty_y,temp_wly_y,temp_w2y_y,np.round(total_ty_cpc_val, 2),np.round(total_ly_cpc_val, 2)]
        return res

    #  --- Get filter data stored in session
    def get_session_filters(self):
        product_type = ''
        f_department = []
        f_commodity = []
        f_group = []
        f_supplier = []
        f_manufacturer = []
        f_replicate_code = ''
        f_promotion_code = ''
        f_plu = ''
        location_type =''
        f_outlets = []
        f_am_zones = []
        f_demo_zones = []
        f_region_zones = []
        TY_start_date = ''
        TY_end_date = ''
        f_sort_type = True

        # Get session filter data
        if 'kpi_filter_data' in session:
            kpi_filter_data = session['kpi_filter_data']
            # Get filter data; If exist
            if kpi_filter_data.get("product_type") != None:
                product_type = kpi_filter_data.get("product_type")

            if kpi_filter_data.get("departments") != None and kpi_filter_data.get("departments") != '':
                # Set selected departments 
                f_departments_json = kpi_filter_data.get("departments")     
                f_departments_array = json.loads(f_departments_json)
                for i in range(0,len(f_departments_array)):
                    f_department.append(int(f_departments_array[i].encode("ascii","replace")))

            if kpi_filter_data.get("commodity") != None and kpi_filter_data.get("commodity") != '':
                # Set selected commodity 
                f_commodity_json = kpi_filter_data.get("commodity")     
                f_commodity_array = json.loads(f_commodity_json)
                for i in range(0,len(f_commodity_array)):
                    f_commodity.append(int(f_commodity_array[i].encode("ascii","replace")))

            if kpi_filter_data.get("group") != None and kpi_filter_data.get("group") != '':
                # Set selected group 
                f_group_json = kpi_filter_data.get("group")     
                f_group_array = json.loads(f_group_json)
                for i in range(0,len(f_group_array)):
                    f_group.append(int(f_group_array[i].encode("ascii","replace")))

            if kpi_filter_data.get("supplier") != None and kpi_filter_data.get("supplier") != '':
                # Set selected supplier 
                f_supplier_json = kpi_filter_data.get("supplier")     
                f_supplier_array = json.loads(f_supplier_json)
                for i in range(0,len(f_supplier_array)):
                    f_supplier.append(str(f_supplier_array[i].encode("ascii","replace")))

            if kpi_filter_data.get("manufacturer") != None and kpi_filter_data.get("manufacturer") != '':
                # Set selected manufacturer 
                f_manufacturer_json = kpi_filter_data.get("manufacturer")     
                f_manufacturer_array = json.loads(f_manufacturer_json)
                for i in range(0,len(f_manufacturer_array)):
                    f_manufacturer.append(str(f_manufacturer_array[i].encode("ascii","replace")))

            if kpi_filter_data.get("replicate_code") != None and kpi_filter_data.get("replicate_code") != '':
                f_replicate_code = kpi_filter_data.get("replicate_code")

            if kpi_filter_data.get("promotion_code") != None and kpi_filter_data.get("promotion_code") != '':
                f_promotion_code = kpi_filter_data.get("promotion_code")

            if kpi_filter_data.get("plu") != None and kpi_filter_data.get("plu") != '':
                f_plu = kpi_filter_data.get("plu")                           

            if kpi_filter_data.get("location_type") != None and kpi_filter_data.get("location_type") != '':
                location_type = kpi_filter_data.get("location_type")

            if kpi_filter_data.get("outlets") != None and kpi_filter_data.get("outlets") != '':
                # Set selected outlets 
                f_outlets_json = kpi_filter_data.get("outlets")     
                f_outlets_array = json.loads(f_outlets_json)
                for i in range(0,len(f_outlets_array)):
                    f_outlets.append(int(f_outlets_array[i].encode("ascii","replace")))

            if kpi_filter_data.get("am_zones") != None and kpi_filter_data.get("am_zones") != '':
                # Set selected am zones 
                f_am_zones_json = kpi_filter_data.get("am_zones")   
                f_am_zones_array = json.loads(f_am_zones_json)
                for i in range(0,len(f_am_zones_array)):
                    f_am_zones.append(int(f_am_zones_array[i].encode("ascii","replace")))

            if kpi_filter_data.get("demo_zones") != None and kpi_filter_data.get("demo_zones") != '':
                # Set selected demo zones 
                f_demo_zones_json = kpi_filter_data.get("demo_zones")   
                f_demo_zones_array = json.loads(f_demo_zones_json)
                for i in range(0,len(f_demo_zones_array)):
                    f_demo_zones.append(int(f_demo_zones_array[i].encode("ascii","replace")))

            if kpi_filter_data.get("region_zones") != None and kpi_filter_data.get("region_zones") != '':
                # Set selected region zones 
                f_region_zones_json = kpi_filter_data.get("region_zones")    
                f_region_zones_array = json.loads(f_region_zones_json)
                for i in range(0,len(f_region_zones_array)):
                    f_region_zones.append(int(f_region_zones_array[i].encode("ascii","replace")))       
                    
            if kpi_filter_data.get("start_date") != None and kpi_filter_data.get("end_date") != None:
                TY_start_date = kpi_filter_data.get("start_date")
                TY_end_date = kpi_filter_data.get("end_date")

            if kpi_filter_data.get("sort_type") != None:
                sort_type = kpi_filter_data.get("sort_type")
                if sort_type == '2' :
                    f_sort_type = False
        # Set filter params
        filters = {}
        filters['product_type'] = product_type
        filters['f_department'] = f_department
        filters['f_commodity']  = f_commodity
        filters['f_group']  = f_group
        filters['f_supplier']  = f_supplier
        filters['f_manufacturer']  = f_manufacturer
        filters['f_replicate_code']  = f_replicate_code
        filters['f_promotion_code']  = f_promotion_code
        filters['f_plu']  = f_plu
        filters['location_type'] = location_type
        filters['f_outlets'] = f_outlets
        filters['f_am_zones'] = f_am_zones
        filters['f_demo_zones'] = f_demo_zones
        filters['f_region_zones'] = f_region_zones
        filters['TY_start_date'] = TY_start_date
        filters['TY_end_date'] = TY_end_date
        filters['f_sort_type'] = f_sort_type       
        return filters


    #  --- Check if the int given year is a leap year: return true if leap year or false otherwise
    @cache.memoize(timeout=timeout)
    def is_leap_year(self,year):
        leap_year_days = 366
        non_leap_year_days = 365
        if (year % 4) == 0:
            if (year % 100) == 0:
                if (year % 400) == 0:
                    return leap_year_days
                else:
                    return non_leap_year_days
            else:
                return leap_year_days
        else:
            return non_leap_year_days    



    def __repr__(self):
        return '<Book %r>' % (self.title) 
    