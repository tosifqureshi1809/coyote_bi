from flask import Flask, Response, g, request,session
from flask_caching import Cache
from flask_sqlalchemy import SQLAlchemy
import sqlalchemy
import pandas as pd
import numpy as np
import re
import base64
import json
import time
from datetime import datetime, timedelta
# -- Important Assets STARTS --
from biapp.db import get_db,get_livedb
from biapp.app import server

app = Flask(__name__, instance_relative_config=True)
# -- Important Assets ENDS --
cache = Cache(server, config={'CACHE_TYPE': 'simple'})
timeout = 20

class dashboard():
   
    title = ""
    db = []

    @cache.memoize(timeout=timeout)
    def __init__(self):
        with server.app_context():        
            if 'db' not in g:
                self.db = get_db()
                g.db = self.db
            if 'live_db' not in g:
                self.live_db = get_livedb()
                g.live_db = self.live_db        
    
    # -- Manage user's sales data and prepare that for dashboard charts
    @cache.memoize(timeout=timeout)
    def pg1_dashboard_data(self,filter_date):
       
        user_id = str(44)
        # Get user's outlet records
        user_data = self.get_outlets(user_id)
        user_outlets = user_data['user_outlets']
        user_role = user_data['user_role']
        # Get open stores data
        store_data = self.get_open_n_close_stores(user_outlets,filter_date)
        # Get total gross sales
        gross_sales_amount= self.get_gross_sales(user_outlets,filter_date)
        ty_total_sales_amount = 0
        ly_total_sales_amount = 0
        if gross_sales_amount[0] > 0 :
            ty_total_sales_amount = gross_sales_amount[0]
        if gross_sales_amount[1] > 0 :
            ly_total_sales_amount = gross_sales_amount[1]
        
        # Prepare dictionary
        myDict = {}
        myDict["total_stores"] = len(user_outlets)
        myDict["ty_total_sales_amount"] = round(np.divide(ty_total_sales_amount,1000000), 2)
        myDict["ly_total_sales_amount"] = round(np.divide(ly_total_sales_amount,1000000), 2)
        myDict["ty_total_customers"] = round(np.divide(store_data['ty_total_customers'],1000000), 2) if (store_data['ty_total_customers'] > 0) else 0
        myDict["ly_total_customers"] = round(np.divide(store_data['ly_total_customers'],1000000), 2) if (store_data['ly_total_customers'] > 0) else 0
        myDict["ty_avg_item_per_customer"] = store_data['ty_avg_item_per_customer']
        myDict["ly_avg_item_per_customer"] = store_data['ly_avg_item_per_customer']
        myDict["ty_avg_spend_per_customer"] = store_data['ty_avg_spend_per_customer']
        myDict["ly_avg_spend_per_customer"] = store_data['ly_avg_spend_per_customer']
        myDict["group_sales"] = self.group_sales(filter_date)
        myDict["demo_sales"] = self.demo_sales(filter_date)
        myDict["region_sales"] = self.region_sales(filter_date)
        myDict["rtm_sales"] = self.rtm_sales(filter_date)
        myDict["ty_total_avg_item_per_customer"] = round(np.divide(store_data['ty_total_qty'],store_data['ty_total_customers']), 2)
        myDict["ly_total_avg_item_per_customer"] = round(np.divide(store_data['ly_total_qty'],store_data['ly_total_customers']), 2)

        res = [myDict]
        return res
    
    # Get user's outlet records
    def get_outlets(self,user_id):
        user_outlets = []
        user_outlet = ''
        user_zone = ''   
        user_role = 'USER'
        # Get user basic details
        user_res = pd.read_sql_query("SELECT id,outlet,zone from bi_user where user_number = "+user_id+"",con=self.db)

        for v in user_res.to_dict(orient="records"):
            user_outlet = v['outlet']
            #user_zone = v['zone']
            user_zone = 'ALL' # Set temperary zone untill system not follow user account 
            # Get outlet records
            if user_outlet != None and user_outlet != '' :
                user_outlets.append(user_outlet)
            elif user_zone != None and user_zone != '' :
                zone_outlet_res = pd.read_sql_query("SELECT zo.zout_outlet_id as outlet_id from zone_outlet zo join zone z on z.zone_id = zo.zout_zone_id where z.zone_code = '"+user_zone+"'",con=self.db)
                zone_outlet_res=zone_outlet_res.groupby(['outlet_id'])
                for k, v in zone_outlet_res:
                    temp_zone_split=v.to_dict(orient="records")
                    for v in temp_zone_split:
                        user_outlets.append(v['outlet_id'])
            else :
                # Fetch user's outlet sales
                outlet_res = pd.read_sql_query("SELECT outl_id as outlet_id from bi_outlets where 	outl_status = 'Active' and SubString(outl_name, 1, 2) not in ('ZZ','ZG','ZA') order by outl_name asc",con=self.db)
                outlet_res=outlet_res.groupby(['outlet_id'])
                for k, v in outlet_res:
                    temp_outlet_split=v.to_dict(orient="records")
                    for v in temp_outlet_split:
                        user_outlets.append(v['outlet_id'])
                # Set user role        
                user_role = 'ADMIN'        
        # Prepare dictionary 
        user_data = {}
        user_data["user_role"] = user_role
        user_data["user_outlets"] = user_outlets                
        return user_data
    
    # Get open and close stores sales log
    def get_open_n_close_stores(self,outlets,filter_date):
        ty_total_customers = 0
        ly_total_customers = 0
        ty_avg_item_per_customer = 0
        ly_avg_item_per_customer = 0
        ty_avg_spend_per_customer = 0
        ly_avg_spend_per_customer = 0
        ty_total_qty = 0
        ly_total_qty = 0
       
        # Get sales customers
        # Get sales customers
        customer_query = "SELECT SUM(btoa.btda_customers_count) as total_customers,SUM(btoa.btda_qty) as total_qty,1 as result_type FROM bi_trx_department_aggregates btoa JOIN bi_trx_time btt ON btt.trxt_id = btoa.btda_time_id WHERE btt.trxt_date  >= CONVERT(date, DATEADD(day,-364, '"+filter_date+"')) AND btt.trxt_date  <= '"+filter_date+"' AND ("
        # Set outlet condition
        for i in range(0,len(outlets)):
            customer_query += " btoa.btda_outlet_id = "+str(outlets[i])+" "
            if i != (len(outlets)-1) :
                customer_query += " OR "
        customer_query += ") UNION SELECT SUM(btoa.btda_customers_count) as total_customers,SUM(btoa.btda_qty) as total_qty,2 as result_type FROM bi_trx_department_aggregates btoa JOIN bi_trx_time btt ON btt.trxt_id = btoa.btda_time_id WHERE btt.trxt_date >= CONVERT(date, DATEADD(day,-728, '"+filter_date+"')) AND btt.trxt_date <= CONVERT(date, DATEADD(day,-365, '"+filter_date+"')) AND ("
        # Set outlet condition
        for i in range(0,len(outlets)):
            customer_query += " btoa.btda_outlet_id  = "+str(outlets[i])+" "
            if i != (len(outlets)-1) :
                customer_query += " OR "
        customer_query += ")"
        customer_sql = pd.read_sql_query(customer_query,con=self.db)
        for v in customer_sql.to_dict(orient="records"):
            #convert amount in Million
            if v['result_type'] == 1 :
                ty_total_customers = v['total_customers']
                ty_total_qty = v['total_qty']
            elif v['result_type'] == 2 :
                ly_total_customers = v['total_customers']
                ly_total_qty = v['total_qty']

        # Get average sales counts
        sales_query = "SELECT ROUND(SUM(btoa.btda_amt),2) as total_amount,ROUND(SUM(btoa.btda_qty),2) as total_qty,SUM(btoa.btda_customers_count) as total_customers,1 as result_type FROM bi_trx_department_aggregates btoa JOIN bi_trx_time btt ON btt.trxt_id = btoa.btda_time_id WHERE btt.trxt_date  >= CONVERT(date, DATEADD(day,-364, '"+filter_date+"')) AND btt.trxt_date  <= '"+filter_date+"' AND ("
        # Set outlet condition
        for i in range(0,len(outlets)):
            sales_query += " btoa.btda_outlet_id = "+str(outlets[i])+" "
            if i != (len(outlets)-1) :
                sales_query += " OR "
        sales_query += ") UNION SELECT ROUND(SUM(btoa.btda_amt),2) as total_amount,ROUND(SUM(btoa.btda_qty),2) as total_qty,SUM(btoa.btda_customers_count) as total_customers,2 as result_type FROM bi_trx_department_aggregates btoa JOIN bi_trx_time btt ON btt.trxt_id = btoa.btda_time_id WHERE btt.trxt_date  >= CONVERT(date, DATEADD(day,-728, '"+filter_date+"')) AND btt.trxt_date <= CONVERT(date, DATEADD(day,-365, '"+filter_date+"')) AND ("
        # Set outlet condition
        for i in range(0,len(outlets)):
            sales_query += " btoa.btda_outlet_id  = "+str(outlets[i])+" "
            if i != (len(outlets)-1) :
                sales_query += " OR "
        sales_query += ")"

        sales_sql = pd.read_sql_query(sales_query,con=self.db)
        for v in sales_sql.to_dict(orient="records"):
            #convert amount in Million
            if v['total_amount'] > 0 :
                if v['result_type'] == 1 :
                    ty_avg_item_per_customer = round(np.divide(v['total_qty'],v['total_customers']), 2)
                    ty_avg_spend_per_customer = round(np.divide(v['total_amount'],v['total_customers']), 2)
                elif v['result_type'] == 2 :
                    ly_avg_item_per_customer = round(np.divide(v['total_qty'],v['total_customers']), 2)
                    ly_avg_spend_per_customer = round(np.divide(v['total_amount'],v['total_customers']), 2)

        
        # Prepare dictionary 
        store_data = {}
        store_data["ty_total_customers"] = ty_total_customers
        store_data["ty_total_qty"] = ty_total_qty
        store_data["ly_total_qty"] = ly_total_qty
        store_data["ly_total_customers"] = ly_total_customers  
        store_data["ty_avg_item_per_customer"] = ty_avg_item_per_customer 
        store_data["ly_avg_item_per_customer"] = ly_avg_item_per_customer   
        store_data["ty_avg_spend_per_customer"] = ty_avg_spend_per_customer 
        store_data["ly_avg_spend_per_customer"] = ly_avg_spend_per_customer     
        return store_data


    # Get user's total sales for this year
    def get_gross_sales(self,outlets,filter_date):
        ty_total_amount = 0
        ly_total_amount = 0
        # Fetch outlets total sales for this year
        if len(outlets) > 0 :
            outlet_query = "SELECT SUM(btoa.btda_amt) as total_amount,1 as result_type FROM bi_trx_department_aggregates btoa JOIN bi_trx_time btt ON btt.trxt_id = btoa.btda_time_id WHERE btt.trxt_date  >= CONVERT(date, DATEADD(day,-364, '"+filter_date+"')) AND btt.trxt_date  <= '"+filter_date+"' AND ("
            # Set outlet condition
            for i in range(0,len(outlets)):
                outlet_query += " btoa.btda_outlet_id  = "+str(outlets[i])+" "
                if i != (len(outlets)-1) :
                    outlet_query += " OR "
            outlet_query += ") UNION SELECT SUM(btoa.btda_amt) as total_amount,2 as result_type FROM bi_trx_department_aggregates btoa JOIN bi_trx_time btt ON btt.trxt_id = btoa.btda_time_id WHERE btt.trxt_date  >= CONVERT(date, DATEADD(day,-728, '"+filter_date+"')) AND btt.trxt_date  <= CONVERT(date, DATEADD(day,-365, '"+filter_date+"')) AND ("
            # Set outlet condition
            for i in range(0,len(outlets)):
                outlet_query += " btoa.btda_outlet_id  = "+str(outlets[i])+" "
                if i != (len(outlets)-1) :
                    outlet_query += " OR "
            outlet_query += ")"
            outlet_res = pd.read_sql_query(outlet_query,con=self.db)
            for v in outlet_res.to_dict(orient="records"):
                if v['result_type'] == 1 :
                    ty_total_amount = v['total_amount']
                elif v['result_type'] == 2 :
                    ly_total_amount = v['total_amount']    
        return [ty_total_amount,ly_total_amount]

    # Get demographic this year & last year sales
    def demo_sales(self,filter_date):
        temp_demos = []
        temp_ly_demos_sales = []
        temp_ty_demos_sales = []
        
        # Set demo wise sales
        demo_query = "SELECT SUM(btoa.btda_amt) as total_amount,z.zone_id,z.zone_code,z.zone_label as label,1 as zone_type FROM bi_trx_department_aggregates btoa JOIN bi_trx_time btt ON btt.trxt_id = btoa.btda_time_id JOIN bi_outlets bo ON bo.outl_id = btoa.btda_outlet_id JOIN zone_outlet zo ON zo.zout_outlet_id = btoa.btda_outlet_id JOIN zone z ON z.zone_id = zo.zout_zone_id WHERE btt.trxt_date  >= CONVERT(date, DATEADD(day,-364, '"+filter_date+"')) AND btt.trxt_date  <= '"+filter_date+"' AND z.zone_label LIKE '%demo%' AND btda_outlet_id IN ( select distinct(zo.zout_outlet_id) FROM zone_outlet zo JOIN zone z ON (zo.zout_zone_id = z.zone_id and z.zone_label LIKE '%demo%')) GROUP BY z.zone_id,z.zone_code,z.zone_label UNION SELECT SUM(btoa.btda_amt) as total_amount,z.zone_id,z.zone_code,z.zone_label as label,2 as zone_type FROM bi_trx_department_aggregates btoa JOIN bi_trx_time btt ON btt.trxt_id = btoa.btda_time_id JOIN bi_outlets bo ON bo.outl_id = btoa.btda_outlet_id JOIN zone_outlet zo ON zo.zout_outlet_id = btoa.btda_outlet_id JOIN zone z ON z.zone_id = zo.zout_zone_id WHERE btt.trxt_date  >= CONVERT(date, DATEADD(day,-728, '"+filter_date+"')) AND btt.trxt_date <= CONVERT(date, DATEADD(day,-365, '"+filter_date+"')) AND z.zone_label LIKE '%demo%' AND btda_outlet_id IN ( select distinct(zo.zout_outlet_id) FROM zone_outlet zo JOIN zone z ON (zo.zout_zone_id = z.zone_id and z.zone_label LIKE '%demo%')) GROUP BY z.zone_id,z.zone_code,z.zone_label ORDER BY total_amount DESC"
        
        df_demos_ty = pd.read_sql_query(demo_query,con=self.db)
        df_demos_ty=df_demos_ty.groupby(['label'])
        for k, v in df_demos_ty:
            temp_demos_split=v.to_dict(orient="records")
            for v in temp_demos_split:
                zone_lbl = v['label'].encode("ascii")
                zone_lbl = zone_lbl.split(' ', 1)[1]
                if zone_lbl not in temp_demos :
                    temp_demos.append(zone_lbl)

                if v['zone_type'] == 1 :
                    temp_ty_demos_sales.append(round(np.divide(v['total_amount'],1000000), 2))
                if v['zone_type'] == 2 :  
                    temp_ly_demos_sales.append(round(np.divide(v['total_amount'],1000000), 2))

        graph_dictionary = []
        total_ty_value = 0
        total_ly_value = 0
        # Set marker name as combination of name and axis values
        for i in range(0,len(temp_demos)):
            # Handle exeption  
            try:
                ty_value = float(temp_ty_demos_sales[i])
            except IndexError:
                ty_value = 0
            try:
                ly_value = float(temp_ly_demos_sales[i])
            except IndexError:
                ly_value = 0
            # Set this and last year total value
            total_ty_value =  total_ty_value+ty_value
            total_ly_value =  total_ly_value+ly_value
            # Prepare dictionary 
            regionDict = {}
            regionDict["demos"] = temp_demos[i]
            regionDict["ty_value"] = ty_value
            regionDict["ly_value"] = ly_value
            graph_dictionary.append(regionDict)
        
        return [graph_dictionary,total_ty_value,total_ly_value] 

    # Get rtm this year sales
    def rtm_sales(self,filter_date):
        temp_rtm = []
        temp_ty_rtm_sales = []
        temp_ly_rtm_sales = []
        # Set rtm wise sales
        rtm_query = "SELECT SUM(btoa.btda_amt) as total_amount,z.zone_id,z.zone_code,z.zone_label as label,1 as zone_type FROM bi_trx_department_aggregates btoa JOIN bi_trx_time btt ON btt.trxt_id = btoa.btda_time_id JOIN bi_outlets bo ON bo.outl_id = btoa.btda_outlet_id JOIN zone_outlet zo ON zo.zout_outlet_id = btoa.btda_outlet_id JOIN zone z ON z.zone_id = zo.zout_zone_id WHERE btt.trxt_date  >= CONVERT(date, DATEADD(day,-364, '"+filter_date+"')) AND btt.trxt_date  <= '"+filter_date+"' AND z.zone_label LIKE '%rtm%' AND btda_outlet_id IN ( select distinct(zo.zout_outlet_id) FROM zone_outlet zo JOIN zone z ON (zo.zout_zone_id = z.zone_id and z.zone_label LIKE '%rtm%')) GROUP BY z.zone_id,z.zone_code,z.zone_label UNION SELECT SUM(btoa.btda_amt) as total_amount,z.zone_id,z.zone_code,z.zone_label as label,2 as zone_type FROM bi_trx_department_aggregates btoa JOIN bi_trx_time btt ON btt.trxt_id = btoa.btda_time_id JOIN bi_outlets bo ON bo.outl_id = btoa.btda_outlet_id JOIN zone_outlet zo ON zo.zout_outlet_id = btoa.btda_outlet_id JOIN zone z ON z.zone_id = zo.zout_zone_id WHERE btt.trxt_date  >= CONVERT(date, DATEADD(day,-728, '"+filter_date+"')) AND btt.trxt_date <= CONVERT(date, DATEADD(day,-365, '"+filter_date+"')) AND z.zone_label LIKE '%rtm%' AND btda_outlet_id IN ( select distinct(zo.zout_outlet_id) FROM zone_outlet zo JOIN zone z ON (zo.zout_zone_id = z.zone_id and z.zone_label LIKE '%rtm%')) GROUP BY z.zone_id,z.zone_code,z.zone_label ORDER BY total_amount DESC"
        #print(rtm_query)
        df_rtm_ty = pd.read_sql_query(rtm_query,con=self.db)
        df_rtm_ty=df_rtm_ty.groupby(['label'])
        for k, v in df_rtm_ty:
            temp_rtm_split=v.to_dict(orient="records")
            for v in temp_rtm_split:
                zone_lbl = v['label'].encode("ascii")
                zone_lbl = zone_lbl.split(' ', 2)[2]
                if zone_lbl not in temp_rtm :
                    temp_rtm.append(zone_lbl)
                if v['zone_type'] == 1 :
                    temp_ty_rtm_sales.append(round(np.divide(v['total_amount'],1000000), 2))
                if v['zone_type'] == 2 :  
                    temp_ly_rtm_sales.append(round(np.divide(v['total_amount'],1000000), 2))

        graph_dictionary = []
        category_dictionary = []
        ty_region_dict = {}
        ly_region_dict = {}
        total_ty_value = 0
        total_ly_value = 0
        # Set marker name as combination of name and axis values
        for i in range(0,len(temp_rtm)):
            # Handle exeption  
            try:
                ty_value = float(temp_ty_rtm_sales[i])
            except IndexError:
                ty_value = 0
            try:
                ly_value = float(temp_ly_rtm_sales[i])
            except IndexError:
                ly_value = 0
            # Set this and last year total value
            total_ty_value =  total_ty_value+ty_value
            total_ly_value =  total_ly_value+ly_value
            # Prepare dictionary 
            rtm_lbl = temp_rtm[i].replace(" ", "-") 
            ty_region_dict[rtm_lbl] = ty_value
            ly_region_dict[rtm_lbl] = ly_value
            category_dict = {}
            category_dict['lbl_key'] = rtm_lbl
            category_dict['lbl_value'] = temp_rtm[i]
            category_dictionary.append(category_dict)

        
        ly_region_dict['year'] = 'Previous Year'
        ty_region_dict['year'] = 'This Year'
        graph_dictionary.append(ly_region_dict)
        graph_dictionary.append(ty_region_dict)
        
        
        return [graph_dictionary,category_dictionary,total_ty_value,total_ly_value] 

    # Get region this year & last year sales
    def region_sales(self,filter_date):
        temp_regions = []
        temp_ly_region_sales = []
        temp_ty_region_sales = []
        # Set region wise sales
        region_query = "SELECT SUM(btoa.btda_amt) as total_amount,z.zone_id,z.zone_code,z.zone_label as label,1 as zone_type FROM bi_trx_department_aggregates btoa JOIN bi_trx_time btt ON btt.trxt_id = btoa.btda_time_id JOIN bi_outlets bo ON bo.outl_id = btoa.btda_outlet_id JOIN zone_outlet zo ON zo.zout_outlet_id = btoa.btda_outlet_id JOIN zone z ON z.zone_id = zo.zout_zone_id WHERE btt.trxt_date  >= CONVERT(date, DATEADD(day,-364, '"+filter_date+"')) AND btt.trxt_date  <= '"+filter_date+"' AND z.zone_label LIKE '%region%' AND z.zone_code LIKE '%RGN%' AND btda_outlet_id IN ( select distinct(zo.zout_outlet_id) FROM zone_outlet zo JOIN zone z ON (zo.zout_zone_id = z.zone_id and z.zone_label LIKE '%region%' AND z.zone_code LIKE '%RGN%')) GROUP BY z.zone_id,z.zone_code,z.zone_label UNION SELECT SUM(btoa.btda_amt) as total_amount,z.zone_id,z.zone_code,z.zone_label as label,2 as zone_type FROM bi_trx_department_aggregates btoa JOIN bi_trx_time btt ON btt.trxt_id = btoa.btda_time_id JOIN bi_outlets bo ON bo.outl_id = btoa.btda_outlet_id JOIN zone_outlet zo ON zo.zout_outlet_id = btoa.btda_outlet_id JOIN zone z ON z.zone_id = zo.zout_zone_id WHERE btt.trxt_date  >= CONVERT(date, DATEADD(day,-728, '"+filter_date+"')) AND btt.trxt_date <= CONVERT(date, DATEADD(day,-365, '"+filter_date+"')) AND z.zone_label LIKE '%region%' AND z.zone_code LIKE '%RGN%' AND btda_outlet_id IN ( select distinct(zo.zout_outlet_id) FROM zone_outlet zo JOIN zone z ON (zo.zout_zone_id = z.zone_id and z.zone_label LIKE '%region%' AND z.zone_code LIKE '%RGN%')) GROUP BY z.zone_id,z.zone_code,z.zone_label ORDER BY z.zone_code ASC"
        
        df_region_ty = pd.read_sql_query(region_query,con=self.db)
        df_region_ty=df_region_ty.groupby(['label'])
        for k, v in df_region_ty:
            temp_region_split=v.to_dict(orient="records")
            for v in temp_region_split:
                zone_lbl = v['label'].encode("ascii")
                zone_lbl = zone_lbl.split(' ', 1)[1]
                if zone_lbl not in temp_regions :
                    temp_regions.append(zone_lbl)

                if v['zone_type'] == 1 :
                    temp_ty_region_sales.append(round(np.divide(v['total_amount'],1000000), 2))
                if v['zone_type'] == 2 :  
                    temp_ly_region_sales.append(round(np.divide(v['total_amount'],1000000), 2))

        graph_dictionary = []
        total_ty_value =  0
        total_ly_value =  0
        # Set marker name as combination of name and axis values
        for i in range(0,len(temp_regions)):
            # Handle exeption  
            try:
                ty_value = float(temp_ty_region_sales[i])
            except IndexError:
                ty_value = 0
            try:
                ly_value = float(temp_ly_region_sales[i])
            except IndexError:
                ly_value = 0
            # Set this and last year total value
            total_ty_value =  total_ty_value+ty_value
            total_ly_value =  total_ly_value+ly_value
            # Prepare dictionary 
            regionDict = {}
            regionDict["regions"] = temp_regions[i]
            regionDict["ly_value"] = ly_value
            regionDict["ty_value"] = ty_value
            
            graph_dictionary.append(regionDict)

        return [graph_dictionary,total_ty_value,total_ly_value] 

    # Get groups this year sales
    def group_sales(self,filter_date):
        temp_groups = []
        temp_ly_group_sales = []
        temp_ty_group_sales = []
        # Set group wise sales
        group_query = "SELECT TOP 6 SUM(btga.btga_amt) as total_amount,btga.btga_group_name as label,1 as result_type FROM bi_trx_group_aggregates btga JOIN bi_trx_time btt ON btt.trxt_id = btga.btga_time_id WHERE btt.trxt_date  >= CONVERT(date, DATEADD(day,-364, '"+filter_date+"')) AND btt.trxt_date  <= '"+filter_date+"' GROUP BY btga.btga_group_name UNION SELECT TOP 6 SUM(btga.btga_amt) as total_amount,btga.btga_group_name as label,2 as result_type FROM bi_trx_group_aggregates btga JOIN bi_trx_time btt ON btt.trxt_id = btga.btga_time_id WHERE btt.trxt_date  >= CONVERT(date, DATEADD(day,-728, '"+filter_date+"')) AND btt.trxt_date <= CONVERT(date, DATEADD(day,-365, '"+filter_date+"')) GROUP BY btga.btga_group_name ORDER BY label DESC"
        df_group_ty = pd.read_sql_query(group_query,con=self.db)
        df_group_ty=df_group_ty.groupby(['label'])
        for k, v in df_group_ty:
            temp_group_split=v.to_dict(orient="records")
            for v in temp_group_split:
                group_lbl = v['label'].encode("ascii")
                if group_lbl not in temp_groups :
                    temp_groups.append(group_lbl)

                if v['result_type'] == 1 :
                    temp_ty_group_sales.append(round(np.divide(v['total_amount'],1000000), 2))
                if v['result_type'] == 2 :  
                    temp_ly_group_sales.append(round(np.divide(v['total_amount'],1000000), 2))

        graph_dictionary = []
        total_ty_value =  0
        total_ly_value =  0
        # Set marker name as combination of name and axis values
        for i in range(0,len(temp_groups)):
            # Handle exeption  
            try:
                ty_value = float(temp_ty_group_sales[i])
            except IndexError:
                ty_value = 0
            try:
                ly_value = float(temp_ly_group_sales[i])
            except IndexError:
                ly_value = 0
            # Set this and last year total value
            total_ty_value =  total_ty_value+ty_value
            total_ly_value =  total_ly_value+ly_value
            # Prepare dictionary 
            groupDict = {}
            groupDict["groups"] = temp_groups[i]
            groupDict["ty_value"] = ty_value
            groupDict["ly_value"] = ly_value
            graph_dictionary.append(groupDict)

        return [graph_dictionary,total_ty_value,total_ly_value]     

    # Get stores this year and last year sales
    def ty_ly_outlet_sales_dictionary(self,user_outlets):

        temp_ty_sales = []
        temp_ly_sales = []
        temp_months = []
        current_year = time.strftime("%Y")
        prev_year = str(int(current_year)-1)
        total_outlets = len(user_outlets)
        # Get outlets this and prevoise year sales
        outlet_query = "SELECT SUM(btoa.btoa_amt) as total_amount,SUM(btoa.btoa_cost) as total_cost,SUM(btoa.btoa_avg_basket) as avg_basket,SUM(btoa.btoa_customers) as total_customers,SUM(btoa.btoa_gp_percent) as gp_percent,trxt_month as month,trxt_year as year FROM bi_trx_outlet_aggregates btoa JOIN bi_trx_time btt ON btt.trxt_id = btoa.btoa_time_id WHERE ( btt.trxt_year = '"+current_year+"' OR btt.trxt_year = '"+prev_year+"' ) AND ( "
        # Set outlet condition
        for i in range(0,len(user_outlets)):
            outlet_query += " btoa.btoa_outlet_id  = "+str(user_outlets[i])+" "
            if i != (len(user_outlets)-1) :
                outlet_query += " OR "
        outlet_query += ") GROUP BY btt.trxt_month,btt.trxt_year ORDER BY month,year"
    
        df_outlet_ty = pd.read_sql_query(outlet_query,con=self.db)
        df_outlet_ty=df_outlet_ty.groupby(['year'])
        for k, v in df_outlet_ty:
            temp_outlet_split=v.to_dict(orient="records")
            for v in temp_outlet_split:
                if int(v['year']) == int(current_year) :
                    temp_ty_sales.append({'total_amount':v['total_amount'],'gp_percent':v['gp_percent'],'total_customers':v['total_customers']})
                elif int(v['year']) == int(prev_year) :  
                    temp_ly_sales.append({'total_amount':v['total_amount'],'gp_percent':v['gp_percent'],'total_customers':v['total_customers']})
                    temp_months.append(int(v['month']))
        # Prepare data based on graph types
        graph_sales_dictionary = []
        graph_gp_dictionary = []
        graph_avgbasket_dictionary = []
        graph_customers_dictionary = []
        # Set marker name as combination of name and axis values
        for i in range(0,len(temp_ly_sales)):
             
            # Set last year values
            ly_sale_val = np.divide(float(temp_ly_sales[i]['total_amount']),total_outlets)
            ly_gp_val = np.divide(float(temp_ly_sales[i]['gp_percent']),total_outlets)
            ly_avgbsk_val = np.divide(float(temp_ly_sales[i]['total_amount']),v['total_customers'])/total_outlets
            ly_customer_val = np.divide(temp_ly_sales[i]['total_customers'],total_outlets)
            # Handle exeption for this year values  
            try:
                ty_sale_val = np.divide(float(temp_ty_sales[i]['total_amount']),total_outlets)
                ty_gp_val = np.divide(float(temp_ty_sales[i]['gp_percent']),total_outlets)
                ty_avgbsk_val = np.divide(float(temp_ty_sales[i]['total_amount']),v['total_customers'])/total_outlets
                ty_customer_val = np.divide(temp_ty_sales[i]['total_customers'],total_outlets)
            except IndexError:
                ty_sale_val = 0
                ty_gp_val = 0
                ty_avgbsk_val = 0
                ty_customer_val = 0
            try:
                month_val = float(temp_months[i])
            except IndexError:
                month_val = 0
            # Prepare sales dictionary 
            sales_data = {}
            sales_data["months"] = datetime.date(1900, int(month_val), 1).strftime('%B') 
            sales_data["ty_sales"] = float(ty_sale_val)
            sales_data["ly_sales"] = float(ly_sale_val)
            graph_sales_dictionary.append(sales_data)
            # Prepare gp dictionary 
            gp_data = {}
            gp_data["months"] = datetime.date(1900, int(month_val), 1).strftime('%B') 
            gp_data["ty_gp"] = float(ty_gp_val)
            gp_data["ly_gp"] = float(ly_gp_val)
            graph_gp_dictionary.append(gp_data)
            # Prepare average basket dictionary 
            avgbkt_data = {}
            avgbkt_data["months"] = datetime.date(1900, int(month_val), 1).strftime('%B') 
            avgbkt_data["ty_avgbkt"] = float(ty_avgbsk_val)
            avgbkt_data["ly_avgbkt"] = float(ly_avgbsk_val)
            graph_avgbasket_dictionary.append(avgbkt_data)
            # Prepare cpc dictionary 
            cpc_data = {}
            cpc_data["months"] = datetime.date(1900, int(month_val), 1).strftime('%B') 
            cpc_data["ty_customers"] = float(ty_customer_val)
            cpc_data["ly_customers"] = float(ly_customer_val)
            graph_customers_dictionary.append(cpc_data)

        return [graph_sales_dictionary,graph_gp_dictionary,graph_avgbasket_dictionary,graph_customers_dictionary]

    # Prepare chart data based on zone selection
    def dash_inner_chart_data(self):
        user_id = str(44)
        #user_id = str(79201)
        # Set input request param
        zone_id = ''
        if request.args.get('zone_id') is not None:
            zone_id = request.args.get('zone_id')
         
        # Define data set array 
        temp_ty_sales = []
        temp_ly_sales = []
        temp_months = []
        graph_sales_dictionary = []
        graph_gp_dictionary = []
        graph_avgbasket_dictionary = []
        graph_customers_dictionary = []
        
        current_year = time.strftime("%Y")
        prev_year = str(int(current_year)-1)
        total_outlets = 0
        user_outlets = ''
        # Get total outlets from zone
        if zone_id != '' :
            z_outlet_query = "SELECT count(zout_id) as total_outlets FROM zone_outlet zo WHERE zout_zone_id = '"+zone_id+"'"
            z_outlet_res = pd.read_sql_query(z_outlet_query,con=self.db)
            for v in z_outlet_res.to_dict(orient="records"):
                total_outlets = int(v['total_outlets'])
        else : 
            # Get user's outlet records
            user_data = self.get_outlets(user_id)
            user_outlets = user_data['user_outlets']
            total_outlets = len(user_outlets)
     
        # Get session filter data
        if 'dashboard_filters' in session:
            dashboard_filters = session['dashboard_filters']
        # Set filter params
        period = '1'
        if dashboard_filters.get("period") != None and dashboard_filters.get("period") != '':
            period = dashboard_filters.get("period")
        days_frame = 5
        if period == '2' :
            days_frame = 27
            #import datetime
            # Get outlets this and prevoise year sales
            #outlet_query = "SELECT SUM(btoa.btoa_amt) as total_amount,SUM(btoa.btoa_cost) as total_cost,SUM(btoa.btoa_avg_basket) as avg_basket,SUM(btoa.btoa_customers) as total_customers,SUM(btoa.btoa_gp_percent) as gp_percent,trxt_month as month,trxt_year as year FROM bi_trx_outlet_aggregates btoa JOIN bi_trx_time btt ON btt.trxt_id = btoa.btoa_time_id WHERE ( btt.trxt_year = '"+current_year+"' OR btt.trxt_year = '"+prev_year+"' ) AND "
            #if zone_id != '' :
            #    outlet_query += " btoa.btoa_outlet_id IN ( select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+zone_id+"' )"
            #else :
            #    outlet_query += " ( "
                # Set outlet condition
            #    for i in range(0,len(user_outlets)):
            #        outlet_query += " btoa.btoa_outlet_id  = "+str(user_outlets[i])+" "
            #        if i != (len(user_outlets)-1) :
            #            outlet_query += " OR "
            #    outlet_query += " ) "
            
            #outlet_query += " GROUP BY btt.trxt_month,btt.trxt_year ORDER BY month,year"
            
            #df_outlet_ty = pd.read_sql_query(outlet_query,con=self.db)
            #df_outlet_ty=df_outlet_ty.groupby(['year'])
            #for k, v in df_outlet_ty:
            #    temp_outlet_split=v.to_dict(orient="records")
            #    for v in temp_outlet_split:
            #        if int(v['year']) == int(current_year) :
            #            temp_ty_sales.append({'total_amount':v['total_amount'],'gp_percent':v['gp_percent'],'total_customers':v['total_customers']})
            #        elif int(v['year']) == int(prev_year) :  
            #            temp_ly_sales.append({'total_amount':v['total_amount'],'gp_percent':v['gp_percent'],'total_customers':v['total_customers']})
            #            temp_months.append(int(v['month']))
        elif period == '3' :
            days_frame = 89
                 
        # -- For Graph X-Axis, Y-Axis, Custome Data for current Years. --
        graph_x1 = []
        graph_x2 = []

        start_date = '2019-01-01'
        end_date = '2019-04-05'
        comparison_period = 1
        from datetime import datetime, timedelta
        # -- Set dates
        convt_startdate = datetime.strptime(start_date, '%Y-%m-%d').date()
        current_year = int(convt_startdate.strftime('%Y'))
        current_month = convt_startdate.strftime('%m')
        current_day = convt_startdate.strftime('%d')

        convt_enddate = datetime.strptime(end_date, '%Y-%m-%d').date()
        current_endmonth = convt_enddate.strftime('%m')
        current_endday = convt_enddate.strftime('%d')
        endday = convt_enddate.strftime('%a')
        
        prev_year = current_year-comparison_period
        year_days = self.is_leap_year(prev_year)-1
    
        convt_prev_startdate = datetime.strptime(str(prev_year)+'-'+current_month+'-'+current_day, '%Y-%m-%d').date()
        convt_prev_enddate = datetime.strptime(str(prev_year)+'-'+current_endmonth+'-'+current_endday, '%Y-%m-%d').date()
    
        LY_start_date = convt_prev_startdate.strftime('%Y-%m-%d')
        LY_end_date = convt_prev_enddate.strftime('%Y-%m-%d')

        # -- Append this year sales data in XY graph set
        temp_ty_sales = self.filter_till_weekend_sales(start_date,end_date,user_outlets,zone_id,total_outlets,days_frame)
        for tys in temp_ty_sales :
            # -- X for current year --
            temp_months.append(tys['week_end_date'].encode("ascii"))                 
        
        # -- Append last year sales data in XY graph set
        temp_ly_sales = self.filter_till_weekend_sales(LY_start_date,LY_end_date,user_outlets,zone_id,total_outlets,days_frame)
        
        for i in range(0,len(temp_ly_sales)):

            # Set last year values
            ly_sale_val = np.divide(float(temp_ly_sales[i]['total_amount']),total_outlets)
            ly_gp_val = np.divide(float(temp_ly_sales[i]['gp_percent']),total_outlets)
            ly_avgbsk_val = np.divide(float(temp_ly_sales[i]['total_amount']),temp_ly_sales[i]['total_customers'])/total_outlets
            ly_customer_val = np.divide(temp_ly_sales[i]['total_customers'],total_outlets)
            # Handle exeption for this year values  
            try:
                ty_sale_val = np.divide(float(temp_ty_sales[i]['total_amount']),total_outlets)
                ty_gp_val = np.divide(float(temp_ty_sales[i]['gp_percent']),total_outlets)
                ty_avgbsk_val = np.divide(float(temp_ty_sales[i]['total_amount']),temp_ty_sales[i]['total_customers'])/total_outlets
                ty_customer_val = np.divide(temp_ty_sales[i]['total_customers'],total_outlets)
            except IndexError:
                ty_sale_val = 0
                ty_gp_val = 0
                ty_avgbsk_val = 0
                ty_customer_val = 0
            try:
                
                month_val = temp_months[i]
                #if period == '2' :
                #    month_val = float(temp_months[i])
                #    month_val = datetime.date(1900, int(month_val), 1).strftime('%B') 
               
            except IndexError:
                month_val = ''
            # Prepare sales dictionary 
            sales_data = {}
            sales_data["months"] = month_val 
            sales_data["ty_sales"] = round(ty_sale_val, 2)
            sales_data["ly_sales"] = round(ly_sale_val, 2)
            graph_sales_dictionary.append(sales_data)
            # Prepare gp dictionary 
            gp_data = {}
            gp_data["months"] = month_val 
            gp_data["ty_gp"] = round(ty_gp_val, 2)
            gp_data["ly_gp"] = round(ly_gp_val, 2)
            graph_gp_dictionary.append(gp_data)
            # Prepare average basket dictionary 
            avgbkt_data = {}
            avgbkt_data["months"] = month_val
            avgbkt_data["ty_avgbkt"] = round(ty_avgbsk_val, 2)
            avgbkt_data["ly_avgbkt"] = round(ly_avgbsk_val, 2)
            graph_avgbasket_dictionary.append(avgbkt_data)
            # Prepare cpc dictionary 
            cpc_data = {}
            cpc_data["months"] = month_val 
            cpc_data["ty_customers"] = round(float(ty_customer_val), 2)
            cpc_data["ly_customers"] = round(float(ly_customer_val), 2)
            graph_customers_dictionary.append(cpc_data)
    
        return [graph_sales_dictionary,graph_gp_dictionary,graph_avgbasket_dictionary,graph_customers_dictionary]

    # -- Function used to manage weekwise sales records 
    def filter_till_weekend_sales(self,start_date,end_date,user_outlets='',zone_id=0,total_stores=0,days_frame=6) :
       
        # get day wise sales data
        ty_sales_array = self.sales_aggregate_calculation(start_date,end_date,user_outlets,zone_id)

        weekend_sales = {}
        # manage week wise sales data
        if len(ty_sales_array) > 0 :
            total_amount = 0
            total_cost = 0
            total_customers = 0
            total_gp_percent = 0
            weekend_date = ''
            weekend_dates = ''
            day_count = 0
            day_process_count = 0
            for v in ty_sales_array:
                # -- Set dates
                vsf_trading_date = v['vsf_trading_date']
                if day_count == days_frame or day_process_count == 0:
                    convt_date  = datetime.strptime(vsf_trading_date, '%Y-%m-%d').date()
                    convt_year  = int(convt_date.strftime('%Y'))
                    convt_month = int(convt_date.strftime('%m'))
                    convt_day   = int(convt_date.strftime('%d'))
                    
                    weekend_date = self.get_next_weekdate(convt_year, convt_month, convt_day, days_frame)
                    weekend_date = weekend_date.strftime('%Y-%m-%d')
                    
                    day_count = 0
                else :
                    day_count = day_count+1    
                
                day_process_count = day_process_count+1
                
                # append product sales data
                if weekend_date not in weekend_sales:
                    weekend_sales[weekend_date] = []
                # append searched end date as weekend date
                if vsf_trading_date == end_date :
                    weekend_sales[end_date] = []
                    
                if weekend_date == vsf_trading_date or vsf_trading_date == end_date:
                    # add total sales attributes   
                    total_amount    = total_amount+v['total_amount']
                    total_cost      = total_cost+v['total_cost']
                    total_customers = total_customers+v['total_customers']
                    total_gp_percent= total_gp_percent+v['gp_percent']
                    # append searched end date as weekend date
                    if vsf_trading_date == end_date :
                        convt_end_date = datetime.strptime(end_date, '%Y-%m-%d').date()
                        weekend_date = convt_end_date.strftime('%Y-%m-%d')
                    # prepare journal values for sales array department wise  
                    weekend_sales[weekend_date].append({'week_end_date':weekend_date,'total_customers':total_customers,'total_amount':total_amount,'total_cost':total_cost,'gp_percent':total_gp_percent,"year":convt_year,"total_stores":total_stores})
                    #reset sale values as 0
                    total_amount     = 0
                    total_cost       = 0
                    total_customers  = 0
                    total_gp_percent = 0
                else :
                    # add total sales attributes   
                    total_amount    = total_amount+v['total_amount']
                    total_cost      = total_cost+v['total_cost']
                    total_customers = total_customers+v['total_customers']
                    total_gp_percent= total_gp_percent+v['gp_percent']

        final_array = []
        for k in weekend_sales :
            for j in weekend_sales[k] :
                if 'total_amount' in j:      
                    # append this year department sales value in array
                    final_array.append({'week_end_date':j['week_end_date'],'total_customers':(np.divide(j['total_customers'],total_stores)),'total_amount':(np.divide(j['total_amount'],total_stores)),'total_cost':(np.divide(j['total_cost'],total_stores)),'avg_basket':(np.divide(np.divide(j['total_amount'],j['total_customers']),total_stores)),'gp_percent':(((j['total_amount'] - j['total_cost']) / j['total_amount'])*100),'year':j['year'],'total_stores':total_stores})
       
        # sort by weekend date
        if len(final_array) > 0 :
            final_array =  sorted(final_array, key = lambda i: i['week_end_date'])   

        return final_array

    # --- Function used to manage sales filter sales
    def sales_aggregate_calculation(self,start_date,end_date,user_outlets='',zone_id=''):
        
        # Prepare outlet level sql for fetching this year sales records    
        df_sale_query = "SELECT sum(btoa.btoa_amt) as total_amount,sum(btoa.btoa_cost) as total_cost, sum(btoa.btoa_customers) as total_customers,(((sum(btoa.btoa_amt) - sum(btoa.btoa_cost)) / sum(btoa.btoa_amt))*100) as gp_percent,btt.trxt_date,btt.trxt_year as year  FROM bi_trx_outlet_aggregates btoa JOIN bi_trx_time btt ON btt.trxt_id = btoa.btoa_time_id WHERE "
        if zone_id != '' :
            df_sale_query += " btoa.btoa_outlet_id IN ( select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+zone_id+"' )"
        else :
            df_sale_query += " ( "
            # Set outlet condition
            for i in range(0,len(user_outlets)):
                df_sale_query += " btoa.btoa_outlet_id  = "+str(user_outlets[i])+" "
                if i != (len(user_outlets)-1) :
                    df_sale_query += " OR "
            df_sale_query += " ) "

        df_sale_query += " AND ( btt.trxt_date BETWEEN '"+start_date+"' AND '"+end_date+"' ) GROUP BY btt.trxt_date,btt.trxt_year ORDER BY btt.trxt_date" 
        
        ty_df_sales_result = pd.read_sql_query(df_sale_query,con=self.db)
        ty_df_sales_result=ty_df_sales_result.groupby(['trxt_date'])
        # manage this year sales aggragate data
        ty_sales_array = self.manage_yearly_aggregate_sales(ty_df_sales_result)
        
        final_array = []
        for k in ty_sales_array :
            for v in k :
                for j in k[v] :
                   
                    # append this year department sales value in array
                    final_array.append({'vsf_trading_date':j['vsf_trading_date'],'total_amount':j['vsf_dep_sales_amt'],'total_customers':j['vsf_dep_customers_count'],'total_cost':j['vsf_dep_sales_cost'],'gp_percent':(((j['vsf_dep_sales_amt'] - j['vsf_dep_sales_cost']) / j['vsf_dep_sales_amt'])*100),'total_stores':0})
        # sort by purchase date
        if len(final_array) > 0 :
            final_array =  sorted(final_array, key = lambda i: i['vsf_trading_date'])
           
        return final_array

    # Manage yearly sales record
    def manage_yearly_aggregate_sales(self,df_sales_result):
       
        # initialize department product sales array
        department_array = {}
        for k, v in df_sales_result:
            temp_group_split=v.to_dict(orient="records")
            for v in temp_group_split:
                # set department label
                TRADING_DATE = v['trxt_date']
                TRADING_DATE = TRADING_DATE.strftime('%Y-%m-%d')
                # append product sales data in department index
                if TRADING_DATE not in department_array:
                    department_array[TRADING_DATE] = []
                # set total store count
                total_stores = 1
                if 'total_stores' in v:
                    total_stores = v['total_stores']
                # prepare journal values for sales array department wise  
                department_array[TRADING_DATE].append({'total_amount':v['total_amount'],'total_cost':v['total_cost'],'total_customers':v['total_customers'],'gp_percent':v['gp_percent'],'YEAR':v['year'],'TRADING_DATE':TRADING_DATE,'total_stores':total_stores})
               
        department_sales_array = []
        trx_numbers = []    
        # prepare department sales report result
        for i in department_array:
        
            # initialize default values
            vsf_dep_amt = 0
            vsf_dep_cost = 0
            vsf_dep_customers = 0
            last_trx_no = 0
            dept_data = []
            for v in department_array[i] :
              
                # add sales values
                vsf_dep_amt 		= v['total_amount']
                vsf_dep_cost 		= v['total_cost']
                vsf_dep_customers   = v['total_customers']
                vsf_trading_date    = v['TRADING_DATE']
                total_stores        = v['total_stores']
                
            # append sales value in array
            dept_data.append({'vsf_dep_sales_cost':vsf_dep_cost,'vsf_dep_sales_amt':vsf_dep_amt,'vsf_dep_customers_count':vsf_dep_customers,'vsf_trading_date':vsf_trading_date,'total_stores':total_stores})
            # append result in department array
            department_sales_array.append({i:dept_data})
            
        return department_sales_array
    
    # --- Function used to get next week date from given date
    def get_next_weekdate(self,year, month, day, days_frame=7):
        import datetime
        date0 = datetime.date(year, month, day)
        #next_week = date0 + datetime.timedelta(6 - date0.weekday() or 6)
        next_week = date0 + datetime.timedelta(days=days_frame)
        return next_week

    # Check if the int given year is a leap year
    # return true if leap year or false otherwise
    @cache.memoize(timeout=timeout)
    def is_leap_year(self,year):
        leap_year_days = 366
        non_leap_year_days = 365
        if (year % 4) == 0:
            if (year % 100) == 0:
                if (year % 400) == 0:
                    return leap_year_days
                else:
                    return non_leap_year_days
            else:
                return leap_year_days
        else:
            return non_leap_year_days    

    # Prepare chart data based on zone selection
    def dash_inner_chart_data_temp(self):
        import datetime
        user_id = str(44)
        # Set input request param
        zone_id = ''
        if request.args.get('zone_id') is not None:
            zone_id = request.args.get('zone_id')
         
        # Define data set array 
        temp_ty_sales = []
        temp_ly_sales = []
        temp_2ly_sales = []
        temp_months = []
        graph_sales_dictionary = []
        
        current_year = time.strftime("%Y")
        prev_year = str(int(current_year)-1)
        second_prev_year = str(int(current_year)-2)
        total_outlets = 0
        # Get total outlets from zone
        if zone_id != '' :
            z_outlet_query = "SELECT count(zout_id) as total_outlets FROM zone_outlet zo WHERE zout_zone_id = '"+zone_id+"'"
            z_outlet_res = pd.read_sql_query(z_outlet_query,con=self.db)
            for v in z_outlet_res.to_dict(orient="records"):
                total_outlets = int(v['total_outlets'])
        else : 
            # Get user's outlet records
            user_data = self.get_outlets(user_id)
            user_outlets = user_data['user_outlets']
            total_outlets = len(user_outlets)
                
        # Get outlets this and prevoise year sales
        outlet_query = "SELECT SUM(btoa.btoa_amt) as total_amount,SUM(btoa.btoa_cost) as total_cost,SUM(btoa.btoa_avg_basket) as avg_basket,SUM(btoa.btoa_customers) as total_customers,SUM(btoa.btoa_gp_percent) as gp_percent,trxt_month as month,trxt_year as year FROM bi_trx_outlet_aggregates btoa JOIN bi_trx_time btt ON btt.trxt_id = btoa.btoa_time_id WHERE ( btt.trxt_year = '"+current_year+"' OR btt.trxt_year = '"+prev_year+"' OR btt.trxt_year = '"+second_prev_year+"' ) AND "
        if zone_id != '' :
            outlet_query += " btoa.btoa_outlet_id IN ( select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+zone_id+"' )"
        else :
            outlet_query += " ( "
            # Set outlet condition
            for i in range(0,len(user_outlets)):
                outlet_query += " btoa.btoa_outlet_id  = "+str(user_outlets[i])+" "
                if i != (len(user_outlets)-1) :
                    outlet_query += " OR "
            outlet_query += " ) "
        
        outlet_query += " GROUP BY btt.trxt_month,btt.trxt_year ORDER BY month,year"
        
        df_outlet_ty = pd.read_sql_query(outlet_query,con=self.db)
        df_outlet_ty=df_outlet_ty.groupby(['year'])
        for k, v in df_outlet_ty:
            temp_outlet_split=v.to_dict(orient="records")
            for v in temp_outlet_split:
                if int(v['year']) == int(current_year) :
                    temp_ty_sales.append({'total_amount':v['total_amount'],'gp_percent':v['gp_percent'],'total_customers':v['total_customers']})
                elif int(v['year']) == int(prev_year) :  
                    temp_ly_sales.append({'total_amount':v['total_amount'],'gp_percent':v['gp_percent'],'total_customers':v['total_customers']})
                    temp_months.append(int(v['month']))
                elif int(v['year']) == int(second_prev_year) :  
                    temp_2ly_sales.append({'total_amount':v['total_amount'],'gp_percent':v['gp_percent'],'total_customers':v['total_customers']})      
        
        # Set marker name as combination of name and axis values
        for i in range(0,len(temp_ly_sales)):
            
            # Set last year values
            ly_sale_val = np.divide(float(temp_ly_sales[i]['total_amount']),total_outlets)
            
            # Handle exeption for this year values  
            try:
                ty_sale_val = np.divide(float(temp_ty_sales[i]['total_amount']),total_outlets)
            except IndexError:
                ty_sale_val = 0
            try:
                sly_sale_val = np.divide(float(temp_2ly_sales[i]['total_amount']),total_outlets)
            except IndexError:
                sly_sale_val = 0       
            try:
                month_val = float(temp_months[i])
            except IndexError:
                month_val = 0
            # Prepare sales dictionary 
            sales_data = {}
            sales_data["months"] = datetime.date(1900, int(month_val), 1).strftime('%b') 
            sales_data["ty_sales"] = round(float(ty_sale_val),2)
            sales_data["ly_sales"] = round(float(ly_sale_val),2)
            graph_sales_dictionary.append(sales_data)
            
        return [graph_sales_dictionary]    

    # Prepare chart data based on transaction
    def transaction_chart_data(self,filter_year):
        import datetime
        user_id = str(44)
        # Define data set array 
        temp_ty_sales = []
        temp_ly_sales = []
        temp_2ly_sales = []
        temp_months = []
        graph_sales_dictionary = []
        
        #current_year = time.strftime("%Y")
        current_year = str(filter_year)
        prev_year = str(int(current_year)-1)
        second_prev_year = str(int(current_year)-2)
        total_outlets = 0
        # Set diffrence years
        graph_years_dictionary = [current_year,prev_year,second_prev_year]
        # Get user's outlet records
        user_data = self.get_outlets(user_id)
        user_outlets = user_data['user_outlets']
        total_outlets = len(user_outlets)
                
        # Get outlets this and prevoise year sales
        outlet_query = "SELECT SUM(btoa.btda_amt) as total_amount,SUM(btoa.btda_cost) as total_cost,SUM(btoa.btda_customers_count) as total_customers,trxt_month as month,trxt_year as year FROM bi_trx_department_aggregates btoa JOIN bi_trx_time btt ON btt.trxt_id = btoa.btda_time_id WHERE ( btt.trxt_year = '"+current_year+"' OR btt.trxt_year = '"+prev_year+"' OR btt.trxt_year = '"+second_prev_year+"' ) AND "
        outlet_query += " ( "
        # Set outlet condition
        for i in range(0,len(user_outlets)):
            outlet_query += " btoa.btda_outlet_id  = "+str(user_outlets[i])+" "
            if i != (len(user_outlets)-1) :
                outlet_query += " OR "
        outlet_query += " ) "
        
        outlet_query += " GROUP BY btt.trxt_month,btt.trxt_year ORDER BY month,year"
        df_outlet_ty = pd.read_sql_query(outlet_query,con=self.db)
        df_outlet_ty=df_outlet_ty.groupby(['year'])
        for k, v in df_outlet_ty:
            temp_outlet_split=v.to_dict(orient="records")
            for v in temp_outlet_split:
                if int(v['year']) == int(current_year) :
                    temp_ty_sales.append({'total_customers':v['total_customers']})
                elif int(v['year']) == int(prev_year) :  
                    temp_ly_sales.append({'total_customers':v['total_customers']})
                    temp_months.append(int(v['month']))
                elif int(v['year']) == int(second_prev_year) :  
                    temp_2ly_sales.append({'total_customers':v['total_customers']})    
        
        total_ty_sale_val = 0
        total_ly_sale_val = 0
        # Set marker name as combination of name and axis values
        for i in range(0,len(temp_ly_sales)):
            
            # Set last year values
            ly_sale_val = float(temp_ly_sales[i]['total_customers'])
            
            # Handle exeption for this year values  
            try:
                ty_sale_val = float(temp_ty_sales[i]['total_customers'])
            except IndexError:
                ty_sale_val = 0
            try:
                sly_sale_val = float(temp_2ly_sales[i]['total_customers'])
            except IndexError:
                sly_sale_val = 0    
            try:
                month_val = float(temp_months[i])
            except IndexError:
                month_val = 0
            # Set this year and last totals
            total_ty_sale_val = total_ty_sale_val+ty_sale_val
            total_ly_sale_val = total_ly_sale_val+ly_sale_val
            # Prepare sales dictionary 
            sales_data = {}
            sales_data["months"] = datetime.date(1900, int(month_val), 1).strftime('%b') 
            sales_data["ty_sales"] = round(float(ty_sale_val),2)
            sales_data["ly_sales"] = round(float(ly_sale_val),2)
            sales_data["sly_sales"] = round(float(sly_sale_val),2)
            graph_sales_dictionary.append(sales_data)
            
        return [graph_sales_dictionary,graph_years_dictionary,round(np.divide(total_ty_sale_val,1000000), 2),round(np.divide(total_ly_sale_val,1000000), 2)]  

    # Prepare chart data based on item per customers
    def item_per_customer_chart_data(self,filter_year):
        import datetime
        user_id = str(44)
        # Define data set array 
        temp_ty_sales = []
        temp_ly_sales = []
        temp_2ly_sales = []
        temp_months = []
        graph_sales_dictionary = []
        total_ty_sale_val = 0
        total_ly_sale_val = 0
        
        #current_year = time.strftime("%Y")
        current_year = str(filter_year)
        prev_year = str(int(current_year)-1)
        second_prev_year = str(int(current_year)-2)
        total_outlets = 0
        # Set diffrence years
        graph_years_dictionary = [current_year,prev_year,second_prev_year]
        # Get user's outlet records
        user_data = self.get_outlets(user_id)
        user_outlets = user_data['user_outlets']
        total_outlets = len(user_outlets)
                
        # Get outlets this and prevoise year sales
        outlet_query = "SELECT SUM(btoa.btda_amt) as total_amount,SUM(btoa.btda_qty) as total_qty,SUM(btoa.btda_customers_count) as total_customers,trxt_month as month,trxt_year as year FROM bi_trx_department_aggregates btoa JOIN bi_trx_time btt ON btt.trxt_id = btoa.btda_time_id WHERE ( btt.trxt_year = '"+current_year+"' OR btt.trxt_year = '"+prev_year+"' OR btt.trxt_year = '"+second_prev_year+"' ) AND "
        outlet_query += " ( "
        # Set outlet condition
        for i in range(0,len(user_outlets)):
            outlet_query += " btoa.btda_outlet_id  = "+str(user_outlets[i])+" "
            if i != (len(user_outlets)-1) :
                outlet_query += " OR "
        outlet_query += " ) "
        
        outlet_query += " GROUP BY btt.trxt_month,btt.trxt_year ORDER BY month,year"
        df_outlet_ty = pd.read_sql_query(outlet_query,con=self.db)
        df_outlet_ty=df_outlet_ty.groupby(['year'])
        for k, v in df_outlet_ty:
            temp_outlet_split=v.to_dict(orient="records")
            for v in temp_outlet_split:
                if int(v['year']) == int(current_year) :
                    temp_ty_sales.append({'total_amount':v['total_amount'],'total_qty':v['total_qty'],'total_customers':v['total_customers']})
                elif int(v['year']) == int(prev_year) :  
                    temp_ly_sales.append({'total_amount':v['total_amount'],'total_qty':v['total_qty'],'total_customers':v['total_customers']})
                    temp_months.append(int(v['month']))
                elif int(v['year']) == int(second_prev_year) :  
                    temp_2ly_sales.append({'total_amount':v['total_amount'],'total_qty':v['total_qty'],'total_customers':v['total_customers']})        
        
        # Set marker name as combination of name and axis values
        for i in range(0,len(temp_ly_sales)):
            
            # Set last year values
            ly_sale_val = np.divide(float(temp_ly_sales[i]['total_qty']),float(temp_ly_sales[i]['total_customers']))
            
            # Handle exeption for this year values  
            try:
                ty_sale_val = np.divide(float(temp_ty_sales[i]['total_qty']),float(temp_ty_sales[i]['total_customers']))
            except IndexError:
                ty_sale_val = 0
            try:
                sly_sale_val = np.divide(float(temp_2ly_sales[i]['total_qty']),float(temp_2ly_sales[i]['total_customers']))
            except IndexError:
                sly_sale_val = 0     
            try:
                month_val = float(temp_months[i])
            except IndexError:
                month_val = 0
            # Count this year sales
            total_ty_sale_val = total_ty_sale_val+ty_sale_val
            total_ly_sale_val = total_ly_sale_val+ly_sale_val
            # Prepare sales dictionary 
            sales_data = {}
            sales_data["months"] = datetime.date(1900, int(month_val), 1).strftime('%b') 
            sales_data["ty_sales"] = round(float(ty_sale_val),2)
            sales_data["ly_sales"] = round(float(ly_sale_val),2)
            sales_data["sly_sales"] = round(float(sly_sale_val),2)
            graph_sales_dictionary.append(sales_data)
           
        return [graph_sales_dictionary,graph_years_dictionary, round(float(total_ty_sale_val),2), round(float(total_ly_sale_val),2)]     