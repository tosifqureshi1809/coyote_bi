from flask import Flask, Response, g, request,session
from flask_caching import Cache
from flask_sqlalchemy import SQLAlchemy
import sqlalchemy
import pandas as pd
import numpy as np
import re
import base64
import json
import time
import datetime
# -- Important Assets STARTS --
from biapp.db import get_db,get_livedb,get_testdb
from biapp.app import server

app = Flask(__name__, instance_relative_config=True)
# -- Important Assets ENDS --
cache = Cache(server, config={'CACHE_TYPE': 'simple'})
timeout = 20

class dashboard():
   
    title = ""
    db = []

    @cache.memoize(timeout=timeout)
    def __init__(self):
        # Get session data
        environment = 'LIVE'
        if 'env_data' in session:
            env_data = session['env_data']
            # Get filter data; If exist
            if env_data.get("environment") != None:
                environment = env_data.get("environment")
        with server.app_context():        
            if 'db' not in g:
                self.db = get_db()
                g.db = self.db
            if 'live_db' not in g:
                if environment == 'LIVE' :
                    self.live_db = get_livedb()
                else :
                    self.live_db = get_testdb()    
                g.live_db = self.live_db     
    
    # -- Manage user's sales data and prepare that for dashboard charts
    @cache.memoize(timeout=timeout)
    def pg1_dashboard_data(self):
       
        user_id = str(44)
        # Get user's outlet records
        user_data = self.get_outlets(user_id)
        user_outlets = user_data['user_outlets']
        user_role = user_data['user_role']
        # Prepare dictionary
        myDict = {}
        myDict["total_stores"] = len(user_outlets)
        myDict["total_sales_amount"] = self.get_gross_sales(user_outlets)
        myDict["zone_sales"] = self.zone_sales(user_outlets,user_role)
        res = [myDict]
        return res
    
    # Get user's outlet records
    def get_outlets(self,user_id):
        user_outlets = []
        user_outlet = ''
        user_zone = ''   
        user_role = 'USER'
        # Get user basic details
        user_res = pd.read_sql_query("SELECT id,outlet,zone from bi_user where user_number = "+user_id+"",con=self.db)

        for v in user_res.to_dict(orient="records"):
            user_outlet = v['outlet']
            user_zone = v['zone']
            # Get outlet records
            if user_outlet != None and user_outlet != '' :
                user_outlets.append(user_outlet)
            elif user_zone != None and user_zone != '' :
                zone_outlet_res = pd.read_sql_query("SELECT zout_outlet_id as outlet_id from zone_outlet where zout_zone_id = '"+user_zone+"'",con=self.db)
                zone_outlet_res=zone_outlet_res.groupby(['outlet_id'])
                for k, v in zone_outlet_res:
                    temp_zone_split=v.to_dict(orient="records")
                    for v in temp_zone_split:
                        user_outlets.append(v['outlet_id'])
            else :
                # Fetch user's outlet sales
                outlet_res = pd.read_sql_query("SELECT outl_id as outlet_id from bi_outlets where 	outl_status = 'Active' and SubString(outl_name, 1, 2) not in ('ZZ','ZG','ZA') order by outl_name asc",con=self.db)
                outlet_res=outlet_res.groupby(['outlet_id'])
                for k, v in outlet_res:
                    temp_outlet_split=v.to_dict(orient="records")
                    for v in temp_outlet_split:
                        user_outlets.append(v['outlet_id'])
                # Set user role        
                user_role = 'ADMIN'        
         # Prepare dictionary 
        user_data = {}
        user_data["user_role"] = user_role
        user_data["user_outlets"] = user_outlets                
        return user_data

    # Get user's total sales for this year
    def get_gross_sales(self,outlets):
        total_amount = 0

        # Fetch outlets total sales for this year
        if len(outlets) > 0 :
            outlet_query = "SELECT SUM(btoa.btoa_amt) as total_amount,btt.trxt_year as year FROM bi_trx_outlet_aggregates btoa JOIN bi_trx_time btt ON btt.trxt_id = btoa.btoa_time_id WHERE btt.trxt_year = '"+time.strftime("%Y")+"' AND ("
            # Set outlet condition
            for i in range(0,len(outlets)):
                outlet_query += " btoa.btoa_outlet_id  = "+str(outlets[i])+" "
                if i != (len(outlets)-1) :
                    outlet_query += " OR "
            outlet_query += ") GROUP BY btt.trxt_year"
            outlet_res = pd.read_sql_query(outlet_query,con=self.db)
            for v in outlet_res.to_dict(orient="records"):
                total_amount = v['total_amount']
            #convert aount in Million
            total_amount = round(np.divide(total_amount,1000000), 2)
        return total_amount

    # Get zone wise this year sales
    def zone_sales(self,user_outlets,user_role):
        temp_rtm_sales = []
        temp_demo_sales = []
        temp_region_sales = []
        temp_outlet_sales = []
        if len(user_outlets) > 0 and user_role == 'USER':
            outlet_query = "SELECT top 5 SUM(btoa.btoa_amt) as total_amount,btoa.btoa_outlet_id,bo.outl_name,trxt_year as year,4 as zone_type FROM bi_trx_outlet_aggregates btoa JOIN bi_trx_time btt ON btt.trxt_id = btoa.btoa_time_id JOIN bi_outlets bo ON bo.outl_id = btoa.btoa_outlet_id WHERE btt.trxt_year  = '"+time.strftime("%Y")+"' AND ( "
            # Set outlet condition
            for i in range(0,len(user_outlets)):
                outlet_query += " btoa.btoa_outlet_id  = "+str(user_outlets[i])+" "
                if i != (len(user_outlets)-1) :
                    outlet_query += " OR "
            outlet_query += ") GROUP BY btt.trxt_year,btoa.btoa_outlet_id,bo.outl_name"
           
            df_outlet_ty = pd.read_sql_query(outlet_query,con=self.db)
            df_outlet_ty=df_outlet_ty.groupby(['year'])
            for k, v in df_outlet_ty:
                temp_outlet_split=v.to_dict(orient="records")
                for v in temp_outlet_split:
                    temp_outlet_sales.append({'outlet_id':v['btoa_outlet_id'],'outl_name':v['outl_name'].encode("ascii"),'total_amount':v['total_amount']})  
        else :
            # Set zone wise sales
            zone_query = "SELECT top 5 SUM(btoa.btoa_amt) as total_amount,z.zone_id,z.zone_code,z.zone_label,trxt_year as year,1 as zone_type FROM bi_trx_outlet_aggregates btoa JOIN bi_trx_time btt ON btt.trxt_id = btoa.btoa_time_id JOIN bi_outlets bo ON bo.outl_id = btoa.btoa_outlet_id JOIN zone_outlet zo ON zo.zout_outlet_id = btoa.btoa_outlet_id JOIN zone z ON z.zone_id = zo.zout_zone_id WHERE btt.trxt_year  = '"+time.strftime("%Y")+"' AND z.zone_label LIKE '%rtm%' AND btoa_outlet_id IN ( select distinct(zo.zout_outlet_id) FROM zone_outlet zo JOIN zone z ON (zo.zout_zone_id = z.zone_id and z.zone_label LIKE '%rtm%')) GROUP BY btt.trxt_year,z.zone_id,z.zone_code,z.zone_label UNION SELECT top 5 SUM(btoa.btoa_amt) as total_amount,z.zone_id,z.zone_code,z.zone_label,trxt_year as year,2 as zone_type FROM bi_trx_outlet_aggregates btoa JOIN bi_trx_time btt ON btt.trxt_id = btoa.btoa_time_id JOIN bi_outlets bo ON bo.outl_id = btoa.btoa_outlet_id JOIN zone_outlet zo ON zo.zout_outlet_id = btoa.btoa_outlet_id JOIN zone z ON z.zone_id = zo.zout_zone_id WHERE btt.trxt_year  = '"+time.strftime("%Y")+"' AND z.zone_label LIKE '%demo%' AND btoa_outlet_id IN ( select distinct(zo.zout_outlet_id) FROM zone_outlet zo JOIN zone z ON (zo.zout_zone_id = z.zone_id and z.zone_label LIKE '%demo%')) GROUP BY btt.trxt_year,z.zone_id,z.zone_code,z.zone_label UNION SELECT top 5 SUM(btoa.btoa_amt) as total_amount,z.zone_id,z.zone_code,z.zone_label,trxt_year as year,3 as zone_type FROM bi_trx_outlet_aggregates btoa JOIN bi_trx_time btt ON btt.trxt_id = btoa.btoa_time_id JOIN bi_outlets bo ON bo.outl_id = btoa.btoa_outlet_id JOIN zone_outlet zo ON zo.zout_outlet_id = btoa.btoa_outlet_id JOIN zone z ON z.zone_id = zo.zout_zone_id WHERE btt.trxt_year  = '"+time.strftime("%Y")+"' AND z.zone_label LIKE '%region%' AND btoa_outlet_id IN ( select distinct(zo.zout_outlet_id) FROM zone_outlet zo JOIN zone z ON (zo.zout_zone_id = z.zone_id and z.zone_label LIKE '%region%'))GROUP BY btt.trxt_year,z.zone_id,z.zone_code,z.zone_label ORDER BY total_amount DESC"
            df_zone_ty = pd.read_sql_query(zone_query,con=self.db)
            df_zone_ty=df_zone_ty.groupby(['year'])
            for k, v in df_zone_ty:
                temp_outlet_split=v.to_dict(orient="records")
                for v in temp_outlet_split:
                    if v['zone_type'] == 1 :
                        temp_rtm_sales.append({'zone_id':v['zone_id'],'zone_label':v['zone_label'].encode("ascii"),'zone_code':v['zone_code'].encode("ascii"),'total_amount':v['total_amount']})  
                    elif v['zone_type'] == 2 :
                        temp_demo_sales.append({'zone_id':v['zone_id'],'zone_label':v['zone_label'].encode("ascii"),'zone_code':v['zone_code'].encode("ascii"),'total_amount':v['total_amount']}) 
                    elif v['zone_type'] == 3 : 
                        temp_region_sales.append({'zone_id':v['zone_id'],'zone_label':v['zone_label'].encode("ascii"),'zone_code':v['zone_code'].encode("ascii"),'total_amount':v['total_amount']})
        # Prepare dictionary 
        zoneDict = {}
        zoneDict["rtm_sales"] = temp_rtm_sales
        zoneDict["demo_sales"] = temp_demo_sales
        zoneDict["region_sales"] = temp_region_sales
        zoneDict["outlet_sales"] = temp_outlet_sales
        return zoneDict

    # Get stores this year and last year sales
    def ty_ly_outlet_sales_dictionary(self,user_outlets):

        temp_ty_sales = []
        temp_ly_sales = []
        temp_months = []
        current_year = time.strftime("%Y")
        prev_year = str(int(current_year)-1)
        total_outlets = len(user_outlets)
        # Get outlets this and prevoise year sales
        outlet_query = "SELECT SUM(btoa.btoa_amt) as total_amount,SUM(btoa.btoa_cost) as total_cost,SUM(btoa.btoa_avg_basket) as avg_basket,SUM(btoa.btoa_customers) as total_customers,SUM(btoa.btoa_gp_percent) as gp_percent,trxt_month as month,trxt_year as year FROM bi_trx_outlet_aggregates btoa JOIN bi_trx_time btt ON btt.trxt_id = btoa.btoa_time_id WHERE ( btt.trxt_year = '"+current_year+"' OR btt.trxt_year = '"+prev_year+"' ) AND ( "
        # Set outlet condition
        for i in range(0,len(user_outlets)):
            outlet_query += " btoa.btoa_outlet_id  = "+str(user_outlets[i])+" "
            if i != (len(user_outlets)-1) :
                outlet_query += " OR "
        outlet_query += ") GROUP BY btt.trxt_month,btt.trxt_year ORDER BY month,year"
        df_outlet_ty = pd.read_sql_query(outlet_query,con=self.db)
        df_outlet_ty=df_outlet_ty.groupby(['year'])
        for k, v in df_outlet_ty:
            temp_outlet_split=v.to_dict(orient="records")
            for v in temp_outlet_split:
                if int(v['year']) == int(current_year) :
                    temp_ty_sales.append({'total_amount':v['total_amount'],'gp_percent':v['gp_percent'],'total_customers':v['total_customers']})
                elif int(v['year']) == int(prev_year) :  
                    temp_ly_sales.append({'total_amount':v['total_amount'],'gp_percent':v['gp_percent'],'total_customers':v['total_customers']})
                    temp_months.append(int(v['month']))
        # Prepare data based on graph types
        graph_sales_dictionary = []
        graph_gp_dictionary = []
        graph_avgbasket_dictionary = []
        graph_customers_dictionary = []
        # Set marker name as combination of name and axis values
        for i in range(0,len(temp_ly_sales)):
             
            # Set last year values
            ly_sale_val = np.divide(float(temp_ly_sales[i]['total_amount']),total_outlets)
            ly_gp_val = np.divide(float(temp_ly_sales[i]['gp_percent']),total_outlets)
            ly_avgbsk_val = np.divide(float(temp_ly_sales[i]['total_amount']),v['total_customers'])/total_outlets
            ly_customer_val = np.divide(temp_ly_sales[i]['total_customers'],total_outlets)
            # Handle exeption for this year values  
            try:
                ty_sale_val = np.divide(float(temp_ty_sales[i]['total_amount']),total_outlets)
                ty_gp_val = np.divide(float(temp_ty_sales[i]['gp_percent']),total_outlets)
                ty_avgbsk_val = np.divide(float(temp_ty_sales[i]['total_amount']),v['total_customers'])/total_outlets
                ty_customer_val = np.divide(temp_ty_sales[i]['total_customers'],total_outlets)
            except IndexError:
                ty_sale_val = 0
                ty_gp_val = 0
                ty_avgbsk_val = 0
                ty_customer_val = 0
            try:
                month_val = float(temp_months[i])
            except IndexError:
                month_val = 0
            # Prepare sales dictionary 
            sales_data = {}
            sales_data["months"] = datetime.date(1900, int(month_val), 1).strftime('%B') 
            sales_data["ty_sales"] = float(ty_sale_val)
            sales_data["ly_sales"] = float(ly_sale_val)
            graph_sales_dictionary.append(sales_data)
            # Prepare gp dictionary 
            gp_data = {}
            gp_data["months"] = datetime.date(1900, int(month_val), 1).strftime('%B') 
            gp_data["ty_gp"] = float(ty_gp_val)
            gp_data["ly_gp"] = float(ly_gp_val)
            graph_gp_dictionary.append(gp_data)
            # Prepare average basket dictionary 
            avgbkt_data = {}
            avgbkt_data["months"] = datetime.date(1900, int(month_val), 1).strftime('%B') 
            avgbkt_data["ty_avgbkt"] = float(ty_avgbsk_val)
            avgbkt_data["ly_avgbkt"] = float(ly_avgbsk_val)
            graph_avgbasket_dictionary.append(avgbkt_data)
            # Prepare cpc dictionary 
            cpc_data = {}
            cpc_data["months"] = datetime.date(1900, int(month_val), 1).strftime('%B') 
            cpc_data["ty_customers"] = float(ty_customer_val)
            cpc_data["ly_customers"] = float(ly_customer_val)
            graph_customers_dictionary.append(cpc_data)

        return [graph_sales_dictionary,graph_gp_dictionary,graph_avgbasket_dictionary,graph_customers_dictionary]

    # Prepare chart data based on zone selection
    def dash_inner_chart_data(self):
        user_id = str(44)
        # Set input request param
        zone_id = ''
        if request.args.get('zone_id') is not None:
            zone_id = request.args.get('zone_id')
         
        # Define data set array 
        temp_ty_sales = []
        temp_ly_sales = []
        temp_months = []
        graph_sales_dictionary = []
        graph_gp_dictionary = []
        graph_avgbasket_dictionary = []
        graph_customers_dictionary = []
        
        current_year = time.strftime("%Y")
        prev_year = str(int(current_year)-1)
        total_outlets = 0
        # Get total outlets from zone
        if zone_id != '' :
            z_outlet_query = "SELECT count(zout_id) as total_outlets FROM zone_outlet zo WHERE zout_zone_id = '"+zone_id+"'"
            z_outlet_res = pd.read_sql_query(z_outlet_query,con=self.db)
            for v in z_outlet_res.to_dict(orient="records"):
                total_outlets = int(v['total_outlets'])
        else : 
            # Get user's outlet records
            user_data = self.get_outlets(user_id)
            user_outlets = user_data['user_outlets']
            total_outlets = len(user_outlets)
                
        # Get outlets this and prevoise year sales
        outlet_query = "SELECT SUM(btoa.btoa_amt) as total_amount,SUM(btoa.btoa_cost) as total_cost,SUM(btoa.btoa_avg_basket) as avg_basket,SUM(btoa.btoa_customers) as total_customers,SUM(btoa.btoa_gp_percent) as gp_percent,trxt_month as month,trxt_year as year FROM bi_trx_outlet_aggregates btoa JOIN bi_trx_time btt ON btt.trxt_id = btoa.btoa_time_id WHERE ( btt.trxt_year = '"+current_year+"' OR btt.trxt_year = '"+prev_year+"' ) AND "
        if zone_id != '' :
            outlet_query += " btoa.btoa_outlet_id IN ( select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+zone_id+"' )"
        else :
            outlet_query += " ( "
            # Set outlet condition
            for i in range(0,len(user_outlets)):
                outlet_query += " btoa.btoa_outlet_id  = "+str(user_outlets[i])+" "
                if i != (len(user_outlets)-1) :
                    outlet_query += " OR "
            outlet_query += " ) "
        
        outlet_query += " GROUP BY btt.trxt_month,btt.trxt_year ORDER BY month,year"
        df_outlet_ty = pd.read_sql_query(outlet_query,con=self.db)
        df_outlet_ty=df_outlet_ty.groupby(['year'])
        for k, v in df_outlet_ty:
            temp_outlet_split=v.to_dict(orient="records")
            for v in temp_outlet_split:
                if int(v['year']) == int(current_year) :
                    temp_ty_sales.append({'total_amount':v['total_amount'],'gp_percent':v['gp_percent'],'total_customers':v['total_customers']})
                elif int(v['year']) == int(prev_year) :  
                    temp_ly_sales.append({'total_amount':v['total_amount'],'gp_percent':v['gp_percent'],'total_customers':v['total_customers']})
                    temp_months.append(int(v['month']))
        
        # Set marker name as combination of name and axis values
        for i in range(0,len(temp_ly_sales)):
            
            # Set last year values
            ly_sale_val = np.divide(float(temp_ly_sales[i]['total_amount']),total_outlets)
            ly_gp_val = np.divide(float(temp_ly_sales[i]['gp_percent']),total_outlets)
            ly_avgbsk_val = np.divide(float(temp_ly_sales[i]['total_amount']),v['total_customers'])/total_outlets
            ly_customer_val = np.divide(temp_ly_sales[i]['total_customers'],total_outlets)
            # Handle exeption for this year values  
            try:
                ty_sale_val = np.divide(float(temp_ty_sales[i]['total_amount']),total_outlets)
                ty_gp_val = np.divide(float(temp_ty_sales[i]['gp_percent']),total_outlets)
                ty_avgbsk_val = np.divide(float(temp_ty_sales[i]['total_amount']),v['total_customers'])/total_outlets
                ty_customer_val = np.divide(temp_ty_sales[i]['total_customers'],total_outlets)
            except IndexError:
                ty_sale_val = 0
                ty_gp_val = 0
                ty_avgbsk_val = 0
                ty_customer_val = 0
            try:
                month_val = float(temp_months[i])
            except IndexError:
                month_val = 0
            # Prepare sales dictionary 
            sales_data = {}
            sales_data["months"] = datetime.date(1900, int(month_val), 1).strftime('%B') 
            sales_data["ty_sales"] = ty_sale_val
            sales_data["ly_sales"] = ly_sale_val
            graph_sales_dictionary.append(sales_data)
            # Prepare gp dictionary 
            gp_data = {}
            gp_data["months"] = datetime.date(1900, int(month_val), 1).strftime('%B') 
            gp_data["ty_gp"] = ty_gp_val
            gp_data["ly_gp"] = ly_gp_val
            graph_gp_dictionary.append(gp_data)
            # Prepare average basket dictionary 
            avgbkt_data = {}
            avgbkt_data["months"] = datetime.date(1900, int(month_val), 1).strftime('%B') 
            avgbkt_data["ty_avgbkt"] = ty_avgbsk_val
            avgbkt_data["ly_avgbkt"] = ly_avgbsk_val
            graph_avgbasket_dictionary.append(avgbkt_data)
            # Prepare cpc dictionary 
            cpc_data = {}
            cpc_data["months"] = datetime.date(1900, int(month_val), 1).strftime('%B') 
            cpc_data["ty_customers"] = ty_customer_val
            cpc_data["ly_customers"] = ly_customer_val
            graph_customers_dictionary.append(cpc_data)

        return [graph_sales_dictionary,graph_gp_dictionary,graph_avgbasket_dictionary,graph_customers_dictionary]