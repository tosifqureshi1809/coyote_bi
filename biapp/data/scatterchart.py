from flask import Flask, Response, g, request,session
from flask_caching import Cache
from flask_sqlalchemy import SQLAlchemy
import sqlalchemy
import pandas as pd
import numpy as np
import re
import base64
import math

# -- Important Assets STARTS --
from biapp.db import get_db,get_livedb
from biapp.app import server
from datetime import datetime, timedelta

app = Flask(__name__, instance_relative_config=True)
# -- Important Assets ENDS --
cache = Cache(server, config={'CACHE_TYPE': 'simple'})
timeout = 20

class scatterchart():

    title = ""
    db = []
    live_db = []

    @cache.memoize(timeout=timeout)
    def __init__(self):
        with server.app_context():        
            if 'db' not in g:
                self.db = get_db()
                g.db = self.db
            if 'live_db' not in g:
                if request.args.get('level') is not None and request.args.get('level') == '3':
                    self.live_db = get_livedb()
                    g.live_db = self.live_db    
    
    #@cache.memoize(timeout=timeout)
    def pg1_scatterchart_20_20_Report(self):
        
        temp_Sale_TYS_VS_LYS_X={
        "x" : [],
        "y" : []
        }
        # -- For X-Axis for current Years. --
        temp_Sale_TYS_VS_LYS_Y=[]
        # -- For Graph X-Axis, Y-Axis, Custome Data for current Years. --
        graph_x = []
        graph_y = []
        graph_text = []
        graph_customdata = []
        graph_bubble_size = []
        total_store_sale=0
        product_query = ''
        total_amount_x = 0
        total_amount_y = 0
        TY_total_customers = 0
        LY_total_customers = 0
        TYC_total_customers = 0
        TY_primary_total_stores = 0
        TY_secondary_total_stores = 0
        # Get drilldown level filter data; If exist
        analysis_level = 1
        analysis_index_id = ''
        department_name = ''
        analysis_chart_label = '20-20 Report'
        if request.args.get('level') is not None:
            analysis_level = request.args.get('level')
        if request.args.get('id') is not None:    
            analysis_index_id = request.args.get('id')
        if request.args.get('label') is not None:    
            analysis_chart_label = request.args.get('label')
            analysis_chart_label = base64.b64decode(analysis_chart_label)
        if request.args.get('department_name') is not None:    
            department_name = request.args.get('department_name')
            department_name = base64.b64decode(department_name)

        # Get session filter data
        if 'filter_data' in session:
            filter_data = session['filter_data']
        # Set filter params
        p_outlet_id = ''
        p_zone_id = ''
        s_outlet_id = ''
        s_zone_id = ''
        start_date = ''
        end_date = ''
        comparison_period = 1
        measurement_type = '1'
        if filter_data.get("p_outlet_id") != None and filter_data.get("p_outlet_id") != '':
            p_outlet_id = filter_data.get("p_outlet_id")
        
        if filter_data.get("p_zone_id") != None and filter_data.get("p_zone_id") != '':
            p_zone_id = filter_data.get("p_zone_id")
       
        if filter_data.get("s_outlet_id") != None and filter_data.get("s_outlet_id") != '':
            s_outlet_id = filter_data.get("s_outlet_id")
        
        if filter_data.get("s_zone_id") != None and filter_data.get("s_zone_id") != '':
            s_zone_id = filter_data.get("s_zone_id")    
        
        if filter_data.get("start_date") != None and filter_data.get("start_date") != '' and filter_data.get("end_date") != None:
            start_date = filter_data.get("start_date")
            end_date = filter_data.get("end_date")

        if filter_data.get("comparison_period") != None and filter_data.get("comparison_period") != '':
            comparison_period = int(filter_data.get("comparison_period"))

        if filter_data.get("measurement_type") != None and filter_data.get("measurement_type") != '':
            measurement_type = filter_data.get("measurement_type")    

        # return if date range empty
        if start_date == '' or end_date == '' :
            res = [graph_x, graph_y, graph_text, graph_customdata, graph_bubble_size]
            return res

        # -- Set dates
        convt_startdate = datetime.strptime(start_date, '%Y-%m-%d').date()
        current_year = int(convt_startdate.strftime('%Y'))
        current_month = convt_startdate.strftime('%m')
        current_day = convt_startdate.strftime('%d')

        convt_enddate = datetime.strptime(end_date, '%Y-%m-%d').date()
        current_endmonth = convt_enddate.strftime('%m')
        current_endday = convt_enddate.strftime('%d')
        prev_year = current_year-comparison_period

        convt_prev_startdate = datetime.strptime(str(prev_year)+'-'+current_month+'-'+current_day, '%Y-%m-%d').date()
        convt_prev_enddate = datetime.strptime(str(prev_year)+'-'+current_endmonth+'-'+current_endday, '%Y-%m-%d').date()
        convt_prev_startdate = convt_prev_startdate + timedelta(days=1)
        # set previous end date for end of year date
        if current_endmonth != '12' and current_endday != '31' :
            convt_prev_enddate = convt_prev_enddate + timedelta(days=1)
            last_day_of_prev_year = ''
        else :
            convt_prev_enddate = convt_prev_enddate
            last_day_of_prev_year = convt_prev_enddate + timedelta(days=1)
            last_day_of_prev_year = last_day_of_prev_year.strftime('%Y-%m-%d')
        
        convt_startdate.strftime('%Y-%m-%d')
        LY_start_date = convt_prev_startdate.strftime('%Y-%m-%d')
        convt_enddate = datetime.strptime(end_date, '%Y-%m-%d').date()
        LY_end_date = convt_prev_enddate.strftime('%Y-%m-%d')
        
        # Get product level sql query array
        p_zone_outl_sql = ''
        if analysis_level == '3' :
            # Get zone outlets and concatinate outlets in query
            if p_zone_id != '' :
                p_zone_outl_sql = self.zone_outl_query_condition(p_zone_id)

        # -- For X, Y of Y-Axis for selected 2 Years. --
        if(p_outlet_id != '' or p_zone_id != '') :
            
            if analysis_level == '3' : 
                commodity_id = str(analysis_index_id)
                TY_top_products = []
                # Prepare this year top 20 product query 
                TY_p_sql = "select top (20) (btpa.btpa_product_id) as id,btpa.btpa_product_name as title,SUM(btpa.btpa_promo_sales) as total_promo_sale,SUM(btpa.btpa_discount) as total_discount,SUM(btpa.btpa_qty) as total_qty,SUM(btpa.btpa_amt) as total_amount,SUM(btpa.btpa_cost) as total_cost,SUM(btpa.btpa_margin) as total_margin,(((SUM(btpa.btpa_amt) - SUM(btpa.btpa_cost)) / NULLIF(SUM(btpa.btpa_amt), 0))*100) as gp_percent,SUM(btpa.btpa_customers) as total_customers,btt.trxt_year FROM bi_trx_product_aggregates btpa join bi_trx_time btt on btt.trxt_id = btpa.btpa_time_id WHERE (btt.trxt_date BETWEEN '"+start_date+"' AND '"+end_date+"') AND "
                if p_zone_id != '' :
                    TY_p_sql += "btpa.btpa_outlet_id  IN ( select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+p_zone_id+"' )  AND "
                else :  
                    TY_p_sql += " btpa.btpa_outlet_id = '"+p_outlet_id+"' AND "
                TY_p_sql += "btpa_commodity_id = '"+commodity_id+"' GROUP BY btpa.btpa_product_id, btpa.btpa_product_name,btpa.btpa_commodity_id,btt.trxt_year ORDER BY total_amount DESC"
               
                # Get this year's product sales
                df_sale_TYS = pd.read_sql_query(TY_p_sql,con=self.db)
                #df_sale_TYS = pd.read_sql_query(df_sale_TYS_query,con=self.db)
                df_sale_TYS=df_sale_TYS.groupby(['title'])
                for k, v in df_sale_TYS:
                    temp_group_split=v.to_dict(orient="records")
                    for v in temp_group_split:
                        temp_Sale_TYS_VS_LYS_X["y"].append({'id':v['id'],'name':v['title'].encode("ascii"), 'total_promo_sale': v['total_promo_sale'],"total_discount":v['total_discount'], "total_qty":v['total_qty'], "total_cost":v['total_cost'], "gp_percent":v['gp_percent'], "total_margin":v['total_margin'], "total_amount":v['total_amount'],"total_customers":v['total_customers'], "trxt_year":v['trxt_year']})
                        if measurement_type == '2' :
                            if v['total_margin'] != None and v['total_margin'] > 0:
                                total_store_sale = total_store_sale+float(v['total_margin'])
                        elif measurement_type == '3' :
                            if v['total_amount'] != None and v['total_amount'] > 0:
                                total_store_sale = total_store_sale+float(v['total_amount'])   
                        else:
                            if v['total_amount'] != None and v['total_amount'] > 0:
                                total_store_sale = total_store_sale+v['total_amount']
                        TY_total_customers = TY_total_customers+v['total_customers'] 
                        # Append top products in this year array
                        TY_top_products.append(v['id'])

                # Prepare last year top 20 product query 
                if len(TY_top_products) > 0 :
                    LY_p_sql = "select (btpa.btpa_product_id) as id,btpa.btpa_product_name as title,SUM(btpa.btpa_promo_sales) as total_promo_sale,SUM(btpa.btpa_discount) as total_discount,SUM(btpa.btpa_qty) as total_qty,SUM(btpa.btpa_amt) as total_amount,SUM(btpa.btpa_cost) as total_cost,SUM(btpa.btpa_margin) as total_margin,(((SUM(btpa.btpa_amt) - SUM(btpa.btpa_cost)) / NULLIF(SUM(btpa.btpa_amt), 0))*100) as gp_percent,SUM(btpa.btpa_customers) as total_customers,btt.trxt_year FROM bi_trx_product_aggregates btpa join bi_trx_time btt on btt.trxt_id = btpa.btpa_time_id WHERE ("

                    for i in range(0,len(TY_top_products)):
                        LY_p_sql += " btpa.btpa_product_id  = '"+str(TY_top_products[i])+"' "
                        if i != (len(TY_top_products)-1) :
                            LY_p_sql += " OR " 
                    LY_p_sql += ") AND (btt.trxt_date BETWEEN '"+LY_start_date+"' AND '"+LY_end_date+"') AND "
                    if p_zone_id != '' :
                        LY_p_sql += "btpa.btpa_outlet_id  IN ( select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+p_zone_id+"' )  AND "
                    else :   
                        LY_p_sql += " btpa.btpa_outlet_id = '"+p_outlet_id+"' AND "    
                    LY_p_sql += " btpa_commodity_id = '"+commodity_id+"' GROUP BY btpa.btpa_product_id, btpa.btpa_product_name,btpa.btpa_commodity_id,btt.trxt_year ORDER BY total_amount DESC"
                   
                    # Get last year's product sales
                    df_sale_LYS = pd.read_sql_query(LY_p_sql,con=self.db)
                    # Get => Y-Axis!!
                    df_sale_LYS=df_sale_LYS.groupby(['title'])
                    for k, v in df_sale_LYS:
                        temp_group_split=v.to_dict(orient="records")
                        for v in temp_group_split:
                            temp_Sale_TYS_VS_LYS_X["x"].append({'id':v['id'],'name':v['title'].encode("ascii"), 'total_promo_sale': v['total_promo_sale'],"total_discount":v['total_discount'], "total_qty":v['total_qty'], "total_cost":v['total_cost'], "gp_percent":v['gp_percent'], "total_margin":v['total_margin'], "total_amount":v['total_amount'],"total_customers":v['total_customers'], "trxt_year":v['trxt_year']})
                            LY_total_customers = LY_total_customers+v['total_customers']       
            else :     
                if analysis_level == '2' :
                    # Get year wise commodity sales
                    df_sale_TYS_VS_LYS_query = "SELECT btca.btca_commodity_id as id,(select commodity from commodity where commodity_id = btca.btca_commodity_id) as title,SUM(btca.btca_promo_sales) as total_promo_sale,SUM(btca.btca_discount) as total_discount,SUM(btca.btca_qty) as total_qty,SUM(btca.btca_cost) as total_cost,SUM(btca.btca_margin) as total_margin,(((SUM(btca.btca_amt) - SUM(btca.btca_cost)) / NULLIF(SUM(btca.btca_amt), 0))*100) as gp_percent,SUM(btca.btca_amt) as total_amount,SUM(btca.btca_customers_count) as total_customers,btt.trxt_year FROM bi_trx_commodity_aggregates btca JOIN bi_trx_time btt ON btt.trxt_id = btca.btca_time_id WHERE btca.btca_department_id  = '"+analysis_index_id+"' AND "
                    if p_outlet_id != '':
                        df_sale_TYS_VS_LYS_query += "btca.btca_outlet_id  = '"+p_outlet_id+"' AND "
                    elif p_zone_id != '' :
                        df_sale_TYS_VS_LYS_query += "btca.btca_outlet_id  IN ( select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+p_zone_id+"' )  AND "
                    df_sale_TYS_VS_LYS_query += "( (btt.trxt_date BETWEEN '"+start_date+"' AND '"+end_date+"') OR (btt.trxt_date BETWEEN '"+LY_start_date+"' AND '"+LY_end_date+"') ) GROUP BY btca.btca_commodity_id,btt.trxt_year ORDER BY btca.btca_commodity_id"

                    # Get previose years last day sales;if applicable
                    if last_day_of_prev_year != '' :
                        last_day_of_prev_year_query = "SELECT btca.btca_commodity_id as id,(select commodity from commodity where commodity_id = btca.btca_commodity_id) as title,SUM(btca.btca_promo_sales) as total_promo_sale,SUM(btca.btca_discount) as total_discount,SUM(btca.btca_qty) as total_qty,SUM(btca.btca_cost) as total_cost,SUM(btca.btca_margin) as total_margin,(((SUM(btca.btca_amt) - SUM(btca.btca_cost)) / NULLIF(SUM(btca.btca_amt), 0))*100) as gp_percent,SUM(btca.btca_amt) as total_amount,SUM(btca.btca_customers_count) as total_customers,btt.trxt_year FROM bi_trx_commodity_aggregates btca JOIN bi_trx_time btt ON btt.trxt_id = btca.btca_time_id WHERE btca.btca_department_id  = '"+analysis_index_id+"' AND "
                        if p_outlet_id != '':
                            last_day_of_prev_year_query += "btca.btca_outlet_id  = '"+p_outlet_id+"' AND "
                        elif p_zone_id != '' :
                            last_day_of_prev_year_query += "btca.btca_outlet_id  IN ( select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+p_zone_id+"' )  AND "
                        last_day_of_prev_year_query += "( btt.trxt_date = '"+last_day_of_prev_year+"' ) GROUP BY btca.btca_commodity_id,btt.trxt_year ORDER BY btca.btca_commodity_id"
                else :
                    # Get year wise department sales
                    df_sale_TYS_VS_LYS_query = "SELECT btda.btda_department_id as id,btda.btda_department_name as title,SUM(btda.btda_promo_sales) as total_promo_sale,SUM(btda.btda_discount) as total_discount,SUM(btda.btda_qty) as total_qty,SUM(btda.btda_cost) as total_cost,SUM(btda.btda_margin) as total_margin,(((SUM(btda.btda_amt) - SUM(btda.btda_cost)) / NULLIF(SUM(btda.btda_amt),0))*100) as gp_percent,SUM(btda.btda_amt) as total_amount,SUM(btda.btda_customers_count) as total_customers,btt.trxt_year FROM bi_trx_department_aggregates btda JOIN bi_trx_time btt ON btt.trxt_id = btda.btda_time_id WHERE "
                    if p_outlet_id != '':
                        df_sale_TYS_VS_LYS_query += "btda.btda_outlet_id  = '"+p_outlet_id+"' AND "
                    elif p_zone_id != '' :
                        df_sale_TYS_VS_LYS_query += "btda.btda_outlet_id  IN ( select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+p_zone_id+"' )  AND "

                    df_sale_TYS_VS_LYS_query += "( (btt.trxt_date BETWEEN '"+start_date+"' AND '"+end_date+"') OR (btt.trxt_date BETWEEN '"+LY_start_date+"' AND '"+LY_end_date+"') ) GROUP BY btda.btda_department_id,btda.btda_department_name,btt.trxt_year ORDER BY btda.btda_department_id"
                    # Get previose years last day sales;if applicable
                    if last_day_of_prev_year != '' :
                        last_day_of_prev_year_query = "SELECT btda.btda_department_id as id,btda.btda_department_name as title,SUM(btda.btda_promo_sales) as total_promo_sale,SUM(btda.btda_discount) as total_discount,SUM(btda.btda_qty) as total_qty,SUM(btda.btda_cost) as total_cost,SUM(btda.btda_margin) as total_margin,(((SUM(btda.btda_amt) - SUM(btda.btda_cost)) / NULLIF(SUM(btda.btda_amt),0))*100) as gp_percent,SUM(btda.btda_amt) as total_amount,SUM(btda.btda_customers_count) as total_customers,btt.trxt_year FROM bi_trx_department_aggregates btda JOIN bi_trx_time btt ON btt.trxt_id = btda.btda_time_id WHERE "
                        if p_outlet_id != '':
                            last_day_of_prev_year_query += "btda.btda_outlet_id  = '"+p_outlet_id+"' AND "
                        elif p_zone_id != '' :
                            last_day_of_prev_year_query += "btda.btda_outlet_id  IN ( select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+p_zone_id+"' )  AND "

                        last_day_of_prev_year_query += "( btt.trxt_date = '"+last_day_of_prev_year+"' ) GROUP BY btda.btda_department_id,btda.btda_department_name,btt.trxt_year ORDER BY btda.btda_department_id"
                
                df_sale_TYS_VS_LYS = pd.read_sql_query(df_sale_TYS_VS_LYS_query,con=self.db)
                
                last_day_sales = {}
                # Get aggregated data for last day sales
                if last_day_of_prev_year != '' :
                    last_day_sale_TYS_VS_LYS = pd.read_sql_query(last_day_of_prev_year_query,con=self.db)
                    last_day_sale_TYS_VS_LYS=last_day_sale_TYS_VS_LYS.groupby(['title'])
                    for k, v in last_day_sale_TYS_VS_LYS:
                        temp_group_split=v.to_dict(orient="records")
                        for v in temp_group_split:
                            index_id = v['id']
                            last_day_sales[index_id] = []
                            last_day_sales[index_id].append({'id':v['id'],'name':v['title'].encode("ascii"), 'total_promo_sale': v['total_promo_sale'],"total_discount":v['total_discount'], "total_qty":v['total_qty'], "total_cost":v['total_cost'], "gp_percent":v['gp_percent'], "total_margin":v['total_margin'], "total_amount":v['total_amount'],"total_customers":v['total_customers'], "trxt_year":v['trxt_year']})

                # Get => Y-Axis!!
                df_sale_TYS_VS_LYS=df_sale_TYS_VS_LYS.groupby(['title'])
                for k, v in df_sale_TYS_VS_LYS:
                    temp_group_split=v.to_dict(orient="records")
                    c=0  # Help to Split Array by Years Filter.
                    for v in temp_group_split:
                        if c == 0 and v["trxt_year"] == prev_year: # Replace this with year filter
                            total_promo_sale = v['total_promo_sale']
                            total_discount = v['total_discount']
                            total_qty = v['total_qty']
                            total_cost = v['total_cost']
                            gp_percent = v['gp_percent']
                            total_margin = v['total_margin']
                            total_amount = v['total_amount']
                            total_customers = v['total_customers']
                            if v['id'] in last_day_sales :
                               
                                total_promo_sale = last_day_sales[v['id']][0]['total_promo_sale']+v['total_promo_sale']
                                total_discount = last_day_sales[v['id']][0]['total_discount']+v['total_discount']
                                total_qty = last_day_sales[v['id']][0]['total_qty']+v['total_qty']
                                total_cost = last_day_sales[v['id']][0]['total_cost']+v['total_cost']
                                gp_percent = last_day_sales[v['id']][0]['gp_percent']+v['gp_percent']
                                total_margin = last_day_sales[v['id']][0]['total_margin']+v['total_margin']
                                total_amount = last_day_sales[v['id']][0]['total_amount']+v['total_amount']
                                total_customers = last_day_sales[v['id']][0]['total_customers']+v['total_customers']
                                
                            temp_Sale_TYS_VS_LYS_X["x"].append({'id':v['id'],'name':v['title'].encode("ascii"), 'total_promo_sale': total_promo_sale,"total_discount":total_discount, "total_qty":total_qty, "total_cost":total_cost, "gp_percent":gp_percent, "total_margin":total_margin, "total_amount":total_amount,"total_customers":total_customers, "trxt_year":v['trxt_year']}) 
                            LY_total_customers = LY_total_customers+total_customers
                        elif c == 0 and v["trxt_year"] == current_year:   
                            temp_Sale_TYS_VS_LYS_X["y"].append({'id':v['id'],'name':v['title'].encode("ascii"), 'total_promo_sale': v['total_promo_sale'],"total_discount":v['total_discount'], "total_qty":v['total_qty'], "total_cost":v['total_cost'], "gp_percent":v['gp_percent'], "total_margin":v['total_margin'], "total_amount":v['total_amount'],"total_customers":v['total_customers'], "trxt_year":v['trxt_year']})
                            if measurement_type == '2' :
                                if v['total_margin'] != None and v['total_margin'] > 0:
                                    total_store_sale = total_store_sale+float(v['total_margin'])
                            elif measurement_type == '3' :
                                if v['total_amount'] != None and v['total_amount'] > 0:
                                    total_store_sale = total_store_sale+float(v['total_amount'])  
                            else:
                                if v['total_amount'] != None and v['total_amount'] > 0:
                                    total_store_sale = total_store_sale+v['total_amount']
                            TY_total_customers = TY_total_customers+v['total_customers']        
                        elif c == 1 and v["trxt_year"] == prev_year:  
                            
                            total_promo_sale = v['total_promo_sale']
                            total_discount = v['total_discount']
                            total_qty = v['total_qty']
                            total_cost = v['total_cost']
                            gp_percent = v['gp_percent']
                            total_margin = v['total_margin']
                            total_amount = v['total_amount']
                            total_customers = v['total_customers']
                            if v['id'] in last_day_sales :
                                total_promo_sale = last_day_sales[v['id']][0]['total_promo_sale']+v['total_promo_sale']
                                total_discount = last_day_sales[v['id']][0]['total_discount']+v['total_discount']
                                total_qty = last_day_sales[v['id']][0]['total_qty']+v['total_qty']
                                total_cost = last_day_sales[v['id']][0]['total_cost']+v['total_cost']
                                gp_percent = last_day_sales[v['id']][0]['gp_percent']+v['gp_percent']
                                total_margin = last_day_sales[v['id']][0]['total_margin']+v['total_margin']
                                total_amount = last_day_sales[v['id']][0]['total_amount']+v['total_amount']
                                total_customers = last_day_sales[v['id']][0]['total_customers']+v['total_customers']
                                
                            temp_Sale_TYS_VS_LYS_X["x"].append({'id':v['id'],'name':v['title'].encode("ascii"), 'total_promo_sale': total_promo_sale,"total_discount":total_discount, "total_qty":total_qty, "total_cost":total_cost, "gp_percent":gp_percent, "total_margin":total_margin, "total_amount":total_amount,"total_customers":total_customers, "trxt_year":v['trxt_year']})  
                            LY_total_customers = LY_total_customers+total_customers
                        elif c ==1 and v["trxt_year"] == current_year :         
                            temp_Sale_TYS_VS_LYS_X["y"].append({'id':v['id'],'name':v['title'].encode("ascii"), "total_promo_sale": v['total_promo_sale'],"total_discount":v['total_discount'], "total_qty":v['total_qty'], "total_cost":v['total_cost'], "gp_percent":v['gp_percent'], "total_margin":v['total_margin'], "total_amount":v['total_amount'],"total_customers":v['total_customers'], "trxt_year":v['trxt_year']})
                            if measurement_type == '2' :
                                if v['total_margin'] != None and v['total_margin'] > 0:
                                    total_store_sale = total_store_sale+float(v['total_margin'])
                            elif measurement_type == '3' :
                                if v['total_amount'] != None and v['total_amount'] > 0:
                                    total_store_sale = total_store_sale+float(v['total_amount'])  
                            else:
                                if v['total_amount'] != None and v['total_amount'] > 0:
                                    total_store_sale = total_store_sale+v['total_amount']
                            TY_total_customers = TY_total_customers+v['total_customers']        
                        c=c+1 
                
            # Get total stores for primary years
            TY_primary_total_stores = 1
            if p_zone_id != '' :
                TY_stores_query = "SELECT distinct (btoa.btoa_outlet_id) as id FROM bi_trx_outlet_aggregates btoa JOIN bi_trx_time btt ON (btt.trxt_id = btoa.btoa_time_id and (btt.trxt_date BETWEEN '"+start_date+"' AND '"+end_date+"')) WHERE btoa.btoa_outlet_id = (SELECT zo.zout_outlet_id from zone_outlet zo where btoa.btoa_outlet_id = zo.zout_outlet_id AND zo.zout_zone_id ='"+p_zone_id+"')"
                TY_stores_sql = pd.read_sql_query(TY_stores_query,con=self.db)
                TY_stores_result=TY_stores_sql.groupby(['id'])
                TY_primary_total_stores = len(TY_stores_result)

            # Do x/y now to get = Y !!
            temp_Sale_TYS_VS_LYS_X["x"]=sorted(temp_Sale_TYS_VS_LYS_X["x"],key=lambda i: i["id"])
            temp_Sale_TYS_VS_LYS_X["y"]=sorted(temp_Sale_TYS_VS_LYS_X["y"],key=lambda i: i["id"])
            #print('=====XXXXX=====')            
            #print(temp_Sale_TYS_VS_LYS_X["x"])
            #print('=====YYYYY=====')
            #print(temp_Sale_TYS_VS_LYS_X["y"])
            temp_Sale_TYS_VS_LYS_X_F = []
            #if analysis_level == '3' :
            for i in range(0,len(temp_Sale_TYS_VS_LYS_X["y"])):
                is_found=0
                for v in range(0,len(temp_Sale_TYS_VS_LYS_X["x"])):
                    if temp_Sale_TYS_VS_LYS_X["x"][v]['id'] == temp_Sale_TYS_VS_LYS_X["y"][i]['id'] :
                        temp_Sale_TYS_VS_LYS_X_F.append({'id':temp_Sale_TYS_VS_LYS_X["x"][v]['id'],'name':temp_Sale_TYS_VS_LYS_X["x"][v]['name'].encode("ascii"), "total_promo_sale": temp_Sale_TYS_VS_LYS_X["x"][v]['total_promo_sale'],"total_discount":temp_Sale_TYS_VS_LYS_X["x"][v]['total_discount'], "total_qty":temp_Sale_TYS_VS_LYS_X["x"][v]['total_qty'], "total_cost":temp_Sale_TYS_VS_LYS_X["x"][v]['total_cost'], "gp_percent":temp_Sale_TYS_VS_LYS_X["x"][v]['gp_percent'], "total_margin":temp_Sale_TYS_VS_LYS_X["x"][v]['total_margin'], "total_amount":temp_Sale_TYS_VS_LYS_X["x"][v]['total_amount'], 
                        "total_customers":temp_Sale_TYS_VS_LYS_X["x"][v]['total_customers'],
                        "trxt_year":temp_Sale_TYS_VS_LYS_X["x"][v]['trxt_year']})
                        is_found=1
                        
                if is_found == 0 :
                    temp_Sale_TYS_VS_LYS_X_F.append({'id':temp_Sale_TYS_VS_LYS_X["y"][i]['id'],'name':temp_Sale_TYS_VS_LYS_X["y"][i]['name'].encode("ascii"), "total_promo_sale": 0,"total_discount":0, "total_qty":0, "total_cost":0, "gp_percent":0, "total_margin":0, "total_amount":0,"total_customers":0, "trxt_year":''})    
        
            temp_Sale_TYS_VS_LYS_X["x"] = temp_Sale_TYS_VS_LYS_X_F
            # print("====11111111111====>")
            # print(temp_Sale_TYS_VS_LYS_X["x"])
            # print("====22222222222====>")
            # print(temp_Sale_TYS_VS_LYS_X["y"])
            #print("====33333333333====>")
            #print("========temp_Sale_TYS_VS_LYS_X=====>")
            TY_VS_LY_AVG_Y = []
            if temp_Sale_TYS_VS_LYS_X["x"] and temp_Sale_TYS_VS_LYS_X["y"] :
                for i in range(0,len(temp_Sale_TYS_VS_LYS_X["x"])):
                    if i < 20 :
                        total_amount_x = 0
                        total_amount_y = 0
                        if measurement_type == '2' :
                            total_amount_x = np.divide(temp_Sale_TYS_VS_LYS_X["x"][i]["total_margin"],TY_primary_total_stores)
                            total_amount_y = np.divide(temp_Sale_TYS_VS_LYS_X["y"][i]["total_margin"],TY_primary_total_stores)
                        elif measurement_type == '3' :
                            if temp_Sale_TYS_VS_LYS_X["x"][i]["total_amount"] > 0 and LY_total_customers > 0 :
                                total_amount_x = np.divide(np.divide(temp_Sale_TYS_VS_LYS_X["x"][i]["total_amount"],TY_primary_total_stores),np.divide(LY_total_customers,TY_primary_total_stores))
                                
                            if temp_Sale_TYS_VS_LYS_X["y"][i]["total_amount"] > 0 and TY_total_customers > 0 :  
                                total_amount_y = np.divide(np.divide(temp_Sale_TYS_VS_LYS_X["y"][i]["total_amount"],TY_primary_total_stores),np.divide(TY_total_customers,TY_primary_total_stores))
                        else:
                            total_amount_x = np.divide(temp_Sale_TYS_VS_LYS_X["x"][i]["total_amount"],TY_primary_total_stores) 
                            total_amount_y = np.divide(temp_Sale_TYS_VS_LYS_X["y"][i]["total_amount"],TY_primary_total_stores)

                        # Set last this year param values
                        id = ''
                        name = ''
                        #if i in temp_Sale_TYS_VS_LYS_X["y"]:
                        id = temp_Sale_TYS_VS_LYS_X["y"][i]["id"]
                        name = temp_Sale_TYS_VS_LYS_X["y"][i]["name"]
                        temp_total_amount_y = 0
                        if total_amount_y > 0 and total_amount_x > 0 :
                            temp_total_amount_y = ((np.divide(total_amount_y,total_amount_x))-1)*100
                        
                        # Calculate bubble size
                        bubble_size = 0
                        if total_amount_y > 0 and total_store_sale > 0 :
                            bubble_size = (np.divide(total_amount_y,total_store_sale))*500
                            #bubble_size = np.power(bubble_size, np.true_divide(1,3))
                        #if bubble_size > 80 :
                            #bubble_size = 80 
                        graph_bubble_size.append(bubble_size)
                        gy_total_amount = 0
                        if float('-inf') < float(temp_total_amount_y) < float('inf'):
                            gy_total_amount = temp_total_amount_y
                        TY_VS_LY_AVG_Y.append({"id":id,"name":name,"total_amount": gy_total_amount,"total_amount_y":total_amount_y})    
                        # -- Graph Y Axis --
                        graph_y.append(gy_total_amount)  # -- Y Axis Points
                        graph_text.append(temp_Sale_TYS_VS_LYS_X["x"][i]["name"])   # -- Axis Points Lables
                        graph_customdata.append(temp_Sale_TYS_VS_LYS_X["x"][i]["id"]) # -- Axis Points Customdata

        # Get secondary comparison data
        if(s_outlet_id != '' or s_zone_id != '') :
            if s_zone_id != '' :
                s_zone_outl_sql = self.zone_outl_query_condition(s_zone_id)
            
            # Get total stores for secondary years
            TYC_total_stores = 1
            if s_zone_id != '' :
                TYC_stores_query = "SELECT distinct (btoa.btoa_outlet_id) as id FROM bi_trx_outlet_aggregates btoa JOIN bi_trx_time btt ON (btt.trxt_id = btoa.btoa_time_id and (btt.trxt_date BETWEEN '"+start_date+"' AND '"+end_date+"')) WHERE btoa.btoa_outlet_id = (SELECT zo.zout_outlet_id from zone_outlet zo where btoa.btoa_outlet_id = zo.zout_outlet_id AND zo.zout_zone_id ='"+s_zone_id+"')"
                TYC_stores_sql = pd.read_sql_query(TYC_stores_query,con=self.db)
                TYC_stores_result=TYC_stores_sql.groupby(['id'])
                TYC_total_stores = len(TYC_stores_result)
            
            if analysis_level == '2' : 
                df_zone_sale_TYS_query = "SELECT btca.btca_commodity_id as id,(select commodity from commodity where commodity_id = btca.btca_commodity_id) as title,SUM(btca.btca_promo_sales) as total_promo_sale,SUM(btca.btca_discount) as total_discount,SUM(btca.btca_qty) as total_qty,SUM(btca.btca_cost) as total_cost,SUM(btca.btca_margin) as total_margin,(((SUM(btca.btca_amt) - SUM(btca.btca_cost)) / NULLIF(SUM(btca.btca_amt), 0))*100) as gp_percent,SUM(btca.btca_amt) as total_amount,SUM(btca.btca_customers_count) as total_customers,btt.trxt_year as year FROM bi_trx_commodity_aggregates btca JOIN bi_trx_time btt ON btt.trxt_id = btca.btca_time_id WHERE btca.btca_department_id  = '"+analysis_index_id+"' AND "
                if s_zone_id != '' :
                    df_zone_sale_TYS_query += "btca.btca_outlet_id  IN ( select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+s_zone_id+"' )  AND "
                elif s_outlet_id != '':
                    df_zone_sale_TYS_query += "btca.btca_outlet_id = '"+s_outlet_id+"' AND "    
                df_zone_sale_TYS_query += "(btt.trxt_date BETWEEN '"+start_date+"' AND '"+end_date+"') GROUP BY btca.btca_commodity_id,btt.trxt_year ORDER BY btca.btca_commodity_id"
                df_zone_sale_TYS = pd.read_sql_query(df_zone_sale_TYS_query,con=self.db) 
                
            elif analysis_level == '3' :
                # Prepare comparison secondary this year top 20 product query
                if len(TY_top_products) > 0 :
                    df_zone_sale_TYS_query = "select (btpa.btpa_product_id) as id,btpa.btpa_product_name as title,SUM(btpa.btpa_promo_sales) as total_promo_sale,SUM(btpa.btpa_discount) as total_discount,SUM(btpa.btpa_qty) as total_qty,SUM(btpa.btpa_amt) as total_amount,SUM(btpa.btpa_cost) as total_cost,SUM(btpa.btpa_margin) as total_margin,(((SUM(btpa.btpa_amt) - SUM(btpa.btpa_cost)) / NULLIF(SUM(btpa.btpa_amt),0))*100) as gp_percent,SUM(btpa.btpa_customers) as total_customers,btt.trxt_year FROM bi_trx_product_aggregates btpa join bi_trx_time btt on btt.trxt_id = btpa.btpa_time_id WHERE ("

                    for i in range(0,len(TY_top_products)):
                        df_zone_sale_TYS_query += " btpa.btpa_product_id  = '"+str(TY_top_products[i])+"' "
                        if i != (len(TY_top_products)-1) :
                            df_zone_sale_TYS_query += " OR " 
                    df_zone_sale_TYS_query += ") AND (btt.trxt_date BETWEEN '"+start_date+"' AND '"+end_date+"') AND "
                    if s_zone_id != '' :
                        df_zone_sale_TYS_query += "btpa.btpa_outlet_id  IN ( select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+s_zone_id+"' )  AND "
                    else :
                        df_zone_sale_TYS_query += " btpa.btpa_outlet_id = '"+s_outlet_id+"' AND "    
                    df_zone_sale_TYS_query += "btpa_commodity_id = '"+commodity_id+"' GROUP BY btpa.btpa_product_id, btpa.btpa_product_name,btpa.btpa_commodity_id,btt.trxt_year having SUM(btpa.btpa_amt) > 0 ORDER BY total_amount DESC"
                    df_zone_sale_TYS = pd.read_sql_query(df_zone_sale_TYS_query,con=self.db)  
            else:
                df_zone_sale_TYS_query = "SELECT btda.btda_department_id as id,btda.btda_department_name as title,SUM(btda.btda_promo_sales) as total_promo_sale,SUM(btda.btda_discount) as total_discount,SUM(btda.btda_qty) as total_qty,SUM(btda.btda_cost) as total_cost,SUM(btda.btda_margin) as total_margin,(((SUM(btda.btda_amt) - SUM(btda.btda_cost)) / NULLIF(SUM(btda.btda_amt),0))*100) as gp_percent,SUM(btda.btda_amt) as total_amount,SUM(btda.btda_customers_count) as total_customers,btt.trxt_year as year FROM bi_trx_department_aggregates btda JOIN bi_trx_time btt ON btt.trxt_id = btda.btda_time_id WHERE "
                if s_zone_id != '' :
                    df_zone_sale_TYS_query += "btda.btda_outlet_id  IN ( select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+s_zone_id+"' )  AND "
                elif s_outlet_id != '':
                    df_zone_sale_TYS_query += "btda.btda_outlet_id = '"+s_outlet_id+"' AND "    
                df_zone_sale_TYS_query += "(btt.trxt_date BETWEEN '"+start_date+"' AND '"+end_date+"') GROUP BY btda.btda_department_id,btda.btda_department_name,btt.trxt_year ORDER BY btda.btda_department_id"    
                df_zone_sale_TYS = pd.read_sql_query(df_zone_sale_TYS_query,con=self.db)
            
            for v in df_zone_sale_TYS.to_dict(orient="records"):
                # print(v['btda_department_id'])
                total_amount = 0
                z_total_amount = 0
                compare_avg_amt = 0
                # Set amount as per measurement type
                if measurement_type == '2' :
                    total_amount = v['total_margin']
                elif measurement_type == '3' :
                    total_amount = v['total_amount']
                    TYC_total_customers = TYC_total_customers+v['total_customers']
                else:
                    total_amount = v['total_amount']
                # Set amount as per filter type outlet/zone    
                if s_zone_id != '' :
                    total_stores = TYC_total_stores 
                    compare_avg_amt = total_amount/total_stores
                    TY_secondary_total_stores = total_stores 

                elif s_outlet_id != '':
                    compare_avg_amt = total_amount
                    TY_secondary_total_stores = 1
                if float('-inf') < float(compare_avg_amt) < float('inf'):
                    z_total_amount = compare_avg_amt
                temp_Sale_TYS_VS_LYS_Y.append({"id": v['id'], "name":v['title'].encode("ascii"),"z_total_amount": z_total_amount,"total_amount": total_amount})

            # Get => X-Axis!!
            # Do x/y now to get = X !!
            TY_VS_LY_AVG_Y = sorted(TY_VS_LY_AVG_Y,key=lambda i: i["id"])
            temp_Sale_TYS_VS_LYS_Y = sorted(temp_Sale_TYS_VS_LYS_Y,key=lambda i: i["name"])
            
            temp_Sale_TYS_VS_LYS_Y_F = []
            #if analysis_level == '3' :
            for i in range(0,len(temp_Sale_TYS_VS_LYS_X["y"])):
                is_found=0
                for v in range(0,len(temp_Sale_TYS_VS_LYS_Y)):
                    if temp_Sale_TYS_VS_LYS_Y[v]['id'] == temp_Sale_TYS_VS_LYS_X["y"][i]['id'] :
                        temp_Sale_TYS_VS_LYS_Y_F.append({'id':temp_Sale_TYS_VS_LYS_Y[v]['id'],'name':temp_Sale_TYS_VS_LYS_Y[v]['name'].encode("ascii"), "total_amount":temp_Sale_TYS_VS_LYS_Y[v]['total_amount']})
                        is_found=1
                        
                if is_found == 0 :
                    temp_Sale_TYS_VS_LYS_Y_F.append({'id':temp_Sale_TYS_VS_LYS_X["y"][i]['id'],'name':temp_Sale_TYS_VS_LYS_X["y"][i]['name'].encode("ascii"),"total_amount":0})    
        
            temp_Sale_TYS_VS_LYS_Y = temp_Sale_TYS_VS_LYS_Y_F
            
            primary_TY_VS_LY_X = []
            if temp_Sale_TYS_VS_LYS_Y :
                for i in range(0,len(temp_Sale_TYS_VS_LYS_Y)):
                    d_id = temp_Sale_TYS_VS_LYS_Y[i]["id"]
                    name = temp_Sale_TYS_VS_LYS_Y[i]["name"]
                    total_amount = 0
                    TY_total_amount = 0
                    for j in range(0,len(TY_VS_LY_AVG_Y)):
                        if temp_Sale_TYS_VS_LYS_Y[i]["id"] == TY_VS_LY_AVG_Y[j]["id"]:
                            d_id = TY_VS_LY_AVG_Y[j]["id"]
                            name = TY_VS_LY_AVG_Y[j]["name"]
                            total_amount = temp_Sale_TYS_VS_LYS_Y[i]["total_amount"]
                            TY_total_amount = TY_VS_LY_AVG_Y[j]["total_amount_y"]
                    p_total_amount = 0
                    if float('-inf') < float(total_amount) < float('inf'):
                        p_total_amount = total_amount
                    # Set amount as per measurement type
                    if measurement_type == '3' :
                        p_total_amount = np.divide(p_total_amount,TY_secondary_total_stores)
                        p_total_amount = np.divide(p_total_amount,np.divide(TYC_total_customers,TY_secondary_total_stores))
                    else :
                        TY_total_amount = np.divide(p_total_amount,TY_secondary_total_stores)
                        p_total_amount = np.divide(p_total_amount,TY_secondary_total_stores)
                    primary_TY_VS_LY_X.append({"id": d_id, "name":name,"total_amount":p_total_amount,"TY_total_amount":TY_total_amount})
            #print('=TY_total_customers=')
            #print(TY_total_customers)
            #print('=LY_total_customers=')
            #print(LY_total_customers)        
            
            if TY_VS_LY_AVG_Y :
                for i in range(0,len(TY_VS_LY_AVG_Y)):
                    temp_total_amount_x = 0
                    if primary_TY_VS_LY_X[i]["id"] == TY_VS_LY_AVG_Y[i]["id"] :
                        #if primary_TY_VS_LY_X[i]["id"] == 1 :
                        #    print(primary_TY_VS_LY_X[i]["total_amount"])
                        #    print(TY_VS_LY_AVG_Y[i]["total_amount"])
                        # Set amount as per measurement type
                        TY_total_amount = primary_TY_VS_LY_X[i]["total_amount"]
                        total_amount_y = TY_VS_LY_AVG_Y[i]["total_amount_y"]
                        if total_amount_y > 0 :
                            temp_total_amount_x = (np.divide(total_amount_y,TY_total_amount)-1)*100
                        # -- Graph X Axis --

                    gx_total_amount = 0
                    if float('-inf') < float(temp_total_amount_x) < float('inf'):
                        gx_total_amount = temp_total_amount_x    
                    graph_x.append(gx_total_amount)

        # Get unsold ranging products
        ranging_products = []
        if analysis_level == '3' :
            ranging_products = self.ranging_products(start_date,end_date,p_outlet_id,p_zone_id,analysis_index_id,p_zone_outl_sql)

        graph_marker_labels = []
        graph_inside_marker_labels = []
        graph_dictionary = []
        # Define color code array
        colors = ["#FF6600", "#FCD202", "#B0DE09", "#0D8ECF", "#2A0CD0", "#CD0D74", "#CC0000", "#00CC00", "#0000CC", "#DDDDDD", "#999999", "#333333", "#990000","#FF6600", "#FCD202", "#B0DE09", "#0D8ECF", "#2A0CD0", "#CD0D74", "#CC0000"]
        # Set marker name as combination of name and axis values
        if len(graph_x) > 0 :
            for i in range(0,len(graph_text)):
                # Set marker label with axis points
                m_label = re.sub(' +', ' ',graph_text[i])+'('+str(round(graph_x[i], 2))+' , '+str(round(graph_y[i], 2))+')'
                graph_marker_labels.append(m_label)
                # Set inside marker label
                graph_inside_marker_labels.append(re.sub(' +', ' ',graph_text[i]))
                # Prepare dictionary 
                myDict = {}
                myDict["x"] = round(graph_x[i], 2)
                myDict["y"] = round(graph_y[i], 2)
                myDict["value"] = graph_bubble_size[i]
                label = graph_marker_labels[i]
                #if myDict["x"] < 0 and myDict["y"] < 0 :
                #    label = graph_marker_labels[i]+'('+str(graph_customdata[i])+')'
                myDict["label"] = label
                myDict["inside_marker_label"] = graph_inside_marker_labels[i]
                myDict["id"] = graph_customdata[i]
                myDict["color"] = colors[i]
                #myDict["department_id"] = department_id
                myDict["level"] = analysis_level
                graph_dictionary.append(myDict)
       
        res = [graph_x, graph_y, graph_marker_labels, graph_customdata, graph_bubble_size,graph_dictionary,analysis_chart_label,department_name,temp_Sale_TYS_VS_LYS_X["y"],temp_Sale_TYS_VS_LYS_X["x"],temp_Sale_TYS_VS_LYS_Y,ranging_products,TY_total_customers,LY_total_customers,TYC_total_customers,TY_secondary_total_stores,TY_primary_total_stores]
        return res

    # --- Manage product level ranging sales
    def ranging_products(self,start_date,end_date,p_outlet_id,p_zone_id,analysis_index_id,p_zone_outl_sql):
        cGroup = {}
        # Get group sale 
        df_group_sale_TYS_query = "SELECT TRX_PRODUCT, PROD_DESC, PROD_NATIONAL, SUM(TRX_QTY) as SUM_QTY, SUM(TRX_AMT) as SUM_AMT, (SUM(TRX_AMT) / NULLIF(COUNT(TRX_PRODUCT), 0)) as AVG_SALES FROM TRXTBL WITH(NOLOCK),PRODTBL WHERE (TRX_DATE BETWEEN '"+start_date+"' AND '"+end_date+"') AND (TRX_PRODUCT=PROD_NUMBER) AND  (TRX_COMMODITY IN ('"+str(analysis_index_id)+"')) AND (TRX_TYPE = 'ITEMSALE') GROUP BY TRX_PRODUCT,PROD_DESC,PROD_NATIONAL ORDER BY SUM_AMT ASC"
        df_group_sale_TYS = pd.read_sql_query(df_group_sale_TYS_query,con=self.live_db)
        
        for v in df_group_sale_TYS.to_dict(orient="records"):
            cGroup[v['TRX_PRODUCT']] = {'TRX_PRODUCT':int(v['TRX_PRODUCT']),'PROD_DESC':v['PROD_DESC'].encode("ascii"),'SUM_AMT':v['SUM_AMT']}

        # Get outlet sale 
        outlet_excl_products = []
        df_outlet_sale_TYS_query = "SELECT TRX_PRODUCT, PROD_DESC, PROD_NATIONAL, SUM(TRX_QTY) as SUM_QTY, SUM(TRX_AMT) as SUM_AMT, (SUM(TRX_AMT) / NULLIF(COUNT(TRX_PRODUCT), 0)) as AVG_SALES FROM TRXTBL WITH(NOLOCK),PRODTBL WHERE (TRX_DATE BETWEEN '"+start_date+"' AND '"+end_date+"') AND (TRX_PRODUCT=PROD_NUMBER) AND "
        # Get zone outlets and concatinate outlets in query
        if p_zone_id != '' :
            if p_zone_outl_sql != '' :
                df_outlet_sale_TYS_query += "("+p_zone_outl_sql+") AND "
        else :
            df_outlet_sale_TYS_query += " TRX_OUTLET = '"+p_outlet_id+"' AND "
        df_outlet_sale_TYS_query += " (TRX_COMMODITY IN ('"+str(analysis_index_id)+"')) AND (TRX_TYPE = 'ITEMSALE') GROUP BY TRX_PRODUCT,PROD_DESC,PROD_NATIONAL ORDER BY SUM_AMT ASC"
        df_outlet_sale_TYS = pd.read_sql_query(df_outlet_sale_TYS_query,con=self.live_db)
        
        for v in df_outlet_sale_TYS.to_dict(orient="records"):
            outlet_excl_products.append(v['TRX_PRODUCT'])

        # Remove outlet product from group sale
        if outlet_excl_products :
            for i in range(0,len(outlet_excl_products)):
                del cGroup[outlet_excl_products[i]]
        return cGroup        

    # --- Manage zone outlets sql condition
    @cache.memoize(timeout=timeout)
    def zone_outl_query_condition(self,p_zone_id):
        zone_outl_res = {}
        p_zone_outl_sql = ''
        Zone_outlet_res = pd.read_sql_query("select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+p_zone_id+"'",con=self.db)
        Zone_outlet_res=Zone_outlet_res.groupby(['zout_outlet_id'])
        i = 1
        total_zone_outlets = len(Zone_outlet_res) 
        for k, v in Zone_outlet_res:
            temp_zone_split=v.to_dict(orient="records")
            for v in temp_zone_split:
                p_zone_outl_sql += "(TRX_OUTLET = '"+str(v['zout_outlet_id'])+"')"
                if i < total_zone_outlets :
                    p_zone_outl_sql += " OR "  
            i = i+1
        zone_outl_res['zone_outl_sql'] = p_zone_outl_sql
        zone_outl_res['total_zone_outlets'] = total_zone_outlets
        return zone_outl_res   

    # --- Manage product level sql queries
    @cache.memoize(timeout=timeout)
    def prepare_product_query(self,p_outlet_id,p_zone_id,start_date,end_date,commodity_id,LY_start_date,LY_end_date,p_zone_outl_sql):
        product_sql = {}
        # Prepare this year top 20 product query 
        TY_p_sql = "select top (20) (btpa.btpa_product_id) as id,btpa.btpa_product_name as title,SUM(btpa.btpa_amt) as total_amount,SUM(btpa.btpa_cost) as total_cost,SUM(btpa.btpa_margin) as total_margin,(((SUM(btpa.btpa_amt) - SUM(btpa.btpa_cost)) / NULLIF(SUM(btpa.btpa_amt), 0))*100) as gp_percent,SUM(btpa.btpa_customers) as total_customers FROM bi_trx_product_aggregates btpa join bi_trx_time btt on btt.trxt_id = btpa.btpa_time_id WHERE (btt.trxt_date BETWEEN '"+start_date+"' AND '"+end_date+"') AND "
        if p_zone_id != '' :
            TY_p_sql += "btpa.btpa_outlet_id  IN ( select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+p_zone_id+"' )  AND "
        else :   
            TY_p_sql += " btpa.btpa_outlet_id = '"+p_outlet_id+"' AND "
        TY_p_sql += "btpa_commodity_id = '"+commodity_id+"' GROUP BY btpa.btpa_product_id, btpa.btpa_product_name,btpa.btpa_commodity_id,btpa.btpa_outlet_id ORDER BY total_amount DESC"

        #TY_p_sql = "SELECT * FROM ( SELECT TRX_COMMODITY AS SUM_CODE,TRX_COMMODITY,SUM(TRX_QTY) as total_qty,SUM(TRX_AMT) as total_amount, SUM(TRX_COST) as total_cost,SUM(TRX_PROM_SALES) as total_promo_sale,SUM(TRX_PROM_SALES_GST) AS SUM_PROM_SALES_GST,SUM(TRX_AMT - TRX_COST) as total_margin,(((SUM(TRX_AMT) - SUM(TRX_COST)) * 100) / NULLIF(SUM(TRX_AMT), 0)) as gp_percent,SUM(TRX_DISCOUNT) as total_discount,X.CODE_DESC AS CODE_DESC,TRX_PRODUCT as id,PROD_DESC as title,ROW_NUMBER() OVER (ORDER BY TRX_PRODUCT DESC) as row  FROM TRXTBL LEFT JOIN CODETBL X ON X.CODE_KEY_TYPE = 'COMMODITY' AND X.CODE_KEY_NUM = TRX_COMMODITY LEFT JOIN PRODTBL ON PROD_NUMBER = TRX_PRODUCT WHERE (TRX_DATE BETWEEN '"+start_date+"' AND '"+end_date+"') AND "
        #if p_zone_id != '' :
        #    if p_zone_outl_sql['zone_outl_sql'] != '' :
        #        TY_p_sql += " ("+p_zone_outl_sql['zone_outl_sql']+") AND "
        #else :   
        #    TY_p_sql += " TRX_OUTLET = '"+p_outlet_id+"' AND "
        #TY_p_sql += "(TRX_COMMODITY = '"+commodity_id+"' ) AND (TRX_TYPE = 'ITEMSALE') GROUP BY CODE_DESC, PROD_DESC ,TRX_COMMODITY, TRX_PRODUCT  ) a WHERE row > 0 AND row <= 20"

        # Prepare last year top 20 product query
        LY_p_sql = "SELECT TRX_COMMODITY AS SUM_CODE,TRX_COMMODITY,SUM(TRX_QTY) as total_qty,SUM(TRX_AMT) as total_amount, SUM(TRX_COST) as total_cost,SUM(TRX_PROM_SALES) as total_promo_sale,SUM(TRX_PROM_SALES_GST) AS SUM_PROM_SALES_GST,SUM(TRX_AMT - TRX_COST) as total_margin,(((SUM(TRX_AMT) - SUM(TRX_COST)) * 100) / NULLIF(SUM(TRX_AMT), 0)) as gp_percent,SUM(TRX_DISCOUNT) as total_discount,X.CODE_DESC AS CODE_DESC,TRX_PRODUCT as id,PROD_DESC as title FROM TRXTBL LEFT JOIN CODETBL X ON X.CODE_KEY_TYPE = 'COMMODITY' AND X.CODE_KEY_NUM = TRX_COMMODITY LEFT JOIN PRODTBL ON PROD_NUMBER = TRX_PRODUCT WHERE TRX_PRODUCT IN (SELECT TOP 20 TRX_PRODUCT FROM TRXTBL LEFT JOIN CODETBL X ON X.CODE_KEY_TYPE = 'COMMODITY' AND X.CODE_KEY_NUM = TRX_COMMODITY LEFT JOIN PRODTBL ON PROD_NUMBER = TRX_PRODUCT WHERE (TRX_DATE BETWEEN '"+start_date+"' AND '"+end_date+"') AND "
        if p_zone_id  != '' :
            if p_zone_outl_sql['zone_outl_sql'] != '' :
                LY_p_sql += "("+p_zone_outl_sql['zone_outl_sql']+") AND "
        else :    
            LY_p_sql += " TRX_OUTLET = '"+p_outlet_id+"' AND "

        LY_p_sql += "(TRX_COMMODITY = '"+commodity_id+"' ) AND (TRX_TYPE = 'ITEMSALE') GROUP BY CODE_DESC, PROD_DESC ,TRX_COMMODITY, TRX_PRODUCT ORDER BY TRX_PRODUCT DESC) AND (TRX_DATE BETWEEN '"+LY_start_date+"' AND '"+LY_end_date+"') AND "
        if p_zone_id  != '' :
            if p_zone_outl_sql != '' :
                LY_p_sql += "("+p_zone_outl_sql+") AND "
        else :    
            LY_p_sql += " TRX_OUTLET = '"+p_outlet_id+"' AND "
        LY_p_sql += "(TRX_COMMODITY = '"+commodity_id+"' ) AND (TRX_TYPE = 'ITEMSALE') GROUP BY CODE_DESC, PROD_DESC ,TRX_COMMODITY, TRX_PRODUCT ORDER BY TRX_PRODUCT DESC"
        
        # Append data in array
        product_sql['TY_p_sql'] = TY_p_sql
        product_sql['LY_p_sql'] = LY_p_sql
        return product_sql 

    #  --- Check if the int given year is a leap year: return true if leap year or false otherwise
    @cache.memoize(timeout=timeout)
    def is_leap_year(self,year):
        leap_year_days = 366
        non_leap_year_days = 365
        if (year % 4) == 0:
            if (year % 100) == 0:
                if (year % 400) == 0:
                    return leap_year_days
                else:
                    return non_leap_year_days
            else:
                return leap_year_days
        else:
            return non_leap_year_days    

    def __repr__(self):
        return '<Book %r>' % (self.title) 
    