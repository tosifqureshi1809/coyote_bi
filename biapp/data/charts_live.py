from flask import Flask, Response, g, request,session
from flask_caching import Cache
from flask_sqlalchemy import SQLAlchemy
import sqlalchemy
import pandas as pd
import numpy as np

# -- Important Assets STARTS --
from biapp.db import get_db,get_livedb,get_testdb
from biapp.app import server
from datetime import datetime , timedelta

# -- Important Assets ENDS --
cache = Cache(server, config={'CACHE_TYPE': 'simple'})
timeout = 20
# --- Initialize redis server ---
#import redis
#redis_conn = redis.Redis('127.0.0.1')

class charts():
    title = ""
    db = []
    TYS_result = []
    LYS_result = []
    TYCS_result = []
    g_stores_count = 0
    
    @cache.memoize(timeout=timeout)
    def __init__(self):
        # Get session data
        environment = 'LIVE'
        if 'env_data' in session:
            env_data = session['env_data']
            # Get filter data; If exist
            if env_data.get("environment") != None:
                environment = env_data.get("environment")
        with server.app_context():        
            if 'db' not in g:
                self.db = get_db()
                g.db = self.db
            if 'live_db' not in g:
                if environment == 'LIVE' :
                    self.live_db = get_livedb()
                else :
                    self.live_db = get_testdb()  
                g.live_db = self.live_db
        
    
    @cache.memoize(timeout=timeout)
    def pg_scatterchart_TYS_vs_LYS(self):
        # -- For Graph X-Axis, Y-Axis, Custome Data for current Years. --
        temp_Sale_TYS_VS_LYS_X = []
        temp_Sale_TYS_VS_LYS_X2 = []
        temp_Sale_TYS_VS_LYS_Y = []
        
        # -- For Graph X-Axis, Y-Axis, Custome Data for current Years. --
        graph_x1 = []
        graph_x2 = []
        graph_y1 = []
        graph_y2 = []
        graph_y3 = []
        graph_text = []
        graph_customdata = []
        TY_weeks_count = 0
        LY_weeks_count = 0
        TY_week_start_date = ""
        LY_last_weekend_date = ""
        LY_first_weekend = ""
        LY_second_week_start_date = ""
        primary_type = 1
        secondary_type = 2
        # Get drilldown level filter data; If exist
        analysis_level = '1'
        analysis_index_id = ''
        analysis_department_id = ''
        if request.args.get('level') is not None:
            analysis_level = request.args.get('level')
        if request.args.get('id') is not None:    
            analysis_index_id = request.args.get('id')
        if request.args.get('department_id') is not None:    
            analysis_department_id = request.args.get('department_id')  
            
        # Get session filter data
        if 'filter_data' in session:
            filter_data = session['filter_data']
        # Set filter params
        p_outlet_id = ''
        p_zone_id = ''
        s_outlet_id = ''
        s_zone_id = ''
        start_date = ''
        end_date = ''
        comparison_period = 1
        if filter_data.get("p_outlet_id") != None and filter_data.get("p_outlet_id") != '':
            p_outlet_id = filter_data.get("p_outlet_id")
        
        if filter_data.get("p_zone_id") != None and filter_data.get("p_zone_id") != '':
            p_zone_id = filter_data.get("p_zone_id")
            # Get zone code
            zonecode_query = "SELECT zone_code FROM zone  WHERE zone_id = '"+p_zone_id+"'"
            zonecode_result = pd.read_sql_query(zonecode_query,con=self.db)
            for v in zonecode_result.to_dict(orient="records"):
               p_zone_id = v['zone_code']
       
        if filter_data.get("s_outlet_id") != None and filter_data.get("s_outlet_id") != '':
            s_outlet_id = filter_data.get("s_outlet_id")
        
        if filter_data.get("s_zone_id") != None and filter_data.get("s_zone_id") != '':
            s_zone_id = filter_data.get("s_zone_id")
            # Get zone code
            zonecode_query = "SELECT zone_code FROM zone  WHERE zone_id = '"+s_zone_id+"'"
            zonecode_result = pd.read_sql_query(zonecode_query,con=self.db)
            for v in zonecode_result.to_dict(orient="records"):
               s_zone_id = v['zone_code']
        
        if filter_data.get("start_date") != None and filter_data.get("start_date") != '' and filter_data.get("end_date") != None and filter_data.get("end_date") != '':
            start_date = filter_data.get("start_date")
            end_date = filter_data.get("end_date")

        if filter_data.get("comparison_period") != None and filter_data.get("comparison_period") != '':
            comparison_period = int(filter_data.get("comparison_period"))
        
        # return if date range empty
        if start_date == '' or end_date == '' :
            res = [graph_x1, graph_y1, graph_y2, graph_y3, graph_x2]
            return res

        # -- Set dates
        convt_startdate = datetime.strptime(start_date, '%Y-%m-%d').date()
        current_year = int(convt_startdate.strftime('%Y'))
        current_month = convt_startdate.strftime('%m')
        current_day = convt_startdate.strftime('%d')

        convt_enddate = datetime.strptime(end_date, '%Y-%m-%d').date()
        current_endmonth = convt_enddate.strftime('%m')
        current_endday = convt_enddate.strftime('%d')
        
        prev_year = current_year-comparison_period
        year_days = self.is_leap_year(prev_year)-1
        
        convt_prev_startdate = datetime.strptime(str(prev_year)+'-'+current_month+'-'+current_day, '%Y-%m-%d').date()
        convt_prev_enddate = datetime.strptime(str(prev_year)+'-'+current_endmonth+'-'+current_endday, '%Y-%m-%d').date()
        
        convt_startdate.strftime('%Y-%m-%d')
        
        #LY_start_date = convt_startdate - timedelta(days=year_days)
        LY_start_date = convt_prev_startdate.strftime('%Y-%m-%d')
        
        convt_enddate = datetime.strptime(end_date, '%Y-%m-%d').date()
        convt_enddate.strftime('%Y-%m-%d')
        
        #LY_end_date = convt_enddate - timedelta(days=year_days)
        LY_end_date = convt_prev_enddate.strftime('%Y-%m-%d')
        # -- Set departmet | commodity id based on level
        department_id = ''
        commodity_id  = ''
        if analysis_level == '2' :
            department_id  = analysis_index_id
        elif analysis_level == '3' :
            department_id  = analysis_department_id
            commodity_id  = analysis_index_id

        # -- For this year sales. --
        if(p_outlet_id != '' or p_zone_id != '') :

            # -- Append this year sales data in XY graph set
            df_sale_TYS_VS_LYS = self.filter_till_weekend_sales(start_date,end_date,p_outlet_id,p_zone_id,analysis_level,department_id,commodity_id,primary_type)
            global TYS_result
            TYS_result = df_sale_TYS_VS_LYS
            for tys in df_sale_TYS_VS_LYS :
                 # -- X for current year --
                graph_x1.append(tys['week_end_date'].encode("ascii"))                 
                graph_y1.append((tys['total_amount']/tys['total_stores']))

            # -- Append last year sales data in XY graph set
            df_sale_TYS_VS_LYS = self.filter_till_weekend_sales(LY_start_date,LY_end_date,p_outlet_id,p_zone_id,analysis_level,department_id,commodity_id,primary_type)
            global LYS_result
            LYS_result = df_sale_TYS_VS_LYS
            for tys in df_sale_TYS_VS_LYS :
                 # -- X for current year --
                graph_x2.append(tys['week_end_date'].encode("ascii"))                 
                graph_y2.append((tys['total_amount']/tys['total_stores']))

        # -- Append this year secondary sales data in XY graph set
        df_sale_sec_TYS_VS_LYS = self.filter_till_weekend_sales(start_date,end_date,s_outlet_id,s_zone_id,analysis_level,department_id,commodity_id,secondary_type)
        global TYCS_result
        TYCS_result = df_sale_sec_TYS_VS_LYS
        for tyss in df_sale_sec_TYS_VS_LYS :
            # -- Y for current year secondary filter --               
            graph_y3.append((tyss['total_amount']/tyss['total_stores']))
            
        graph_dictionary = []
        # Set marker name as combination of name and axis values
        for i in range(0,len(graph_x1)):
            # Handle exeption  
            try:
                pty_value = float(graph_y1[i])
            except IndexError:
                pty_value = 0
            try:
                ply_value = float(graph_y2[i])
            except IndexError:
                ply_value = 0
            try:
                sty_value = float(graph_y3[i])
            except IndexError:
                sty_value = 0
            # Prepare dictionary 
            myDict = {}
            myDict["weeks"] = graph_x1[i]
            myDict["pty_value"] = pty_value
            myDict["ply_value"] = ply_value
            myDict["sty_value"] = sty_value
            graph_dictionary.append(myDict)

        res = [graph_x1, graph_y1, graph_y2, graph_y3, graph_x2,graph_dictionary]

        return res

    # Check if the int given year is a leap year
    # return true if leap year or false otherwise
    @cache.memoize(timeout=timeout)
    def is_leap_year(self,year):
        leap_year_days = 366
        non_leap_year_days = 365
        if (year % 4) == 0:
            if (year % 100) == 0:
                if (year % 400) == 0:
                    return leap_year_days
                else:
                    return non_leap_year_days
            else:
                return leap_year_days
        else:
            return non_leap_year_days

    @cache.memoize(timeout=timeout)
    def pg_scatterchart_TYGP_vs_LYGP(self):
        
        # -- For Graph X-Axis, Y-Axis, Custome Data for current Years. --
        temp_Sale_TYGP_vs_LYGP_X = []
        temp_Sale_TYGP_vs_LYGP_Y = []
        temp_Sale_TYGP_VS_LYGP_X2 = []
        
        # -- For Graph X-Axis, Y-Axis, Custome Data for current Years. --
        graph_x1 = []
        graph_x2 = []
        graph_y1 = []
        graph_y2 = []
        graph_y3 = []
        graph_text = []
        graph_customdata = []
        TY_weeks_count = 0
        LY_weeks_count = 0
        TY_week_start_date = ""
        LY_last_weekend_date = ""
        LY_first_weekend = ""
        LY_second_week_start_date = ""
        primary_type = 1
        secondary_type = 2
         # Get drilldown level filter data; If exist
        analysis_level = '1'
        analysis_index_id = ''
        analysis_department_id = ''
        if request.args.get('level') is not None:
            analysis_level = request.args.get('level')
        if request.args.get('id') is not None:    
            analysis_index_id = request.args.get('id')
        if request.args.get('department_id') is not None:    
            analysis_department_id = request.args.get('department_id')
        # Get filter data; If exist
        #filter_data = redis_conn.hgetall("filter_data")
        # Get session filter data
        if 'filter_data' in session:
            filter_data = session['filter_data']
        # Set filter params
        p_outlet_id = ''
        p_zone_id = ''
        s_outlet_id = ''
        s_zone_id = ''
        start_date = ''
        end_date = ''
        comparison_period = 1
        if filter_data.get("p_outlet_id") != None and filter_data.get("p_outlet_id") != '':
            p_outlet_id = filter_data.get("p_outlet_id")
        
        if filter_data.get("p_zone_id") != None and filter_data.get("p_zone_id") != '':
            p_zone_id = filter_data.get("p_zone_id")
       
        if filter_data.get("s_outlet_id") != None and filter_data.get("s_outlet_id") != '':
            s_outlet_id = filter_data.get("s_outlet_id")
        
        if filter_data.get("s_zone_id") != None and filter_data.get("s_zone_id") != '':
            s_zone_id = filter_data.get("s_zone_id")
        
        if filter_data.get("start_date") != None and filter_data.get("start_date") != '' and filter_data.get("end_date") != None:
            start_date = filter_data.get("start_date")
            end_date = filter_data.get("end_date")

        if filter_data.get("comparison_period") != None and filter_data.get("comparison_period") != '':
            comparison_period = int(filter_data.get("comparison_period"))

        # return if date range empty
        if start_date == '' or end_date == '' :
            res = [graph_x1, graph_y1, graph_y2, graph_y3, graph_x2]
            return res

       # -- Set dates
        convt_startdate = datetime.strptime(start_date, '%Y-%m-%d').date()
        current_year = int(convt_startdate.strftime('%Y'))
        current_month = convt_startdate.strftime('%m')
        current_day = convt_startdate.strftime('%d')
        convt_enddate = datetime.strptime(end_date, '%Y-%m-%d').date()
        current_endmonth = convt_enddate.strftime('%m')
        current_endday = convt_enddate.strftime('%d')
        prev_year = current_year-comparison_period
        year_days = self.is_leap_year(prev_year)-1
        convt_prev_startdate = datetime.strptime(str(prev_year)+'-'+current_month+'-'+current_day, '%Y-%m-%d').date()
        convt_prev_enddate = datetime.strptime(str(prev_year)+'-'+current_endmonth+'-'+current_endday, '%Y-%m-%d').date()
        convt_startdate.strftime('%Y-%m-%d')
        LY_start_date = convt_prev_startdate.strftime('%Y-%m-%d')
        convt_enddate = datetime.strptime(end_date, '%Y-%m-%d').date()
        convt_enddate.strftime('%Y-%m-%d') 
        LY_end_date = convt_prev_enddate.strftime('%Y-%m-%d')
        # -- Set departmet | commodity id based on level
        department_id = ''
        commodity_id  = ''
        if analysis_level == '2' :
            department_id  = analysis_index_id
        elif analysis_level == '3' :
            department_id  = analysis_department_id
            commodity_id  = analysis_index_id
        # Get this year sale result
        if(p_outlet_id != '' or p_zone_id != '') :
            
            # -- Manage this year sales data in XY graph set   
            if not TYS_result:
                df_sale_TYGP_vs_LYGP = self.filter_till_weekend_sales(start_date,end_date,p_outlet_id,p_zone_id,analysis_level,department_id,commodity_id,primary_type)
            else:
                df_sale_TYGP_vs_LYGP = TYS_result
           
            # -- Append this year sales data in XY graph set
            for tygp in df_sale_TYGP_vs_LYGP :
                # -- X for current year --
                graph_x1.append(tygp['week_end_date'].encode("ascii"))
                gp_percent = np.divide(((tygp['total_amount'] - tygp['total_cost']) * 100) , tygp['total_amount'])           
                graph_y1.append((gp_percent/tygp['total_stores']))

            # -- Manage last year sales data in XY graph set   
            if not LYS_result:
                df_sale_TYGP_vs_LYGP = self.filter_till_weekend_sales(LY_start_date,LY_end_date,p_outlet_id,p_zone_id,analysis_level,department_id,commodity_id,primary_type)
            else:
                df_sale_TYGP_vs_LYGP = LYS_result
           
            # -- Append last year sales data in XY graph set
            for lygp in df_sale_TYGP_vs_LYGP :
                # -- X for current year --
                graph_x2.append(lygp['week_end_date'].encode("ascii"))
                gp_percent = np.divide(((lygp['total_amount'] - lygp['total_cost']) * 100) , lygp['total_amount'])                
                graph_y2.append((gp_percent/lygp['total_stores']))

        # -- Manage this year comparison sales data in Y graph set
        if not TYCS_result:
            df_sale_TYGP_vs_LYGP = self.filter_till_weekend_sales(start_date,end_date,s_outlet_id,s_zone_id,analysis_level,department_id,commodity_id,secondary_type)
        else:
            df_sale_TYGP_vs_LYGP = TYCS_result
        
        # -- Append last year sales data in XY graph set
        for tycgp in df_sale_TYGP_vs_LYGP :
            # -- Y for current year --
            gp_percent = np.divide(((tycgp['total_amount'] - tycgp['total_cost']) * 100) , tycgp['total_amount'])                
            graph_y3.append((gp_percent/tycgp['total_stores']))


        graph_dictionary = []
        # Set marker name as combination of name and axis values
        for i in range(0,len(graph_x1)):
            # Prepare dictionary 
            myDict = {}
            # Handle exeption  
            try:
                pty_value = float(graph_y1[i])
            except IndexError:
                pty_value = 0
            try:
                ply_value = float(graph_y2[i])
            except IndexError:
                ply_value = 0
            try:
                sty_value = float(graph_y3[i])
            except IndexError:
                sty_value = 0
            myDict["weeks"] = graph_x1[i]
            myDict["pty_value"] = pty_value
            myDict["ply_value"] = ply_value
            myDict["sty_value"] = sty_value
            graph_dictionary.append(myDict)
        res = [graph_x1, graph_y1, graph_y2, graph_y3, graph_x2, graph_dictionary]
        return res

    @cache.memoize(timeout=timeout)
    def pg_scatterchart_THAVGB_vs_LYAVGB(self):
       
        # -- For Graph X-Axis, Y-Axis, Custome Data for current Years. --
        temp_Sale_THAVGB_vs_LYAVGB_X = []
        temp_Sale_THAVGB_vs_LYAVGB_X2 = []
        temp_Sale_THAVGB_vs_LYAVGB_Y = []
        
        # -- For Graph X-Axis, Y-Axis, Custome Data for current Years. --
        graph_x1 = []
        graph_x2 = []
        graph_y1 = []
        graph_y2 = []
        graph_y3 = []
        graph_text = []
        graph_customdata = []
        TY_weeks_count = 0
        LY_weeks_count = 0
        TY_week_start_date = ""
        LY_last_weekend_date = ""
        LY_first_weekend = ""
        LY_second_week_start_date = ""
        primary_type = 1
        secondary_type = 2
        # Get drilldown level filter data; If exist
        analysis_level = '1'
        analysis_index_id = ''
        analysis_department_id = ''
        if request.args.get('level') is not None:
            analysis_level = request.args.get('level')
        if request.args.get('id') is not None:    
            analysis_index_id = request.args.get('id')
        if request.args.get('department_id') is not None:    
            analysis_department_id = request.args.get('department_id')
        #filter_data = redis_conn.hgetall("filter_data")
        # Get session filter data
        if 'filter_data' in session:
            filter_data = session['filter_data']
        # Set filter params
        p_outlet_id = ''
        p_zone_id = ''
        s_outlet_id = ''
        s_zone_id = ''
        start_date = ''
        end_date = ''
        comparison_period = 1
        if filter_data.get("p_outlet_id") != None and filter_data.get("p_outlet_id") != '':
            p_outlet_id = filter_data.get("p_outlet_id")
        
        if filter_data.get("p_zone_id") != None and filter_data.get("p_zone_id") != '':
            p_zone_id = filter_data.get("p_zone_id")
       
        if filter_data.get("s_outlet_id") != None and filter_data.get("s_outlet_id") != '':
            s_outlet_id = filter_data.get("s_outlet_id")
        
        if filter_data.get("s_zone_id") != None and filter_data.get("s_zone_id") != '':
            s_zone_id = filter_data.get("s_zone_id")
        
        if filter_data.get("start_date") != None and filter_data.get("start_date") != '' and filter_data.get("end_date") != None:
            start_date = filter_data.get("start_date")
            end_date = filter_data.get("end_date")

        if filter_data.get("comparison_period") != None and filter_data.get("comparison_period") != '':
            comparison_period = int(filter_data.get("comparison_period"))

        # return if date range empty
        if start_date == '' or end_date == '' :
            res = [graph_x1, graph_y1, graph_y2, graph_y3, graph_x2]
            return res

        # -- Set dates
        convt_startdate = datetime.strptime(start_date, '%Y-%m-%d').date()
        current_year = int(convt_startdate.strftime('%Y'))
        current_month = convt_startdate.strftime('%m')
        current_day = convt_startdate.strftime('%d')
        convt_enddate = datetime.strptime(end_date, '%Y-%m-%d').date()
        current_endmonth = convt_enddate.strftime('%m')
        current_endday = convt_enddate.strftime('%d')
        prev_year = current_year-comparison_period
        year_days = self.is_leap_year(prev_year)-1
        convt_prev_startdate = datetime.strptime(str(prev_year)+'-'+current_month+'-'+current_day, '%Y-%m-%d').date()
        convt_prev_enddate = datetime.strptime(str(prev_year)+'-'+current_endmonth+'-'+current_endday, '%Y-%m-%d').date()
        convt_startdate.strftime('%Y-%m-%d')
        LY_start_date = convt_prev_startdate.strftime('%Y-%m-%d')
        convt_enddate = datetime.strptime(end_date, '%Y-%m-%d').date()
        convt_enddate.strftime('%Y-%m-%d')
        LY_end_date = convt_prev_enddate.strftime('%Y-%m-%d')
        # -- Set departmet | commodity id based on level
        department_id = ''
        commodity_id  = ''
        if analysis_level == '2' :
            department_id  = analysis_index_id
        elif analysis_level == '3' :
            department_id  = analysis_department_id
            commodity_id  = analysis_index_id
        # -- For this year sales. --
        if(p_outlet_id != '' or p_zone_id != '') :
            # -- Manage this year sales data in XY graph set   
            if not TYS_result:
                df_sale_THAVGB_vs_LYAVGB = self.filter_till_weekend_sales(start_date,end_date,p_outlet_id,p_zone_id,analysis_level,department_id,commodity_id,primary_type)
            else:
                df_sale_THAVGB_vs_LYAVGB = TYS_result

            # -- Append this year sales data in XY graph set
            for tyavgp in df_sale_THAVGB_vs_LYAVGB :
                # -- X for current year --
                graph_x1.append(tyavgp['week_end_date'].encode("ascii"))
                graph_y1.append(np.divide(tyavgp['total_amount'],tyavgp['total_customers'])/tyavgp['total_stores'])

            # -- Manage last year sales data in XY graph set   
            if not LYS_result:
                df_sale_THAVGB_vs_LYAVGB = self.filter_till_weekend_sales(LY_start_date,LY_end_date,p_outlet_id,p_zone_id,analysis_level,department_id,commodity_id,primary_type)
            else:
                df_sale_THAVGB_vs_LYAVGB = LYS_result

            # -- Append last year sales data in XY graph set
            for lyavgp in df_sale_THAVGB_vs_LYAVGB :
                # -- X for current year --
                graph_x2.append(lyavgp['week_end_date'].encode("ascii"))                 
                graph_y2.append(np.divide(lyavgp['total_amount'],lyavgp['total_customers'])/lyavgp['total_stores'])

        # -- Manage this year comparison sales data in Y graph set
        if not TYCS_result:
            df_sale_THAVGB_vs_LYAVGB = self.filter_till_weekend_sales(start_date,end_date,s_outlet_id,s_zone_id,analysis_level,department_id,commodity_id,secondary_type)
        else:
            df_sale_THAVGB_vs_LYAVGB = TYCS_result

        # -- Append last year sales data in XY graph set
        for tyavgp in df_sale_THAVGB_vs_LYAVGB :
            # -- Y for current year --           
            graph_y3.append(np.divide(tyavgp['total_amount'],tyavgp['total_customers'])/tyavgp['total_stores'])

        graph_dictionary = []
        # Set marker name as combination of name and axis values
        for i in range(0,len(graph_x1)):
            # Prepare dictionary 
            myDict = {}
            # Handle exeption  
            try:
                pty_value = float(graph_y1[i])
            except IndexError:
                pty_value = 0
            try:
                ply_value = float(graph_y2[i])
            except IndexError:
                ply_value = 0
            try:
                sty_value = float(graph_y3[i])
            except IndexError:
                sty_value = 0
            myDict["weeks"] = graph_x1[i]
            myDict["pty_value"] = pty_value
            myDict["ply_value"] = ply_value
            myDict["sty_value"] = sty_value
            graph_dictionary.append(myDict)

        res = [graph_x1, graph_y1, graph_y2, graph_y3, graph_x2, graph_dictionary]
        return res

    @cache.memoize(timeout=timeout)
    def pg_scatterchart_TYC_vs_LYC(self):
        
        # -- For Graph X-Axis, Y-Axis, Custome Data for current Years. --
        temp_Sale_TYC_vs_LYC_X = []
        temp_Sale_TYC_vs_LYC_X2 = []
        temp_Sale_TYC_vs_LYC_Y = []
        
        # -- For Graph X-Axis, Y-Axis, Custome Data for current Years. --
        graph_x1 = []
        graph_x2 = []
        graph_y1 = []
        graph_y2 = []
        graph_y3 = []
        graph_text = []
        graph_customdata = []
        TY_weeks_count = 0
        LY_weeks_count = 0
        TY_week_start_date = ""
        LY_last_weekend_date = ""
        LY_first_weekend = ""
        LY_second_week_start_date = ""
        primary_type = 1
        secondary_type = 2
        # Get drilldown level filter data; If exist
        analysis_level = '1'
        analysis_index_id = ''
        analysis_department_id = ''
        if request.args.get('level') is not None:
            analysis_level = request.args.get('level')
        if request.args.get('id') is not None:    
            analysis_index_id = request.args.get('id')
        if request.args.get('department_id') is not None:    
            analysis_department_id = request.args.get('department_id')
        #filter_data = redis_conn.hgetall("filter_data")
        # Get session filter data
        if 'filter_data' in session:
            filter_data = session['filter_data']
        # Set filter params
        p_outlet_id = ''
        p_zone_id = ''
        s_outlet_id = ''
        s_zone_id = ''
        start_date = ''
        end_date = ''
        comparison_period = 1
        if filter_data.get("p_outlet_id") != None and filter_data.get("p_outlet_id") != '':
            p_outlet_id = filter_data.get("p_outlet_id")
        
        if filter_data.get("p_zone_id") != None and filter_data.get("p_zone_id") != '':
            p_zone_id = filter_data.get("p_zone_id")
       
        if filter_data.get("s_outlet_id") != None and filter_data.get("s_outlet_id") != '':
            s_outlet_id = filter_data.get("s_outlet_id")
        
        if filter_data.get("s_zone_id") != None and filter_data.get("s_zone_id") != '':
            s_zone_id = filter_data.get("s_zone_id")
        
        if filter_data.get("start_date") != None and filter_data.get("start_date") != '' and filter_data.get("end_date") != None:
            start_date = filter_data.get("start_date")
            end_date = filter_data.get("end_date")

        if filter_data.get("comparison_period") != None and filter_data.get("comparison_period") != '':
            comparison_period = int(filter_data.get("comparison_period"))

        # return if date range empty
        if start_date == '' or end_date == '' :
            res = [graph_x1, graph_y1, graph_y2, graph_y3, graph_x2]
            return res

        # -- Set dates
        convt_startdate = datetime.strptime(start_date, '%Y-%m-%d').date()
        current_year = int(convt_startdate.strftime('%Y'))
        current_month = convt_startdate.strftime('%m')
        current_day = convt_startdate.strftime('%d')
        convt_enddate = datetime.strptime(end_date, '%Y-%m-%d').date()
        current_endmonth = convt_enddate.strftime('%m')
        current_endday = convt_enddate.strftime('%d')
        prev_year = current_year-comparison_period
        year_days = self.is_leap_year(prev_year)-1
        convt_prev_startdate = datetime.strptime(str(prev_year)+'-'+current_month+'-'+current_day, '%Y-%m-%d').date()
        convt_prev_enddate = datetime.strptime(str(prev_year)+'-'+current_endmonth+'-'+current_endday, '%Y-%m-%d').date()
        convt_startdate.strftime('%Y-%m-%d')
        LY_start_date = convt_prev_startdate.strftime('%Y-%m-%d')
        convt_enddate = datetime.strptime(end_date, '%Y-%m-%d').date()
        convt_enddate.strftime('%Y-%m-%d')
        LY_end_date = convt_prev_enddate.strftime('%Y-%m-%d')
        # -- Set departmet | commodity id based on level
        department_id = ''
        commodity_id  = ''
        if analysis_level == '2' :
            department_id  = analysis_index_id
        elif analysis_level == '3' :
            department_id  = analysis_department_id
            commodity_id  = analysis_index_id
        # -- For this year sales. --
        if(p_outlet_id != '' or p_zone_id != '') :
            # -- Manage this year sales data in XY graph set   
            if not TYS_result:
                df_sale_TYC_VS_LYC = self.filter_till_weekend_sales(start_date,end_date,p_outlet_id,p_zone_id,analysis_level,department_id,commodity_id,primary_type)
            else:
                df_sale_TYC_VS_LYC = TYS_result

            # -- Append this year sales data in XY graph set
            for tyc in df_sale_TYC_VS_LYC :
                # -- X for current year --
                graph_x1.append(tyc['week_end_date'].encode("ascii"))
                graph_y1.append(tyc['total_customers']/tyc['total_stores'])

            # -- Manage last year sales data in XY graph set   
            if not LYS_result:
                df_sale_TYC_VS_LYC = self.filter_till_weekend_sales(LY_start_date,LY_end_date,p_outlet_id,p_zone_id,analysis_level,department_id,commodity_id,primary_type)
            else:
                df_sale_TYC_VS_LYC = LYS_result

            # -- Append last year sales data in XY graph set
            for tyc in df_sale_TYC_VS_LYC :
                # -- X for current year --
                graph_x2.append(tyc['week_end_date'].encode("ascii"))   
                graph_y2.append(tyc['total_customers']/tyc['total_stores'])              
            
            # -- Manage this year comparison sales data in Y graph set
            if not TYCS_result:
                df_sale_TYC_VS_LYC = self.filter_till_weekend_sales(start_date,end_date,s_outlet_id,s_zone_id,analysis_level,department_id,commodity_id,secondary_type)
            else:
                df_sale_TYC_VS_LYC = TYCS_result

            # -- Append last year sales data in XY graph set
            for tyc in df_sale_TYC_VS_LYC :
                # -- Y for current year --           
                graph_y3.append(tyc['total_customers']/tyc['total_stores']) 

        graph_dictionary = []
        # Set marker name as combination of name and axis values
        for i in range(0,len(graph_x1)):
            # Prepare dictionary 
            myDict = {}
            # Handle exeption  
            try:
                pty_value = float(graph_y1[i])
            except IndexError:
                pty_value = 0
            try:
                ply_value = float(graph_y2[i])
            except IndexError:
                ply_value = 0
            try:
                sty_value = float(graph_y3[i])
            except IndexError:
                sty_value = 0
            myDict["weeks"] = graph_x1[i]
            myDict["pty_value"] = pty_value
            myDict["ply_value"] = ply_value
            myDict["sty_value"] = sty_value
            graph_dictionary.append(myDict)

        res = [graph_x1, graph_y1, graph_y2, graph_y3, graph_x2, graph_dictionary]
        return res        

    # -- Function used to manage weekwise sales records 
    def filter_till_weekend_sales(self,start_date,end_date,outlet_id,zone_id='',analysis_level='1',department_id='',commodity_id='',aggregate_type=1) :
       
        # get day wise sales data
        ty_sales_array = self.sales_aggregate_calculation(start_date,end_date,outlet_id,zone_id,analysis_level,department_id,commodity_id,aggregate_type)

        weekend_sales = {}
        # manage week wise sales data
        if len(ty_sales_array) > 0 :
            total_amount = 0
            total_cost = 0
            total_customers = 0
            total_gp_percent = 0
            weekend_date = ''
            for v in ty_sales_array:
                if v['vsf_trading_date'] != '' :
                    # -- Set dates
                    vsf_trading_date = v['vsf_trading_date']
                    total_stores = v['total_stores']
                    if weekend_date == '' or weekend_date != vsf_trading_date :
                        convt_date = datetime.strptime(vsf_trading_date, '%Y-%m-%d').date()
                        convt_year  = int(convt_date.strftime('%Y'))
                        convt_month = int(convt_date.strftime('%m'))
                        convt_day   = int(convt_date.strftime('%d'))
                        # get next saturday date
                        coming_saturday =  self.get_next_weekdate(convt_year, convt_month, convt_day)
                        weekend_date = coming_saturday.strftime('%Y-%m-%d')
                    else :
                        weekend_date = vsf_trading_date
            
                    # append product sales data
                    if weekend_date not in weekend_sales:
                        weekend_sales[weekend_date] = []
                    # append searched end date as weekend date
                    if vsf_trading_date == end_date :
                        weekend_sales[end_date] = []
                        
                    if weekend_date == vsf_trading_date or vsf_trading_date == end_date:
                        # add total sales attributes   
                        total_amount    = total_amount+v['total_amount']
                        total_cost      = total_cost+v['total_cost']
                        total_customers = total_customers+v['total_customers']
                        total_gp_percent= total_gp_percent+v['gp_percent']
                        # append searched end date as weekend date
                        if vsf_trading_date == end_date :
                            convt_end_date = datetime.strptime(end_date, '%Y-%m-%d').date()
                            weekend_date = convt_end_date.strftime('%Y-%m-%d')
                        # prepare journal values for sales array department wise  
                        weekend_sales[weekend_date].append({'week_end_date':weekend_date,'total_customers':total_customers,'total_amount':total_amount,'total_cost':total_cost,'gp_percent':total_gp_percent,"year":convt_year,"total_stores":total_stores})
                        #reset sale values as 0
                        total_amount     = 0
                        total_cost       = 0
                        total_customers  = 0
                        total_gp_percent = 0
                    else :
                        # add total sales attributes   
                        total_amount    = total_amount+v['total_amount']
                        total_cost      = total_cost+v['total_cost']
                        total_customers = total_customers+v['total_customers']
                        total_gp_percent= total_gp_percent+v['gp_percent']

        final_array = []
        for k in weekend_sales :
            for j in weekend_sales[k] :
                if 'total_amount' in j:
                    total_stores = int(j['total_stores'])             
                    # append this year department sales value in array
                    final_array.append({'week_end_date':j['week_end_date'],'total_customers':(np.divide(j['total_customers'],total_stores)),'total_amount':(np.divide(j['total_amount'],total_stores)),'total_cost':(np.divide(j['total_cost'],total_stores)),'avg_basket':(np.divide(np.divide(j['total_amount'],j['total_customers']),total_stores)),'gp_percent':(np.divide(v['gp_percent'],total_stores)),'year':j['year'],'total_stores':total_stores})

        # sort by weekend date
        if len(final_array) > 0 :
            final_array =  sorted(final_array, key = lambda i: i['week_end_date'])   

        return final_array

    # --- Function used to get next week date from given date
    def get_next_weekdate(self,year, month, day):
        import datetime
        date0 = datetime.date(year, month, day)
        next_week = date0 + datetime.timedelta(6 - date0.weekday() or 6)
        return next_week

    # --- Function used to manage sales filter sales
    def sales_aggregate_calculation(self,start_date,end_date,outlet_id='',zone_id='',analysis_level='1',department_id='',commodity_id='',aggregate_type=1):
        
        # prepare query for this year financial sales query
        ty_sales_query = "SELECT JNLH_OUTLET, JNLH_TILL, JNLH_TRX_NO, JNLH_TYPE, JNLH_STATUS, JNLH_TRX_AMT, JNLH_CASHIER, JNLD_TYPE, JNLD_STATUS, JNLD_DESC, JNLD_MIXMATCH, JNLD_OFFER, JNLD_QTY, JNLD_AMT, JNLD_COST, JNLD_GST_AMT, JNLD_DISC_AMT, JNLD_POST_STATUS, PROD_TAX_CODE, PROD_COMMODITY, PROD_CATEGORY, PROD_DEPARTMENT, PROD_CARTON_QTY, PROD_UNIT_QTY, OUTP_CARTON_COST, OUTP_PROM_CTN_COST,CODE_DESC,CODE_KEY_NUM,JNLH_TRADING_DATE,YEAR(JNLH_TRADING_DATE) AS JNLH_YEAR  "
        if aggregate_type == 2 :
            if zone_id != '' :
                ty_sales_query += ",(SELECT count(CODE_KEY_NUM) FROM CODETBL WHERE CODE_KEY_TYPE = 'ZONEOUTLET' and CODE_KEY_ALP = '"+zone_id+"') as total_stores"
        ty_sales_query += " FROM JNLHTBL JOIN JNLDTBL ON JNLD_YYYYMMDD = JNLH_YYYYMMDD AND JNLD_HHMMSS = JNLH_HHMMSS AND JNLD_OUTLET = JNLH_OUTLET AND JNLD_TILL = JNLH_TILL AND JNLD_TRX_NO = JNLH_TRX_NO LEFT JOIN PRODTBL ON PROD_NUMBER = JNLD_PRODUCT LEFT JOIN CODETBL "    
        if analysis_level == '3' :
            ty_sales_query += "ON CODE_KEY_NUM = PROD_COMMODITY AND CODE_KEY_TYPE = 'COMMODITY'"
        else :
            ty_sales_query += "ON CODE_KEY_NUM = PROD_DEPARTMENT AND CODE_KEY_TYPE = 'DEPARTMENT'"
        ty_sales_query += " LEFT JOIN OUTPTBL ON OUTP_PRODUCT = JNLD_PRODUCT AND OUTP_OUTLET = JNLD_OUTLET WHERE JNLH_TRADING_DATE <> '' AND "    
        if outlet_id != '':
            ty_sales_query += "JNLH_OUTLET  = '"+outlet_id+"' AND "
        elif zone_id != '' :
            ty_sales_query += "JNLH_OUTLET IN ( select DISTINCT(CODE_KEY_NUM) from CODETBL where CODE_KEY_TYPE = 'ZONEOUTLET' AND CODE_KEY_ALP = '"+zone_id+"' )  AND "
        ty_sales_query += "(JNLH_TRADING_DATE BETWEEN '"+start_date+"' AND '"+end_date+"') AND JNLD_TYPE = 'SALE' "
        if analysis_level == '2' :
            ty_sales_query += " AND ( PROD_DEPARTMENT = '"+department_id+"' )"
        elif analysis_level == '3' :
            ty_sales_query += " AND ( PROD_DEPARTMENT = '"+department_id+"' ) AND ( PROD_COMMODITY = '"+commodity_id+"' )"    

        ty_sales_query += " ORDER BY CODE_KEY_NUM,JNLH_YYYYMMDD, JNLH_HHMMSS, JNLH_TILL, JNLH_TRX_NO"  
       
        ty_df_sales_result = pd.read_sql_query(ty_sales_query,con=self.live_db)
        ty_df_sales_result=ty_df_sales_result.groupby(['JNLH_OUTLET'])
        # manage this year sales aggragate data
        ty_sales_array = self.manage_yearly_aggregate_sales(ty_df_sales_result,analysis_level)

        final_array = []
        for k in ty_sales_array :
            for v in k :
                for j in k[v] :
                    
                    # append this year department sales value in array
                    final_array.append({'vsf_trading_date':j['vsf_trading_date'],'vsf_dep_sale_count':j['vsf_dep_sale_count'],'total_amount':j['vsf_dep_sales_amt'],'total_customers':j['vsf_dep_customers_count'],'vsf_dep_gst_amt':j['vsf_dep_gst_amt'],'total_margin':j['vsf_dep_margin'],'total_cost':j['vsf_dep_sales_cost'],'vsf_dep_sales_ex_gst':j['vsf_dep_sales_ex_gst'],'total_qty':j['vsf_dep_sales_qty'],'gp_percent':(np.divide((j['vsf_dep_sales_amt'] - j['vsf_dep_sales_cost']),j['vsf_dep_sales_amt'])*100),'total_promo_sale':0,'total_discount':0,'total_stores':j['total_stores']})
        # sort by purchase date
        if len(final_array) > 0 :
            final_array =  sorted(final_array, key = lambda i: i['vsf_trading_date'])
        return final_array

    # Manage yearly sales record
    def manage_yearly_aggregate_sales(self,df_sales_result,analysis_level):
       
        # initialize department product sales array
        department_array = {} 
        for k, v in df_sales_result:
            temp_group_split=v.to_dict(orient="records")
            for v in temp_group_split:
                # set department label
                CODE_DESC = v['CODE_DESC'].encode("ascii")
                TRADING_DATE = v['JNLH_TRADING_DATE']
                TRADING_DATE = TRADING_DATE.strftime('%Y-%m-%d')
                # append product sales data in department index
                if TRADING_DATE not in department_array:
                    department_array[TRADING_DATE] = []
                # set total store count
                total_stores = 1
                if 'total_stores' in v:
                    total_stores = v['total_stores']
                # prepare journal values for sales array department wise  
                department_array[TRADING_DATE].append({'JNLH_OUTLET':v['JNLH_OUTLET'],'JNLH_TRX_NO':v['JNLH_TRX_NO'],'JNLH_TYPE':v['JNLH_TYPE'],'JNLH_STATUS':v['JNLH_STATUS'],'JNLH_TRX_AMT':v['JNLH_TRX_AMT'],'JNLH_CASHIER':v['JNLH_CASHIER'],'JNLD_TYPE':v['JNLD_TYPE'],'JNLD_STATUS':v['JNLD_STATUS'],'JNLD_QTY':v['JNLD_QTY'],'JNLD_AMT':v['JNLD_AMT'],'JNLD_COST':v['JNLD_COST'],'JNLD_GST_AMT':v['JNLD_GST_AMT'],'JNLD_DESC':v['JNLD_DESC'],'JNLD_OFFER':v['JNLD_OFFER'],'JNLD_MIXMATCH':v['JNLD_MIXMATCH'],'JNLD_DISC_AMT':v['JNLD_DISC_AMT'],'JNLD_POST_STATUS':v['JNLD_POST_STATUS'],'PROD_DEPARTMENT':v['PROD_DEPARTMENT'],'PROD_TAX_CODE':v['PROD_TAX_CODE'],'PROD_CATEGORY':v['PROD_CATEGORY'],'PROD_COMMODITY':v['PROD_COMMODITY'],'YEAR':v['JNLH_YEAR'],'CODE_DESC':CODE_DESC,'TRADING_DATE':TRADING_DATE,'total_stores':total_stores})
               
        department_sales_array = []
        trx_numbers = []    
        # prepare department sales report result
        for i in department_array:
        
            # initialize default values
            vsf_dep_amt = 0
            vsf_dep_cost = 0
            vsf_dep_gst_amt = 0
            vsf_dep_sale_count = 0
            vsf_dep_margin = 0
            vsf_dep_sales_qty = 0
            vsf_dep_promo_disc_qty = 0
            vsf_dep_promo_disc_amt = 0
            vsf_dep_customers_count = 0
            vsf_dep_item_qty = 0
            vsf_trading_date = ''
            last_trx_no = 0
            dept_data = []
            for v in department_array[i] :
                if i != '' :
                    if v['JNLD_STATUS'] == 1 and v['JNLD_TYPE'] == 'SALE' :
                        # add sales values
                        vsf_dep_amt 		= vsf_dep_amt+v['JNLD_AMT']
                        vsf_dep_cost 		= vsf_dep_cost+v['JNLD_COST']
                        vsf_dep_gst_amt	    = vsf_dep_gst_amt+v['JNLD_GST_AMT']
                        vsf_dep_sales_qty	= vsf_dep_sales_qty+v['JNLD_QTY']
                        vsf_dep_sale_count  = vsf_dep_sale_count+1   
                        vsf_trading_date    = v['TRADING_DATE']
                        total_stores        = v['total_stores']
                
                # set discount item values
                if v['JNLD_DISC_AMT'] != 0 :
                    if v['JNLD_MIXMATCH'] == '' and v['JNLD_OFFER'] == '' :
                        #do nothing
                        disc = 0
                    else :
                        # set promo discount values
                        vsf_dep_promo_disc_qty = vsf_dep_promo_disc_qty+v['JNLD_QTY']
                        vsf_dep_promo_disc_amt = vsf_dep_promo_disc_amt+v['JNLD_DISC_AMT']
                    
                # set total customers
                if v['JNLH_STATUS'] == 1 and v['JNLH_TYPE'] == 'SALE' :
                    if v['JNLH_TRX_NO'] not in trx_numbers :
                        trx_numbers.append(v['JNLH_TRX_NO'])
                        # add customer count
                        if last_trx_no != v['JNLH_TRX_NO'] :
                            vsf_dep_customers_count = vsf_dep_customers_count+1
        
                        # set last trx numbers
                        last_trx_no = v['JNLH_TRX_NO'];

                if v['JNLD_STATUS'] == 1 and v['JNLD_TYPE'] == 'SALE' :
                    # set item quantities
                    vsf_dep_item_qty = vsf_dep_item_qty+v['JNLD_QTY']
                
            # append sales value in array
            dept_data.append({'vsf_dep_sales_cost':vsf_dep_cost,'vsf_dep_sales_amt':vsf_dep_amt,'vsf_dep_gst_amt':vsf_dep_gst_amt,'vsf_dep_margin':vsf_dep_amt-vsf_dep_cost,'vsf_dep_sales_ex_gst':vsf_dep_amt-vsf_dep_gst_amt,'vsf_dep_sales_qty':vsf_dep_sales_qty,'vsf_dep_customers_count':vsf_dep_customers_count,'vsf_dep_sale_count':vsf_dep_sale_count,'vsf_trading_date':vsf_trading_date,'total_stores':total_stores})
            # append result in department array
            department_sales_array.append({i:dept_data})
                
        return department_sales_array

    def __repr__(self):
        return '<Book %r>' % (self.title) 
