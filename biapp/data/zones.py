from flask import Flask, Response, g, request,session
from flask_caching import Cache
from flask_sqlalchemy import SQLAlchemy
import sqlalchemy
import pandas as pd
import numpy as np
import re
import base64
import json
import time
from datetime import datetime, timedelta
# -- Important Assets STARTS --
from biapp.db import get_db
from biapp.app import server

app = Flask(__name__, instance_relative_config=True)
# -- Important Assets ENDS --
cache = Cache(server, config={'CACHE_TYPE': 'simple'})
timeout = 20

class zones():
   
    title = ""
    db = []

    @cache.memoize(timeout=timeout)
    def __init__(self):
        with server.app_context():        
            if 'db' not in g:
                self.db = get_db()
                g.db = self.db
    
    # Prepare chart data based on zone selection
    def zone_inner_chart_data(self):
        user_id = str(44)

        # Get session filter data if exist
        session_filters = self.get_session_filters()
        # Set filter parameters
        f_demo_zones    = session_filters['demo_zones']
        f_region_zones  = session_filters['region_zones']
        f_rtm_zones     = session_filters['rtm_zones']
        sales_group     = session_filters['sales_group']
        TY_start_date   = session_filters['TY_start_date']
        TY_end_date     = session_filters['TY_end_date']
        f_sort_type     = session_filters['f_sort_type']
         
        # Define data set array 
        temp_categories_sales = []
        temp_ty_sales = []
        temp_ly_sales = []
        temp_cat1_sales = []
        temp_cat2_sales = []
        temp_cat3_sales = []
        temp_cat4_sales = []
        temp_cat5_sales = []
        temp_cat6_sales = []
        temp_cat7_sales = []
        temp_cat8_sales = []
        temp_cat9_sales = []
        temp_cat10_sales = []
        temp_months = []
        graph_sales_dictionary = []
        graph_gp_dictionary = []
        graph_avgbasket_dictionary = []
        graph_customers_dictionary = []
        graph_avgitem_dictionary = []
        graph_sales_categories_dictionary = []
        graph_gp_categories_dictionary = []
        graph_avgbasket_categories_dictionary = []
        graph_customers_categories_dictionary = []
        graph_avgitem_categories_dictionary = []
        primary_selections_lbl = []
        primary_selections_ids = []
        secondary_selections_ids = []
        secondary_outlets_ids = []
        total_sales_amount = 0
        total_sales_cost = 0
        total_sales_customer = 0
        total_sales_qty = 0
        total_sales_gp = 0
        total_sales_avgbkt = 0
        total_sales_avgitem = 0
        # Set primary filter array
        if f_sort_type == '1':
            for i in range(0,len(f_demo_zones)):
                primary_selections_ids.append(f_demo_zones[i]['id'])
                primary_selections_lbl.append(f_demo_zones[i]['lbl'])
        elif f_sort_type == '2':
            for i in range(0,len(f_region_zones)):
                primary_selections_ids.append(f_region_zones[i]['id'])
                primary_selections_lbl.append(f_region_zones[i]['lbl'])
        elif f_sort_type == '3':
            for i in range(0,len(f_rtm_zones)):
                primary_selections_ids.append(f_rtm_zones[i]['id'])
                primary_selections_lbl.append(f_rtm_zones[i]['lbl'])   
        elif f_sort_type == '4':
            for i in range(0,len(sales_group)):
                primary_selections_ids.append(sales_group[i]['id'])
                primary_selections_lbl.append(sales_group[i]['lbl'])  
        
        if TY_start_date != '' and TY_end_date != '' and len(primary_selections_ids) > 0:
                     
            # Set secondary selection ids
            if f_sort_type != '1' and len(f_demo_zones) > 0:
                for i in range(0,len(f_demo_zones)):
                    secondary_selections_ids.append(f_demo_zones[i]['id'])
            if f_sort_type != '2' and len(f_region_zones) > 0:
                for i in range(0,len(f_region_zones)):
                    secondary_selections_ids.append(f_region_zones[i]['id'])
            if f_sort_type != '3' and len(f_rtm_zones) > 0:
                for i in range(0,len(f_rtm_zones)):
                    secondary_selections_ids.append(f_rtm_zones[i]['id'])

            # Set last year start and end dates
            if len(primary_selections_ids) == 1 :
                convt_startdate = datetime.strptime(TY_start_date, '%Y-%m-%d').date()
                current_year = int(convt_startdate.strftime('%Y'))
                current_month = convt_startdate.strftime('%m')
                current_day = convt_startdate.strftime('%d')

                convt_enddate = datetime.strptime(TY_end_date, '%Y-%m-%d').date()
                current_endmonth = convt_enddate.strftime('%m')
                current_endday = convt_enddate.strftime('%d')
                endday = convt_enddate.strftime('%a')
                
                prev_year = current_year-1
                year_days = self.is_leap_year(prev_year)-1
                
                convt_prev_startdate = datetime.strptime(str(prev_year)+'-'+current_month+'-'+current_day, '%Y-%m-%d').date()
                convt_prev_enddate = datetime.strptime(str(prev_year)+'-'+current_endmonth+'-'+current_endday, '%Y-%m-%d').date()
                
                convt_startdate.strftime('%Y-%m-%d')
                LY_start_date = convt_prev_startdate.strftime('%Y-%m-%d')
                LY_end_date = convt_prev_enddate.strftime('%Y-%m-%d')
            # Get secondary selections outlet ids
            if len(secondary_selections_ids) > 0 :
                sec_outlet_sql = "select distinct zout_outlet_id FROM zone_outlet WHERE "
                for i in range(0,len(secondary_selections_ids)):
                    sec_outlet_sql += " zout_zone_id  = "+str(secondary_selections_ids[i])+" "
                    if i != (len(secondary_selections_ids)-1) :
                        sec_outlet_sql += " OR "
                sec_outlets = pd.read_sql_query(sec_outlet_sql,con=self.db)
                sec_outlets = sec_outlets.groupby(['zout_outlet_id'])
                for k, v in sec_outlets:
                    temp_sec_outlet_split = v.to_dict(orient="records")
                    for v in temp_sec_outlet_split:
                        secondary_outlets_ids.append(v['zout_outlet_id'])
            # Get outlet ids of sales group
            if f_sort_type != '4' and len(sales_group) > 0 :
                sec_outlet_sql = "select distinct btga_outlet_id FROM bi_trx_group_aggregates JOIN bi_trx_time ON bi_trx_time.trxt_id = bi_trx_group_aggregates.btga_time_id WHERE ( trxt_date >= '"+TY_start_date+"' AND trxt_date <= '"+TY_end_date+"' ) AND ( "
                for i in range(0,len(sales_group)):
                    sec_outlet_sql += " btga_group_id = "+str(sales_group[i]['id'])+" "
                    if i != (len(sales_group)-1) :
                        sec_outlet_sql += "OR"
                sec_outlet_sql += " )"
                sec_outlets = pd.read_sql_query(sec_outlet_sql,con=self.db)
                sec_outlets = sec_outlets.groupby(['btga_outlet_id'])
                for k, v in sec_outlets:
                    temp_sec_outlet_split = v.to_dict(orient="records")
                    for v in temp_sec_outlet_split:
                        if v['btga_outlet_id'] not in secondary_outlets_ids :
                            secondary_outlets_ids.append(v['btga_outlet_id'])
           
            current_year = time.strftime("%Y")
            prev_year = str(int(current_year)-1)
            # Get user's outlet records
            user_data = self.get_outlets(user_id)
            user_outlets = user_data['user_outlets']
            total_outlets = len(user_outlets)
            selection_counts = []       
            # Get outlets this and prevoise year sales
            outlet_query = ""
            for i in range(0,len(primary_selections_ids)):
                id = primary_selections_ids[i]
                selection_count = i+1
                intersected_outlet_ids = []
                primary_outlets = []
                # Get primary selections outlet ids for zones
                if f_sort_type != '4':
                    prime_outlet_sql = "select distinct zout_outlet_id FROM zone_outlet WHERE zout_zone_id  = '"+id+"' "
                    prime_outlets = pd.read_sql_query(prime_outlet_sql,con=self.db)
                    prime_outlets = prime_outlets.groupby(['zout_outlet_id'])
                    for k, v in prime_outlets:
                        temp_prime_outlet_split = v.to_dict(orient="records")
                        for v in temp_prime_outlet_split:
                            primary_outlets.append(v['zout_outlet_id'])
                else:
                    # Get group sales outlet ids
                    prime_outlet_sql = "select distinct btga_outlet_id FROM bi_trx_group_aggregates WHERE btga_group_id  = '"+id+"' "
                    prime_outlets = pd.read_sql_query(prime_outlet_sql,con=self.db)
                    prime_outlets = prime_outlets.groupby(['btga_outlet_id'])
                    for k, v in prime_outlets:
                        temp_prime_outlet_split = v.to_dict(orient="records")
                        for v in temp_prime_outlet_split:
                            primary_outlets.append(v['btga_outlet_id'])

                # Set common outlet ids from primary and secondary zones
                if len(secondary_outlets_ids) > 0 :
                    intersected_outlet_ids = self.returnMatches(primary_outlets,secondary_outlets_ids)
                else :
                    intersected_outlet_ids = primary_outlets

                if len(intersected_outlet_ids) == 0 :
                    intersected_outlet_ids = primary_outlets    

                if i == 0 :
                    if f_sort_type != '4':
                        # Get zone sales records
                        outlet_query += "SELECT SUM(btoa.btda_amt) as total_amount,SUM(btoa.btda_cost) as total_cost,SUM(btoa.btda_customers_count) as total_customers,SUM(btoa.btda_gp_percent) as gp_percent,SUM(btoa.btda_qty) as total_qty,btt.trxt_date as trx_date,1 as result_type,"+str(id)+" as zone_id FROM bi_trx_department_aggregates btoa JOIN bi_trx_time btt ON btt.trxt_id = btoa.btda_time_id WHERE ( btt.trxt_date >= '"+TY_start_date+"' AND btt.trxt_date <= '"+TY_end_date+"' ) AND ( "
                        # Set outlet ids condition
                        if len(intersected_outlet_ids) > 0 : 
                            for i in range(0,len(intersected_outlet_ids)):
                                outlet_query += " btoa.btda_outlet_id = "+str(intersected_outlet_ids[i])+" "
                                if i != (len(intersected_outlet_ids)-1) :
                                    outlet_query += "OR"
                        outlet_query += " ) GROUP BY btt.trxt_date "
                    else :
                        # Get group sales records
                        outlet_query += "SELECT SUM(btga.btga_amt) as total_amount,SUM(btga.btga_cost) as total_cost,SUM(btga.btga_customers) as total_customers,SUM(btga.btga_gp_percent) as gp_percent,SUM(btga.btga_qty) as total_qty,btt.trxt_date as trx_date,1 as result_type,"+str(id)+" as zone_id FROM bi_trx_group_aggregates btga JOIN bi_trx_time btt ON btt.trxt_id = btga.btga_time_id WHERE ( btt.trxt_date >= '"+TY_start_date+"' AND btt.trxt_date <= '"+TY_end_date+"' ) AND btga.btga_group_id = '"+id+"' AND ("
                        # Set outlet ids condition
                        if len(intersected_outlet_ids) > 0 : 
                            for i in range(0,len(intersected_outlet_ids)):
                                outlet_query += " btga.btga_outlet_id = "+str(intersected_outlet_ids[i])+" "
                                if i != (len(intersected_outlet_ids)-1) :
                                    outlet_query += "OR"
                        outlet_query += " ) GROUP BY btt.trxt_date "

                    # Prepare query for single selection count
                    if len(primary_selections_ids) == 1 :
                        if f_sort_type != '4':
                            # Get zone sales records as union set
                            outlet_query += "UNION SELECT SUM(btoa.btda_amt) as total_amount,SUM(btoa.btda_cost) as total_cost,SUM(btoa.btda_customers_count) as total_customers,SUM(btoa.btda_gp_percent) as gp_percent,SUM(btoa.btda_qty) as total_qty,btt.trxt_date as trx_date,2 as result_type,"+str(id)+" as zone_id FROM bi_trx_department_aggregates btoa JOIN bi_trx_time btt ON btt.trxt_id = btoa.btda_time_id WHERE ( btt.trxt_date >= '"+LY_start_date+"' AND btt.trxt_date <= '"+LY_end_date+"' ) AND ( "
                            # Set outlet ids condition
                            if len(intersected_outlet_ids) > 0 : 
                                for i in range(0,len(intersected_outlet_ids)):
                                    outlet_query += " btoa.btda_outlet_id = "+str(intersected_outlet_ids[i])+" "
                                    if i != (len(intersected_outlet_ids)-1) :
                                        outlet_query += "OR"
                            outlet_query += " ) GROUP BY btt.trxt_date "
                        else :
                            # Get group sales records as union set
                            outlet_query += "UNION SELECT SUM(btga.btga_amt) as total_amount,SUM(btga.btga_cost) as total_cost,SUM(btga.btga_customers) as total_customers,SUM(btga.btga_gp_percent) as gp_percent,SUM(btga.btga_qty) as total_qty,btt.trxt_date as trx_date,2 as result_type,"+str(id)+" as zone_id FROM bi_trx_group_aggregates btga JOIN bi_trx_time btt ON btt.trxt_id = btga.btga_time_id WHERE ( btt.trxt_date >= '"+LY_start_date+"' AND btt.trxt_date <= '"+LY_end_date+"' ) AND btga.btga_group_id = '"+id+"' AND ("
                            # Set outlet ids condition
                            if len(intersected_outlet_ids) > 0 : 
                                for i in range(0,len(intersected_outlet_ids)):
                                    outlet_query += " btga.btga_outlet_id = "+str(intersected_outlet_ids[i])+" "
                                    if i != (len(intersected_outlet_ids)-1) :
                                        outlet_query += "OR"
                            outlet_query += " ) GROUP BY btt.trxt_date "
                else :
                    if f_sort_type != '4':
                        # Get zone sales records as union set
                        outlet_query += "UNION SELECT SUM(btoa.btda_amt) as total_amount,SUM(btoa.btda_cost) as total_cost,SUM(btoa.btda_customers_count) as total_customers,SUM(btoa.btda_gp_percent) as gp_percent,SUM(btoa.btda_qty) as total_qty,btt.trxt_date as trx_date,"+str(selection_count)+" as result_type,"+str(id)+" as zone_id FROM bi_trx_department_aggregates btoa JOIN bi_trx_time btt ON btt.trxt_id = btoa.btda_time_id WHERE ( btt.trxt_date >= '"+TY_start_date+"' AND btt.trxt_date <= '"+TY_end_date+"' ) AND ( "
                        # Set outlet ids condition
                        if len(intersected_outlet_ids) > 0 : 
                            for i in range(0,len(intersected_outlet_ids)):
                                outlet_query += " btoa.btda_outlet_id = "+str(intersected_outlet_ids[i])+" "
                                if i != (len(intersected_outlet_ids)-1) :
                                    outlet_query += "OR"
                        outlet_query += " ) GROUP BY btt.trxt_date "
                    else :
                        # Get group sales records as union set
                        outlet_query += "UNION SELECT SUM(btga.btga_amt) as total_amount,SUM(btga.btga_cost) as total_cost,SUM(btga.btga_customers) as total_customers,SUM(btga.btga_gp_percent) as gp_percent,SUM(btga.btga_qty) as total_qty,btt.trxt_date as trx_date,"+str(selection_count)+" as result_type,"+str(id)+" as zone_id FROM bi_trx_group_aggregates btga JOIN bi_trx_time btt ON btt.trxt_id = btga.btga_time_id WHERE ( btt.trxt_date >= '"+TY_start_date+"' AND btt.trxt_date <= '"+TY_end_date+"' ) AND btga.btga_group_id = '"+id+"' AND ("
                        # Set outlet ids condition
                        if len(intersected_outlet_ids) > 0 : 
                            for i in range(0,len(intersected_outlet_ids)):
                                outlet_query += " btga.btga_outlet_id = "+str(intersected_outlet_ids[i])+" "
                                if i != (len(intersected_outlet_ids)-1) :
                                    outlet_query += "OR"
                        outlet_query += " ) GROUP BY btt.trxt_date "
                # add counter in selection counts    
                selection_counts.append(selection_count)    
            outlet_query += " ORDER BY trx_date"
            
            df_outlet_ty = pd.read_sql_query(outlet_query,con=self.db)
            df_outlet_ty=df_outlet_ty.groupby(['trx_date'])
            ty_ls_records = {}
            filtered_unique_data = {}
            unique_zones = []
            for k, v in df_outlet_ty:
                temp_outlet_split=v.to_dict(orient="records")
                for v in temp_outlet_split:
                    # if v['zone_id'] not in unique_zones :
                    #     unique_zones.append(v['zone_id'])

                    # for i in range(0,len(primary_selections_ids)):
                    #     if int(v['zone_id']) == int(primary_selections_ids[i]) :
                    #         t ={}
                    #         t['total_amount'] = v['total_amount']
                    #         t['gp_percent'] = v['gp_percent']
                    #         t['total_customers'] = v['total_customers']
                    #         filtered_unique_data[str(v['zone_id'])] = t
                    #         #ty_ls_records[str(v['zone_id'])] = {'total_amount':v['total_amount'],'gp_percent':v['gp_percent'],'total_customers':v['total_customers']}
                    # temp_categories_sales.append(filtered_unique_data)

                    if int(v['result_type']) == 1 :
                        temp_months.append(v['trx_date'])
                    
                    if int(v['result_type']) == 1 :
                        temp_cat1_sales.append({'total_amount':v['total_amount'],'total_cost':v['total_cost'],'gp_percent':v['gp_percent'],'total_customers':v['total_customers'],'total_qty':v['total_qty']})
                    elif int(v['result_type']) == 2 :  
                        temp_cat2_sales.append({'total_amount':v['total_amount'],'total_cost':v['total_cost'],'gp_percent':v['gp_percent'],'total_customers':v['total_customers'],'total_qty':v['total_qty']})
                        #temp_months.append(v['trx_date'])
                    elif int(v['result_type']) == 3 :  
                        temp_cat3_sales.append({'total_amount':v['total_amount'],'total_cost':v['total_cost'],'gp_percent':v['gp_percent'],'total_customers':v['total_customers'],'total_qty':v['total_qty']})
                    elif int(v['result_type']) == 4 :  
                        temp_cat4_sales.append({'total_amount':v['total_amount'],'total_cost':v['total_cost'],'gp_percent':v['gp_percent'],'total_customers':v['total_customers'],'total_qty':v['total_qty']})
                    elif int(v['result_type']) == 5 :  
                        temp_cat5_sales.append({'total_amount':v['total_amount'],'total_cost':v['total_cost'],'gp_percent':v['gp_percent'],'total_customers':v['total_customers'],'total_qty':v['total_qty']})
                    elif int(v['result_type']) == 6 :  
                        temp_cat6_sales.append({'total_amount':v['total_amount'],'total_cost':v['total_cost'],'gp_percent':v['gp_percent'],'total_customers':v['total_customers'],'total_qty':v['total_qty']})
                    elif int(v['result_type']) == 7 :  
                        temp_cat7_sales.append({'total_amount':v['total_amount'],'total_cost':v['total_cost'],'gp_percent':v['gp_percent'],'total_customers':v['total_customers'],'total_qty':v['total_qty']})
                    elif int(v['result_type']) == 8 :  
                        temp_cat8_sales.append({'total_amount':v['total_amount'],'total_cost':v['total_cost'],'gp_percent':v['gp_percent'],'total_customers':v['total_customers'],'total_qty':v['total_qty']})
                    elif int(v['result_type']) == 9 :  
                        temp_cat9_sales.append({'total_amount':v['total_amount'],'total_cost':v['total_cost'],'gp_percent':v['gp_percent'],'total_customers':v['total_customers'],'total_qty':v['total_qty']})
                    elif int(v['result_type']) == 10 :  
                        temp_cat10_sales.append({'total_amount':v['total_amount'],'total_cost':v['total_cost'],'gp_percent':v['gp_percent'],'total_customers':v['total_customers'],'total_qty':v['total_qty']})                        
                    # Set total counts
                    total_sales_amount = total_sales_amount+v['total_amount']
                    total_sales_cost = total_sales_cost+v['total_cost']
                    total_sales_customer = total_sales_customer+v['total_customers']
                    total_sales_qty = total_sales_qty+v['total_qty'] 
            
            total_sales_gp = np.divide(((float(total_sales_amount) - float(total_sales_cost)) * 100), float(total_sales_amount))
            total_sales_avgbkt = np.divide(float(total_sales_amount),total_sales_customer)
            total_sales_avgitem = np.divide(total_sales_qty,total_sales_customer)
            # Set marker name as combination of name and axis values
            for i in range(0,len(temp_cat1_sales)):
                
                # Set first category values
                cat1_sale_val = float(temp_cat1_sales[i]['total_amount'])
                #cat1_gp_val = float(temp_cat1_sales[i]['gp_percent'])
                cat1_gp_val = np.divide(((float(temp_cat1_sales[i]['total_amount']) - float(temp_cat1_sales[i]['total_cost'])) * 100), float(temp_cat1_sales[i]['total_amount']))
                cat1_avgbsk_val = np.divide(float(temp_cat1_sales[i]['total_amount']),v['total_customers'])
                cat1_customer_val = temp_cat1_sales[i]['total_customers']
                cat1_avgitem_val = np.divide(temp_cat1_sales[i]['total_qty'],v['total_customers'])
                # Handle exeption other category values  
                try:
                    cat2_sale_val = float(temp_cat2_sales[i]['total_amount'])
                    #cat2_gp_val = float(temp_cat2_sales[i]['gp_percent'])
                    cat2_gp_val = np.divide(((float(temp_cat2_sales[i]['total_amount']) - float(temp_cat2_sales[i]['total_cost'])) * 100), float(temp_cat2_sales[i]['total_amount']))
                    cat2_avgbsk_val = np.divide(float(temp_cat2_sales[i]['total_amount']),v['total_customers'])
                    cat2_customer_val = temp_cat2_sales[i]['total_customers']
                    cat2_avgitem_val = np.divide(temp_cat2_sales[i]['total_qty'],v['total_customers'])
                except IndexError:
                    cat2_sale_val = 0
                    cat2_gp_val = 0
                    cat2_avgbsk_val = 0
                    cat2_customer_val = 0
                    cat2_avgitem_val = 0
                try:
                    cat3_sale_val = float(temp_cat3_sales[i]['total_amount'])
                    #cat3_gp_val = float(temp_cat3_sales[i]['gp_percent'])
                    cat3_gp_val = np.divide(((float(temp_cat3_sales[i]['total_amount']) - float(temp_cat3_sales[i]['total_cost'])) * 100), float(temp_cat3_sales[i]['total_amount']))
                    cat3_avgbsk_val = np.divide(float(temp_cat3_sales[i]['total_amount']),v['total_customers'])
                    cat3_customer_val = temp_cat3_sales[i]['total_customers']
                    cat3_avgitem_val = np.divide(temp_cat3_sales[i]['total_qty'],v['total_customers'])
                except IndexError:
                    cat3_sale_val = 0
                    cat3_gp_val = 0
                    cat3_avgbsk_val = 0
                    cat3_customer_val = 0
                    cat3_avgitem_val = 0
                try:
                    cat4_sale_val = float(temp_cat4_sales[i]['total_amount'])
                    #cat4_gp_val = float(temp_cat4_sales[i]['gp_percent'])
                    cat4_gp_val = np.divide(((float(temp_cat4_sales[i]['total_amount']) - float(temp_cat4_sales[i]['total_cost'])) * 100), float(temp_cat4_sales[i]['total_amount']))
                    cat4_avgbsk_val = np.divide(float(temp_cat4_sales[i]['total_amount']),v['total_customers'])
                    cat4_customer_val = temp_cat4_sales[i]['total_customers']
                    cat4_avgitem_val = np.divide(temp_cat4_sales[i]['total_qty'],v['total_customers'])
                except IndexError:
                    cat4_sale_val = 0
                    cat4_gp_val = 0
                    cat4_avgbsk_val = 0
                    cat4_customer_val = 0
                    cat4_avgitem_val = 0
                try:
                    cat5_sale_val = float(temp_cat5_sales[i]['total_amount'])
                    #cat5_gp_val = float(temp_cat5_sales[i]['gp_percent'])
                    cat5_gp_val = np.divide(((float(temp_cat5_sales[i]['total_amount']) - float(temp_cat5_sales[i]['total_cost'])) * 100), float(temp_cat5_sales[i]['total_amount']))
                    cat5_avgbsk_val = np.divide(float(temp_cat5_sales[i]['total_amount']),v['total_customers'])
                    cat5_customer_val = temp_cat5_sales[i]['total_customers']
                    cat5_avgitem_val = np.divide(temp_cat5_sales[i]['total_qty'],v['total_customers'])
                except IndexError:
                    cat5_sale_val = 0
                    cat5_gp_val = 0
                    cat5_avgbsk_val = 0
                    cat5_customer_val = 0
                    cat5_avgitem_val = 0
                try:
                    cat6_sale_val = float(temp_cat6_sales[i]['total_amount'])
                    #cat6_gp_val = float(temp_cat6_sales[i]['gp_percent'])
                    cat6_gp_val = np.divide(((float(temp_cat6_sales[i]['total_amount']) - float(temp_cat6_sales[i]['total_cost'])) * 100), float(temp_cat6_sales[i]['total_amount']))
                    cat6_avgbsk_val = np.divide(float(temp_cat6_sales[i]['total_amount']),v['total_customers'])
                    cat6_customer_val = temp_cat6_sales[i]['total_customers']
                    cat6_avgitem_val = np.divide(temp_cat6_sales[i]['total_qty'],v['total_customers'])
                except IndexError:
                    cat6_sale_val = 0
                    cat6_gp_val = 0
                    cat6_avgbsk_val = 0
                    cat6_customer_val = 0
                    cat6_avgitem_val = 0
                try:
                    cat7_sale_val = float(temp_cat7_sales[i]['total_amount'])
                    #cat7_gp_val = float(temp_cat7_sales[i]['gp_percent'])
                    cat7_gp_val = np.divide(((float(temp_cat7_sales[i]['total_amount']) - float(temp_cat7_sales[i]['total_cost'])) * 100), float(temp_cat7_sales[i]['total_amount']))
                    cat7_avgbsk_val = np.divide(float(temp_cat7_sales[i]['total_amount']),v['total_customers'])
                    cat7_customer_val = temp_cat7_sales[i]['total_customers']
                    cat7_avgitem_val = np.divide(temp_cat7_sales[i]['total_qty'],v['total_customers'])
                except IndexError:
                    cat7_sale_val = 0
                    cat7_gp_val = 0
                    cat7_avgbsk_val = 0
                    cat7_customer_val = 0
                    cat7_avgitem_val = 0
                try:
                    cat8_sale_val = float(temp_cat8_sales[i]['total_amount'])
                    #cat8_gp_val = float(temp_cat8_sales[i]['gp_percent'])
                    cat8_gp_val = np.divide(((float(temp_cat8_sales[i]['total_amount']) - float(temp_cat8_sales[i]['total_cost'])) * 100), float(temp_cat8_sales[i]['total_amount']))
                    cat8_avgbsk_val = np.divide(float(temp_cat8_sales[i]['total_amount']),v['total_customers'])
                    cat8_customer_val = temp_cat8_sales[i]['total_customers']
                    cat8_avgitem_val = np.divide(temp_cat8_sales[i]['total_qty'],v['total_customers'])
                except IndexError:
                    cat8_sale_val = 0
                    cat8_gp_val = 0
                    cat8_avgbsk_val = 0
                    cat8_customer_val = 0
                    cat8_avgitem_val = 0
                try:
                    cat9_sale_val = float(temp_cat9_sales[i]['total_amount'])
                    #cat9_gp_val = float(temp_cat9_sales[i]['gp_percent'])
                    cat9_gp_val = np.divide(((float(temp_cat9_sales[i]['total_amount']) - float(temp_cat9_sales[i]['total_cost'])) * 100), float(temp_cat9_sales[i]['total_amount']))
                    cat9_avgbsk_val = np.divide(float(temp_cat9_sales[i]['total_amount']),v['total_customers'])
                    cat9_customer_val = temp_cat9_sales[i]['total_customers']
                    cat9_avgitem_val = np.divide(temp_cat9_sales[i]['total_qty'],v['total_customers'])
                except IndexError:
                    cat9_sale_val = 0
                    cat9_gp_val = 0
                    cat9_avgbsk_val = 0
                    cat9_customer_val = 0
                    cat9_avgitem_val = 0
                try:
                    cat10_sale_val = float(temp_cat10_sales[i]['total_amount'])
                    #cat10_gp_val = float(temp_cat10_sales[i]['gp_percent'])
                    cat10_gp_val = np.divide(((float(temp_cat10_sales[i]['total_amount']) - float(temp_cat10_sales[i]['total_cost'])) * 100), float(temp_cat10_sales[i]['total_amount']))
                    cat10_avgbsk_val = np.divide(float(temp_cat10_sales[i]['total_amount']),v['total_customers'])
                    cat10_customer_val = temp_cat10_sales[i]['total_customers']
                    cat10_avgitem_val = np.divide(temp_cat10_sales[i]['total_qty'],v['total_customers'])
                except IndexError:
                    cat10_sale_val = 0
                    cat10_gp_val = 0
                    cat10_avgbsk_val = 0
                    cat10_customer_val = 0
                    cat10_avgitem_val = 0
                                                
                try:
                    month_val = temp_months[i]
                except IndexError:
                    month_val = 0
                # Prepare sales dictionary 
                sales_data = {}
                sales_data["date"] =  month_val.strftime('%Y-%m-%d')
                sales_data["category_sales_1"] = round(float(cat1_sale_val),2)
                sales_data["category_sales_2"] = round(float(cat2_sale_val),2)
                sales_data["category_sales_3"] = round(float(cat3_sale_val),2)
                sales_data["category_sales_4"] = round(float(cat4_sale_val),2)
                sales_data["category_sales_5"] = round(float(cat5_sale_val),2)
                sales_data["category_sales_6"] = round(float(cat6_sale_val),2)
                sales_data["category_sales_7"] = round(float(cat7_sale_val),2)
                sales_data["category_sales_8"] = round(float(cat8_sale_val),2)
                sales_data["category_sales_9"] = round(float(cat9_sale_val),2)
                sales_data["category_sales_10"] = round(float(cat10_sale_val),2)
                graph_sales_dictionary.append(sales_data)
                # Prepare gp dictionary 
                gp_data = {}
                gp_data["date"] = month_val.strftime('%Y-%m-%d')
                gp_data["category_gp_1"] = round(float(cat1_gp_val),2)
                gp_data["category_gp_2"] = round(float(cat2_gp_val),2)
                gp_data["category_gp_3"] = round(float(cat3_gp_val),2)
                gp_data["category_gp_4"] = round(float(cat4_gp_val),2)
                gp_data["category_gp_5"] = round(float(cat5_gp_val),2)
                gp_data["category_gp_6"] = round(float(cat6_gp_val),2)
                gp_data["category_gp_7"] = round(float(cat7_gp_val),2)
                gp_data["category_gp_8"] = round(float(cat8_gp_val),2)
                gp_data["category_gp_9"] = round(float(cat9_gp_val),2)
                gp_data["category_gp_10"] = round(float(cat10_gp_val),2)
                graph_gp_dictionary.append(gp_data)
                # Prepare average basket dictionary 
                avgbkt_data = {}
                avgbkt_data["date"] = month_val.strftime('%Y-%m-%d')
                avgbkt_data["category_avgbasket_1"] = round(float(cat1_avgbsk_val),2)
                avgbkt_data["category_avgbasket_2"] = round(float(cat2_avgbsk_val),2)
                avgbkt_data["category_avgbasket_3"] = round(float(cat3_avgbsk_val),2)
                avgbkt_data["category_avgbasket_4"] = round(float(cat4_avgbsk_val),2)
                avgbkt_data["category_avgbasket_5"] = round(float(cat5_avgbsk_val),2)
                avgbkt_data["category_avgbasket_6"] = round(float(cat6_avgbsk_val),2)
                avgbkt_data["category_avgbasket_7"] = round(float(cat7_avgbsk_val),2)
                avgbkt_data["category_avgbasket_8"] = round(float(cat8_avgbsk_val),2)
                avgbkt_data["category_avgbasket_9"] = round(float(cat9_avgbsk_val),2)
                avgbkt_data["category_avgbasket_10"] = round(float(cat10_avgbsk_val),2)
                graph_avgbasket_dictionary.append(avgbkt_data)
                # Prepare cpc dictionary 
                cpc_data = {}
                cpc_data["date"] = month_val.strftime('%Y-%m-%d')
                cpc_data["category_customers_1"] = round(float(cat1_customer_val),2)
                cpc_data["category_customers_2"] = round(float(cat2_customer_val),2)
                cpc_data["category_customers_3"] = round(float(cat3_customer_val),2)
                cpc_data["category_customers_4"] = round(float(cat4_customer_val),2)
                cpc_data["category_customers_5"] = round(float(cat5_customer_val),2)
                cpc_data["category_customers_6"] = round(float(cat6_customer_val),2)
                cpc_data["category_customers_7"] = round(float(cat7_customer_val),2)
                cpc_data["category_customers_8"] = round(float(cat8_customer_val),2)
                cpc_data["category_customers_9"] = round(float(cat9_customer_val),2)
                cpc_data["category_customers_10"] = round(float(cat10_customer_val),2)
                graph_customers_dictionary.append(cpc_data)
                 # Prepare sales item dictionary 
                item_sales_data = {}
                item_sales_data["date"] =  month_val.strftime('%Y-%m-%d')
                item_sales_data["category_avgitem_1"] = round(float(cat1_avgitem_val),2)
                item_sales_data["category_avgitem_2"] = round(float(cat2_avgitem_val),2)
                item_sales_data["category_avgitem_3"] = round(float(cat3_avgitem_val),2)
                item_sales_data["category_avgitem_4"] = round(float(cat4_avgitem_val),2)
                item_sales_data["category_avgitem_5"] = round(float(cat5_avgitem_val),2)
                item_sales_data["category_avgitem_6"] = round(float(cat6_avgitem_val),2)
                item_sales_data["category_avgitem_7"] = round(float(cat7_avgitem_val),2)
                item_sales_data["category_avgitem_8"] = round(float(cat8_avgitem_val),2)
                item_sales_data["category_avgitem_9"] = round(float(cat9_avgitem_val),2)
                item_sales_data["category_avgitem_10"] = round(float(cat10_avgitem_val),2)
                graph_avgitem_dictionary.append(item_sales_data)
            # Swap primary selection array in case of single selection
            if len(primary_selections_lbl) == 1 :
                primary_selections_lbl = ['This Year','previous Year']

            # Prepare graph category dictionary based on types
            for i in range(0,len(primary_selections_lbl)):
                cat_count = i+1
                # Prepare graph sales category dictionary 
                sales_category_data = {}
                sales_category_data["id"] = 'g'+str(cat_count)
                sales_category_data["balloonText"] = "<span style='font-size:18px;'>[[value]]</span>"
                sales_category_data["bullet"] = "round"
                sales_category_data["title"] = primary_selections_lbl[i]
                sales_category_data["valueField"] = 'category_sales_'+str(cat_count)
                sales_category_data["bulletBorderAlpha"] = 1
                sales_category_data["bulletColor"] = "#FFFFFF"
                sales_category_data["bulletSize"] = 5
                sales_category_data["hideBulletsCount"] = 50
                sales_category_data["lineThickness"] = 2
                sales_category_data["useLineColorForBulletBorder"] = True
                graph_sales_categories_dictionary.append(sales_category_data)
                # Prepare graph gp category dictionary 
                gp_category_data = {}
                gp_category_data["balloonText"] = "[[value]]"
                gp_category_data["bullet"] = "round"
                gp_category_data["title"] = primary_selections_lbl[i]
                gp_category_data["valueField"] = 'category_gp_'+str(cat_count)
                gp_category_data["bulletBorderAlpha"] = 1
                gp_category_data["bulletColor"] = "#FFFFFF"
                gp_category_data["bulletSize"] = 5
                gp_category_data["hideBulletsCount"] = 50
                gp_category_data["lineThickness"] = 2
                gp_category_data["useLineColorForBulletBorder"] = True
                graph_gp_categories_dictionary.append(gp_category_data)
                # Prepare graph avgbasket category dictionary 
                avgbasket_category_data = {}
                avgbasket_category_data["balloonText"] = "[[value]]"
                avgbasket_category_data["bullet"] = "round"
                avgbasket_category_data["title"] = primary_selections_lbl[i]
                avgbasket_category_data["valueField"] = 'category_avgbasket_'+str(cat_count)
                avgbasket_category_data["bulletBorderAlpha"] = 1
                avgbasket_category_data["bulletColor"] = "#FFFFFF"
                avgbasket_category_data["bulletSize"] = 5
                avgbasket_category_data["hideBulletsCount"] = 50
                avgbasket_category_data["lineThickness"] = 2
                avgbasket_category_data["useLineColorForBulletBorder"] = True
                graph_avgbasket_categories_dictionary.append(avgbasket_category_data)
                # Prepare graph customers category dictionary 
                customers_category_data = {}
                customers_category_data["balloonText"] = "[[value]]"
                customers_category_data["bullet"] = "round"
                customers_category_data["title"] = primary_selections_lbl[i]
                customers_category_data["valueField"] = 'category_customers_'+str(cat_count)
                customers_category_data["bulletBorderAlpha"] = 1
                customers_category_data["bulletColor"] = "#FFFFFF"
                customers_category_data["bulletSize"] = 5
                customers_category_data["hideBulletsCount"] = 50
                customers_category_data["lineThickness"] = 2
                customers_category_data["useLineColorForBulletBorder"] = True
                graph_customers_categories_dictionary.append(customers_category_data)
                # Prepare graph average item category dictionary 
                avgitem_category_data = {}
                avgitem_category_data["balloonText"] = "[[value]]"
                avgitem_category_data["bullet"] = "round"
                avgitem_category_data["title"] = primary_selections_lbl[i]
                avgitem_category_data["valueField"] = 'category_avgitem_'+str(cat_count)
                avgitem_category_data["bulletBorderAlpha"] = 1
                avgitem_category_data["bulletColor"] = "#FFFFFF"
                avgitem_category_data["bulletSize"] = 5
                avgitem_category_data["hideBulletsCount"] = 50
                avgitem_category_data["lineThickness"] = 2
                avgitem_category_data["useLineColorForBulletBorder"] = True
                graph_avgitem_categories_dictionary.append(avgitem_category_data)
                
        return [graph_sales_dictionary,graph_gp_dictionary,graph_avgbasket_dictionary,graph_customers_dictionary,graph_sales_categories_dictionary,graph_gp_categories_dictionary,graph_avgbasket_categories_dictionary,graph_customers_categories_dictionary,graph_avgitem_dictionary,graph_avgitem_categories_dictionary,round(float(total_sales_amount),2),round(float(total_sales_gp),2),round(float(total_sales_avgbkt),2),total_sales_customer,round(float(total_sales_avgitem),2) ]
    
    #  --- Get filter data stored in session
    def get_session_filters(self):
        demo_zones = []
        region_zones = []
        rtm_zones = []
        sales_group = []
        TY_start_date = ''
        TY_end_date = ''
        sort_type = 4

        # Get session filter data
        if 'zone_filter_data' in session:
            zone_filter_data = session['zone_filter_data']
            # Get filter data; If exist
            if zone_filter_data.get("demo_zones") != None and zone_filter_data.get("demo_zones") != '':
                # Set selected demo zones 
                f_demo_zones_json = zone_filter_data.get("demo_zones")
                f_demo_zones_array = json.loads(f_demo_zones_json)
                for i in range(0,len(f_demo_zones_array)):
                    demo_zones.append(f_demo_zones_array[i])

            if zone_filter_data.get("region_zones") != None and zone_filter_data.get("region_zones") != '':
                # Set selected region zones 
                f_region_zones_json = zone_filter_data.get("region_zones") 
                f_region_zones_array = json.loads(f_region_zones_json)
                for i in range(0,len(f_region_zones_array)):
                    region_zones.append(f_region_zones_array[i])       

            if zone_filter_data.get("rtm_zones") != None and zone_filter_data.get("rtm_zones") != '':
                # Set selected rtm zones 
                f_rtm_zones_json = zone_filter_data.get("rtm_zones")   
                f_rtm_zones_array = json.loads(f_rtm_zones_json)
                for i in range(0,len(f_rtm_zones_array)):
                    rtm_zones.append(f_rtm_zones_array[i])

            if zone_filter_data.get("sales_group") != None and zone_filter_data.get("sales_group") != '':
                # Set selected sales groups
                f_am_zones_json = zone_filter_data.get("sales_group")   
                f_am_zones_array = json.loads(f_am_zones_json)
                for i in range(0,len(f_am_zones_array)):
                    sales_group.append(f_am_zones_array[i])       

            if zone_filter_data.get("start_date") != None and zone_filter_data.get("end_date") != None:
                TY_start_date = zone_filter_data.get("start_date")
                TY_end_date = zone_filter_data.get("end_date")

            if zone_filter_data.get("sort_type") != None:
                sort_type = zone_filter_data.get("sort_type")
        # Set filter params
        filters = {}
        filters['demo_zones'] = demo_zones
        filters['region_zones'] = region_zones
        filters['rtm_zones'] = rtm_zones
        filters['sales_group'] = sales_group
        filters['TY_start_date'] = TY_start_date
        filters['TY_end_date'] = TY_end_date
        filters['f_sort_type'] = sort_type       
        return filters

    # Get user's outlet records
    def get_outlets(self,user_id):
        user_outlets = []
        user_outlet = ''
        user_zone = ''   
        user_role = 'USER'
        # Get user basic details
        user_res = pd.read_sql_query("SELECT id,outlet,zone from bi_user where user_number = "+user_id+"",con=self.db)

        for v in user_res.to_dict(orient="records"):
            user_outlet = v['outlet']
            user_zone = v['zone']
            # Get outlet records
            if user_outlet != None and user_outlet != '' :
                user_outlets.append(user_outlet)
            elif user_zone != None and user_zone != '' :
                zone_outlet_res = pd.read_sql_query("SELECT zout_outlet_id as outlet_id from zone_outlet where zout_zone_id = '"+user_zone+"'",con=self.db)
                zone_outlet_res=zone_outlet_res.groupby(['outlet_id'])
                for k, v in zone_outlet_res:
                    temp_zone_split=v.to_dict(orient="records")
                    for v in temp_zone_split:
                        user_outlets.append(v['outlet_id'])
            else :
                # Fetch user's outlet sales
                outlet_res = pd.read_sql_query("SELECT outl_id as outlet_id from bi_outlets where 	outl_status = 'Active' and SubString(outl_name, 1, 2) not in ('ZZ','ZG','ZA') order by outl_name asc",con=self.db)
                outlet_res=outlet_res.groupby(['outlet_id'])
                for k, v in outlet_res:
                    temp_outlet_split=v.to_dict(orient="records")
                    for v in temp_outlet_split:
                        user_outlets.append(v['outlet_id'])
                # Set user role        
                user_role = 'ADMIN'        
         # Prepare dictionary 
        user_data = {}
        user_data["user_role"] = user_role
        user_data["user_outlets"] = user_outlets                
        return user_data

    # Check if the int given year is a leap year
    # return true if leap year or false otherwise
    @cache.memoize(timeout=timeout)
    def is_leap_year(self,year):
        leap_year_days = 366
        non_leap_year_days = 365
        if (year % 4) == 0:
            if (year % 100) == 0:
                if (year % 400) == 0:
                    return leap_year_days
                else:
                    return non_leap_year_days
            else:
                return leap_year_days
        else:
            return non_leap_year_days

    # return intersected values of two arrays
    def returnMatches(self,a,b):
       return list(set(a) & set(b))