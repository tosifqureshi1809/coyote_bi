from flask import Flask, Response, g, request,session
from flask_caching import Cache
from flask_sqlalchemy import SQLAlchemy
import sqlalchemy
import pandas as pd
import numpy as np
# -- Important Assets STARTS --
from biapp.db import get_db
from biapp.app import server
from datetime import datetime, timedelta
# -- Important Assets ENDS --
cache = Cache(server, config={'CACHE_TYPE': 'simple'})
timeout = 20
# --- Initialize redis server ---
#import redis
#redis_conn = redis.Redis('127.0.0.1')

class charts():
    title = ""
    db = []
    TYS_result = []
    LYS_result = []
    TYCS_result = []
    g_stores_count = 0
    
    @cache.memoize(timeout=timeout)
    def __init__(self):
        with server.app_context():        
            if 'db' not in g:
                self.db = get_db()
                g.db = self.db
    
    #@cache.memoize(timeout=timeout)
    def pg_scatterchart_TYS_vs_LYS(self):
        # -- For Graph X-Axis, Y-Axis, Custome Data for current Years. --
        temp_Sale_TYS_VS_LYS_X = []
        temp_Sale_TYS_VS_LYS_X2 = []
        temp_Sale_TYS_VS_LYS_Y = []
        
        # -- For Graph X-Axis, Y-Axis, Custome Data for current Years. --
        graph_x1 = []
        graph_x2 = []
        graph_y1 = []
        graph_y2 = []
        graph_y3 = []
        graph_text = []
        graph_customdata = []
        TY_weeks_count = 0
        LY_weeks_count = 0
        TY_week_start_date = ""
        LY_last_weekend_date = ""
        LY_first_weekend = ""
        LY_second_week_start_date = ""
        primary_type = 1
        secondary_type = 2
        # Get drilldown level filter data; If exist
        analysis_level = '1'
        analysis_index_id = ''
        analysis_department_id = ''
        if request.args.get('level') is not None:
            analysis_level = request.args.get('level')
        if request.args.get('id') is not None:    
            analysis_index_id = request.args.get('id')
        if request.args.get('department_id') is not None:    
            analysis_department_id = request.args.get('department_id')  
            
        # Get filter data; If exist
        #filter_data = redis_conn.hgetall("filter_data")
        # Get session filter data
        if 'filter_data' in session:
            filter_data = session['filter_data']
        # Set filter params
        p_outlet_id = ''
        p_zone_id = ''
        s_outlet_id = ''
        s_zone_id = ''
        start_date = ''
        end_date = ''
        comparison_period = 1
        if filter_data.get("p_outlet_id") != None and filter_data.get("p_outlet_id") != '':
            p_outlet_id = filter_data.get("p_outlet_id")
        
        if filter_data.get("p_zone_id") != None and filter_data.get("p_zone_id") != '':
            p_zone_id = filter_data.get("p_zone_id")
       
        if filter_data.get("s_outlet_id") != None and filter_data.get("s_outlet_id") != '':
            s_outlet_id = filter_data.get("s_outlet_id")
        
        if filter_data.get("s_zone_id") != None and filter_data.get("s_zone_id") != '':
            s_zone_id = filter_data.get("s_zone_id")
        
        if filter_data.get("start_date") != None and filter_data.get("start_date") != '' and filter_data.get("end_date") != None and filter_data.get("end_date") != '':
            start_date = filter_data.get("start_date")
            end_date = filter_data.get("end_date")

        if filter_data.get("comparison_period") != None and filter_data.get("comparison_period") != '':
            comparison_period = int(filter_data.get("comparison_period"))
        
        # return if date range empty
        if start_date == '' or end_date == '' :
            res = [graph_x1, graph_y1, graph_y2, graph_y3, graph_x2]
            return res

        # -- Set dates
        convt_startdate = datetime.strptime(start_date, '%Y-%m-%d').date()
        current_year = int(convt_startdate.strftime('%Y'))
        current_month = convt_startdate.strftime('%m')
        current_day = convt_startdate.strftime('%d')

        convt_enddate = datetime.strptime(end_date, '%Y-%m-%d').date()
        current_endmonth = convt_enddate.strftime('%m')
        current_endday = convt_enddate.strftime('%d')
        endday = convt_enddate.strftime('%a')
        
        prev_year = current_year-comparison_period
        year_days = self.is_leap_year(prev_year)-1
        
        convt_prev_startdate = datetime.strptime(str(prev_year)+'-'+current_month+'-'+current_day, '%Y-%m-%d').date()
        convt_prev_enddate = datetime.strptime(str(prev_year)+'-'+current_endmonth+'-'+current_endday, '%Y-%m-%d').date()
        
        convt_startdate.strftime('%Y-%m-%d')
        
        #LY_start_date = convt_startdate - timedelta(days=year_days)
        LY_start_date = convt_prev_startdate.strftime('%Y-%m-%d')
        
        convt_enddate = datetime.strptime(end_date, '%Y-%m-%d').date()
        convt_enddate.strftime('%Y-%m-%d')
        LY_endday = convt_enddate.strftime('%a')

        #LY_end_date = convt_enddate - timedelta(days=year_days)
        LY_end_date = convt_prev_enddate.strftime('%Y-%m-%d')
        
        # -- Set departmet | commodity id based on level
        department_id = ''
        commodity_id  = ''
        if analysis_level == '2' :
            department_id  = analysis_index_id
        elif analysis_level == '3' :
            department_id  = analysis_department_id
            commodity_id  = analysis_index_id

        # -- For this year sales. --
        if(p_outlet_id != '' or p_zone_id != '') :
            # -- Append this year sales data in XY graph set
            df_sale_TYS_VS_LYS = self.filter_till_weekend_sales(start_date,end_date,p_outlet_id,p_zone_id,analysis_level,department_id,commodity_id,primary_type)
           
            global TYS_result
            TYS_result = df_sale_TYS_VS_LYS
            for tys in df_sale_TYS_VS_LYS :
                 # -- X for current year --
                graph_x1.append(tys['week_end_date'].encode("ascii"))                 
                graph_y1.append((tys['total_amount']/tys['total_stores']))

            # -- Append last year sales data in XY graph set
            df_sale_TYS_VS_LYS = self.filter_till_weekend_sales(LY_start_date,LY_end_date,p_outlet_id,p_zone_id,analysis_level,department_id,commodity_id,primary_type)
           
            global LYS_result
            LYS_result = df_sale_TYS_VS_LYS
            for tys in df_sale_TYS_VS_LYS :
                 # -- X for current year --
                graph_x2.append(tys['week_end_date'].encode("ascii"))                 
                graph_y2.append((tys['total_amount']/tys['total_stores']))
    
        # -- Append this year secondary sales data in XY graph set
        df_sale_sec_TYS_VS_LYS = self.filter_till_weekend_sales(start_date,end_date,s_outlet_id,s_zone_id,analysis_level,department_id,commodity_id,secondary_type)
        global TYCS_result
        TYCS_result = df_sale_sec_TYS_VS_LYS
        for tyss in df_sale_sec_TYS_VS_LYS :
            # -- Y for current year secondary filter --               
            graph_y3.append((tyss['total_amount']/tyss['total_stores']))


        graph_dictionary = []
        total_ty_sale_val = 0
        total_ly_sale_val = 0
        # Set marker name as combination of name and axis values
        for i in range(0,len(graph_x1)):
            # Handle exeption  
            try:
                pty_value = float(graph_y1[i])
            except IndexError:
                pty_value = 0
            try:
                ply_value = float(graph_y2[i])
            except IndexError:
                ply_value = 0
            try:
                sty_value = float(graph_y3[i])
            except IndexError:
                sty_value = 0
            # Set this year and last totals
            total_ty_sale_val = total_ty_sale_val+pty_value
            total_ly_sale_val = total_ly_sale_val+ply_value
            # Prepare dictionary 
            myDict = {}
            myDict["weeks"] = graph_x1[i]
            myDict["pty_value"] = round(pty_value, 2)
            myDict["ply_value"] = round(ply_value, 2)
            myDict["sty_value"] = round(sty_value, 2)
            graph_dictionary.append(myDict)
       
        res = [graph_x1, graph_y1, graph_y2, graph_y3, graph_x2,graph_dictionary,round(total_ty_sale_val, 2),round(total_ly_sale_val, 2)]

        return res

    # -- Function used to manage weekwise sales records 
    def filter_till_weekend_sales(self,start_date,end_date,outlet_id,zone_id='',analysis_level='1',department_id='',commodity_id='',aggregate_type=1) :
       
        # get day wise sales data
        ty_sales_array = self.sales_aggregate_calculation(start_date,end_date,outlet_id,zone_id,analysis_level,department_id,commodity_id,aggregate_type)

        weekend_sales = {}
        # manage week wise sales data
        if len(ty_sales_array) > 0 :
            total_amount = 0
            total_cost = 0
            total_customers = 0
            total_gp_percent = 0
            weekend_date = ''
            for v in ty_sales_array:
                # -- Set dates
                vsf_trading_date = v['vsf_trading_date']
                total_stores = v['total_stores']
                if weekend_date == '' or weekend_date != vsf_trading_date :
                    convt_date = datetime.strptime(vsf_trading_date, '%Y-%m-%d').date()
                    convt_year  = int(convt_date.strftime('%Y'))
                    convt_month = int(convt_date.strftime('%m'))
                    convt_day   = int(convt_date.strftime('%d'))
                    # get next saturday date
                    coming_saturday =  self.get_next_weekdate(convt_year, convt_month, convt_day)
                    weekend_date = coming_saturday.strftime('%Y-%m-%d')
                else :
                    weekend_date = vsf_trading_date
        
                # append product sales data
                if weekend_date not in weekend_sales:
                    weekend_sales[weekend_date] = []
                # append searched end date as weekend date
                if vsf_trading_date == end_date :
                    weekend_sales[end_date] = []
                    
                if weekend_date == vsf_trading_date or vsf_trading_date == end_date:
                    # add total sales attributes   
                    total_amount    = total_amount+v['total_amount']
                    total_cost      = total_cost+v['total_cost']
                    total_customers = total_customers+v['total_customers']
                    total_gp_percent= total_gp_percent+v['gp_percent']
                    # append searched end date as weekend date
                    if vsf_trading_date == end_date :
                        convt_end_date = datetime.strptime(end_date, '%Y-%m-%d').date()
                        weekend_date = convt_end_date.strftime('%Y-%m-%d')
                    # prepare journal values for sales array department wise  
                    weekend_sales[weekend_date].append({'week_end_date':weekend_date,'total_customers':total_customers,'total_amount':total_amount,'total_cost':total_cost,'gp_percent':total_gp_percent,"year":convt_year,"total_stores":total_stores})
                    #reset sale values as 0
                    total_amount     = 0
                    total_cost       = 0
                    total_customers  = 0
                    total_gp_percent = 0
                else :
                    # add total sales attributes   
                    total_amount    = total_amount+v['total_amount']
                    total_cost      = total_cost+v['total_cost']
                    total_customers = total_customers+v['total_customers']
                    total_gp_percent= total_gp_percent+v['gp_percent']

        final_array = []
        for k in weekend_sales :
            for j in weekend_sales[k] :
                if 'total_amount' in j:
                    total_stores = int(j['total_stores'])             
                    # append this year department sales value in array
                    final_array.append({'week_end_date':j['week_end_date'],'total_customers':(np.divide(j['total_customers'],total_stores)),'total_amount':(np.divide(j['total_amount'],total_stores)),'total_cost':(np.divide(j['total_cost'],total_stores)),'avg_basket':(np.divide(np.divide(j['total_amount'],j['total_customers']),total_stores)),'gp_percent':(np.divide(v['gp_percent'],total_stores)),'year':j['year'],'total_stores':total_stores})

        # sort by weekend date
        if len(final_array) > 0 :
            final_array =  sorted(final_array, key = lambda i: i['week_end_date'])   

        return final_array

    # --- Function used to get next week date from given date
    def get_next_weekdate(self,year, month, day):
        import datetime
        date0 = datetime.date(year, month, day)
        next_week = date0 + datetime.timedelta(6 - date0.weekday() or 6)
        return next_week

    # --- Function used to manage sales filter sales
    def sales_aggregate_calculation(self,start_date,end_date,outlet_id='',zone_id='',analysis_level='1',department_id='',commodity_id='',aggregate_type=1):
        
        # Set table, colomn name based on analysis level
        tbl_alais = 'btoa'
        tbl_prefix = 'btoa.btoa'
        tbl_name = 'bi_trx_outlet_aggregates'
        tbl_customer_col = 'btoa.btoa_customers'
        if analysis_level == '2' :
            tbl_alais = 'btda'
            tbl_prefix = 'btda.btda'
            tbl_name = 'bi_trx_department_aggregates'
            tbl_customer_col = 'btda.btda_customers_count'
        elif analysis_level == '3' :
            tbl_alais = 'btca'
            tbl_prefix = 'btca.btca'
            tbl_name = 'bi_trx_commodity_aggregates'
            tbl_customer_col = 'btca.btca_customers_count'
        # Prepare outlet level sql for fetching this year sales records    
        if analysis_level == '1':
            df_sale_TYS_VS_LYS_query = "SELECT sum(btda.btda_amt) as total_amount,sum(btda.btda_cost) as total_cost, sum(btda.btda_customers_count) as total_customers,(((sum(btda.btda_amt) - sum(btda.btda_cost)) / sum(btda.btda_amt))*100) as gp_percent, btt.trxt_date,btt.trxt_year as year"
            if aggregate_type == 2 :
                if zone_id != '' :
                    df_sale_TYS_VS_LYS_query += ",(SELECT count(zout_id) FROM zone_outlet zo WHERE zout_zone_id = '"+zone_id+"') as total_stores"
            df_sale_TYS_VS_LYS_query += " FROM bi_trx_department_aggregates btda JOIN bi_trx_time btt ON btt.trxt_id = btda.btda_time_id WHERE "
            if outlet_id != '':
                df_sale_TYS_VS_LYS_query += " btda.btda_outlet_id  = '"+outlet_id+"' AND "
            elif zone_id != '' :
                df_sale_TYS_VS_LYS_query += " btda.btda_outlet_id  IN ( select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+zone_id+"' )  AND "
            df_sale_TYS_VS_LYS_query += " ( btt.trxt_date BETWEEN '"+start_date+"' AND '"+end_date+"' ) GROUP BY btt.trxt_date,btt.trxt_year  ORDER BY btt.trxt_date"
        else :
            # Prepare sql for department & commodity level this year sales records
            df_sale_TYS_VS_LYS_query = "SELECT "+tbl_prefix+"_amt as total_amount,"+tbl_prefix+"_cost as total_cost, "+tbl_customer_col+" as total_customers,"+tbl_prefix+"_gp_percent as gp_percent, btt.trxt_date,btt.trxt_year as year"
            if aggregate_type == 2 :
                if zone_id != '' :
                    df_sale_TYS_VS_LYS_query += ",(SELECT count(zout_id) FROM zone_outlet zo WHERE zout_zone_id = '"+zone_id+"') as total_stores"
            df_sale_TYS_VS_LYS_query += " FROM "+tbl_name+" "+tbl_alais+" JOIN bi_trx_time btt ON btt.trxt_id = "+tbl_prefix+"_time_id WHERE "  
            if outlet_id != '':
                df_sale_TYS_VS_LYS_query += ""+tbl_prefix+"_outlet_id  = '"+outlet_id+"' AND "
            elif zone_id != '' :
                df_sale_TYS_VS_LYS_query += ""+tbl_prefix+"_outlet_id  IN ( select zout_outlet_id FROM zone_outlet WHERE zout_zone_id = '"+zone_id+"' )  AND "
            if analysis_level == '2' :
                df_sale_TYS_VS_LYS_query += " "+tbl_prefix+"_department_id  = '"+department_id+"' AND "
            elif analysis_level == '3' :
                df_sale_TYS_VS_LYS_query += " "+tbl_prefix+"_department_id  = '"+department_id+"' AND "+tbl_prefix+"_commodity_id  = '"+commodity_id+"' AND "
            df_sale_TYS_VS_LYS_query += " ( btt.trxt_date BETWEEN '"+start_date+"' AND '"+end_date+"' )  ORDER BY btt.trxt_date" 
        
        ty_df_sales_result = pd.read_sql_query(df_sale_TYS_VS_LYS_query,con=self.db)
        ty_df_sales_result=ty_df_sales_result.groupby(['trxt_date'])
        # manage this year sales aggragate data
        ty_sales_array = self.manage_yearly_aggregate_sales(ty_df_sales_result,analysis_level)

        final_array = []
        for k in ty_sales_array :
            for v in k :
                for j in k[v] :
                   
                    # append this year department sales value in array
                    final_array.append({'vsf_trading_date':j['vsf_trading_date'],'total_amount':j['vsf_dep_sales_amt'],'total_customers':j['vsf_dep_customers_count'],'total_cost':j['vsf_dep_sales_cost'],'gp_percent':(((j['vsf_dep_sales_amt'] - j['vsf_dep_sales_cost']) / j['vsf_dep_sales_amt'])*100),'total_stores':j['total_stores']})
        # sort by purchase date
        if len(final_array) > 0 :
            final_array =  sorted(final_array, key = lambda i: i['vsf_trading_date'])
        return final_array

    # Manage yearly sales record
    def manage_yearly_aggregate_sales(self,df_sales_result,analysis_level):
       
        # initialize department product sales array
        department_array = {} 
        for k, v in df_sales_result:
            temp_group_split=v.to_dict(orient="records")
            for v in temp_group_split:
                # set department label
                TRADING_DATE = v['trxt_date']
                TRADING_DATE = TRADING_DATE.strftime('%Y-%m-%d')
                # append product sales data in department index
                if TRADING_DATE not in department_array:
                    department_array[TRADING_DATE] = []
                # set total store count
                total_stores = 1
                if 'total_stores' in v:
                    total_stores = v['total_stores']
                # prepare journal values for sales array department wise  
                department_array[TRADING_DATE].append({'total_amount':v['total_amount'],'total_cost':v['total_cost'],'total_customers':v['total_customers'],'gp_percent':v['gp_percent'],'YEAR':v['year'],'TRADING_DATE':TRADING_DATE,'total_stores':total_stores})
               
        department_sales_array = []
        trx_numbers = []    
        # prepare department sales report result
        for i in department_array:
        
            # initialize default values
            vsf_dep_amt = 0
            vsf_dep_cost = 0
            vsf_dep_customers = 0
            last_trx_no = 0
            dept_data = []
            for v in department_array[i] :
              
                # add sales values
                vsf_dep_amt 		= v['total_amount']
                vsf_dep_cost 		= v['total_cost']
                vsf_dep_customers   = v['total_customers']
                vsf_trading_date    = v['TRADING_DATE']
                total_stores        = v['total_stores']
                
            # append sales value in array
            dept_data.append({'vsf_dep_sales_cost':vsf_dep_cost,'vsf_dep_sales_amt':vsf_dep_amt,'vsf_dep_customers_count':vsf_dep_customers,'vsf_trading_date':vsf_trading_date,'total_stores':total_stores})
            # append result in department array
            department_sales_array.append({i:dept_data})
            
        return department_sales_array

    # Check if the int given year is a leap year
    # return true if leap year or false otherwise
    @cache.memoize(timeout=timeout)
    def is_leap_year(self,year):
        leap_year_days = 366
        non_leap_year_days = 365
        if (year % 4) == 0:
            if (year % 100) == 0:
                if (year % 400) == 0:
                    return leap_year_days
                else:
                    return non_leap_year_days
            else:
                return leap_year_days
        else:
            return non_leap_year_days

    @cache.memoize(timeout=timeout)
    def pg_scatterchart_TYGP_vs_LYGP(self):
        
        # -- For Graph X-Axis, Y-Axis, Custome Data for current Years. --
        temp_Sale_TYGP_vs_LYGP_X = []
        temp_Sale_TYGP_vs_LYGP_Y = []
        temp_Sale_TYGP_VS_LYGP_X2 = []
        
        # -- For Graph X-Axis, Y-Axis, Custome Data for current Years. --
        graph_x1 = []
        graph_x2 = []
        graph_y1 = []
        graph_y2 = []
        graph_y3 = []
        graph_text = []
        graph_customdata = []
        TY_weeks_count = 0
        LY_weeks_count = 0
        TY_week_start_date = ""
        LY_last_weekend_date = ""
        LY_first_weekend = ""
        LY_second_week_start_date = ""
        primary_type = 1
        secondary_type = 2
        TY_total_amount = 0
        TY_total_cost = 0
        LY_total_amount = 0
        LY_total_cost = 0
         # Get drilldown level filter data; If exist
        analysis_level = '1'
        analysis_index_id = ''
        analysis_department_id = ''
        if request.args.get('level') is not None:
            analysis_level = request.args.get('level')
        if request.args.get('id') is not None:    
            analysis_index_id = request.args.get('id')
        if request.args.get('department_id') is not None:    
            analysis_department_id = request.args.get('department_id')
        # Get filter data; If exist
        #filter_data = redis_conn.hgetall("filter_data")
        # Get session filter data
        if 'filter_data' in session:
            filter_data = session['filter_data']
        # Set filter params
        p_outlet_id = ''
        p_zone_id = ''
        s_outlet_id = ''
        s_zone_id = ''
        start_date = ''
        end_date = ''
        comparison_period = 1
        if filter_data.get("p_outlet_id") != None and filter_data.get("p_outlet_id") != '':
            p_outlet_id = filter_data.get("p_outlet_id")
        
        if filter_data.get("p_zone_id") != None and filter_data.get("p_zone_id") != '':
            p_zone_id = filter_data.get("p_zone_id")
       
        if filter_data.get("s_outlet_id") != None and filter_data.get("s_outlet_id") != '':
            s_outlet_id = filter_data.get("s_outlet_id")
        
        if filter_data.get("s_zone_id") != None and filter_data.get("s_zone_id") != '':
            s_zone_id = filter_data.get("s_zone_id")
        
        if filter_data.get("start_date") != None and filter_data.get("start_date") != '' and filter_data.get("end_date") != None:
            start_date = filter_data.get("start_date")
            end_date = filter_data.get("end_date")

        if filter_data.get("comparison_period") != None and filter_data.get("comparison_period") != '':
            comparison_period = int(filter_data.get("comparison_period"))

        # return if date range empty
        if start_date == '' or end_date == '' :
            res = [graph_x1, graph_y1, graph_y2, graph_y3, graph_x2]
            return res

       # -- Set dates
        convt_startdate = datetime.strptime(start_date, '%Y-%m-%d').date()
        current_year = int(convt_startdate.strftime('%Y'))
        current_month = convt_startdate.strftime('%m')
        current_day = convt_startdate.strftime('%d')
        convt_enddate = datetime.strptime(end_date, '%Y-%m-%d').date()
        current_endmonth = convt_enddate.strftime('%m')
        current_endday = convt_enddate.strftime('%d')
        prev_year = current_year-comparison_period
        year_days = self.is_leap_year(prev_year)-1
        convt_prev_startdate = datetime.strptime(str(prev_year)+'-'+current_month+'-'+current_day, '%Y-%m-%d').date()
        convt_prev_enddate = datetime.strptime(str(prev_year)+'-'+current_endmonth+'-'+current_endday, '%Y-%m-%d').date()
        convt_startdate.strftime('%Y-%m-%d')
        LY_start_date = convt_prev_startdate.strftime('%Y-%m-%d')
        convt_enddate = datetime.strptime(end_date, '%Y-%m-%d').date()
        convt_enddate.strftime('%Y-%m-%d') 
        LY_end_date = convt_prev_enddate.strftime('%Y-%m-%d')

       # -- Set departmet | commodity id based on level
        department_id = ''
        commodity_id  = ''
        if analysis_level == '2' :
            department_id  = analysis_index_id
        elif analysis_level == '3' :
            department_id  = analysis_department_id
            commodity_id  = analysis_index_id
        # Get this year sale result
        if(p_outlet_id != '' or p_zone_id != '') :
            
            # -- Manage this year sales data in XY graph set   
            if not TYS_result:
                df_sale_TYGP_vs_LYGP = self.filter_till_weekend_sales(start_date,end_date,p_outlet_id,p_zone_id,analysis_level,department_id,commodity_id,primary_type)
            else:
                df_sale_TYGP_vs_LYGP = TYS_result
           
            # -- Append this year sales data in XY graph set
            for tygp in df_sale_TYGP_vs_LYGP :
                # -- X for current year --
                graph_x1.append(tygp['week_end_date'].encode("ascii"))
                gp_percent = np.divide(((tygp['total_amount'] - tygp['total_cost']) * 100) , tygp['total_amount'])           
                graph_y1.append((gp_percent/tygp['total_stores']))
                TY_total_amount = TY_total_amount+tygp['total_amount']
                TY_total_cost = TY_total_cost+tygp['total_cost']
                
            # -- Manage last year sales data in XY graph set   
            if not LYS_result:
                df_sale_TYGP_vs_LYGP = self.filter_till_weekend_sales(LY_start_date,LY_end_date,p_outlet_id,p_zone_id,analysis_level,department_id,commodity_id,primary_type)
            else:
                df_sale_TYGP_vs_LYGP = LYS_result
           
            # -- Append last year sales data in XY graph set
            for lygp in df_sale_TYGP_vs_LYGP :
                # -- X for current year --
                graph_x2.append(lygp['week_end_date'].encode("ascii"))
                gp_percent = np.divide(((lygp['total_amount'] - lygp['total_cost']) * 100) , lygp['total_amount'])                
                graph_y2.append((gp_percent/lygp['total_stores']))
                LY_total_amount = LY_total_amount+lygp['total_amount']
                LY_total_cost = LY_total_cost+lygp['total_cost']

        # -- Manage this year comparison sales data in Y graph set
        if not TYCS_result:
            df_sale_TYGP_vs_LYGP = self.filter_till_weekend_sales(start_date,end_date,s_outlet_id,s_zone_id,analysis_level,department_id,commodity_id,secondary_type)
        else:
            df_sale_TYGP_vs_LYGP = TYCS_result
        
        # -- Append last year sales data in XY graph set
        for tycgp in df_sale_TYGP_vs_LYGP :
            # -- Y for current year --
            gp_percent = np.divide(((tycgp['total_amount'] - tycgp['total_cost']) * 100) , tycgp['total_amount'])                
            graph_y3.append((gp_percent/tycgp['total_stores']))


        graph_dictionary = []
        total_ty_sale_val = 0
        total_ly_sale_val = 0
        # Set marker name as combination of name and axis values
        for i in range(0,len(graph_x1)):
            # Prepare dictionary 
            myDict = {}
            # Handle exeption  
            try:
                pty_value = float(graph_y1[i])
            except IndexError:
                pty_value = 0
            try:
                ply_value = float(graph_y2[i])
            except IndexError:
                ply_value = 0
            try:
                sty_value = float(graph_y3[i])
            except IndexError:
                sty_value = 0
            myDict["weeks"] = graph_x1[i]
            myDict["pty_value"] = round(pty_value, 2)
            myDict["ply_value"] = round(ply_value, 2)
            myDict["sty_value"] = round(sty_value, 2)
            graph_dictionary.append(myDict)
        # Set total gp of this and last year
        total_ty_sale_val = np.divide(((TY_total_amount - TY_total_cost) * 100) , TY_total_amount)
        total_ly_sale_val = np.divide(((LY_total_amount - LY_total_cost) * 100) , LY_total_amount)
        res = [graph_x1, graph_y1, graph_y2, graph_y3, graph_x2, graph_dictionary,round(total_ty_sale_val, 2),round(total_ly_sale_val, 2)]
        return res

    @cache.memoize(timeout=timeout)
    def pg_scatterchart_THAVGB_vs_LYAVGB(self):
       
        # -- For Graph X-Axis, Y-Axis, Custome Data for current Years. --
        temp_Sale_THAVGB_vs_LYAVGB_X = []
        temp_Sale_THAVGB_vs_LYAVGB_X2 = []
        temp_Sale_THAVGB_vs_LYAVGB_Y = []
        
        # -- For Graph X-Axis, Y-Axis, Custome Data for current Years. --
        graph_x1 = []
        graph_x2 = []
        graph_y1 = []
        graph_y2 = []
        graph_y3 = []
        graph_text = []
        graph_customdata = []
        TY_weeks_count = 0
        LY_weeks_count = 0
        TY_week_start_date = ""
        LY_last_weekend_date = ""
        LY_first_weekend = ""
        LY_second_week_start_date = ""
        primary_type = 1
        secondary_type = 2
        TY_total_amount = 0
        TY_total_customer = 0
        TY_total_store = 0
        LY_total_amount = 0
        LY_total_customer = 0
        LY_total_store = 0
        # Get drilldown level filter data; If exist
        analysis_level = '1'
        analysis_index_id = ''
        analysis_department_id = ''
        if request.args.get('level') is not None:
            analysis_level = request.args.get('level')
        if request.args.get('id') is not None:    
            analysis_index_id = request.args.get('id')
        if request.args.get('department_id') is not None:    
            analysis_department_id = request.args.get('department_id')
        #filter_data = redis_conn.hgetall("filter_data")
        # Get session filter data
        if 'filter_data' in session:
            filter_data = session['filter_data']
        # Set filter params
        p_outlet_id = ''
        p_zone_id = ''
        s_outlet_id = ''
        s_zone_id = ''
        start_date = ''
        end_date = ''
        comparison_period = 1
        if filter_data.get("p_outlet_id") != None and filter_data.get("p_outlet_id") != '':
            p_outlet_id = filter_data.get("p_outlet_id")
        
        if filter_data.get("p_zone_id") != None and filter_data.get("p_zone_id") != '':
            p_zone_id = filter_data.get("p_zone_id")
       
        if filter_data.get("s_outlet_id") != None and filter_data.get("s_outlet_id") != '':
            s_outlet_id = filter_data.get("s_outlet_id")
        
        if filter_data.get("s_zone_id") != None and filter_data.get("s_zone_id") != '':
            s_zone_id = filter_data.get("s_zone_id")
        
        if filter_data.get("start_date") != None and filter_data.get("start_date") != '' and filter_data.get("end_date") != None:
            start_date = filter_data.get("start_date")
            end_date = filter_data.get("end_date")

        if filter_data.get("comparison_period") != None and filter_data.get("comparison_period") != '':
            comparison_period = int(filter_data.get("comparison_period"))

        # return if date range empty
        if start_date == '' or end_date == '' :
            res = [graph_x1, graph_y1, graph_y2, graph_y3, graph_x2]
            return res

        # -- Set dates
        convt_startdate = datetime.strptime(start_date, '%Y-%m-%d').date()
        current_year = int(convt_startdate.strftime('%Y'))
        current_month = convt_startdate.strftime('%m')
        current_day = convt_startdate.strftime('%d')
        convt_enddate = datetime.strptime(end_date, '%Y-%m-%d').date()
        current_endmonth = convt_enddate.strftime('%m')
        current_endday = convt_enddate.strftime('%d')
        prev_year = current_year-comparison_period
        year_days = self.is_leap_year(prev_year)-1
        convt_prev_startdate = datetime.strptime(str(prev_year)+'-'+current_month+'-'+current_day, '%Y-%m-%d').date()
        convt_prev_enddate = datetime.strptime(str(prev_year)+'-'+current_endmonth+'-'+current_endday, '%Y-%m-%d').date()
        convt_startdate.strftime('%Y-%m-%d')
        LY_start_date = convt_prev_startdate.strftime('%Y-%m-%d')
        convt_enddate = datetime.strptime(end_date, '%Y-%m-%d').date()
        convt_enddate.strftime('%Y-%m-%d')
        LY_end_date = convt_prev_enddate.strftime('%Y-%m-%d')
         # -- Set departmet | commodity id based on level
        department_id = ''
        commodity_id  = ''
        if analysis_level == '2' :
            department_id  = analysis_index_id
        elif analysis_level == '3' :
            department_id  = analysis_department_id
            commodity_id  = analysis_index_id
        # -- For this year sales. --
        if(p_outlet_id != '' or p_zone_id != '') :
            # -- Manage this year sales data in XY graph set   
            if not TYS_result:
                df_sale_THAVGB_vs_LYAVGB = self.filter_till_weekend_sales(start_date,end_date,p_outlet_id,p_zone_id,analysis_level,department_id,commodity_id,primary_type)
            else:
                df_sale_THAVGB_vs_LYAVGB = TYS_result

            # -- Append this year sales data in XY graph set
            for tyavgp in df_sale_THAVGB_vs_LYAVGB :
                # -- X for current year --
                graph_x1.append(tyavgp['week_end_date'].encode("ascii"))
                graph_y1.append(np.divide(tyavgp['total_amount'],tyavgp['total_customers'])/tyavgp['total_stores'])
                TY_total_amount = TY_total_amount+tyavgp['total_amount']
                TY_total_customer = TY_total_customer+tyavgp['total_customers']
                TY_total_store = TY_total_store+tyavgp['total_stores']

            # -- Manage last year sales data in XY graph set   
            if not LYS_result:
                df_sale_THAVGB_vs_LYAVGB = self.filter_till_weekend_sales(LY_start_date,LY_end_date,p_outlet_id,p_zone_id,analysis_level,department_id,commodity_id,primary_type)
            else:
                df_sale_THAVGB_vs_LYAVGB = LYS_result

            # -- Append last year sales data in XY graph set
            for lyavgp in df_sale_THAVGB_vs_LYAVGB :
                # -- X for current year --
                graph_x2.append(lyavgp['week_end_date'].encode("ascii"))                 
                graph_y2.append(np.divide(lyavgp['total_amount'],lyavgp['total_customers'])/lyavgp['total_stores'])
                LY_total_amount = LY_total_amount+lyavgp['total_amount']
                LY_total_customer = LY_total_customer+lyavgp['total_customers']
                LY_total_store = LY_total_store+lyavgp['total_stores']

        # -- Manage this year comparison sales data in Y graph set
        if not TYCS_result:
            df_sale_THAVGB_vs_LYAVGB = self.filter_till_weekend_sales(start_date,end_date,s_outlet_id,s_zone_id,analysis_level,department_id,commodity_id,secondary_type)
        else:
            df_sale_THAVGB_vs_LYAVGB = TYCS_result

        # -- Append last year sales data in XY graph set
        for tyavgp in df_sale_THAVGB_vs_LYAVGB :
            # -- Y for current year --           
            graph_y3.append(np.divide(tyavgp['total_amount'],tyavgp['total_customers'])/tyavgp['total_stores'])

        graph_dictionary = []
        # Set marker name as combination of name and axis values
        for i in range(0,len(graph_x1)):
            # Prepare dictionary 
            myDict = {}
            # Handle exeption  
            try:
                pty_value = float(graph_y1[i])
            except IndexError:
                pty_value = 0
            try:
                ply_value = float(graph_y2[i])
            except IndexError:
                ply_value = 0
            try:
                sty_value = float(graph_y3[i])
            except IndexError:
                sty_value = 0

            myDict["weeks"] = graph_x1[i]
            myDict["pty_value"] = round(pty_value, 2)
            myDict["ply_value"] = round(ply_value, 2)
            myDict["sty_value"] = round(sty_value, 2)
            graph_dictionary.append(myDict)
        # Set avg basket of this and last year
        total_ty_sale_val = np.divide(TY_total_amount,TY_total_customer)/TY_total_store
        total_ly_sale_val = np.divide(LY_total_amount,LY_total_customer)/LY_total_store
        res = [graph_x1, graph_y1, graph_y2, graph_y3, graph_x2, graph_dictionary,round(total_ty_sale_val, 2),round(total_ly_sale_val, 2)]
        return res

    @cache.memoize(timeout=timeout)
    def pg_scatterchart_TYC_vs_LYC(self):
        
        # -- For Graph X-Axis, Y-Axis, Customer Data for current Years. --
        temp_Sale_TYC_vs_LYC_X = []
        temp_Sale_TYC_vs_LYC_X2 = []
        temp_Sale_TYC_vs_LYC_Y = []
        
        # -- For Graph X-Axis, Y-Axis, Customer Data for current Years. --
        graph_x1 = []
        graph_x2 = []
        graph_y1 = []
        graph_y2 = []
        graph_y3 = []
        graph_text = []
        graph_customdata = []
        TY_weeks_count = 0
        LY_weeks_count = 0
        TY_week_start_date = ""
        LY_last_weekend_date = ""
        LY_first_weekend = ""
        LY_second_week_start_date = ""
        primary_type = 1
        secondary_type = 2
        # Get drilldown level filter data; If exist
        analysis_level = '1'
        analysis_index_id = ''
        analysis_department_id = ''
        if request.args.get('level') is not None:
            analysis_level = request.args.get('level')
        if request.args.get('id') is not None:    
            analysis_index_id = request.args.get('id')
        if request.args.get('department_id') is not None:    
            analysis_department_id = request.args.get('department_id')
        #filter_data = redis_conn.hgetall("filter_data")
        # Get session filter data
        if 'filter_data' in session:
            filter_data = session['filter_data']
        # Set filter params
        p_outlet_id = ''
        p_zone_id = ''
        s_outlet_id = ''
        s_zone_id = ''
        start_date = ''
        end_date = ''
        comparison_period = 1
        if filter_data.get("p_outlet_id") != None and filter_data.get("p_outlet_id") != '':
            p_outlet_id = filter_data.get("p_outlet_id")
        
        if filter_data.get("p_zone_id") != None and filter_data.get("p_zone_id") != '':
            p_zone_id = filter_data.get("p_zone_id")
       
        if filter_data.get("s_outlet_id") != None and filter_data.get("s_outlet_id") != '':
            s_outlet_id = filter_data.get("s_outlet_id")
        
        if filter_data.get("s_zone_id") != None and filter_data.get("s_zone_id") != '':
            s_zone_id = filter_data.get("s_zone_id")
        
        if filter_data.get("start_date") != None and filter_data.get("start_date") != '' and filter_data.get("end_date") != None:
            start_date = filter_data.get("start_date")
            end_date = filter_data.get("end_date")

        if filter_data.get("comparison_period") != None and filter_data.get("comparison_period") != '':
            comparison_period = int(filter_data.get("comparison_period"))

        # return if date range empty
        if start_date == '' or end_date == '' :
            res = [graph_x1, graph_y1, graph_y2, graph_y3, graph_x2]
            return res

        # -- Set dates
        convt_startdate = datetime.strptime(start_date, '%Y-%m-%d').date()
        current_year = int(convt_startdate.strftime('%Y'))
        current_month = convt_startdate.strftime('%m')
        current_day = convt_startdate.strftime('%d')
        convt_enddate = datetime.strptime(end_date, '%Y-%m-%d').date()
        current_endmonth = convt_enddate.strftime('%m')
        current_endday = convt_enddate.strftime('%d')
        prev_year = current_year-comparison_period
        year_days = self.is_leap_year(prev_year)-1
        convt_prev_startdate = datetime.strptime(str(prev_year)+'-'+current_month+'-'+current_day, '%Y-%m-%d').date()
        convt_prev_enddate = datetime.strptime(str(prev_year)+'-'+current_endmonth+'-'+current_endday, '%Y-%m-%d').date()
        convt_startdate.strftime('%Y-%m-%d')
        LY_start_date = convt_prev_startdate.strftime('%Y-%m-%d')
        convt_enddate = datetime.strptime(end_date, '%Y-%m-%d').date()
        convt_enddate.strftime('%Y-%m-%d')
        LY_end_date = convt_prev_enddate.strftime('%Y-%m-%d')

        # -- Set department | commodity id based on level
        department_id = ''
        commodity_id  = ''
        if analysis_level == '2' :
            department_id  = analysis_index_id
        elif analysis_level == '3' :
            department_id  = analysis_department_id
            commodity_id  = analysis_index_id
        # -- For this year sales. --
        if(p_outlet_id != '' or p_zone_id != '') :
            # -- Manage this year sales data in XY graph set   
            if not TYS_result:
                df_sale_TYC_VS_LYC = self.filter_till_weekend_sales(start_date,end_date,p_outlet_id,p_zone_id,analysis_level,department_id,commodity_id,primary_type)
            else:
                df_sale_TYC_VS_LYC = TYS_result

            # -- Append this year sales data in XY graph set
            for tyc in df_sale_TYC_VS_LYC :
                # -- X for current year --
                graph_x1.append(tyc['week_end_date'].encode("ascii"))
                graph_y1.append(tyc['total_customers']/tyc['total_stores'])

            # -- Manage last year sales data in XY graph set   
            if not LYS_result:
                df_sale_TYC_VS_LYC = self.filter_till_weekend_sales(LY_start_date,LY_end_date,p_outlet_id,p_zone_id,analysis_level,department_id,commodity_id,primary_type)
            else:
                df_sale_TYC_VS_LYC = LYS_result

            # -- Append last year sales data in XY graph set
            for tyc in df_sale_TYC_VS_LYC :
                # -- X for current year --
                graph_x2.append(tyc['week_end_date'].encode("ascii"))   
                graph_y2.append(tyc['total_customers']/tyc['total_stores'])              
            
            # -- Manage this year comparison sales data in Y graph set
            if not TYCS_result:
                df_sale_TYC_VS_LYC = self.filter_till_weekend_sales(start_date,end_date,s_outlet_id,s_zone_id,analysis_level,department_id,commodity_id,secondary_type)
            else:
                df_sale_TYC_VS_LYC = TYCS_result

            # -- Append last year sales data in XY graph set
            for tyc in df_sale_TYC_VS_LYC :
                # -- Y for current year --           
                graph_y3.append(tyc['total_customers']/tyc['total_stores']) 

        graph_dictionary = []
        total_ty_sale_val = 0
        total_ly_sale_val = 0
        # Set marker name as combination of name and axis values
        for i in range(0,len(graph_x1)):
            # Prepare dictionary 
            myDict = {}
            # Handle exeption  
            try:
                pty_value = float(graph_y1[i])
            except IndexError:
                pty_value = 0
            try:
                ply_value = float(graph_y2[i])
            except IndexError:
                ply_value = 0
            try:
                sty_value = float(graph_y3[i])
            except IndexError:
                sty_value = 0
            # Set this and last year sales
            total_ty_sale_val = total_ty_sale_val+pty_value
            total_ly_sale_val = total_ly_sale_val+ply_value
            myDict["weeks"] = graph_x1[i]
            myDict["pty_value"] = round(pty_value, 2)
            myDict["ply_value"] = round(ply_value, 2)
            myDict["sty_value"] = round(sty_value, 2)
            graph_dictionary.append(myDict)
        
        res = [graph_x1, graph_y1, graph_y2, graph_y3, graph_x2, graph_dictionary,round(total_ty_sale_val),round(total_ly_sale_val)]
        return res        

    def __repr__(self):
        return '<Book %r>' % (self.title) 
