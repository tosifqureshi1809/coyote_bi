from flask import Flask, Response, g, request,session
from flask_caching import Cache
from flask_sqlalchemy import SQLAlchemy
import sqlalchemy
import pandas as pd
import numpy as np
import re
import base64
import json

# -- Important Assets STARTS --
from biapp.db import get_db,get_livedb,get_testdb
from biapp.app import server
from datetime import datetime, timedelta

app = Flask(__name__, instance_relative_config=True)
# -- Important Assets ENDS --
cache = Cache(server, config={'CACHE_TYPE': 'simple'})
timeout = 20

class storekpi():
   
    title = ""
    db = []

    @cache.memoize(timeout=timeout)
    def __init__(self):
        # Get session data
        environment = 'LIVE'
        if 'env_data' in session:
            env_data = session['env_data']
            # Get filter data; If exist
            if env_data.get("environment") != None:
                environment = env_data.get("environment")
        with server.app_context():        
            if 'db' not in g:
                self.db = get_db()
                g.db = self.db
            if 'live_db' not in g:
                if environment == 'LIVE' :
                    self.live_db = get_livedb()
                else :
                    self.live_db = get_testdb()    
                g.live_db = self.live_db       
    
    @cache.memoize(timeout=timeout)
    def pg1_store_kpi_report(self):
        
        # -- For Graph X-Axis, Y-Axis for Outlets. --
        temp_wty_y = {}
        temp_wly_y = {}
        temp_w2y_y = {}
        graph_dictionary = []
        # Get session filter data if exist
        session_filters = self.get_session_filters()
        # Set filter parameters
        product_type = session_filters['product_type']
        f_department = session_filters['f_department']
        f_commodity = session_filters['f_commodity']
        f_group = session_filters['f_group']
        f_supplier = session_filters['f_supplier']
        f_manufacturer = session_filters['f_manufacturer']
        f_replicate_code = session_filters['f_replicate_code']
        f_promotion_code = session_filters['f_promotion_code']
        f_plu = session_filters['f_plu']
        location_type = session_filters['location_type']
        f_outlets = session_filters['f_outlets']
        f_am_zones = session_filters['f_am_zones']
        f_demo_zones = session_filters['f_demo_zones']
        f_region_zones = session_filters['f_region_zones']
        TY_start_date = session_filters['TY_start_date']
        TY_end_date = session_filters['TY_end_date']
        f_sort_type = session_filters['f_sort_type']
        # Set filtered product key value
        tbl_product_type_child_val = f_department
        if product_type == '2' :  # Set data for commodity
            tbl_product_type_child_val = f_commodity
        if product_type == '3' :  # Set data for group
            tbl_product_type_child_val = f_group
        if product_type == '4' :  # Set data for product plu code
            tbl_product_type_child_val = f_plu    
        if product_type == '5' :  # Set data for replicate code
            tbl_product_type_child_val = f_replicate_code    
        if product_type == '6' :  # Set data for promotion
            tbl_product_type_child_val = f_promotion_code    
        if product_type == '7' :  # Set data for supplier
            tbl_product_type_child_val = f_supplier
        if product_type == '8' :  # Set data for manufacturer
            tbl_product_type_child_val = f_manufacturer        

        if (len(f_outlets) > 0 or len(f_am_zones) > 0 or len(f_demo_zones) > 0 or len(f_region_zones) > 0) and TY_start_date != '' : 
            # -- Set this year dates
            convt_startdate = datetime.strptime(TY_start_date, '%Y-%m-%d').date()
            current_year = int(convt_startdate.strftime('%Y'))
            current_month = convt_startdate.strftime('%m')
            current_day = convt_startdate.strftime('%d')

            convt_enddate = datetime.strptime(TY_end_date, '%Y-%m-%d').date()
            current_endmonth = convt_enddate.strftime('%m')
            current_endday = convt_enddate.strftime('%d')
            # -- Set last year dates
            prev_year = current_year-1
            convt_prev_startdate = datetime.strptime(str(prev_year)+'-'+current_month+'-'+current_day, '%Y-%m-%d').date()
            convt_prev_enddate = datetime.strptime(str(prev_year)+'-'+current_endmonth+'-'+current_endday, '%Y-%m-%d').date()

            LY_start_date = convt_prev_startdate.strftime('%Y-%m-%d')
            LY_end_date = convt_prev_enddate.strftime('%Y-%m-%d')
            # -- Set second last year dates
            prev_2lyear = current_year-2
            convt_2ly_startdate = datetime.strptime(str(prev_2lyear)+'-'+current_month+'-'+current_day, '%Y-%m-%d').date()
            convt_2ly_enddate = datetime.strptime(str(prev_2lyear)+'-'+current_endmonth+'-'+current_endday, '%Y-%m-%d').date()

            SLY_start_date = convt_2ly_startdate.strftime('%Y-%m-%d')
            SLY_end_date = convt_2ly_enddate.strftime('%Y-%m-%d')

            temp_wty_customers = {}
            temp_wly_customers = {}
            temp_w2y_customers = {}
            # Set outlet condition
            location_condition = ''
            if len(f_outlets) > 0 :
                for i in range(0,len(f_outlets)):
                    location_condition += " JNLH_OUTLET = "+str(f_outlets[i])+" "
                    if i != (len(f_outlets)-1) :
                        location_condition += " OR "

            elif len(f_am_zones) > 0 != '' :
                for i in range(0,len(f_am_zones)):
                    p_zone_id = str(f_am_zones[i])
                    # Get zone code
                    zonecode_query = "SELECT zone_code FROM zone  WHERE zone_id = '"+p_zone_id+"'"
                    zonecode_result = pd.read_sql_query(zonecode_query,con=self.db)
                    for v in zonecode_result.to_dict(orient="records"):
                        p_zone_id = v['zone_code']
                    location_condition += " JNLH_OUTLET IN ( select DISTINCT(CODE_KEY_NUM) from CODETBL where CODE_KEY_TYPE = 'ZONEOUTLET' AND CODE_KEY_ALP = '"+str(p_zone_id)+"' ) "
                    if i != (len(f_am_zones)-1) :
                        location_condition += " OR "

            elif len(f_demo_zones) > 0 != '' :
                for i in range(0,len(f_demo_zones)):
                    p_zone_id = str(f_demo_zones[i])
                    # Get zone code
                    zonecode_query = "SELECT zone_code FROM zone  WHERE zone_id = '"+p_zone_id+"'"
                    zonecode_result = pd.read_sql_query(zonecode_query,con=self.db)
                    for v in zonecode_result.to_dict(orient="records"):
                        p_zone_id = v['zone_code']
                    location_condition += " JNLH_OUTLET IN ( select DISTINCT(CODE_KEY_NUM) from CODETBL where CODE_KEY_TYPE = 'ZONEOUTLET' AND CODE_KEY_ALP = '"+str(p_zone_id)+"' ) "
                    if i != (len(f_demo_zones)-1) :
                        location_condition += " OR "

            elif len(f_region_zones) > 0 != '' :
                for i in range(0,len(f_region_zones)):
                    p_zone_id = str(f_region_zones[i])
                    # Get zone code
                    zonecode_query = "SELECT zone_code FROM zone  WHERE zone_id = '"+p_zone_id+"'"
                    zonecode_result = pd.read_sql_query(zonecode_query,con=self.db)
                    for v in zonecode_result.to_dict(orient="records"):
                        p_zone_id = v['zone_code']
                    location_condition += " JNLH_OUTLET IN ( select DISTINCT(CODE_KEY_NUM) from CODETBL where CODE_KEY_TYPE = 'ZONEOUTLET' AND CODE_KEY_ALP = '"+str(p_zone_id)+"' ) "
                    if i != (len(f_region_zones)-1) :
                        location_condition += " OR "

            # Get customers count for years
            outl_customers_data = self.do_customer_aggregate_calculation(TY_start_date,TY_end_date,LY_start_date,LY_end_date,SLY_start_date,SLY_end_date,location_condition,product_type,tbl_product_type_child_val)
            
            for v in outl_customers_data: 
                if (int(v['year']) == int(current_year)):
                    temp_wty_customers[v['outlet_id']]=v['customers']
                elif (int(v['year']) == int(prev_year)):
                    temp_wly_customers[v['outlet_id']]=v['customers']
                elif (int(v['year']) == int(prev_2lyear)):
                    temp_w2y_customers[v['outlet_id']]=v['customers']

             # Fetching sales records over the comparison years
            outl_sales_data = self.do_sales_aggregate_calculation(TY_start_date,TY_end_date,LY_start_date,LY_end_date,SLY_start_date,SLY_end_date,location_condition,product_type,tbl_product_type_child_val)
            
            for v in outl_sales_data:
                outlet_id = v['outlet_id']
                if (int(v['year']) == int(current_year)):
                    if outlet_id not in temp_wty_y:
                        temp_wty_y[outlet_id] = []
                    customers = temp_wty_customers[v['outlet_id']]
                    cpc_percent = np.divide(v['total_amount'],customers) 
                    temp_wty_y[outlet_id].append({'cpc_percent':cpc_percent,'outlet_id':v['outlet_id'],'outl_name':v['outl_name']})
                elif (int(v['year']) == int(prev_year)):
                    if outlet_id not in temp_wly_y:
                        temp_wly_y[outlet_id] = []
                    customers = temp_wly_customers[v['outlet_id']]
                    cpc_percent = np.divide(v['total_amount'],customers)
                    temp_wly_y[outlet_id].append({'cpc_percent':cpc_percent,'outlet_id':v['outlet_id'],'outl_name':v['outl_name']})
                elif (int(v['year']) == int(prev_2lyear)):
                    if outlet_id not in temp_w2y_y:
                        temp_w2y_y[outlet_id] = []
                    customers = temp_w2y_customers[v['outlet_id']]
                    cpc_percent = np.divide(v['total_amount'],customers)
                    temp_w2y_y[outlet_id].append({'cpc_percent':cpc_percent,'outlet_id':v['outlet_id'],'outl_name':v['outl_name']})

            # Set marker name as combination of name and axis values
            for v in temp_wty_y: 
                for i in temp_wty_y[v] :
                    # Handle exeption  
                    try:
                        wty_cpc_percent = np.multiply(i['cpc_percent'],100)
                    except IndexError:
                        wty_cpc_percent = 0
                    try:
                        if v in temp_wly_y :
                            wly_cpc_percent = np.multiply(temp_wly_y[v][0]['cpc_percent'],100)
                        else :
                            wly_cpc_percent = 0
                    except IndexError:
                        wly_cpc_percent = 0
                    try: 
                        if v in temp_w2y_y :
                            w2y_cpc_percent = np.multiply(temp_w2y_y[v][0]['cpc_percent'],100)
                        else :
                            w2y_cpc_percent = 0
                    except IndexError:
                        w2y_cpc_percent = 0
                    
                    # Prepare dictionary 
                    myDict = {}
                    myDict["outlets"] = i['outl_name'].encode("ascii")
                    myDict["wty_value"] = np.round(wty_cpc_percent, 2)
                    myDict["wly_value"] = np.round(wly_cpc_percent, 2)
                    myDict["w2y_value"] = np.round(w2y_cpc_percent, 2)
                    graph_dictionary.append(myDict)
            # Sort dictionary based on TY CPC    
            graph_dictionary = sorted(graph_dictionary, key=lambda dct: dct['wty_value'],reverse=f_sort_type)
        res = [graph_dictionary]
        return res

    #  --- Get filter data stored in session
    def get_session_filters(self):
        product_type = ''
        f_department = ''
        f_commodity = ''
        f_group = ''
        f_supplier = ''
        f_manufacturer = ''
        f_replicate_code = ''
        f_promotion_code = ''
        f_plu = ''
        location_type =''
        f_outlets = []
        f_am_zones = []
        f_demo_zones = []
        f_region_zones = []
        TY_start_date = ''
        TY_end_date = ''
        f_sort_type = True

        # Get session filter data
        if 'kpi_filter_data' in session:
            kpi_filter_data = session['kpi_filter_data']
            # Get filter data; If exist
            if kpi_filter_data.get("product_type") != None:
                product_type = kpi_filter_data.get("product_type")

            if kpi_filter_data.get("departments") != None and kpi_filter_data.get("departments") != '':
                f_department = kpi_filter_data.get("departments")

            if kpi_filter_data.get("commodity") != None and kpi_filter_data.get("commodity") != '':
                f_commodity = kpi_filter_data.get("commodity") 

            if kpi_filter_data.get("group") != None and kpi_filter_data.get("group") != '':
                f_group = kpi_filter_data.get("group") 

            if kpi_filter_data.get("supplier") != None and kpi_filter_data.get("supplier") != '':
                f_supplier = kpi_filter_data.get("supplier") 

            if kpi_filter_data.get("manufacturer") != None and kpi_filter_data.get("manufacturer") != '':
                f_manufacturer = kpi_filter_data.get("manufacturer")

            if kpi_filter_data.get("replicate_code") != None and kpi_filter_data.get("replicate_code") != '':
                f_replicate_code = kpi_filter_data.get("replicate_code")

            if kpi_filter_data.get("promotion_code") != None and kpi_filter_data.get("promotion_code") != '':
                f_promotion_code = kpi_filter_data.get("promotion_code")

            if kpi_filter_data.get("plu") != None and kpi_filter_data.get("plu") != '':
                f_plu = kpi_filter_data.get("plu")                           

            if kpi_filter_data.get("location_type") != None and kpi_filter_data.get("location_type") != '':
                location_type = kpi_filter_data.get("location_type")

            if kpi_filter_data.get("outlets") != None and kpi_filter_data.get("outlets") != '':
                # Set selected outlets 
                f_outlets_json = kpi_filter_data.get("outlets")     
                f_outlets_array = json.loads(f_outlets_json)
                for i in range(0,len(f_outlets_array)):
                    f_outlets.append(int(f_outlets_array[i].encode("ascii","replace")))

            if kpi_filter_data.get("am_zones") != None and kpi_filter_data.get("am_zones") != '':
                # Set selected am zones 
                f_am_zones_json = kpi_filter_data.get("am_zones")   
                f_am_zones_array = json.loads(f_am_zones_json)
                for i in range(0,len(f_am_zones_array)):
                    f_am_zones.append(int(f_am_zones_array[i].encode("ascii","replace")))

            if kpi_filter_data.get("demo_zones") != None and kpi_filter_data.get("demo_zones") != '':
                # Set selected demo zones 
                f_demo_zones_json = kpi_filter_data.get("demo_zones")   
                f_demo_zones_array = json.loads(f_demo_zones_json)
                for i in range(0,len(f_demo_zones_array)):
                    f_demo_zones.append(int(f_demo_zones_array[i].encode("ascii","replace")))

            if kpi_filter_data.get("region_zones") != None and kpi_filter_data.get("region_zones") != '':
                # Set selected region zones 
                f_region_zones_json = kpi_filter_data.get("region_zones")    
                f_region_zones_array = json.loads(f_region_zones_json)
                for i in range(0,len(f_region_zones_array)):
                    f_region_zones.append(int(f_region_zones_array[i].encode("ascii","replace")))       
                    
            if kpi_filter_data.get("start_date") != None and kpi_filter_data.get("end_date") != None:
                TY_start_date = kpi_filter_data.get("start_date")
                TY_end_date = kpi_filter_data.get("end_date")

            if kpi_filter_data.get("sort_type") != None:
                sort_type = kpi_filter_data.get("sort_type")
                if sort_type == '2' :
                    f_sort_type = False
        # Set filter params
        filters = {}
        filters['product_type'] = product_type
        filters['f_department'] = f_department
        filters['f_commodity']  = f_commodity
        filters['f_group']  = f_group
        filters['f_supplier']  = f_supplier
        filters['f_manufacturer']  = f_manufacturer
        filters['f_replicate_code']  = f_replicate_code
        filters['f_promotion_code']  = f_promotion_code
        filters['f_plu']  = f_plu
        filters['location_type'] = location_type
        filters['f_outlets'] = f_outlets
        filters['f_am_zones'] = f_am_zones
        filters['f_demo_zones'] = f_demo_zones
        filters['f_region_zones'] = f_region_zones
        filters['TY_start_date'] = TY_start_date
        filters['TY_end_date'] = TY_end_date
        filters['f_sort_type'] = f_sort_type       
        return filters


    #  --- Check if the int given year is a leap year: return true if leap year or false otherwise
    @cache.memoize(timeout=timeout)
    def is_leap_year(self,year):
        leap_year_days = 366
        non_leap_year_days = 365
        if (year % 4) == 0:
            if (year % 100) == 0:
                if (year % 400) == 0:
                    return leap_year_days
                else:
                    return non_leap_year_days
            else:
                return leap_year_days
        else:
            return non_leap_year_days    

    # --- Function used to manage primary filter sales
    def do_sales_aggregate_calculation(self,TY_start_date,TY_end_date,LY_start_date,LY_end_date,SLY_start_date,SLY_end_date,location_condition,product_type='1',product_key_value='') :

        # Prepare query for this year financial sales query
        ty_sales_query = "SELECT JNLH_OUTLET,OUTL_DESC, JNLD_AMT, PROD_COMMODITY, PROD_CATEGORY, PROD_DEPARTMENT,YEAR(JNLH_TRADING_DATE) AS JNLH_YEAR FROM JNLHTBL JOIN JNLDTBL ON JNLD_YYYYMMDD = JNLH_YYYYMMDD AND JNLD_HHMMSS = JNLH_HHMMSS AND JNLD_OUTLET = JNLH_OUTLET AND JNLD_TILL = JNLH_TILL AND JNLD_TRX_NO = JNLH_TRX_NO LEFT JOIN PRODTBL ON PROD_NUMBER = JNLD_PRODUCT "
        if product_type == '1' :
            ty_sales_query += " LEFT JOIN CODETBL ON CODE_KEY_NUM = PROD_DEPARTMENT AND CODE_KEY_TYPE = 'DEPARTMENT'"
        elif product_type == '2' :
            ty_sales_query += " LEFT JOIN CODETBL ON CODE_KEY_NUM = PROD_COMMODITY AND CODE_KEY_TYPE = 'COMMODITY'"
        elif product_type == '3' :
            ty_sales_query += " LEFT JOIN CODETBL ON CODE_KEY_NUM = PROD_GROUP AND CODE_KEY_TYPE = 'GROUP'"
        elif product_type == '7' :
            ty_sales_query += " LEFT JOIN CODETBL ON CODE_KEY_ALP = PROD_SUPPLIER AND CODE_KEY_TYPE = 'SUPPLIER'"    
        elif product_type == '8' :
            ty_sales_query += " LEFT JOIN CODETBL ON CODE_KEY_ALP = PROD_MANUFACTURER AND CODE_KEY_TYPE = 'MANUFACTURER'"

        ty_sales_query += " LEFT JOIN OUTPTBL ON OUTP_PRODUCT = JNLD_PRODUCT AND OUTP_OUTLET = JNLD_OUTLET LEFT JOIN OUTLTBL ON OUTL_OUTLET = JNLH_OUTLET WHERE  ("+location_condition+") AND "
        ty_sales_query += "(JNLH_TRADING_DATE BETWEEN '"+TY_start_date+"' AND '"+TY_end_date+"') AND JNLD_TYPE = 'SALE' AND JNLD_STATUS = 1 "
        if product_type == '1' :
            ty_sales_query += " AND ( PROD_DEPARTMENT = '"+product_key_value+"' )"
        elif product_type == '2' :
            ty_sales_query += " AND ( PROD_COMMODITY = '"+product_key_value+"' )"   
        elif product_type == '3' :     
            ty_sales_query += " AND ( PROD_GROUP = '"+product_key_value+"' )"
        elif product_type == '4' :     
            ty_sales_query += " AND ( PROD_NUMBER = '"+product_key_value+"' )"
        elif product_type == '5' :     
            ty_sales_query += " AND ( PROD_REPLICATE = '"+product_key_value+"' ) AND PROD_REPLICATE <> '' "
        elif product_type == '6' :     
            ty_sales_query += " AND ( (JNLD_OFFER = '"+product_key_value+"') OR (JNLD_MIXMATCH = '"+product_key_value+"') ) AND JNLD_DISC_AMT <> 0 AND (JNLD_MIXMATCH <> '' OR JNLD_OFFER <> '') "          
        ty_sales_query += " ORDER BY JNLH_OUTLET,JNLH_YYYYMMDD, JNLH_HHMMSS"
       
        ty_df_sales_result = pd.read_sql_query(ty_sales_query,con=self.live_db)
        ty_df_sales_result=ty_df_sales_result.groupby(['JNLH_OUTLET'])
        # Manage this year sales aggragate data
        ty_sales_array = self.manage_yearly_aggregate_sales(ty_df_sales_result,product_type)
        
        # Prepare query for last year financial sales query
        ly_sales_query = "SELECT JNLH_OUTLET,OUTL_DESC, JNLD_AMT, PROD_COMMODITY, PROD_CATEGORY, PROD_DEPARTMENT,YEAR(JNLH_TRADING_DATE) AS JNLH_YEAR FROM JNLHTBL JOIN JNLDTBL ON JNLD_YYYYMMDD = JNLH_YYYYMMDD AND JNLD_HHMMSS = JNLH_HHMMSS AND JNLD_OUTLET = JNLH_OUTLET AND JNLD_TILL = JNLH_TILL AND JNLD_TRX_NO = JNLH_TRX_NO LEFT JOIN PRODTBL ON PROD_NUMBER = JNLD_PRODUCT "
        if product_type == '1' :
            ly_sales_query += " LEFT JOIN CODETBL ON CODE_KEY_NUM = PROD_DEPARTMENT AND CODE_KEY_TYPE = 'DEPARTMENT'"
        elif product_type == '2' :
            ly_sales_query += " LEFT JOIN CODETBL ON CODE_KEY_NUM = PROD_COMMODITY AND CODE_KEY_TYPE = 'COMMODITY'"
        elif product_type == '3' :
            ly_sales_query += " LEFT JOIN CODETBL ON CODE_KEY_NUM = PROD_GROUP AND CODE_KEY_TYPE = 'GROUP'"
        elif product_type == '7' :
            ly_sales_query += " LEFT JOIN CODETBL ON CODE_KEY_ALP = PROD_SUPPLIER AND CODE_KEY_TYPE = 'SUPPLIER'"
        elif product_type == '8' :
            ly_sales_query += " LEFT JOIN CODETBL ON CODE_KEY_ALP = PROD_MANUFACTURER AND CODE_KEY_TYPE = 'MANUFACTURER'"

        ly_sales_query += " LEFT JOIN OUTPTBL ON OUTP_PRODUCT = JNLD_PRODUCT AND OUTP_OUTLET = JNLD_OUTLET LEFT JOIN OUTLTBL ON OUTL_OUTLET = JNLH_OUTLET WHERE  ("+location_condition+") AND "
        ly_sales_query += "(JNLH_TRADING_DATE BETWEEN '"+LY_start_date+"' AND '"+LY_end_date+"') AND JNLD_TYPE = 'SALE' AND JNLD_STATUS = 1 "
        if product_type == '1' :
            ly_sales_query += " AND ( PROD_DEPARTMENT = '"+product_key_value+"' )"
        elif product_type == '2' :
            ly_sales_query += " AND ( PROD_COMMODITY = '"+product_key_value+"' )"
        elif product_type == '3' :     
            ly_sales_query += " AND ( PROD_GROUP = '"+product_key_value+"' )"
        elif product_type == '4' :     
            ly_sales_query += " AND ( PROD_NUMBER = '"+product_key_value+"' )"
        elif product_type == '5' :     
            ly_sales_query += " AND ( PROD_REPLICATE = '"+product_key_value+"' ) AND PROD_REPLICATE <> '' "
        elif product_type == '6' :     
            ly_sales_query += " AND ( (JNLD_OFFER = '"+product_key_value+"') OR (JNLD_MIXMATCH = '"+product_key_value+"') ) AND JNLD_DISC_AMT <> 0 AND (JNLD_MIXMATCH <> '' OR JNLD_OFFER <> '') "    
        ly_sales_query += " ORDER BY JNLH_OUTLET,JNLH_YYYYMMDD, JNLH_HHMMSS"
        
        ly_df_sales_result = pd.read_sql_query(ly_sales_query,con=self.live_db)
        ly_df_sales_result=ly_df_sales_result.groupby(['JNLH_OUTLET'])
        # Manage last year sales aggragate data
        ly_sales_array = self.manage_yearly_aggregate_sales(ly_df_sales_result,product_type)
        
        # Prepare query for second last year financial sales query
        sly_sales_query = "SELECT JNLH_OUTLET,OUTL_DESC, JNLD_AMT, PROD_COMMODITY, PROD_CATEGORY, PROD_DEPARTMENT,YEAR(JNLH_TRADING_DATE) AS JNLH_YEAR FROM JNLHTBL JOIN JNLDTBL ON JNLD_YYYYMMDD = JNLH_YYYYMMDD AND JNLD_HHMMSS = JNLH_HHMMSS AND JNLD_OUTLET = JNLH_OUTLET AND JNLD_TILL = JNLH_TILL AND JNLD_TRX_NO = JNLH_TRX_NO LEFT JOIN PRODTBL ON PROD_NUMBER = JNLD_PRODUCT  "
        if product_type == '1' :
            sly_sales_query += " LEFT JOIN CODETBL ON CODE_KEY_NUM = PROD_DEPARTMENT AND CODE_KEY_TYPE = 'DEPARTMENT'"
        elif product_type == '2' :
            sly_sales_query += " LEFT JOIN CODETBL ON CODE_KEY_NUM = PROD_COMMODITY AND CODE_KEY_TYPE = 'COMMODITY'"
        elif product_type == '3' :
            sly_sales_query += " LEFT JOIN CODETBL ON CODE_KEY_NUM = PROD_GROUP AND CODE_KEY_TYPE = 'GROUP'"
        elif product_type == '7' :
            sly_sales_query += " LEFT JOIN CODETBL ON CODE_KEY_ALP = PROD_SUPPLIER AND CODE_KEY_TYPE = 'SUPPLIER'"    
        elif product_type == '8' :
            sly_sales_query += " LEFT JOIN CODETBL ON CODE_KEY_ALP = PROD_MANUFACTURER AND CODE_KEY_TYPE = 'MANUFACTURER'"
        sly_sales_query += " LEFT JOIN OUTPTBL ON OUTP_PRODUCT = JNLD_PRODUCT AND OUTP_OUTLET = JNLD_OUTLET LEFT JOIN OUTLTBL ON OUTL_OUTLET = JNLH_OUTLET WHERE  ("+location_condition+") AND "
        sly_sales_query += "(JNLH_TRADING_DATE BETWEEN '"+SLY_start_date+"' AND '"+SLY_end_date+"') AND JNLD_TYPE = 'SALE' AND JNLD_STATUS = 1 "
        if product_type == '1' :
            sly_sales_query += " AND ( PROD_DEPARTMENT = '"+product_key_value+"' )"
        elif product_type == '2' :
            sly_sales_query += " AND ( PROD_COMMODITY = '"+product_key_value+"' )"    
        elif product_type == '3' :     
            sly_sales_query += " AND ( PROD_GROUP = '"+product_key_value+"' )"
        elif product_type == '4' :     
            sly_sales_query += " AND ( PROD_NUMBER = '"+product_key_value+"' )"
        elif product_type == '5' :     
            sly_sales_query += " AND ( PROD_REPLICATE = '"+product_key_value+"' ) AND PROD_REPLICATE <> '' "
        elif product_type == '6' :     
            sly_sales_query += " AND ( (JNLD_OFFER = '"+product_key_value+"') OR (JNLD_MIXMATCH = '"+product_key_value+"') ) AND JNLD_DISC_AMT <> 0 AND (JNLD_MIXMATCH <> '' OR JNLD_OFFER <> '') "            
        sly_sales_query += " ORDER BY JNLH_OUTLET,JNLH_YYYYMMDD, JNLH_HHMMSS"

        sly_df_sales_result = pd.read_sql_query(sly_sales_query,con=self.live_db)
        sly_df_sales_result=sly_df_sales_result.groupby(['JNLH_OUTLET'])
        # Manage second last year sales aggragate data
        sly_sales_array = self.manage_yearly_aggregate_sales(sly_df_sales_result,product_type)
        
        final_array = []
        # Append this year sales in final sales array
        for k in ty_sales_array :
            for v in k :
                for j in k[v] :
                    # append this year department sales value in array
                    final_array.append({'outlet_id':j['vsf_outl_number'],'outl_name':j['vsf_outl_name'],'total_amount':j['vsf_dep_sales_amt'],'year':j['year']})
        # Append thislast year sales in final sales array
        for k in ly_sales_array :
            for v in k :
                for j in k[v] :
                    # append this year department sales value in array
                    final_array.append({'outlet_id':j['vsf_outl_number'],'outl_name':j['vsf_outl_name'],'total_amount':j['vsf_dep_sales_amt'],'year':j['year']})
        # Append second last year sales in final sales array
        for k in sly_sales_array :
            for v in k :
                for j in k[v] :
                    # append this year department sales value in array
                    final_array.append({'outlet_id':j['vsf_outl_number'],'outl_name':j['vsf_outl_name'],'total_amount':j['vsf_dep_sales_amt'],'year':j['year']})                        
       
        return final_array

    # -- Function used to fetch customers count and prepare list based on years 
    def do_customer_aggregate_calculation(self,TY_start_date,TY_end_date,LY_start_date,LY_end_date,SLY_start_date,SLY_end_date,location_condition,product_type='1',product_key_value='') :
        
        customers_array = []
        # Prepare query for fething customers count for this years  
        outl_customers_sql = "SELECT JNLH_OUTLET,JNLH_TRX_NO,JNLH_TYPE, JNLH_STATUS,JNLD_TYPE, JNLD_STATUS,OUTL_DESC, JNLD_AMT, PROD_COMMODITY, PROD_CATEGORY, PROD_DEPARTMENT,YEAR(JNLH_TRADING_DATE) AS JNLH_YEAR FROM JNLHTBL JOIN JNLDTBL ON JNLD_YYYYMMDD = JNLH_YYYYMMDD AND JNLD_HHMMSS = JNLH_HHMMSS AND JNLD_OUTLET = JNLH_OUTLET AND JNLD_TILL = JNLH_TILL AND JNLD_TRX_NO = JNLH_TRX_NO LEFT JOIN PRODTBL ON PROD_NUMBER = JNLD_PRODUCT "
        if product_type == '1' :
            outl_customers_sql += " LEFT JOIN CODETBL ON CODE_KEY_NUM = PROD_DEPARTMENT AND CODE_KEY_TYPE = 'DEPARTMENT'"
        elif product_type == '2' :
            outl_customers_sql += " LEFT JOIN CODETBL ON CODE_KEY_NUM = PROD_COMMODITY AND CODE_KEY_TYPE = 'COMMODITY'"
        elif product_type == '3' :
            outl_customers_sql += " LEFT JOIN CODETBL ON CODE_KEY_NUM = PROD_GROUP AND CODE_KEY_TYPE = 'GROUP'"
        elif product_type == '7' :
            outl_customers_sql += " LEFT JOIN CODETBL ON CODE_KEY_ALP = PROD_SUPPLIER AND CODE_KEY_TYPE = 'SUPPLIER'"
        elif product_type == '8' :
            outl_customers_sql += " LEFT JOIN CODETBL ON CODE_KEY_ALP = PROD_MANUFACTURER AND CODE_KEY_TYPE = 'MANUFACTURER'"
        outl_customers_sql += " LEFT JOIN OUTPTBL ON OUTP_PRODUCT = JNLD_PRODUCT AND OUTP_OUTLET = JNLD_OUTLET LEFT JOIN OUTLTBL ON OUTL_OUTLET = JNLH_OUTLET WHERE  ("+location_condition+") AND "
        outl_customers_sql += "(JNLH_TRADING_DATE BETWEEN '"+TY_start_date+"' AND '"+TY_end_date+"') AND JNLD_TYPE = 'SALE' "
        if product_type == '5' :
            outl_customers_sql += " AND PROD_REPLICATE <> '' "
        elif product_type == '6' :
            outl_customers_sql += " AND JNLD_DISC_AMT <> 0 AND (JNLD_MIXMATCH <> '' OR JNLD_OFFER <> '') "
        outl_customers_sql += " ORDER BY JNLH_OUTLET,JNLH_YYYYMMDD, JNLH_HHMMSS"
        outl_customers_result = pd.read_sql_query(outl_customers_sql,con=self.live_db)
        outl_customers_result = outl_customers_result.groupby(['JNLH_OUTLET'])
        # Manage customers sales aggragate data
        customers_count_array = self.manage_customers_aggregate_count(outl_customers_result)
        
        # Append year wise customers data in array
        for k in customers_count_array :
            for v in k :
                for j in k[v] :
                    # append this year department sales value in array
                    customers_array.append({'outlet_id':j['vsf_outl_number'],'customers':j['vsf_dep_customers_count'],'year':j['year']})

         # Prepare query for fething customers count for last year  
        outl_customers_sql = "SELECT JNLH_OUTLET,JNLH_TRX_NO,JNLH_TYPE,JNLH_STATUS,JNLD_TYPE,JNLD_STATUS,OUTL_DESC, JNLD_AMT, PROD_COMMODITY, PROD_CATEGORY, PROD_DEPARTMENT,YEAR(JNLH_TRADING_DATE) AS JNLH_YEAR FROM JNLHTBL JOIN JNLDTBL ON JNLD_YYYYMMDD = JNLH_YYYYMMDD AND JNLD_HHMMSS = JNLH_HHMMSS AND JNLD_OUTLET = JNLH_OUTLET AND JNLD_TILL = JNLH_TILL AND JNLD_TRX_NO = JNLH_TRX_NO LEFT JOIN PRODTBL ON PROD_NUMBER = JNLD_PRODUCT "
        if product_type == '1' :
            outl_customers_sql += " LEFT JOIN CODETBL ON CODE_KEY_NUM = PROD_DEPARTMENT AND CODE_KEY_TYPE = 'DEPARTMENT'"
        elif product_type == '2' :
            outl_customers_sql += " LEFT JOIN CODETBL ON CODE_KEY_NUM = PROD_COMMODITY AND CODE_KEY_TYPE = 'COMMODITY'"
        elif product_type == '3' :
            outl_customers_sql += " LEFT JOIN CODETBL ON CODE_KEY_NUM = PROD_GROUP AND CODE_KEY_TYPE = 'GROUP'"
        elif product_type == '7' :
            outl_customers_sql += " LEFT JOIN CODETBL ON CODE_KEY_ALP = PROD_SUPPLIER AND CODE_KEY_TYPE = 'SUPPLIER'"
        elif product_type == '8' :
            outl_customers_sql += " LEFT JOIN CODETBL ON CODE_KEY_ALP = PROD_MANUFACTURER AND CODE_KEY_TYPE = 'MANUFACTURER'"
        outl_customers_sql += " LEFT JOIN OUTPTBL ON OUTP_PRODUCT = JNLD_PRODUCT AND OUTP_OUTLET = JNLD_OUTLET LEFT JOIN OUTLTBL ON OUTL_OUTLET = JNLH_OUTLET WHERE  ("+location_condition+") AND "
        outl_customers_sql += "(JNLH_TRADING_DATE BETWEEN '"+LY_start_date+"' AND '"+LY_end_date+"') AND JNLD_TYPE = 'SALE' "
        if product_type == '6' :
            outl_customers_sql += " AND JNLD_DISC_AMT <> 0 AND (JNLD_MIXMATCH <> '' OR JNLD_OFFER <> '') "
        outl_customers_sql += " ORDER BY JNLH_OUTLET,JNLH_YYYYMMDD, JNLH_HHMMSS"
       
        outl_customers_result = pd.read_sql_query(outl_customers_sql,con=self.live_db)
        outl_customers_result = outl_customers_result.groupby(['JNLH_OUTLET'])
        # Manage customers sales aggragate data
        customers_count_array = self.manage_customers_aggregate_count(outl_customers_result)
        
        # Append year wise customers data in array
        for k in customers_count_array :
            for v in k :
                for j in k[v] :
                    # append this year department sales value in array
                    customers_array.append({'outlet_id':j['vsf_outl_number'],'customers':j['vsf_dep_customers_count'],'year':j['year']})

         # Prepare query for fething customers count for second last year  
        outl_customers_sql = "SELECT JNLH_OUTLET,JNLH_TRX_NO,JNLH_TYPE,JNLH_STATUS,JNLD_TYPE,JNLD_STATUS,OUTL_DESC, JNLD_AMT, PROD_COMMODITY, PROD_CATEGORY, PROD_DEPARTMENT,YEAR(JNLH_TRADING_DATE) AS JNLH_YEAR FROM JNLHTBL JOIN JNLDTBL ON JNLD_YYYYMMDD = JNLH_YYYYMMDD AND JNLD_HHMMSS = JNLH_HHMMSS AND JNLD_OUTLET = JNLH_OUTLET AND JNLD_TILL = JNLH_TILL AND JNLD_TRX_NO = JNLH_TRX_NO LEFT JOIN PRODTBL ON PROD_NUMBER = JNLD_PRODUCT "
        if product_type == '1' :
            outl_customers_sql += " LEFT JOIN CODETBL ON CODE_KEY_NUM = PROD_DEPARTMENT AND CODE_KEY_TYPE = 'DEPARTMENT'"
        elif product_type == '2' :
            outl_customers_sql += " LEFT JOIN CODETBL ON CODE_KEY_NUM = PROD_COMMODITY AND CODE_KEY_TYPE = 'COMMODITY'"
        elif product_type == '3' :
            outl_customers_sql += " LEFT JOIN CODETBL ON CODE_KEY_NUM = PROD_GROUP AND CODE_KEY_TYPE = 'GROUP'"
        elif product_type == '7' :
            outl_customers_sql += " LEFT JOIN CODETBL ON CODE_KEY_ALP = PROD_SUPPLIER AND CODE_KEY_TYPE = 'SUPPLIER'"
        elif product_type == '8' :
            outl_customers_sql += " LEFT JOIN CODETBL ON CODE_KEY_ALP = PROD_MANUFACTURER AND CODE_KEY_TYPE = 'MANUFACTURER'"
        outl_customers_sql += " LEFT JOIN OUTPTBL ON OUTP_PRODUCT = JNLD_PRODUCT AND OUTP_OUTLET = JNLD_OUTLET LEFT JOIN OUTLTBL ON OUTL_OUTLET = JNLH_OUTLET WHERE  ("+location_condition+") AND "
        outl_customers_sql += "(JNLH_TRADING_DATE BETWEEN '"+SLY_start_date+"' AND '"+SLY_end_date+"') AND JNLD_TYPE = 'SALE' "
        if product_type == '6' :
            outl_customers_sql += " AND JNLD_DISC_AMT <> 0 AND (JNLD_MIXMATCH <> '' OR JNLD_OFFER <> '') "
        outl_customers_sql += " ORDER BY JNLH_OUTLET,JNLH_YYYYMMDD, JNLH_HHMMSS"
        outl_customers_result = pd.read_sql_query(outl_customers_sql,con=self.live_db)
        outl_customers_result = outl_customers_result.groupby(['JNLH_OUTLET'])
        # Manage customers sales aggragate data
        customers_count_array = self.manage_customers_aggregate_count(outl_customers_result)
        
        # Append year wise customers data in array
        for k in customers_count_array :
            for v in k :
                for j in k[v] :
                    # append this year department sales value in array
                    customers_array.append({'outlet_id':j['vsf_outl_number'],'customers':j['vsf_dep_customers_count'],'year':j['year']})                 

        return customers_array

    # Manage customers year wise count record
    def manage_customers_aggregate_count(self,df_sales_result):
       
        # initialize department product sales array
        customer_array = {}
        for k, v in df_sales_result:
            temp_group_split=v.to_dict(orient="records")
            for v in temp_group_split:
                # set year value
                JNLH_OUTLET = v['JNLH_OUTLET']
                # append product sales data in department index
                if JNLH_OUTLET not in customer_array:
                    customer_array[JNLH_OUTLET] = []
                # prepare journal values for sales array department wise  
                customer_array[JNLH_OUTLET].append({'JNLH_OUTLET':v['JNLH_OUTLET'],'JNLH_TRX_NO':v['JNLH_TRX_NO'],'JNLH_STATUS':v['JNLH_STATUS'],'JNLH_TYPE':v['JNLH_TYPE'],'OUTL_DESC':v['OUTL_DESC'],'JNLD_AMT':v['JNLD_AMT'],'JNLD_STATUS':v['JNLD_STATUS'],'JNLD_TYPE':v['JNLD_TYPE'],'PROD_DEPARTMENT':v['PROD_DEPARTMENT'],'PROD_COMMODITY':v['PROD_COMMODITY'],'YEAR':v['JNLH_YEAR']})
            
        department_sales_array = []
        # prepare department sales report result
        for i in customer_array:
            # initialize default values
            vsf_dep_number = ''
            vsf_dep_name = ''
            vsf_dep_amt = 0
            vsf_dep_customers_count = 0
            last_trx_no = 0
            year = ''
            dept_data = []
            trx_numbers = []
            for v in customer_array[i] :
                #if i != '' :
                if v['JNLD_STATUS'] == 1 and v['JNLD_TYPE'] == 'SALE' :
                    # add sales values
                    vsf_dep_number	= v['JNLH_OUTLET']
                    year = v['YEAR']
                
                # set total customers
                if v['JNLH_STATUS'] == 1 and v['JNLH_TYPE'] == 'SALE' :
                    if v['JNLH_TRX_NO'] not in trx_numbers :
                        trx_numbers.append(v['JNLH_TRX_NO'])
                        # add customer count
                        if last_trx_no != v['JNLH_TRX_NO'] :
                            vsf_dep_customers_count = vsf_dep_customers_count+1
        
                        # set last trx numbers
                        last_trx_no = v['JNLH_TRX_NO'];

            # append sales value in array
            dept_data.append({'vsf_outl_number':vsf_dep_number,'vsf_dep_customers_count':vsf_dep_customers_count,'vsf_dep_number':vsf_dep_number,'year':year})
            # append result in department array
            department_sales_array.append({i:dept_data})
                
        return department_sales_array 

    # Manage yearly sales record
    def manage_yearly_aggregate_sales(self,df_sales_result,analysis_level):
       
        # initialize department product sales array
        department_array = {}
        for k, v in df_sales_result:
            temp_group_split=v.to_dict(orient="records")
            for v in temp_group_split:
                # set department label
                JNLH_OUTLET = v['JNLH_OUTLET']
                year = v['JNLH_YEAR']
                # append product sales data in department index
                if JNLH_OUTLET not in department_array:
                    department_array[JNLH_OUTLET] = []
                
                # prepare journal values for sales array department wise  
                department_array[JNLH_OUTLET].append({'JNLH_OUTLET':v['JNLH_OUTLET'],'OUTL_DESC':v['OUTL_DESC'],'JNLD_AMT':v['JNLD_AMT'],'PROD_DEPARTMENT':v['PROD_DEPARTMENT'],'PROD_COMMODITY':v['PROD_COMMODITY'],'YEAR':v['JNLH_YEAR']})
               
        department_sales_array = []
        # prepare department sales report result
        for i in department_array:
        
            # initialize default values
            vsf_outl_number = ''
            vsf_outl_name = ''
            vsf_dep_amt = 0
            year = ''
            dept_data = []
            for v in department_array[i] :
                if i != '' :
                    # add sales values
                    vsf_dep_amt 		= vsf_dep_amt+v['JNLD_AMT']
                    vsf_outl_number	    = v['JNLH_OUTLET']    
                    vsf_outl_name       = v['OUTL_DESC']
                    year                = v['YEAR']

            # append sales value in array
            dept_data.append({'vsf_dep_sales_amt':vsf_dep_amt,'vsf_outl_number':vsf_outl_number,'vsf_outl_name':vsf_outl_name,'year':year})
            # append result in department array
            department_sales_array.append({i:dept_data})
                
        return department_sales_array   

    def testdata(self) :
         # Prepare query for last year financial sales query
        outl_customers_sql = "SELECT JNLH_OUTLET,JNLH_TRX_NO,JNLH_TYPE,JNLH_STATUS,JNLD_TYPE,JNLD_STATUS,OUTL_DESC, JNLD_AMT, PROD_COMMODITY, PROD_CATEGORY, PROD_DEPARTMENT,YEAR(JNLH_TRADING_DATE) AS JNLH_YEAR FROM JNLHTBL JOIN JNLDTBL ON JNLD_YYYYMMDD = JNLH_YYYYMMDD AND JNLD_HHMMSS = JNLH_HHMMSS AND JNLD_OUTLET = JNLH_OUTLET AND JNLD_TILL = JNLH_TILL AND JNLD_TRX_NO = JNLH_TRX_NO LEFT JOIN PRODTBL ON PROD_NUMBER = JNLD_PRODUCT  LEFT JOIN CODETBL ON CODE_KEY_NUM = PROD_COMMODITY AND CODE_KEY_TYPE = 'COMMODITY' LEFT JOIN OUTPTBL ON OUTP_PRODUCT = JNLD_PRODUCT AND OUTP_OUTLET = JNLD_OUTLET LEFT JOIN OUTLTBL ON OUTL_OUTLET = JNLH_OUTLET WHERE  ( JNLH_OUTLET IN ( select DISTINCT(CODE_KEY_NUM) from CODETBL where CODE_KEY_TYPE = 'ZONEOUTLET' AND CODE_KEY_ALP = 'INNER' ) ) AND (JNLH_TRADING_DATE BETWEEN '2018-02-03' AND '2018-02-04') AND JNLD_TYPE = 'SALE'  ORDER BY JNLH_OUTLET,JNLH_YYYYMMDD, JNLH_HHMMSS"

        outl_customers_result = pd.read_sql_query(outl_customers_sql,con=self.live_db)
        outl_customers_result = outl_customers_result.groupby(['JNLH_OUTLET'])
        # Manage customers sales aggragate data
        customers_count_array = self.manage_customers_aggregate_count(outl_customers_result)
        return customers_count_array

    def __repr__(self):
        return '<Book %r>' % (self.title) 
    