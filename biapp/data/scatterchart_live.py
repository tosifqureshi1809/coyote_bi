from flask import Flask, Response, g, request,session
from flask_caching import Cache
from flask_sqlalchemy import SQLAlchemy
import json
import sqlalchemy
import pandas as pd
import numpy as np
import re
import base64

# -- Important Assets STARTS --
from biapp.db import get_db,get_livedb,get_testdb
from biapp.app import server
from datetime import datetime, timedelta

app = Flask(__name__, instance_relative_config=True)
# -- Important Assets ENDS --
cache = Cache(server, config={'CACHE_TYPE': 'simple'})
timeout = 20

class scatterchart():
   
    title = ""
    db = []
    live_db = []

    @cache.memoize(timeout=timeout)
    def __init__(self):
        # Get session data
        environment = 'LIVE'
        if 'env_data' in session:
            env_data = session['env_data']
            # Get filter data; If exist
            if env_data.get("environment") != None:
                environment = env_data.get("environment")
        with server.app_context():        
            if 'db' not in g:
                self.db = get_db()
                g.db = self.db
            if 'live_db' not in g:
                if environment == 'LIVE' :
                    self.live_db = get_livedb()
                else :
                    self.live_db = get_testdb()  
                g.live_db = self.live_db    
    
    @cache.memoize(timeout=timeout)
    def pg1_scatterchart_20_20_Report(self):
        
        temp_Sale_TYS_VS_LYS_X={
        "x" : [],
        "y" : []
        }
        # -- For X-Axis for current Years. --
        temp_Sale_TYS_VS_LYS_Y=[]
        # -- For Graph X-Axis, Y-Axis, Custome Data for current Years. --
        graph_x = []
        graph_y = []
        graph_text = []
        graph_customdata = []
        graph_bubble_size = []
        total_store_sale=0
        product_query = ''
        total_amount_x = 0
        total_amount_y = 0
        # Get drilldown level filter data; If exist
        analysis_level = 1
        analysis_index_id = ''
        department_name = ''
        analysis_chart_label = '20-20 Report'
        if request.args.get('level') is not None:
            analysis_level = request.args.get('level')
        if request.args.get('id') is not None:    
            analysis_index_id = request.args.get('id')
        if request.args.get('label') is not None:    
            analysis_chart_label = request.args.get('label')
            analysis_chart_label = base64.b64decode(analysis_chart_label)
        if request.args.get('department_name') is not None:    
            department_name = request.args.get('department_name')
            department_name = base64.b64decode(department_name) 

        # Get session filter data
        if 'filter_data' in session:
            filter_data = session['filter_data']
        # Set filter params
        p_outlet_id = ''
        p_zone_id = ''
        s_outlet_id = ''
        s_zone_id = ''
        start_date = ''
        end_date = ''
        comparison_period = 1
        measurement_type = '1'
        if filter_data.get("p_outlet_id") != None and filter_data.get("p_outlet_id") != '':
            p_outlet_id = filter_data.get("p_outlet_id")
        
        if filter_data.get("p_zone_id") != None and filter_data.get("p_zone_id") != '':
            p_zone_id = filter_data.get("p_zone_id")
            # Get zone code
            zonecode_query = "SELECT zone_code FROM zone  WHERE zone_id = '"+p_zone_id+"'"
            zonecode_result = pd.read_sql_query(zonecode_query,con=self.db)
            for v in zonecode_result.to_dict(orient="records"):
               p_zone_id = v['zone_code']

        if filter_data.get("s_outlet_id") != None and filter_data.get("s_outlet_id") != '':
            s_outlet_id = filter_data.get("s_outlet_id")
        
        if filter_data.get("s_zone_id") != None and filter_data.get("s_zone_id") != '':
            s_zone_id = filter_data.get("s_zone_id")
            # Get zone code
            zonecode_query = "SELECT zone_code FROM zone  WHERE zone_id = '"+s_zone_id+"'"
            zonecode_result = pd.read_sql_query(zonecode_query,con=self.db)
            for v in zonecode_result.to_dict(orient="records"):
               s_zone_id = v['zone_code']
        
        if filter_data.get("start_date") != None and filter_data.get("start_date") != '' and filter_data.get("end_date") != None:
            start_date = filter_data.get("start_date")
            end_date = filter_data.get("end_date")

        if filter_data.get("comparison_period") != None and filter_data.get("comparison_period") != '':
            comparison_period = int(filter_data.get("comparison_period"))

        if filter_data.get("measurement_type") != None and filter_data.get("measurement_type") != '':
            measurement_type = filter_data.get("measurement_type")    

        # return if date range empty
        if start_date == '' or end_date == '' :
            res = [graph_x, graph_y, graph_text, graph_customdata, graph_bubble_size]
            return res
        
        # -- Set dates
        convt_startdate = datetime.strptime(start_date, '%Y-%m-%d').date()
        current_year = int(convt_startdate.strftime('%Y'))
        current_month = convt_startdate.strftime('%m')
        current_day = convt_startdate.strftime('%d')

        convt_enddate = datetime.strptime(end_date, '%Y-%m-%d').date()
        current_endmonth = convt_enddate.strftime('%m')
        current_endday = convt_enddate.strftime('%d')
        prev_year = current_year-comparison_period
        year_days = self.is_leap_year(prev_year)-1
        convt_prev_startdate = datetime.strptime(str(prev_year)+'-'+current_month+'-'+current_day, '%Y-%m-%d').date()
        convt_prev_enddate = datetime.strptime(str(prev_year)+'-'+current_endmonth+'-'+current_endday, '%Y-%m-%d').date()
        convt_prev_startdate = convt_prev_startdate + timedelta(days=1)
        convt_prev_enddate = convt_prev_enddate + timedelta(days=1)
        convt_startdate.strftime('%Y-%m-%d')
        LY_start_date = convt_prev_startdate.strftime('%Y-%m-%d')
        convt_enddate = datetime.strptime(end_date, '%Y-%m-%d').date()
        LY_end_date = convt_prev_enddate.strftime('%Y-%m-%d')
        # Get product level sql query array
        p_zone_outl_sql = ''
        if analysis_level == '3' :
            # Get zone outlets and concatinate outlets in query
            if p_zone_id != '' :
                p_zone_outl_sql = self.zone_outl_query_condition(p_zone_id)

            product_query = self.prepare_product_query(p_outlet_id,p_zone_id,start_date,end_date,str(analysis_index_id),LY_start_date,LY_end_date,p_zone_outl_sql)
          
        # -- For X, Y of Y-Axis for selected 2 Years. --
        if(p_outlet_id != '' or p_zone_id != '') :

            if analysis_level == '3' :
                # Get this year's product sales
                df_sale_TYS_query = product_query['TY_p_sql']
                df_sale_TYS = pd.read_sql_query(df_sale_TYS_query,con=self.live_db)
                df_sale_TYS=df_sale_TYS.groupby(['title'])
                for k, v in df_sale_TYS:
                    temp_group_split=v.to_dict(orient="records")
                    for v in temp_group_split:
                        temp_Sale_TYS_VS_LYS_X["y"].append({'id':v['id'],'name':v['title'].encode("ascii"), 'total_promo_sale': v['total_promo_sale'],"total_discount":v['total_discount'], "total_qty":v['total_qty'], "total_cost":v['total_cost'], "gp_percent":v['gp_percent'], "total_margin":v['total_margin'], "total_amount":v['total_amount'],"total_customers":0, "trxt_year":''})
                        if measurement_type == '2' :
                            if v['gp_percent'] != None and v['gp_percent'] > 0:
                                total_store_sale = total_store_sale+float(v['gp_percent'])
                        elif measurement_type == '3' :
                            total_store_sale = 0      
                        else:
                            if v['total_amount'] != None and v['total_amount'] > 0:
                                total_store_sale = total_store_sale+v['total_amount']
                # Get last year's product sales
                df_sale_LYS_query = product_query['LY_p_sql']
                df_sale_LYS = pd.read_sql_query(df_sale_LYS_query,con=self.live_db)
                # Get => Y-Axis!!
                df_sale_LYS=df_sale_LYS.groupby(['title'])
                for k, v in df_sale_LYS:
                    temp_group_split=v.to_dict(orient="records")
                    for v in temp_group_split:
                        temp_Sale_TYS_VS_LYS_X["x"].append({'id':v['id'],'name':v['title'].encode("ascii"), 'total_promo_sale': v['total_promo_sale'],"total_discount":v['total_discount'], "total_qty":v['total_qty'], "total_cost":v['total_cost'], "gp_percent":v['gp_percent'], "total_margin":v['total_margin'], "total_amount":v['total_amount'],"total_customers":0, "trxt_year":''})            
            else :     
                if analysis_level == '2' :
                    # Manage commodity level primary sales records
                    df_sale_TYS_VS_LYS = self.primary_aggregate_calculation(start_date,end_date,LY_start_date,LY_end_date,current_year,prev_year,p_outlet_id,p_zone_id,analysis_level,analysis_index_id)
                else :
                    # Manage department level primary sales records
                    df_sale_TYS_VS_LYS = self.primary_aggregate_calculation(start_date,end_date,LY_start_date,LY_end_date,current_year,prev_year,p_outlet_id,p_zone_id,analysis_level)
                    
                for v in df_sale_TYS_VS_LYS:    
                    #temp_group_split=v.to_dict(orient="records")
                    c=0  # Help to Split Array by Years Filter.
                    #for v in temp_group_split:
                    #print(v['total_discount'])
                    if c == 0 and v["trxt_year"] == prev_year: # Replace this with year filter
                        temp_Sale_TYS_VS_LYS_X["x"].append({'id':v['id'],'name':v['title'].encode("ascii"), 'total_promo_sale': v['total_promo_sale'],"total_discount":v['total_discount'], "total_qty":v['total_qty'], "total_cost":v['total_cost'], "gp_percent":v['gp_percent'], "total_margin":v['total_margin'], "total_amount":v['total_amount'],"total_customers":v['total_customers'], "trxt_year":v['trxt_year']})     
                    elif c == 0 and v["trxt_year"] == current_year:   
                        temp_Sale_TYS_VS_LYS_X["y"].append({'id':v['id'],'name':v['title'].encode("ascii"), 'total_promo_sale': v['total_promo_sale'],"total_discount":v['total_discount'], "total_qty":v['total_qty'], "total_cost":v['total_cost'], "gp_percent":v['gp_percent'], "total_margin":v['total_margin'], "total_amount":v['total_amount'],"total_customers":v['total_customers'], "trxt_year":v['trxt_year']})
                        if measurement_type == '2' :
                            if v['gp_percent'] != None and v['gp_percent'] > 0:
                                total_store_sale = total_store_sale+float(v['gp_percent'])
                        elif measurement_type == '3' :
                            if v['total_customers'] != None and v['total_customers'] > 0:
                                total_store_sale = total_store_sale+float(v['total_customers'])  
                        else:
                            if v['total_amount'] != None and v['total_amount'] > 0:
                                total_store_sale = total_store_sale+v['total_amount']
                    elif c == 1 and v["trxt_year"] == prev_year:    
                        temp_Sale_TYS_VS_LYS_X["x"].append({'id':v['id'],'name':v['title'].encode("ascii"), "total_promo_sale": v['total_promo_sale'],"total_discount":v['total_discount'], "total_qty":v['total_qty'], "total_cost":v['total_cost'], "gp_percent":v['gp_percent'], "total_margin":v['total_margin'], "total_amount":v['total_amount'],"total_customers":v['total_customers'], "trxt_year":v['trxt_year']})     
                    elif c ==1 and v["trxt_year"] == current_year :         
                        temp_Sale_TYS_VS_LYS_X["y"].append({'id':v['id'],'name':v['title'].encode("ascii"), "total_promo_sale": v['total_promo_sale'],"total_discount":v['total_discount'], "total_qty":v['total_qty'], "total_cost":v['total_cost'], "gp_percent":v['gp_percent'], "total_margin":v['total_margin'], "total_amount":v['total_amount'],"total_customers":v['total_customers'], "trxt_year":v['trxt_year']})
                        if measurement_type == '2' :
                            if v['gp_percent'] != None and v['gp_percent'] > 0:
                                total_store_sale = total_store_sale+float(v['gp_percent'])
                        elif measurement_type == '3' :
                            if v['total_customers'] != None and v['total_customers'] > 0:
                                total_store_sale = total_store_sale+float(v['total_customers'])  
                        else:
                            if v['total_amount'] != None and v['total_amount'] > 0:
                                total_store_sale = total_store_sale+v['total_amount']
                    c=c+1 
            
            # Do x/y now to get = Y !!
            temp_Sale_TYS_VS_LYS_X["x"]=sorted(temp_Sale_TYS_VS_LYS_X["x"],key=lambda i: i["id"])
            temp_Sale_TYS_VS_LYS_X["y"]=sorted(temp_Sale_TYS_VS_LYS_X["y"],key=lambda i: i["id"])
           
            temp_Sale_TYS_VS_LYS_X_F = []
            #if analysis_level == '3' :
            for i in range(0,len(temp_Sale_TYS_VS_LYS_X["y"])):
                is_found=0
                for v in range(0,len(temp_Sale_TYS_VS_LYS_X["x"])):
                    if temp_Sale_TYS_VS_LYS_X["x"][v]['id'] == temp_Sale_TYS_VS_LYS_X["y"][i]['id'] :
                        temp_Sale_TYS_VS_LYS_X_F.append({'id':temp_Sale_TYS_VS_LYS_X["x"][v]['id'],'name':temp_Sale_TYS_VS_LYS_X["x"][v]['name'].encode("ascii"), "total_promo_sale": temp_Sale_TYS_VS_LYS_X["x"][v]['total_promo_sale'],"total_discount":temp_Sale_TYS_VS_LYS_X["x"][v]['total_discount'], "total_qty":temp_Sale_TYS_VS_LYS_X["x"][v]['total_qty'], "total_cost":temp_Sale_TYS_VS_LYS_X["x"][v]['total_cost'], "gp_percent":temp_Sale_TYS_VS_LYS_X["x"][v]['gp_percent'], "total_margin":temp_Sale_TYS_VS_LYS_X["x"][v]['total_margin'], "total_amount":temp_Sale_TYS_VS_LYS_X["x"][v]['total_amount'], 
                        "total_customers":temp_Sale_TYS_VS_LYS_X["x"][v]['total_customers'],
                        "trxt_year":temp_Sale_TYS_VS_LYS_X["x"][v]['trxt_year']})
                        is_found=1
                        
                if is_found == 0 :
                    temp_Sale_TYS_VS_LYS_X_F.append({'id':temp_Sale_TYS_VS_LYS_X["y"][i]['id'],'name':temp_Sale_TYS_VS_LYS_X["y"][i]['name'].encode("ascii"), "total_promo_sale": 0,"total_discount":0, "total_qty":0, "total_cost":0, "gp_percent":0, "total_margin":0, "total_amount":0,"total_customers":0, "trxt_year":''})    
        
            temp_Sale_TYS_VS_LYS_X["x"] = temp_Sale_TYS_VS_LYS_X_F
          
            TY_VS_LY_AVG_Y = []
            if temp_Sale_TYS_VS_LYS_X["x"] and temp_Sale_TYS_VS_LYS_X["y"] :
                for i in range(0,len(temp_Sale_TYS_VS_LYS_X["x"])):
                    total_amount_x = 0
                    total_amount_y = 0
                    if measurement_type == '2' :
                        total_amount_x = temp_Sale_TYS_VS_LYS_X["x"][i]["gp_percent"]
                        #if i in temp_Sale_TYS_VS_LYS_X["y"]: 
                        total_amount_y = temp_Sale_TYS_VS_LYS_X["y"][i]["gp_percent"]
                    elif measurement_type == '3' :
                        if temp_Sale_TYS_VS_LYS_X["x"][i]["total_amount"] > 0 and temp_Sale_TYS_VS_LYS_X["x"][i]["total_customers"] > 0 :
                            total_amount_x = np.divide(temp_Sale_TYS_VS_LYS_X["x"][i]["total_amount"],temp_Sale_TYS_VS_LYS_X["x"][i]["total_customers"])
                        if temp_Sale_TYS_VS_LYS_X["y"][i]["total_amount"] > 0 and temp_Sale_TYS_VS_LYS_X["y"][i]["total_customers"] > 0 :  
                            total_amount_y = np.divide(temp_Sale_TYS_VS_LYS_X["y"][i]["total_amount"],temp_Sale_TYS_VS_LYS_X["y"][i]["total_customers"])  
                    else:
                        total_amount_x = temp_Sale_TYS_VS_LYS_X["x"][i]["total_amount"]
                        #if i in temp_Sale_TYS_VS_LYS_X["y"]:  
                        total_amount_y = temp_Sale_TYS_VS_LYS_X["y"][i]["total_amount"]

                    # Set last this year param values
                    id = ''
                    name = ''
                    #if i in temp_Sale_TYS_VS_LYS_X["y"]:
                    id = temp_Sale_TYS_VS_LYS_X["y"][i]["id"]
                    name = temp_Sale_TYS_VS_LYS_X["y"][i]["name"]
                    temp_total_amount_y = 0
                    if total_amount_y > 0 and total_amount_x > 0 :
                        temp_total_amount_y = ((np.divide(total_amount_y,total_amount_x))-1)*100
                    
                    # Calculate bubble size
                    bubble_size = 0
                    if total_amount_y > 0 and total_store_sale > 0 :
                        bubble_size = (np.divide(total_amount_y,total_store_sale))*500
                        #bubble_size = np.power(bubble_size, np.true_divide(1,3))
                    #if bubble_size > 80 :
                        #bubble_size = 80 
                    graph_bubble_size.append(bubble_size)
                    gy_total_amount = 0
                    if float('-inf') < float(temp_total_amount_y) < float('inf'):
                        gy_total_amount = temp_total_amount_y
                    TY_VS_LY_AVG_Y.append({"id":id,"name":name,"total_amount": gy_total_amount,"total_amount_y":total_amount_y})    
                    # -- Graph Y Axis --
                    graph_y.append(gy_total_amount)  # -- Y Axis Points
                    graph_text.append(temp_Sale_TYS_VS_LYS_X["x"][i]["name"])   # -- Axis Points Lables
                    graph_customdata.append(temp_Sale_TYS_VS_LYS_X["x"][i]["id"]) # -- Axis Points Customdata

        # Get secondary comparison data
        if(s_outlet_id != '' or s_zone_id != '') :
            if s_zone_id != '' :
                s_zone_outl_sql = self.zone_outl_query_condition(s_zone_id)
            if analysis_level == '2' : 
                # Manage commodity level secondary sales records
                df_zone_sale_TYS = self.secondary_aggregate_calculation(start_date,end_date,current_year,s_outlet_id,s_zone_id,analysis_level,analysis_index_id)

            elif analysis_level == '3' :
                # Prepare sql for top 20 product sales records 
                df_zone_sale_TYS_query = "SELECT TRX_COMMODITY AS SUM_CODE,TRX_COMMODITY,SUM(TRX_QTY) as total_qty,SUM(TRX_AMT) as total_amount, SUM(TRX_COST) as total_cost,SUM(TRX_PROM_SALES) as total_promo_sale,SUM(TRX_PROM_SALES_GST) AS SUM_PROM_SALES_GST,SUM(TRX_AMT - TRX_COST) as total_margin,(((SUM(TRX_AMT) - SUM(TRX_COST)) * 100) / NULLIF(SUM(TRX_AMT), 0)) as gp_percent,SUM(TRX_DISCOUNT) as total_discount,X.CODE_DESC AS CODE_DESC,TRX_PRODUCT as id,PROD_DESC as title FROM TRXTBL LEFT JOIN CODETBL X ON X.CODE_KEY_TYPE = 'COMMODITY' AND X.CODE_KEY_NUM = TRX_COMMODITY LEFT JOIN PRODTBL ON PROD_NUMBER = TRX_PRODUCT WHERE TRX_PRODUCT IN (SELECT TOP 20 TRX_PRODUCT FROM TRXTBL LEFT JOIN CODETBL X ON X.CODE_KEY_TYPE = 'COMMODITY' AND X.CODE_KEY_NUM = TRX_COMMODITY LEFT JOIN PRODTBL ON PROD_NUMBER = TRX_PRODUCT WHERE (TRX_DATE BETWEEN '"+start_date+"' AND '"+end_date+"') AND"
                # Get zone outlets and concatinate outlets in query
                if p_zone_id != '' :
                    if p_zone_outl_sql != '' :
                        df_zone_sale_TYS_query += "("+p_zone_outl_sql+") AND "
                else :
                    df_zone_sale_TYS_query += " TRX_OUTLET = '"+p_outlet_id+"' AND "
                df_zone_sale_TYS_query += "(TRX_COMMODITY = '"+str(analysis_index_id)+"' ) AND (TRX_TYPE = 'ITEMSALE') GROUP BY CODE_DESC, PROD_DESC ,TRX_COMMODITY, TRX_PRODUCT ORDER BY TRX_PRODUCT DESC)  AND (TRX_DATE BETWEEN '"+start_date+"' AND '"+end_date+"')  AND TRX_OUTLET IN (SELECT CODE_KEY_NUM FROM CodeTbl LEFT JOIN OutlTbl on CODE_KEY_NUM = OUTL_OUTLET WHERE CODE_KEY_TYPE = 'ZONEOUTLET' and CODE_KEY_ALP = 'INNER') AND "
                if s_zone_id != '' :
                    if s_zone_outl_sql['zone_outl_sql'] != '' :
                        df_zone_sale_TYS_query += "("+s_zone_outl_sql['zone_outl_sql']+") AND "
                    total_zone_outlets = s_zone_outl_sql['total_zone_outlets']
                else :
                    df_zone_sale_TYS_query += " TRX_OUTLET = '"+s_outlet_id+"' AND "
                df_zone_sale_TYS_query += "(TRX_COMMODITY = '"+str(analysis_index_id)+"' ) AND (TRX_TYPE = 'ITEMSALE') GROUP BY CODE_DESC, PROD_DESC ,TRX_COMMODITY, TRX_PRODUCT ORDER BY TRX_PRODUCT ASC"
                df_zone_sale_TYS = pd.read_sql_query(df_zone_sale_TYS_query,con=self.live_db)
                df_zone_sale_TYS = df_zone_sale_TYS.to_dict(orient="records")
            else:
                # Manage department level secondary sales records
                df_zone_sale_TYS = self.secondary_aggregate_calculation(start_date,end_date,current_year,s_outlet_id,s_zone_id,analysis_level)
               
            for v in df_zone_sale_TYS:
              
                total_amount = 0
                z_total_amount = 0
                compare_avg_amt = 0
                # Set amount as per measurement type
                if measurement_type == '2' :
                    total_amount = v['gp_percent']
                elif measurement_type == '3' :
    
                    if 'total_customers' in v and  v['total_customers'] != None and v['total_customers'] != '':
                        total_amount = np.divide(v['total_amount'],v['total_customers']) 
                else:
                    total_amount = v['total_amount']
                # Set amount as per filter type outlet/zone    
                if s_zone_id != '' :
                    total_zone_outlets = s_zone_outl_sql['total_zone_outlets']
                    if analysis_level == '3' :
                        total_stores = total_zone_outlets
                    else :
                        total_stores = v['total_stores']   
                    compare_avg_amt = total_amount/total_stores

                elif s_outlet_id != '':
                    compare_avg_amt = total_amount
                if float('-inf') < float(compare_avg_amt) < float('inf'):
                    z_total_amount = compare_avg_amt
                temp_Sale_TYS_VS_LYS_Y.append({"id": v['id'], "name":v['title'].encode("ascii"),"total_amount": z_total_amount})

            # Get => X-Axis!!
            # Do x/y now to get = X !!
            TY_VS_LY_AVG_Y = sorted(TY_VS_LY_AVG_Y,key=lambda i: i["name"])
            temp_Sale_TYS_VS_LYS_Y = sorted(temp_Sale_TYS_VS_LYS_Y,key=lambda i: i["name"])

            temp_Sale_TYS_VS_LYS_Y_F = []
            #if analysis_level == '3' :
            for i in range(0,len(temp_Sale_TYS_VS_LYS_X["y"])):
                is_found=0
                for v in range(0,len(temp_Sale_TYS_VS_LYS_Y)):
                    if temp_Sale_TYS_VS_LYS_Y[v]['id'] == temp_Sale_TYS_VS_LYS_X["y"][i]['id'] :
                        temp_Sale_TYS_VS_LYS_Y_F.append({'id':temp_Sale_TYS_VS_LYS_Y[v]['id'],'name':temp_Sale_TYS_VS_LYS_Y[v]['name'].encode("ascii"), "total_amount":temp_Sale_TYS_VS_LYS_Y[v]['total_amount']})
                        is_found=1
                        
                if is_found == 0 :
                    temp_Sale_TYS_VS_LYS_Y_F.append({'id':temp_Sale_TYS_VS_LYS_X["y"][i]['id'],'name':temp_Sale_TYS_VS_LYS_X["y"][i]['name'].encode("ascii"),"total_amount":0})    
        
            temp_Sale_TYS_VS_LYS_Y = temp_Sale_TYS_VS_LYS_Y_F
            
            primary_TY_VS_LY_X = []
            if temp_Sale_TYS_VS_LYS_Y :
                for i in range(0,len(temp_Sale_TYS_VS_LYS_Y)):
                    d_id = temp_Sale_TYS_VS_LYS_Y[i]["id"]
                    name = temp_Sale_TYS_VS_LYS_Y[i]["name"]
                    total_amount = 0
                    TY_total_amount = 0
                    for j in range(0,len(TY_VS_LY_AVG_Y)):
                        if temp_Sale_TYS_VS_LYS_Y[i]["id"] == TY_VS_LY_AVG_Y[j]["id"]:
                            d_id = TY_VS_LY_AVG_Y[j]["id"]
                            name = TY_VS_LY_AVG_Y[j]["name"]
                            total_amount = TY_VS_LY_AVG_Y[j]["total_amount"]
                            TY_total_amount = TY_VS_LY_AVG_Y[j]["total_amount_y"]
                    p_total_amount = 0
                    if float('-inf') < float(total_amount) < float('inf'):
                        p_total_amount = total_amount
                    primary_TY_VS_LY_X.append({"id": d_id, "name":name,"total_amount":p_total_amount,"TY_total_amount":TY_total_amount})
            
            if temp_Sale_TYS_VS_LYS_Y :
                for i in range(0,len(temp_Sale_TYS_VS_LYS_Y)):
                    temp_total_amount_x = 0
                    if primary_TY_VS_LY_X[i]["id"] == temp_Sale_TYS_VS_LYS_Y[i]["id"] :
                        if primary_TY_VS_LY_X[i]["id"] == 1 :
                            print(primary_TY_VS_LY_X[i]["TY_total_amount"])
                            print(temp_Sale_TYS_VS_LYS_Y[i]["total_amount"])
                        # Set amount as per measurement type
                        TY_total_amount = primary_TY_VS_LY_X[i]["TY_total_amount"]
                        total_amount_y = temp_Sale_TYS_VS_LYS_Y[i]["total_amount"]
                        if total_amount_y > 0 :
                            temp_total_amount_x = (np.divide(TY_total_amount,total_amount_y)-1)*100
                        # -- Graph X Axis --

                    gx_total_amount = 0
                    if float('-inf') < float(temp_total_amount_x) < float('inf'):
                        gx_total_amount = temp_total_amount_x    
                    graph_x.append(gx_total_amount)

        # Get unsold ranging products
        ranging_products = []
        if analysis_level == '3' :
            ranging_products = self.ranging_products(start_date,end_date,p_outlet_id,p_zone_id,analysis_index_id,p_zone_outl_sql)
            print(ranging_products)

        graph_marker_labels = []
        graph_inside_marker_labels = []
        graph_dictionary = []
        # Define color code array
        colors = ["#FF6600", "#FCD202", "#B0DE09", "#0D8ECF", "#2A0CD0", "#CD0D74", "#CC0000", "#00CC00", "#0000CC", "#DDDDDD", "#999999", "#333333", "#990000","#FF6600", "#FCD202", "#B0DE09", "#0D8ECF", "#2A0CD0", "#CD0D74", "#CC0000"]
        # Set marker name as combination of name and axis values
        print(graph_x)
        print('===graph_x====')
        print(graph_y)
        print('===graph_y====')
        for i in range(0,len(graph_x)):
            # Set marker label with axis points
            m_label = re.sub(' +', ' ',graph_text[i])+'('+str(round(graph_x[i], 2))+' , '+str(round(graph_y[i], 2))+')'
            graph_marker_labels.append(m_label)
            # Set inside marker label
            graph_inside_marker_labels.append(re.sub(' +', ' ',graph_text[i]))
            # Prepare dictionary 
            myDict = {}
            myDict["x"] = round(graph_x[i], 2)
            myDict["y"] = round(graph_y[i], 2)
            myDict["value"] = graph_bubble_size[i]
            label = graph_marker_labels[i]
            if myDict["x"] < 0 and myDict["y"] < 0 :
                label = graph_marker_labels[i]+'('+str(graph_customdata[i])+')'
            myDict["label"] = label
            myDict["inside_marker_label"] = graph_inside_marker_labels[i]
            myDict["id"] = graph_customdata[i]
            myDict["color"] = colors[i]
            #myDict["department_id"] = department_id
            myDict["level"] = analysis_level
            graph_dictionary.append(myDict)
       
        res = [graph_x, graph_y, graph_marker_labels, graph_customdata, graph_bubble_size,graph_dictionary,analysis_chart_label,department_name,temp_Sale_TYS_VS_LYS_X["y"],temp_Sale_TYS_VS_LYS_X["x"],temp_Sale_TYS_VS_LYS_Y,ranging_products]
        return res

    # --- Manage product level ranging sales
    def ranging_products(self,start_date,end_date,p_outlet_id,p_zone_id,analysis_index_id,p_zone_outl_sql):
        cGroup = {}
        # Get group sale
        df_group_sale_TYS_query = "SELECT TRX_PRODUCT, PROD_DESC, PROD_NATIONAL, SUM(TRX_QTY) as SUM_QTY, SUM(TRX_AMT) as SUM_AMT, (SUM(TRX_AMT) / COUNT(TRX_PRODUCT)) as AVG_SALES FROM TRXTBL WITH(NOLOCK),PRODTBL WHERE (TRX_DATE BETWEEN '"+start_date+"' AND '"+end_date+"') AND (TRX_PRODUCT=PROD_NUMBER) AND  (TRX_COMMODITY IN ('"+str(analysis_index_id)+"')) AND (TRX_TYPE = 'ITEMSALE') GROUP BY TRX_PRODUCT,PROD_DESC,PROD_NATIONAL ORDER BY SUM_AMT ASC"
        df_group_sale_TYS = pd.read_sql_query(df_group_sale_TYS_query,con=self.live_db)
        
        for v in df_group_sale_TYS.to_dict(orient="records"):
            cGroup[v['TRX_PRODUCT']] = {'TRX_PRODUCT':int(v['TRX_PRODUCT']),'PROD_DESC':v['PROD_DESC'].encode("ascii"),'SUM_AMT':v['SUM_AMT']}

        # Get outlet sale
        outlet_excl_products = []
        df_outlet_sale_TYS_query = "SELECT TRX_PRODUCT, PROD_DESC, PROD_NATIONAL, SUM(TRX_QTY) as SUM_QTY, SUM(TRX_AMT) as SUM_AMT, (SUM(TRX_AMT) / COUNT(TRX_PRODUCT)) as AVG_SALES FROM TRXTBL WITH(NOLOCK),PRODTBL WHERE (TRX_DATE BETWEEN '"+start_date+"' AND '"+end_date+"') AND (TRX_PRODUCT=PROD_NUMBER) AND "
        # Get zone outlets and concatinate outlets in query
        if p_zone_id != '' :
            if p_zone_outl_sql != '' :
                df_outlet_sale_TYS_query += "("+p_zone_outl_sql+") AND "
        else :
            df_outlet_sale_TYS_query += " TRX_OUTLET = '"+p_outlet_id+"' AND "
        df_outlet_sale_TYS_query += " (TRX_COMMODITY IN ('"+str(analysis_index_id)+"')) AND (TRX_TYPE = 'ITEMSALE') GROUP BY TRX_PRODUCT,PROD_DESC,PROD_NATIONAL ORDER BY SUM_AMT ASC"
        df_outlet_sale_TYS = pd.read_sql_query(df_outlet_sale_TYS_query,con=self.live_db)
        
        for v in df_outlet_sale_TYS.to_dict(orient="records"):
            outlet_excl_products.append(v['TRX_PRODUCT'])

        # Remove outlet product from group sale
        if outlet_excl_products :
            for i in range(0,len(outlet_excl_products)):
                del cGroup[outlet_excl_products[i]]
        return cGroup        

    # --- Manage zone outlets sql condition
    @cache.memoize(timeout=timeout)
    def zone_outl_query_condition(self,p_zone_id):
        zone_outl_res = {}
        p_zone_outl_sql = ''
        Zone_outlet_res = pd.read_sql_query("SELECT CODE_KEY_NUM as zout_outlet_id FROM CODETBL WHERE CODE_KEY_TYPE = 'ZONEOUTLET' and CODE_KEY_ALP = '"+p_zone_id+"'",con=self.live_db)
        Zone_outlet_res=Zone_outlet_res.groupby(['zout_outlet_id'])
        i = 1
        total_zone_outlets = len(Zone_outlet_res) 
        for k, v in Zone_outlet_res:
            temp_zone_split=v.to_dict(orient="records")
            for v in temp_zone_split:
                p_zone_outl_sql += "(TRX_OUTLET = '"+str(v['zout_outlet_id'])+"')"
                if i < total_zone_outlets :
                    p_zone_outl_sql += " OR "  
            i = i+1
        zone_outl_res['zone_outl_sql'] = p_zone_outl_sql
        zone_outl_res['total_zone_outlets'] = total_zone_outlets
        return zone_outl_res   

    # --- Manage product level sql queries
    @cache.memoize(timeout=timeout)
    def prepare_product_query(self,p_outlet_id,p_zone_id,start_date,end_date,commodity_id,LY_start_date,LY_end_date,p_zone_outl_sql):
        product_sql = {}
        # Prepare this year top 20 product query
        TY_p_sql = "SELECT * FROM ( SELECT TRX_COMMODITY AS SUM_CODE,TRX_COMMODITY,SUM(TRX_QTY) as total_qty,SUM(TRX_AMT) as total_amount, SUM(TRX_COST) as total_cost,SUM(TRX_PROM_SALES) as total_promo_sale,SUM(TRX_PROM_SALES_GST) AS SUM_PROM_SALES_GST,SUM(TRX_AMT - TRX_COST) as total_margin,(((SUM(TRX_AMT) - SUM(TRX_COST)) * 100) / NULLIF(SUM(TRX_AMT), 0)) as gp_percent,SUM(TRX_DISCOUNT) as total_discount,X.CODE_DESC AS CODE_DESC,TRX_PRODUCT as id,PROD_DESC as title,ROW_NUMBER() OVER (ORDER BY TRX_PRODUCT DESC) as row  FROM TRXTBL LEFT JOIN CODETBL X ON X.CODE_KEY_TYPE = 'COMMODITY' AND X.CODE_KEY_NUM = TRX_COMMODITY LEFT JOIN PRODTBL ON PROD_NUMBER = TRX_PRODUCT WHERE (TRX_DATE BETWEEN '"+start_date+"' AND '"+end_date+"') AND "
        if p_zone_id != '' :
            if p_zone_outl_sql['zone_outl_sql'] != '' :
                TY_p_sql += " ("+p_zone_outl_sql['zone_outl_sql']+") AND "
        else :   
            TY_p_sql += " TRX_OUTLET = '"+p_outlet_id+"' AND "
        TY_p_sql += "(TRX_COMMODITY = '"+commodity_id+"' ) AND (TRX_TYPE = 'ITEMSALE') GROUP BY CODE_DESC, PROD_DESC ,TRX_COMMODITY, TRX_PRODUCT  ) a WHERE row > 0 AND row <= 20"

        # Prepare last year top 20 product query
        LY_p_sql = "SELECT TRX_COMMODITY AS SUM_CODE,TRX_COMMODITY,SUM(TRX_QTY) as total_qty,SUM(TRX_AMT) as total_amount, SUM(TRX_COST) as total_cost,SUM(TRX_PROM_SALES) as total_promo_sale,SUM(TRX_PROM_SALES_GST) AS SUM_PROM_SALES_GST,SUM(TRX_AMT - TRX_COST) as total_margin,(((SUM(TRX_AMT) - SUM(TRX_COST)) * 100) / NULLIF(SUM(TRX_AMT), 0)) as gp_percent,SUM(TRX_DISCOUNT) as total_discount,X.CODE_DESC AS CODE_DESC,TRX_PRODUCT as id,PROD_DESC as title FROM TRXTBL LEFT JOIN CODETBL X ON X.CODE_KEY_TYPE = 'COMMODITY' AND X.CODE_KEY_NUM = TRX_COMMODITY LEFT JOIN PRODTBL ON PROD_NUMBER = TRX_PRODUCT WHERE TRX_PRODUCT IN (SELECT TOP 20 TRX_PRODUCT FROM TRXTBL LEFT JOIN CODETBL X ON X.CODE_KEY_TYPE = 'COMMODITY' AND X.CODE_KEY_NUM = TRX_COMMODITY LEFT JOIN PRODTBL ON PROD_NUMBER = TRX_PRODUCT WHERE (TRX_DATE BETWEEN '"+start_date+"' AND '"+end_date+"') AND "
        if p_zone_id  != '' :
            if p_zone_outl_sql['zone_outl_sql'] != '' :
                LY_p_sql += "("+p_zone_outl_sql['zone_outl_sql']+") AND "
        else :    
            LY_p_sql += " TRX_OUTLET = '"+p_outlet_id+"' AND "

        LY_p_sql += "(TRX_COMMODITY = '"+commodity_id+"' ) AND (TRX_TYPE = 'ITEMSALE') GROUP BY CODE_DESC, PROD_DESC ,TRX_COMMODITY, TRX_PRODUCT ORDER BY TRX_PRODUCT DESC) AND (TRX_DATE BETWEEN '"+LY_start_date+"' AND '"+LY_end_date+"') AND "
        if p_zone_id  != '' :
            if p_zone_outl_sql != '' :
                LY_p_sql += "("+p_zone_outl_sql+") AND "
        else :    
            LY_p_sql += " TRX_OUTLET = '"+p_outlet_id+"' AND "
        LY_p_sql += "(TRX_COMMODITY = '"+commodity_id+"' ) AND (TRX_TYPE = 'ITEMSALE') GROUP BY CODE_DESC, PROD_DESC ,TRX_COMMODITY, TRX_PRODUCT ORDER BY TRX_PRODUCT DESC"
        
        # Append data in array
        product_sql['TY_p_sql'] = TY_p_sql
        product_sql['LY_p_sql'] = LY_p_sql
        return product_sql 

    #  --- Check if the int given year is a leap year: return true if leap year or false otherwise
    @cache.memoize(timeout=timeout)
    def is_leap_year(self,year):
        leap_year_days = 366
        non_leap_year_days = 365
        if (year % 4) == 0:
            if (year % 100) == 0:
                if (year % 400) == 0:
                    return leap_year_days
                else:
                    return non_leap_year_days
            else:
                return leap_year_days
        else:
            return non_leap_year_days    

    # --- Function used to manage primary filter sales
    def primary_aggregate_calculation(self,start_date,end_date,LY_start_date,LY_end_date,current_year,prev_year,outlet_id='',zone_id='',analysis_level='1',department_id=0):
        
        # prepare query for this year financial sales query
        ty_sales_query = "SELECT JNLH_OUTLET, JNLH_TILL, JNLH_TRX_NO, JNLH_TYPE, JNLH_STATUS, JNLH_TRX_AMT, JNLH_CASHIER, JNLD_TYPE, JNLD_STATUS, JNLD_DESC, JNLD_MIXMATCH, JNLD_OFFER, JNLD_QTY, JNLD_AMT, JNLD_COST, JNLD_GST_AMT, JNLD_DISC_AMT, JNLD_POST_STATUS, PROD_TAX_CODE, PROD_COMMODITY, PROD_CATEGORY, PROD_DEPARTMENT, PROD_CARTON_QTY, PROD_UNIT_QTY, OUTP_CARTON_COST, OUTP_PROM_CTN_COST,CODE_DESC,CODE_KEY_NUM,YEAR(JNLH_TRADING_DATE) AS JNLH_YEAR FROM JNLHTBL JOIN JNLDTBL ON JNLD_YYYYMMDD = JNLH_YYYYMMDD AND JNLD_HHMMSS = JNLH_HHMMSS AND JNLD_OUTLET = JNLH_OUTLET AND JNLD_TILL = JNLH_TILL AND JNLD_TRX_NO = JNLH_TRX_NO LEFT JOIN PRODTBL ON PROD_NUMBER = JNLD_PRODUCT LEFT JOIN CODETBL "
        if analysis_level == '2' :
            ty_sales_query += "ON CODE_KEY_NUM = PROD_COMMODITY AND CODE_KEY_TYPE = 'COMMODITY'"
        else :
            ty_sales_query += "ON CODE_KEY_NUM = PROD_DEPARTMENT AND CODE_KEY_TYPE = 'DEPARTMENT'"
        ty_sales_query += " LEFT JOIN OUTPTBL ON OUTP_PRODUCT = JNLD_PRODUCT AND OUTP_OUTLET = JNLD_OUTLET WHERE "    
        if outlet_id != '':
            ty_sales_query += "JNLH_OUTLET  = '"+outlet_id+"' AND "
        elif zone_id != '' :
            ty_sales_query += "JNLH_OUTLET IN ( select DISTINCT(CODE_KEY_NUM) from CODETBL where CODE_KEY_TYPE = 'ZONEOUTLET' AND CODE_KEY_ALP = '"+zone_id+"' )  AND "
        ty_sales_query += "(JNLH_TRADING_DATE BETWEEN '"+start_date+"' AND '"+end_date+"') AND JNLD_TYPE = 'SALE' "
        if analysis_level == '2' :
            ty_sales_query += " AND ( PROD_DEPARTMENT = '"+department_id+"' )"

        ty_sales_query += " ORDER BY CODE_KEY_NUM,JNLH_YYYYMMDD, JNLH_HHMMSS, JNLH_TILL, JNLH_TRX_NO"    
        ty_df_sales_result = pd.read_sql_query(ty_sales_query,con=self.live_db)
        ty_df_sales_result=ty_df_sales_result.groupby(['JNLH_OUTLET'])
        # Manage this year sales aggragate data
        ty_sales_array = self.manage_yearly_aggregate_sales(ty_df_sales_result,analysis_level)

        # prepare query for last year financial sales query
        ly_sales_query = "SELECT JNLH_OUTLET, JNLH_TILL, JNLH_TRX_NO, JNLH_TYPE, JNLH_STATUS, JNLH_TRX_AMT, JNLH_CASHIER, JNLD_TYPE, JNLD_STATUS, JNLD_DESC, JNLD_MIXMATCH, JNLD_OFFER, JNLD_QTY, JNLD_AMT, JNLD_COST, JNLD_GST_AMT, JNLD_DISC_AMT, JNLD_POST_STATUS, PROD_TAX_CODE, PROD_COMMODITY, PROD_CATEGORY, PROD_DEPARTMENT, PROD_CARTON_QTY, PROD_UNIT_QTY, OUTP_CARTON_COST, OUTP_PROM_CTN_COST,CODE_DESC,CODE_KEY_NUM,YEAR(JNLH_TRADING_DATE) AS JNLH_YEAR FROM JNLHTBL JOIN JNLDTBL ON JNLD_YYYYMMDD = JNLH_YYYYMMDD AND JNLD_HHMMSS = JNLH_HHMMSS AND JNLD_OUTLET = JNLH_OUTLET AND JNLD_TILL = JNLH_TILL AND JNLD_TRX_NO = JNLH_TRX_NO LEFT JOIN PRODTBL ON PROD_NUMBER = JNLD_PRODUCT LEFT JOIN CODETBL "
        if analysis_level == '2' :
            ly_sales_query += "ON CODE_KEY_NUM = PROD_COMMODITY AND CODE_KEY_TYPE = 'COMMODITY'"
        else :
            ly_sales_query += "ON CODE_KEY_NUM = PROD_DEPARTMENT AND CODE_KEY_TYPE = 'DEPARTMENT'"
        ly_sales_query += " LEFT JOIN OUTPTBL ON OUTP_PRODUCT = JNLD_PRODUCT AND OUTP_OUTLET = JNLD_OUTLET WHERE " 
        if outlet_id != '':
            ly_sales_query += "JNLH_OUTLET  = '"+outlet_id+"' AND "
        elif zone_id != '' :
            ly_sales_query += "JNLH_OUTLET IN ( select DISTINCT(CODE_KEY_NUM) from CODETBL where CODE_KEY_TYPE = 'ZONEOUTLET' AND CODE_KEY_ALP = '"+zone_id+"' )  AND "
        ly_sales_query += "(JNLH_TRADING_DATE BETWEEN '"+LY_start_date+"' AND '"+LY_end_date+"') AND JNLD_TYPE = 'SALE'" 
        if analysis_level == '2' :
            ly_sales_query += " AND ( PROD_DEPARTMENT = '"+department_id+"' )"

        ly_sales_query += " ORDER BY CODE_KEY_NUM,JNLH_YYYYMMDD, JNLH_HHMMSS, JNLH_TILL, JNLH_TRX_NO"
       
        ly_df_sales_result = pd.read_sql_query(ly_sales_query,con=self.live_db)
        ly_df_sales_result=ly_df_sales_result.groupby(['JNLH_OUTLET'])
        # Manage last year sales aggragate data
        ly_sales_array = self.manage_yearly_aggregate_sales(ly_df_sales_result,analysis_level)
        
        final_array = []
        # Append this year sales in final sales array
        for k in ty_sales_array :
            for v in k :
                for j in k[v] :
                    # append this year department sales value in array
                    final_array.append({'id':j['vsf_dep_number'],'title':j['vsf_dep_name'],'vsf_dep_sale_count':j['vsf_dep_sale_count'],'total_amount':j['vsf_dep_sales_amt'],'total_customers':j['vsf_dep_customers_count'],'vsf_dep_gst_amt':j['vsf_dep_gst_amt'],'total_margin':j['vsf_dep_margin'],'total_cost':j['vsf_dep_sales_cost'],'vsf_dep_sales_ex_gst':j['vsf_dep_sales_ex_gst'],'total_qty':j['vsf_dep_sales_qty'],'gp_percent':(((j['vsf_dep_sales_amt'] - j['vsf_dep_sales_cost']) / j['vsf_dep_sales_amt'])*100),'trxt_year':current_year,'total_promo_sale':0,'total_discount':0})
        # Append last year sales in final sales array    
        for k in ly_sales_array :
            for v in k :
                for ly in k[v] :
                    # append last year's sales values in array
                    final_array.append({'id':ly['vsf_dep_number'],'title':ly['vsf_dep_name'],'vsf_dep_sale_count':ly['vsf_dep_sale_count'],'total_amount':ly['vsf_dep_sales_amt'],'total_customers':ly['vsf_dep_customers_count'],'vsf_dep_gst_amt':ly['vsf_dep_gst_amt'],'total_margin':ly['vsf_dep_margin'],'total_cost':ly['vsf_dep_sales_cost'],'vsf_dep_sales_ex_gst':ly['vsf_dep_sales_ex_gst'],'total_qty':ly['vsf_dep_sales_qty'],'gp_percent':(((ly['vsf_dep_sales_amt'] - ly['vsf_dep_sales_cost']) / ly['vsf_dep_sales_amt'])*100),'trxt_year':prev_year,'total_promo_sale':0,'total_discount':0})
       
        return final_array

    # --- Function used to manage secondary filter sales 
    def secondary_aggregate_calculation(self,start_date,end_date,current_year,outlet_id='',zone_id='',analysis_level='1',department_id=0):
        
        # prepare query for this year financial sales query
        s_sales_query = "SELECT JNLH_OUTLET, JNLH_TILL, JNLH_TRX_NO, JNLH_TYPE, JNLH_STATUS, JNLH_TRX_AMT, JNLH_CASHIER, JNLD_TYPE, JNLD_STATUS, JNLD_DESC, JNLD_MIXMATCH, JNLD_OFFER, JNLD_QTY, JNLD_AMT, JNLD_COST, JNLD_GST_AMT, JNLD_DISC_AMT, JNLD_POST_STATUS, PROD_TAX_CODE, PROD_COMMODITY, PROD_CATEGORY, PROD_DEPARTMENT, PROD_CARTON_QTY, PROD_UNIT_QTY, OUTP_CARTON_COST, OUTP_PROM_CTN_COST,CODE_DESC,CODE_KEY_NUM,YEAR(JNLH_TRADING_DATE) AS JNLH_YEAR "
        if zone_id != '' :
            s_sales_query += ",(SELECT count(CODE_KEY_NUM) FROM CODETBL WHERE CODE_KEY_TYPE = 'ZONEOUTLET' and CODE_KEY_ALP = '"+zone_id+"') as total_stores"
        s_sales_query += " FROM JNLHTBL JOIN JNLDTBL ON JNLD_YYYYMMDD = JNLH_YYYYMMDD AND JNLD_HHMMSS = JNLH_HHMMSS AND JNLD_OUTLET = JNLH_OUTLET AND JNLD_TILL = JNLH_TILL AND JNLD_TRX_NO = JNLH_TRX_NO LEFT JOIN PRODTBL ON PROD_NUMBER = JNLD_PRODUCT LEFT JOIN CODETBL " 
        if analysis_level == '2' :
            s_sales_query += "ON CODE_KEY_NUM = PROD_COMMODITY AND CODE_KEY_TYPE = 'COMMODITY'"
        else :
            s_sales_query += "ON CODE_KEY_NUM = PROD_DEPARTMENT AND CODE_KEY_TYPE = 'DEPARTMENT'"
        s_sales_query += " LEFT JOIN OUTPTBL ON OUTP_PRODUCT = JNLD_PRODUCT AND OUTP_OUTLET = JNLD_OUTLET WHERE " 

        if outlet_id != '':
            s_sales_query += "JNLH_OUTLET  = '"+outlet_id+"' AND "
        elif zone_id != '' :
            s_sales_query += "JNLH_OUTLET IN ( select DISTINCT(CODE_KEY_NUM) from CODETBL where CODE_KEY_TYPE = 'ZONEOUTLET' AND CODE_KEY_ALP = '"+zone_id+"' )  AND "
        s_sales_query += "(JNLH_TRADING_DATE BETWEEN '"+start_date+"' AND '"+end_date+"') AND JNLD_TYPE = 'SALE' "
        if analysis_level == '2' :
            s_sales_query += " AND ( PROD_DEPARTMENT = '"+department_id+"' )"

        s_sales_query += " ORDER BY CODE_KEY_NUM,JNLH_YYYYMMDD, JNLH_HHMMSS, JNLH_TILL, JNLH_TRX_NO"

        s_df_sales_result = pd.read_sql_query(s_sales_query,con=self.live_db)
        s_df_sales_result=s_df_sales_result.groupby(['JNLH_OUTLET'])
        # Manage this year sales aggragate data
        ty_sales_array = self.manage_yearly_aggregate_sales(s_df_sales_result,analysis_level)

        final_array = []
        for k in ty_sales_array :
            for v in k :
                for j in k[v] :
                    
                    # append this year department sales value in array
                    final_array.append({'id':j['vsf_dep_number'],'title':j['vsf_dep_name'],'vsf_dep_sale_count':j['vsf_dep_sale_count'],'total_amount':j['vsf_dep_sales_amt'],'total_customers':j['vsf_dep_customers_count'],'vsf_dep_gst_amt':j['vsf_dep_gst_amt'],'total_margin':j['vsf_dep_margin'],'total_cost':j['vsf_dep_sales_cost'],'vsf_dep_sales_ex_gst':j['vsf_dep_sales_ex_gst'],'total_qty':j['vsf_dep_sales_qty'],'gp_percent':(np.divide((j['vsf_dep_sales_amt'] - j['vsf_dep_sales_cost']),j['vsf_dep_sales_amt'])*100),'trxt_year':current_year,'total_stores':j['total_stores'],'zone_avg_sales_amount':(np.divide(j['vsf_dep_sales_amt'],j['total_stores'])),'total_promo_sale':0,'total_discount':0})
          
        return final_array       
     

    # Manage yearly sales record
    def manage_yearly_aggregate_sales(self,df_sales_result,analysis_level):
       
        # initialize department product sales array
        department_array = {} 
        for k, v in df_sales_result:
            temp_group_split=v.to_dict(orient="records")
            for v in temp_group_split:
                # set department label
                CODE_DESC = v['CODE_DESC'].encode("ascii")
                # append product sales data in department index
                if CODE_DESC not in department_array:
                    department_array[CODE_DESC] = []
                # set total store count
                total_stores = 1
                if 'total_stores' in v:
                    total_stores = v['total_stores']
                # prepare journal values for sales array department wise  
                department_array[CODE_DESC].append({'JNLH_OUTLET':v['JNLH_OUTLET'],'JNLH_TRX_NO':v['JNLH_TRX_NO'],'JNLH_TYPE':v['JNLH_TYPE'],'JNLH_STATUS':v['JNLH_STATUS'],'JNLH_TRX_AMT':v['JNLH_TRX_AMT'],'JNLH_CASHIER':v['JNLH_CASHIER'],'JNLD_TYPE':v['JNLD_TYPE'],'JNLD_STATUS':v['JNLD_STATUS'],'JNLD_QTY':v['JNLD_QTY'],'JNLD_AMT':v['JNLD_AMT'],'JNLD_COST':v['JNLD_COST'],'JNLD_GST_AMT':v['JNLD_GST_AMT'],'JNLD_DESC':v['JNLD_DESC'],'JNLD_OFFER':v['JNLD_OFFER'],'JNLD_MIXMATCH':v['JNLD_MIXMATCH'],'JNLD_DISC_AMT':v['JNLD_DISC_AMT'],'JNLD_POST_STATUS':v['JNLD_POST_STATUS'],'PROD_DEPARTMENT':v['PROD_DEPARTMENT'],'PROD_TAX_CODE':v['PROD_TAX_CODE'],'PROD_CATEGORY':v['PROD_CATEGORY'],'PROD_COMMODITY':v['PROD_COMMODITY'],'YEAR':v['JNLH_YEAR'],'CODE_DESC':CODE_DESC,'total_stores':total_stores})
               
        department_sales_array = []
        trx_numbers = []    
        # prepare department sales report result
        for i in department_array:
        
            # initialize default values
            vsf_dep_number = ''
            vsf_dep_name = ''
            vsf_dep_amt = 0
            vsf_dep_cost = 0
            vsf_dep_gst_amt = 0
            vsf_dep_sale_count = 0
            vsf_dep_margin = 0
            vsf_dep_sales_qty = 0
            vsf_dep_promo_disc_qty = 0
            vsf_dep_promo_disc_amt = 0
            vsf_dep_customers_count = 0
            vsf_dep_item_qty = 0
            last_trx_no = 0
            dept_data = []
            for v in department_array[i] :
                if i != '' :
                    if v['JNLD_STATUS'] == 1 and v['JNLD_TYPE'] == 'SALE' :
                        # add sales values
                        vsf_dep_amt 		= vsf_dep_amt+v['JNLD_AMT']
                        vsf_dep_cost 		= vsf_dep_cost+v['JNLD_COST']
                        vsf_dep_gst_amt	    = vsf_dep_gst_amt+v['JNLD_GST_AMT']
                        vsf_dep_sales_qty	= vsf_dep_sales_qty+v['JNLD_QTY']
                        vsf_dep_sale_count  = vsf_dep_sale_count+1
                        if analysis_level == '2' :
                            vsf_dep_number	= v['PROD_COMMODITY']
                        else :
                            vsf_dep_number	= v['PROD_DEPARTMENT']     
                        vsf_dep_name        = v['CODE_DESC']
                        total_stores        = v['total_stores']
                
                # set discount item values
                if v['JNLD_DISC_AMT'] != 0 :
                    if v['JNLD_MIXMATCH'] == '' and v['JNLD_OFFER'] == '' :
                        #do nothing
                        disc = 0
                    else :
                        # set promo discount values
                        vsf_dep_promo_disc_qty = vsf_dep_promo_disc_qty+v['JNLD_QTY']
                        vsf_dep_promo_disc_amt = vsf_dep_promo_disc_amt+v['JNLD_DISC_AMT']
                    
                # set total customers
                if v['JNLH_STATUS'] == 1 and v['JNLH_TYPE'] == 'SALE' :
                    if v['JNLH_TRX_NO'] not in trx_numbers :
                        trx_numbers.append(v['JNLH_TRX_NO'])
                        # add customer count
                        if last_trx_no != v['JNLH_TRX_NO'] :
                            vsf_dep_customers_count = vsf_dep_customers_count+1
        
                        # set last trx numbers
                        last_trx_no = v['JNLH_TRX_NO'];

                if v['JNLD_STATUS'] == 1 and v['JNLD_TYPE'] == 'SALE' :
                    # set item quantities
                    vsf_dep_item_qty = vsf_dep_item_qty+v['JNLD_QTY']
                
            # append sales value in array
            dept_data.append({'vsf_dep_sales_cost':vsf_dep_cost,'vsf_dep_sales_amt':vsf_dep_amt,'vsf_dep_gst_amt':vsf_dep_gst_amt,'vsf_dep_margin':vsf_dep_amt-vsf_dep_cost,'vsf_dep_sales_ex_gst':vsf_dep_amt-vsf_dep_gst_amt,'vsf_dep_sales_qty':vsf_dep_sales_qty,'vsf_dep_customers_count':vsf_dep_customers_count,'vsf_dep_sale_count':vsf_dep_sale_count,'vsf_dep_number':vsf_dep_number,'vsf_dep_name':vsf_dep_name,'total_stores':total_stores})
            # append result in department array
            department_sales_array.append({i:dept_data})
                
        return department_sales_array
            

    def __repr__(self):
        return '<Book %r>' % (self.title) 
    