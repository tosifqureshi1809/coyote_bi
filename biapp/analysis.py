import functools
from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from werkzeug.security import check_password_hash, generate_password_hash

from biapp.db import get_db

cb = Blueprint('cube', __name__, url_prefix='/cube')

@cb.route('/analysis', methods=('GET', 'POST'))
def dashboard():
    return render_template('cube/cube.html')